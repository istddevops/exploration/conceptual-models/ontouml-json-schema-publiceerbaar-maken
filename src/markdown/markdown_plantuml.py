"""
The interface for a markdown view of a plantUML diagram
"""


from dataclasses import dataclass

from src.plant_uml.plant_uml_diagram import PlantUMLDiagram


@dataclass
class MarkdownPlantUml:
    """
    Markdown view of a PlantUML diagram
    """

    plant_uml_diagram: PlantUMLDiagram
    """
    The plantUML diagram for which the markdown view is needed
    """
    diagram_name: str
    """
    The name of the diagram
    """
    diagram_identifier: str
    """
    The identifier of the diagram
    """

    def _generate_plant_uml_markdown(self, shortcode_open: str, shortcode_close: str):
        """
        `shortcode_open` and `shortcode_close` are mandatory. When not set, return
        `None`.
        When `self.plant_uml_diagram` is not set, inject comment "` No data available
        for diagram `self.diagram_identifier`".
        """
        if (
            None is shortcode_open
            or 0 == len(str(shortcode_open))
            or None is shortcode_close
            or 0 == len(str(shortcode_close))
        ):
            return None

        no_data_comment = f"' No data available for diagram {self.diagram_identifier}"
        result = [f"{shortcode_open}\n"]
        if None is self.plant_uml_diagram:
            result.append(no_data_comment)
        else:
            plant_uml = self.plant_uml_diagram.plant_uml
            if None is plant_uml or 0 == len(plant_uml):
                result.append(no_data_comment)
            else:
                result.extend([line + "\n" for line in plant_uml])
        result.append(f"{shortcode_close}\n")

        return result

    @property
    def frontmatter(self):
        """
        The frontmatter properties for this plantUML diagram.
        """
        return [
            "---\n",
            f"title: {self.diagram_name}\n",
            "type: docs\n",
            "bookToC: false\n",
            "---\n",
        ]

    @property
    def hugo_snippet(self):
        """
        Renders the `plant_uml_diagram` as a markdown snippet for use in any markdown
        page which will be rendered to a static site using HUGO.
        Be advised: the HUGO shortcode must be properly setup for this to work!
        """

        snippet_id = ""
        if self.diagram_identifier is not None and 0 < len(self.diagram_identifier):
            snippet_id = self.diagram_identifier

        snippet_name = ""
        if self.diagram_name is not None and 0 < len(self.diagram_name):
            snippet_name = self.diagram_name

        plant_uml_shortcode = (
            f"""< plantuml id="{snippet_id}" """ + f"""name="{snippet_name}" >"""
        )
        plant_uml_shortcode = "{{" + plant_uml_shortcode + "}}"
        hugo_markdown = self._generate_plant_uml_markdown(
            plant_uml_shortcode, "{{< /plantuml >}}"
        )
        return hugo_markdown

    @property
    def hugo_page(self):
        """
        Renders the `plant_uml_diagram` as a full markdown page (including frontmatter
        attributes) for use in any markdown page which will be rendered to a static
        site using HUGO.
        Be advised: the HUGO shortcode must be properly setup for this to work!
        """

        result = self.frontmatter
        snippet = self.hugo_snippet
        result.extend(snippet)
        return result

    @property
    def gitlab_snippet(self):
        """
        Renders the `plant_uml_diagram` as a markdown snippet for use in any gitlab
        item which takes markdown as input.
        """
        gitlab_markdown = self._generate_plant_uml_markdown("```plantuml", "```")
        return gitlab_markdown

    @property
    def gitlab_page(self):
        """
        Renders the `plant_uml_diagram` as a full markdown page (including frontmatter
        attributes) for use in any gitlab item which takes markdown as input.
        """
        result = self.frontmatter
        snippet = self.gitlab_snippet
        result.extend(snippet)
        return result
