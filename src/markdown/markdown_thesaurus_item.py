"""
The interface for a markdown view of a thesaurus item
"""


class MarkdownThesaurusItem:
    """
    Interface for a markdown view of a thesaurus item
    """

    def __init__(self, identifier: str, concept: str, meaning: str):
        if None is identifier or 0 == len(identifier):
            raise ValueError("identifier")
        if None is concept or 0 == len(concept):
            raise ValueError("concept")

        concept_urlized = (
            concept.lower().replace(" ", "-").replace("(", "-").replace(")", "-")
        )
        reference = f"[{identifier}]: [#{concept_urlized}]  "
        self._reference = [reference]
        self._markdown = [
            f"## [{concept}][{identifier}]  ",
            "",
            f"{meaning}  ",
            "",
        ]
        if None is meaning or 0 == len(meaning):
            self._markdown = list(self._markdown[:-2])

    @property
    def reference(self):
        """The reference link view of the thesaurus item"""
        return self._reference

    @property
    def markdown(self):
        """The markdown view of the thesaurus item"""
        return self._markdown
