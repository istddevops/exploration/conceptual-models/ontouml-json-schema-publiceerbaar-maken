"""
The interface for a markdown view of a thesaurus
"""


# pylint: disable=[R0903]
class MarkdownThesaurus:
    """
    Interface for a markdown view of a thesaurus
    """

    def __init__(self, title, contents: list):
        if None is title:
            raise ValueError("title")
        if not isinstance(title, str):
            raise TypeError("title")
        if 0 == len(title):
            raise ValueError("title")
        if None is contents:
            raise ValueError("contents")
        if not isinstance(contents, list):
            raise TypeError("contents")
        if not all(
            (hasattr(item, "markdown") and hasattr(item, "reference"))
            for item in contents
        ):
            raise ValueError("contents")

        self._title = title
        self._contents = contents

    @property
    def markdown(self):
        """
        The markdown for the thesaurus
        """

        result = [f"# {self._title}  ", ""]
        # pylint: disable=[W0106]
        result.extend(
            [markdown for item in self._contents for markdown in item.markdown]
        )
        result.extend(
            [reference for item in self._contents for reference in item.reference]
        )
        return result
