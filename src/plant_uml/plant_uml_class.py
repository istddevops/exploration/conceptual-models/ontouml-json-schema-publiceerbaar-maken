"""The interface for a PlantUML class"""


from dataclasses import dataclass


@dataclass
class PlantUMLClass:
    """Interface for a class in PlantUML"""

    identifier: str
    """The identifier of the class"""
    name: str
    """The name of the class"""
    stereotype: str = None
    """The (optional) stereotype of the class"""
    color: str = None
    """The (optional) color of the class"""
    thesaurus_link: str = None
    """The (optional) link to the thesaurus for this class"""

    def __post_init__(self):
        self.identifier = self.identifier.replace(".", "___")

    @property
    def plant_uml(self):
        """The PlantUML representation of a OntoUmlClass"""

        result = f"""class "{self.name}" as {self.identifier}"""
        if None is not self.stereotype and 0 < len(self.stereotype):
            result += f""" <<{self.stereotype}>>"""

        if None is not self.thesaurus_link:
            result += f""" [[{self.thesaurus_link}]]"""

        if None is not self.color:
            result += f" #{self.color}"

        return [result]
