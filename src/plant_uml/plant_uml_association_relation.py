"""The interface for a PlantUML relation"""


from dataclasses import dataclass

from src.plant_uml.plant_uml_class import PlantUMLClass
from src.plant_uml.plant_uml_relation import PlantUMLRelation


@dataclass
class PlantUMLAssociationRelation:
    """The interface for a PlantUML association relation"""

    identifier: str
    """The identifier of the association relation"""
    associated_relation: PlantUMLRelation
    """The relation to which the association class, associates"""
    associated_class: PlantUMLClass
    """
    The association class
    """
    association_label: str = None
    """The (optional) label to be added to the association class relation"""

    @property
    def plant_uml(self):
        """The PlantUML representation of the association relation"""
        result = self.associated_relation.plant_uml

        result = (
            f"({self.associated_relation.source.identifier}, "
            + f"{self.associated_relation.target.identifier}) "
            + f".. {self.associated_class.identifier}"
        )
        if None is not self.association_label and 0 < len(self.association_label):
            result += f": {self.association_label}"
        return [result]
