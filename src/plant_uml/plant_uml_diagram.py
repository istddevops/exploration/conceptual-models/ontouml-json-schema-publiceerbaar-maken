"""The interface for a PlantUML class"""


from typing import List

from src.plant_uml.plant_uml_association_relation import PlantUMLAssociationRelation
from src.plant_uml.plant_uml_class import PlantUMLClass
from src.plant_uml.plant_uml_generalization import PlantUMLGeneralization
from src.plant_uml.plant_uml_relation import PlantUMLRelation


class PlantUMLDiagram:
    """Interface for a diagram in PlantUML"""

    # pylint: disable=[R0913]
    def __init__(
        self,
        identifier,
        name,
        description,
        classes: List[PlantUMLClass],
        generalizations: List[PlantUMLGeneralization],
        relations: List[PlantUMLRelation],
        association_relations: List[PlantUMLAssociationRelation],
    ):
        if None is identifier:
            raise ValueError("identifier")
        if not isinstance(identifier, str):
            raise TypeError("identifier")
        if None is not name and not isinstance(name, str):
            raise TypeError("name")
        if None is not description and not isinstance(description, str):
            raise TypeError("description")
        if None is classes:
            raise ValueError("classes")
        if not all(isinstance(item, PlantUMLClass) for item in classes):
            raise TypeError("classes")
        if None is generalizations:
            raise ValueError("generalizations")
        if not all(
            isinstance(item, PlantUMLGeneralization) for item in generalizations
        ):
            raise TypeError("generalizations")
        if None is relations:
            raise ValueError("relations")
        if not all(isinstance(item, PlantUMLRelation) for item in relations):
            raise TypeError("relations")
        if None is association_relations:
            raise ValueError("association_relations")
        if not all(
            isinstance(item, PlantUMLAssociationRelation)
            for item in association_relations
        ):
            raise TypeError("association_relations")

        self._identifier = identifier
        self._name = name
        self._description = description
        self._classes = classes
        self._generalizations = generalizations
        self._relations = relations
        self._association_relations = association_relations

    @property
    def identifier(self):
        """The identifier of the plantUML diagram"""
        return self._identifier

    @property
    def name(self):
        """The name of the plantUML diagram"""
        return self._name

    @property
    def title_and_description(self):
        """If present, the PlantUML representation of the diagram title and / or
        description"""
        has_title = None is not self._name and 0 < len(self._name)
        has_description = None is not self._description and 0 < len(self._description)

        result = []

        if has_title or has_description:
            result.append("note as diagram_title")
            if has_title:
                result.append(f"<b><i>{self._name}</i></b>")

            if has_description:
                result.extend(self._description.splitlines())
            result.append("end note")

        return result

    @property
    def plant_uml(self):
        """The PlantUML representation of a diagram"""
        result = ["@startuml", f"' {self._identifier}"]

        result.extend(self.title_and_description)

        for item in self._classes:
            result.extend(item.plant_uml)

        for item in self._generalizations:
            result.extend(item.plant_uml)

        for item in self._association_relations:
            result.extend(item.plant_uml)

        for item in self._relations:
            result.extend(item.plant_uml)

        result.append("@enduml")

        return result
