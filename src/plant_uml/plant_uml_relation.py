"""The interface for a PlantUML relation"""


class PlantUMLRelationParticipant:
    """The interface for a participant in a PlantUML relation"""

    def __init__(self, identifier, cardinality, aggregation_kind, participant_type):
        """
        Constructs an instance of `PlantUMLRelationParticipant`
        - `identifier` is mandatory; if not provided a `ValueError` will be raised
        - `identifier` needs to be of type `str`; if not a `TypeError` will be raised
        - `cardinality` is optional, however when provided it needs to be of type `str`;
          if not a `TypeError` will be raised
        - `aggregation_kind` is mandatory; if not provided a `ValueError` will be raised
        - `aggregation_kind` needs to be of type `str`; if not a `TypeError` will be
          raised
        - `aggregation_kind` only accepts the values "NONE", "COMPOSITE" or "SHARED"; if
          another string value is provided a `ValueError` will be raised
        - `participant_type` is mandatory; if not provided a `ValueError` will be raised
        - `participant_type` needs to be of type `str`; if not a `TypeError` will be
          raised
        """
        if None is identifier:
            raise ValueError("identifier")
        if not isinstance(identifier, str):
            raise TypeError("identifier")
        if None is not cardinality and not isinstance(cardinality, str):
            raise TypeError("cardinality")
        if None is aggregation_kind:
            aggregation_kind = "NONE"
        if None is not aggregation_kind and not isinstance(aggregation_kind, str):
            raise TypeError("aggregation_kind")
        if (
            None is not aggregation_kind
            and isinstance(aggregation_kind, str)
            and aggregation_kind not in ("NONE", "COMPOSITE", "SHARED")
        ):
            raise ValueError("aggregation_kind")
        if None is participant_type:
            raise ValueError("participant_type")
        if not isinstance(participant_type, str):
            raise TypeError("participant_type")

        self._identifier = identifier
        self._cardinality = cardinality
        self._aggregation_kind = aggregation_kind
        self._type = participant_type

    @property
    def identifier(self):
        """
        The PlantUML representation of the identifier of the relation participant.
        If any periods (`.`) are present in the identifier, these will be replaced
        with three underscores (`___`)
        """
        return self._identifier.replace(".", "___")

    @property
    def cardinality(self):
        """The cardinality of the relation participant"""
        return self._cardinality

    @property
    def aggregation(self):
        """
        The PlantUML representation of the aggregation of the relation participant
        - "COMPOSITE" -> "*"
        - "SHARED" -> "o"
        - "NONE" -> ""
        """
        match self._aggregation_kind:
            case "COMPOSITE":
                return "*"
            case "SHARED":
                return "o"
            case "NONE":
                return ""
            case _:
                raise ValueError("aggregation_kind")

    @property
    def participant_type(self):
        """The object type of the relation participant"""
        return self._type


class PlantUMLRelation:
    """The interface for a PlantUML relation"""

    def __init__(
        self,
        source: PlantUMLRelationParticipant,
        target: PlantUMLRelationParticipant,
        name,
        stereotype,
    ):
        if None is source:
            raise ValueError("source")
        if not isinstance(source, PlantUMLRelationParticipant):
            raise TypeError("source")
        if None is target:
            raise ValueError("target")
        if not isinstance(target, PlantUMLRelationParticipant):
            raise TypeError("target")
        if None is not name and not isinstance(name, str):
            raise TypeError("name")
        if None is not stereotype and not isinstance(stereotype, str):
            raise TypeError("stereotype")

        self._source = source
        self._target = target
        self._name = name
        self._stereotype = stereotype

    @property
    def source(self):
        """The source participant in this relation"""
        return self._source

    @property
    def target(self):
        """The target participant in this relation"""
        return self._target

    @property
    def stereotype(self):
        """The stereotype of the relation"""
        if None is not self._stereotype and 0 < len(self._stereotype):
            return f"<<{self._stereotype}>>"
        return ""

    @property
    def plant_uml(self):
        """The PlantUML representation of the relation"""

        result = f"{self._source.identifier}"

        if None is not self._source.cardinality and 0 < len(self._source.cardinality):
            result = f'{result} "{self._source.cardinality}"'

        result = f"{result} {self._source.aggregation}--{self._target.aggregation}"

        if None is not self._target.cardinality and 0 < len(self._target.cardinality):
            result = f'{result} "{self._target.cardinality}"'

        result = f"{result} {self._target.identifier}"

        has_stereotype = None is not self.stereotype and 0 < len(self.stereotype)
        has_name = None is not self._name and 0 < len(self._name)
        if has_stereotype or has_name:
            result = f"{result} : "
            if has_stereotype:
                result = f"{result}{self.stereotype}"
                if has_name:
                    result = f"{result}\\n"
            if has_name:
                result = f"{result}{self._name}"

        return [result]
