"""The interface for a PlantUML generalization"""


class PlantUMLGeneralization:
    """Interface for a generalization in PlantUML"""

    def __init__(self, specific_identifier, general_identifier, name, description):
        if None is specific_identifier:
            raise ValueError("specific_identifier")
        if not isinstance(specific_identifier, str):
            raise TypeError("specific_identifier")
        if None is general_identifier:
            raise ValueError("general_identifier")
        if not isinstance(general_identifier, str):
            raise TypeError("general_identifier")
        if None is not name and not isinstance(name, str):
            raise TypeError("name")
        if None is not description and not isinstance(description, str):
            raise TypeError("description")

        self._specific = specific_identifier
        self._general = general_identifier
        self._name = name
        self._description = description

    def _transform_identifier(self, attribute):
        return getattr(self, attribute).replace(".", "___")

    @property
    def specific_identifier(self):
        """The identifier of the specific end of the generalization"""
        return self._transform_identifier("_specific")

    @property
    def general_identifier(self):
        """The identifier of the general end of the generalization"""
        return self._transform_identifier("_general")

    @property
    def plant_uml(self):
        """The PlantUML representation of a OntoUmlClass"""

        result = f"{self.general_identifier} <|-- {self.specific_identifier}"
        if None is not self._name and 0 < len(self._name):
            result = f"{result} : {self._name}"

        if None is not self._description and 0 < len(self._description):
            separator = ""
            if " : " in result:
                separator = "<br />"
            else:
                separator = " : "
            result = f"{result}{separator}{self._description}"

        return [result]
