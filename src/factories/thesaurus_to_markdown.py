"""
Module containing factory methods which, based on a ThesaurusBase,
create the corresponding Markdown class.
"""


from src.markdown.markdown_thesaurus import MarkdownThesaurus
from src.markdown.markdown_thesaurus_item import MarkdownThesaurusItem
from src.thesaurus.thesaurus import ThesaurusBase


def to_markdown(thesaurus: ThesaurusBase):
    """
    Creates a markdown view of the `thesaurus`
    """
    if (
        not hasattr(thesaurus, "title")
        or not hasattr(thesaurus, "items")
        or not all(
            "concept" in item and "meaning" in item for item in thesaurus.items.values()
        )
    ):
        return None

    return MarkdownThesaurus(
        thesaurus.title,
        [
            MarkdownThesaurusItem(
                identifier=k,
                concept=v["concept"],
                meaning=v["meaning"],
            )
            for k, v in thesaurus.items.items()
        ],
    )
