"""
Factory methods for mapping excel thesauri to the internal thesaurus format
"""

from openpyxl import load_workbook
from openpyxl.worksheet.worksheet import Worksheet

from src.thesaurus.regie_op_registers import (
    ThesaurusRegieOpRegisters,
    ThesaurusRegieOpRegistersItem,
)
from src.thesaurus.thesaurus import ThesaurusBase


def from_ror_thesaurus(ror_thesaurus) -> ThesaurusBase:
    """
    Converts the data in the excel thesaurus as specified by the "Regie op Registers"
    project, to the internal ThesaurusBase format.
    Will only return a `ThesaurusRegieOpRegisters` object when the following conditions
    are true, otherwise `None` will be returned:
    - A worksheet titled "Thesaurus RoR DG" is present
    - This worksheet contains at least one header row (row one) and one or more data
      rows (rows two and up)
    - The header row contains at least following column titles:
      * ThesaurusIMAR_ID
      * Gekozen concept
      * Concept ZIN (r21)
      * Concept TZW
      * Concept ZiRA
      * Concept NTS (SNOMED)
      * Toelichting RoR
      * Toelcihting TZW
      * Toelichting ZiRA
      * Toelichting NTS (SNOMED)
    - For each data row, the column "ThesaurusIMAR_ID" contains a value
    """

    columns = {
        "ThesaurusIMAR_ID": -1,
        "Gekozen concept": -1,
        "Concept ZIN (r21)": -1,
        "Concept TZW": -1,
        "Concept ZiRA": -1,
        "Concept NTS (SNOMED)": -1,
        "Toelichting RoR": -1,
        "Toelichting TZW": -1,
        "Toelichting ZiRA": -1,
        "Toelichting NTS (SNOMED)": -1,
    }

    # Load the excel file
    workbook = load_workbook(
        ror_thesaurus, read_only=True, data_only=True, keep_links=False
    )
    if "Thesaurus RoR DG" not in workbook:
        return None

    worksheet: Worksheet = workbook["Thesaurus RoR DG"]
    # If no rows are present on the worksheet, then return `None`
    if 0 == len(list(worksheet.rows)):
        return None

    # Check the position of the required columns in the Excel file. That way the setup
    # of the columns can be updated without the need to rewrite code.
    header_row = list(worksheet.rows)[0]
    for header in header_row:
        if header.value in columns:
            # The column is 1-based, lists are 0 based.
            columns[header.value] = header.column - 1

    # This method only supports the format used by the project Regie op Registers
    if not all(
        (
            -1 != value[1][1]  # Contains the value of the dict item
            for value in enumerate(columns.items())
        )
    ):
        return None

    data_rows = list(list(worksheet.rows)[1:])
    # If there are no data rows, return `None`
    if 0 == len(data_rows):
        return None

    # If not all data rows have a value in column "ThesaurusIMAR_ID", return `None`
    if not all(
        (
            None is not no_id_item[columns["ThesaurusIMAR_ID"]].value
            and 0 < len(str(no_id_item[columns["ThesaurusIMAR_ID"]].value))
        )
        for no_id_item in data_rows
    ):
        return None

    return ThesaurusRegieOpRegisters(
        [
            ThesaurusRegieOpRegistersItem(
                row[columns["ThesaurusIMAR_ID"]].value,
                row[columns["Gekozen concept"]].value,
                {
                    "RoR": row[columns["Concept ZIN (r21)"]].value,
                    "TZW": row[columns["Concept TZW"]].value,
                    "ZIRA": row[columns["Concept ZiRA"]].value,
                    "SNOMED": row[columns["Concept NTS (SNOMED)"]].value,
                },
                {
                    "RoR": row[columns["Toelichting RoR"]].value,
                    "TZW": row[columns["Toelichting TZW"]].value,
                    "ZIRA": row[columns["Toelichting ZiRA"]].value,
                    "SNOMED": row[columns["Toelichting NTS (SNOMED)"]].value,
                },
            )
            for row in data_rows
        ]
    )
