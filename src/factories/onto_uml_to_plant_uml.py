"""
Module containing factory methods which, based on an OntoUML viewable type,
create the corresponding PlantUML class.
"""


from typing import List

from src.onto_uml.onto_uml_class import OntoUmlClass
from src.onto_uml.onto_uml_diagram import OntoUmlDiagram
from src.onto_uml.onto_uml_property_assignments import OntoUmlPropertyAssignments
from src.onto_uml.views.class_view import ClassView
from src.onto_uml.views.generalization_view import GeneralizationView
from src.onto_uml.views.relation_view import RelationView
from src.plant_uml.plant_uml_association_relation import PlantUMLAssociationRelation
from src.plant_uml.plant_uml_class import PlantUMLClass
from src.plant_uml.plant_uml_diagram import PlantUMLDiagram
from src.plant_uml.plant_uml_generalization import PlantUMLGeneralization
from src.plant_uml.plant_uml_relation import (
    PlantUMLRelation,
    PlantUMLRelationParticipant,
)
from src.thesaurus.thesaurus import ThesaurusBase


def _build_link(
    property_assignments: OntoUmlPropertyAssignments,
    thesaurus_file,
    thesaurus: ThesaurusBase,
) -> str:
    if None is property_assignments or None is thesaurus_file or None is thesaurus:
        return None
    if property_assignments.thesaurus_imar_id not in thesaurus.items:
        return None
    concept = (
        (thesaurus.items[property_assignments.thesaurus_imar_id]["concept"])
        .lower()
        .replace(" ", "-")
        .replace("(", "-")
        .replace(")", "-")
    )
    definitie: str = thesaurus.items[property_assignments.thesaurus_imar_id]["meaning"]
    result = f"{thesaurus_file}/#{concept}{property_assignments.thesaurus_imar_id}"
    if None is not definitie and 0 < len(definitie):
        result += "{" + definitie.replace("\n", "<br />") + "}"
    return result


# pylint:disable=[W0212, C0123]
def to_plant_uml_diagram(
    onto_uml_diagram: OntoUmlDiagram, thesaurus: ThesaurusBase, thesaurus_file
) -> PlantUMLDiagram:
    """
    Create a `PlantUMLDiagram` based on the `onto_uml_diagram`.

    - `onto_uml_diagram` is mandatory, if not provided -> `ValueError`
    - `onto_uml_diagram` needs to be of type `OntoUmlDiagram`, if not -> `TypeError`
    """

    if None is onto_uml_diagram:
        raise ValueError("onto_uml_diagram")
    if not isinstance(onto_uml_diagram, OntoUmlDiagram):
        raise TypeError("onto_uml_diagram")

    class_views: List[ClassView] = list(
        filter(lambda cv: type(cv) is ClassView, onto_uml_diagram.contents)
    )
    plant_uml_classes = [
        PlantUMLClass(
            identifier=cv.model_element.model_element.identifier,
            name=cv.model_element.model_element.name,
            stereotype=cv.model_element.model_element.stereotype,
            thesaurus_link=_build_link(
                cv.model_element.model_element.property_assignments,
                thesaurus_file,
                thesaurus,
            ),
            color=cv.model_element.model_element.html_color_code,
        )
        for cv in class_views
    ]
    generalization_views: List[GeneralizationView] = list(
        filter(lambda gv: type(gv) is GeneralizationView, onto_uml_diagram.contents)
    )
    plant_uml_generalizations = [
        PlantUMLGeneralization(
            gv.model_element.model_element.specific.identifier,
            gv.model_element.model_element.general.identifier,
            gv.model_element.model_element.name,
            gv.model_element.model_element.description,
        )
        for gv in generalization_views
    ]
    relation_views: List[RelationView] = list(
        filter(lambda rv: type(rv) is RelationView, onto_uml_diagram.contents)
    )
    plant_uml_relations = [
        PlantUMLRelation(
            PlantUMLRelationParticipant(
                rv.model_element.model_element.properties[0].property_type.identifier,
                rv.model_element.model_element.properties[0].cardinality,
                rv.model_element.model_element.properties[0].aggregation_kind,
                rv.model_element.model_element.properties[0].property_type.related_type,
            ),
            PlantUMLRelationParticipant(
                rv.model_element.model_element.properties[1].property_type.identifier,
                rv.model_element.model_element.properties[1].cardinality,
                rv.model_element.model_element.properties[1].aggregation_kind,
                rv.model_element.model_element.properties[1].property_type.related_type,
            ),
            rv.model_element.model_element.name,
            rv.model_element.model_element.stereotype,
        )
        for rv in relation_views
        if (
            2 == len(rv.model_element.model_element.properties)
            and rv.model_element.model_element.properties[0].property_type.related_type
            == "Class"
            and rv.model_element.model_element.properties[1].property_type.related_type
            == "Class"
        )
    ]
    plant_uml_association_relations = [
        PlantUMLAssociationRelation(
            rv.model_element.model_element_id,
            PlantUMLRelation(
                source=PlantUMLRelationParticipant(
                    identifier=source.identifier,
                    cardinality="",
                    aggregation_kind="NONE",
                    participant_type="Class",
                ),
                target=PlantUMLRelationParticipant(
                    identifier=target.identifier,
                    cardinality="",
                    aggregation_kind="NONE",
                    participant_type="Class",
                ),
                name="",
                stereotype=None,
            ),
            PlantUMLClass(
                association_class.identifier,
                association_class.name,
                association_class.stereotype,
            ),
            rv.model_element.model_element.name,
        )
        for rv in relation_views
        if (
            None is not rv.source
            and None is not rv.target
            and "ClassView"
            in (rv.source.model_element_type, rv.target.model_element_type)
            and "RelationView"
            in (rv.source.model_element_type, rv.target.model_element_type)
        )
        for relation_view in [rv.source, rv.target]
        if "RelationView" == relation_view.model_element_type
        for association_class_view in [rv.source, rv.target]
        if "ClassView" == association_class_view.model_element_type
        for association_class in onto_uml_diagram.owner.object_instance.contents
        if (
            type(association_class) is OntoUmlClass
            and association_class.identifier
            == association_class_view.model_element.model_element.model_element_id
        )
        for source in onto_uml_diagram.owner.object_instance.contents
        if (  # pylint:disable=[C0301]
            type(source) is OntoUmlClass
            and source.identifier
            == relation_view.model_element.source.model_element.model_element.model_element_id  # noqa=E501
        )
        for target in onto_uml_diagram.owner.object_instance.contents
        if (  # pylint:disable=[C0301]
            type(target) is OntoUmlClass
            and target.identifier
            == relation_view.model_element.target.model_element.model_element.model_element_id  # noqa=E501
        )
    ]

    return PlantUMLDiagram(
        onto_uml_diagram.identifier,
        onto_uml_diagram.name,
        onto_uml_diagram.description,
        plant_uml_classes,
        plant_uml_generalizations,
        plant_uml_relations,
        plant_uml_association_relations,
    )
