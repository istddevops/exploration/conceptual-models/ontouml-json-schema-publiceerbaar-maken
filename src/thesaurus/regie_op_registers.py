"""
Module containing interfaces for working with the thesaurus format as specified by the
project "Regie op Registers".
"""


from typing import List

from src.thesaurus.thesaurus import ThesaurusBase


class ThesaurusRegieOpRegistersItem:
    """
    Interface for one item from the thesaurus as specified by the project "Regie op
    Registers".
    """

    def __init__(self, identifier, chosen_concept: str, concepts: dict, meanings: dict):
        if None is identifier:
            raise ValueError("identifier")

        if None is chosen_concept:
            raise ValueError("chosen_concept")
        if not isinstance(chosen_concept, str):
            raise TypeError("chosen_concept")

        if None is concepts:
            raise ValueError("concepts")
        if not isinstance(concepts, dict):
            raise TypeError("concepts")
        if 0 == len(concepts):
            raise ValueError("concepts")

        if None is meanings:
            raise ValueError("meanings")
        if not isinstance(meanings, dict):
            raise TypeError("meanings")
        if 0 == len(meanings):
            raise ValueError("meanings")

        self._identifier = identifier
        self._chosen_concept = chosen_concept
        self._concepts = concepts
        self._meanings = meanings

    @property
    def identifier(self):
        """The identifier of the thesaurus item"""
        return self._identifier

    @property
    def concept(self):
        """The name of the concept in the thesaurus"""
        return self._concepts[self._chosen_concept]

    @property
    def meaning(self):
        """The meaning given to the concept in the thesaurus"""
        return self._meanings[self._chosen_concept]


class ThesaurusRegieOpRegisters(ThesaurusBase):
    """
    Interface for the thesaurus as specified by the project "Regie op Registers".
    """

    def __init__(self, items: List[ThesaurusRegieOpRegistersItem]):
        super().__init__("Regie op registers", items)
