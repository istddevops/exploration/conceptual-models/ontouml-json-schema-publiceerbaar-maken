"""
Module containing base interfaces for working with thesauri.
"""


class ThesaurusBase:
    """
    Base interface for thesaurus.
    """

    def __init__(self, title, items: list):
        if None is title:
            raise ValueError("title")
        if not isinstance(title, str):
            raise TypeError("title")

        if None is items:
            raise ValueError("items")

        if not all(
            (
                hasattr(item, "identifier")
                and hasattr(item, "concept")
                and hasattr(item, "meaning")
            )
            for item in items
        ):
            raise TypeError("items")

        self._title = title
        self._items = items
        self._items_dict = None

    @property
    def title(self):
        """The title of the thesaurus"""
        return self._title

    @property
    def items(self):
        """
        The thesaurus items as a dictionary. This allows for easy searching on
        identification.
        Each dictionary item contains yet another dictionary:
        `{"concept": <concept>, "meaning": <meaning>}`
        """
        if self._items_dict is None:
            self._items_dict = dict(
                map(
                    lambda item: (
                        str(item.identifier),
                        {"concept": item.concept, "meaning": item.meaning},
                    ),
                    self._items,
                )
            )
        return self._items_dict
