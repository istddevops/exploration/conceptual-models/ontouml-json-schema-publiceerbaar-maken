"""
Experiment to see if we can import the json more easily by simply letting everything be
simple types as opposed to specialized classes.
"""

# pylint:disable=[R1717]


import json

from src.factories.onto_uml_to_plant_uml import _build_link
from src.onto_uml.onto_uml_json_decoder import onto_uml_typed_object
from src.plant_uml.plant_uml_association_relation import PlantUMLAssociationRelation
from src.plant_uml.plant_uml_diagram import PlantUMLClass, PlantUMLDiagram
from src.plant_uml.plant_uml_generalization import PlantUMLGeneralization
from src.plant_uml.plant_uml_relation import (
    PlantUMLRelation,
    PlantUMLRelationParticipant,
)


def _get_typed_item_from_package(package, targeted_type):
    # First, get all targeted typed items from any nested packages. Recursion!
    result = {}
    # pylint:disable=[W0106]
    [
        result.update(_get_typed_item_from_package(item, targeted_type))
        for item in package["contents"]
        if "contents" in item
        and None is not item["contents"]
        and "Package" == item["type"]
    ]
    result.update(
        dict(
            [
                (item["id"], onto_uml_typed_object(item))
                for item in package["contents"]
                if targeted_type == item["type"]
            ]
        )
    )

    return result


def _get_classes_from_project(from_project):
    return _get_typed_item_from_package(from_project["model"], "Class")


def _get_relations_from_project(from_project):
    return _get_typed_item_from_package(from_project["model"], "Relation")


def _get_generalizations_from_project(from_project):
    return _get_typed_item_from_package(from_project["model"], "Generalization")


def _get_diagrams_from_project(from_project):
    return list(from_project["diagrams"])


def _get_diagrams_to_publish(from_project):
    return [
        item
        for item in _get_diagrams_from_project(from_project)
        if "description" in item
        and None is not item["description"]
        and "##publish##" in item["description"]
    ]


def _get_classviews(diagrams_to_publish, classes):
    return dict(
        [
            (classview["id"], viewed_class)
            for diagram in diagrams_to_publish
            for classview in diagram["contents"]
            if "ClassView" == classview["type"]
            for viewed_class_key, viewed_class in classes.items()
            if viewed_class_key == classview["modelElement"]["id"]
        ]
    )


def _get_classviews_plantuml(classviews, thesaurus_link=None, thesaurus=None):
    worker = dict(
        [
            (
                key,
                PlantUMLClass(
                    identifier=item.identifier,
                    name=item.name,
                    stereotype=item.stereotype,
                    color=item.html_color_code,
                    thesaurus_link=_build_link(
                        item.property_assignments, thesaurus_link, thesaurus
                    ),
                ),
            )
            for key, item in classviews.items()
            if None is not item.property_assignments
        ]
    )
    worker.update(
        dict(
            [
                (
                    key,
                    PlantUMLClass(
                        identifier=item.identifier,
                        name=item.name,
                        stereotype=item.stereotype,
                        color=item.html_color_code,
                        thesaurus_link=_build_link(
                            item.property_assignments, thesaurus_link, thesaurus
                        ),
                    ),
                )
                for key, item in classviews.items()
            ]
        )
    )
    return worker


def _get_generalizationviews(diagrams_to_publish, generalizations, classviews):
    return dict(
        [
            (
                generalizationview["id"],
                {
                    "generalization": generalizations[
                        generalizationview["modelElement"]["id"]
                    ],
                    "general_class": classviews[generalizationview["source"]["id"]],
                    "specific_class": classviews[generalizationview["target"]["id"]],
                },
            )
            for diagram in diagrams_to_publish
            for generalizationview in diagram["contents"]
            if "GeneralizationView" == generalizationview["type"]
        ]
    )


def _get_generalizationviews_plantuml(generalizationviews):
    return dict(
        [
            (
                key,
                PlantUMLGeneralization(
                    specific_identifier=item["specific_class"].identifier,
                    general_identifier=item["general_class"].identifier,
                    name=item["generalization"].name,
                    description=item["generalization"].description,
                ),
            )
            for key, item in generalizationviews.items()
        ]
    )


def _get_relationviews(diagrams_to_publish, relations, classviews):
    return dict(
        [
            (
                relationview["id"],
                {
                    "relation": relations[relationview["modelElement"]["id"]],
                    "source_class": classviews[relationview["source"]["id"]],
                    "source_properties": source,
                    "target_class": classviews[relationview["target"]["id"]],
                    "target_properties": target,
                },
            )
            for diagram in diagrams_to_publish
            for relationview in diagram["contents"]
            if "RelationView" == relationview["type"]
            and "ClassView" == relationview["source"]["type"]
            and "ClassView" == relationview["target"]["type"]
            for source in relations[relationview["modelElement"]["id"]].properties
            if "Class" == source["propertyType"]["type"]
            and source["propertyType"]["id"]
            == classviews[relationview["source"]["id"]].identifier
            for target in relations[relationview["modelElement"]["id"]].properties
            if "Class" == target["propertyType"]["type"]
            and target["propertyType"]["id"]
            == classviews[relationview["target"]["id"]].identifier
        ]
    )


def _get_relationviews_plantuml(relationviews):
    return dict(
        [
            (
                key,
                PlantUMLRelation(
                    source=PlantUMLRelationParticipant(
                        identifier=item["source_class"].identifier,
                        cardinality=item["source_properties"]["cardinality"],
                        aggregation_kind=item["source_properties"]["aggregationKind"],
                        participant_type="Class",
                    ),
                    target=PlantUMLRelationParticipant(
                        identifier=item["target_class"].identifier,
                        cardinality=item["target_properties"]["cardinality"],
                        aggregation_kind=item["target_properties"]["aggregationKind"],
                        participant_type="Class",
                    ),
                    name=item["relation"].name,
                    stereotype=item["relation"].stereotype,
                ),
            )
            for key, item in relationviews.items()
        ]
    )


def _get_association_relationviews(diagrams_to_publish, relations, classviews):
    return dict(
        [
            (
                relationview["id"],
                {
                    "relation": relations[
                        associated_relationview["modelElement"]["id"]
                    ],
                    "association": relations[relationview["modelElement"]["id"]],
                    "source_id": classviews[
                        associated_relationview["source"]["id"]
                    ].identifier,
                    "target_id": classviews[
                        associated_relationview["target"]["id"]
                    ].identifier,
                    "associated_class": associated_classview["id"],
                },
            )
            for diagram in diagrams_to_publish
            for relationview in diagram["contents"]
            if "RelationView" == relationview["type"]
            and "ClassView"
            in (relationview["source"]["type"], relationview["target"]["type"])
            and "RelationView"
            in (relationview["source"]["type"], relationview["target"]["type"])
            for associated_relationview in diagram["contents"]
            if "RelationView" == associated_relationview["type"]
            and (
                (
                    "RelationView" == relationview["source"]["type"]
                    and relationview["source"]["id"] == associated_relationview["id"]
                )
                or (
                    "RelationView" == relationview["target"]["type"]
                    and relationview["target"]["id"] == associated_relationview["id"]
                )
            )
            for associated_classview in diagram["contents"]
            if "ClassView" == associated_classview["type"]
            and (
                (
                    "ClassView" == relationview["source"]["type"]
                    and relationview["source"]["id"] == associated_classview["id"]
                )
                or (
                    "ClassView" == relationview["target"]["type"]
                    and relationview["target"]["id"] == associated_classview["id"]
                )
            )
        ]
    )


def _get_association_relationviews_plantuml(
    association_relationviews, classviews_plantuml
):
    return dict(
        [
            (
                key,
                PlantUMLAssociationRelation(
                    identifier=key,
                    associated_relation=PlantUMLRelation(
                        source=PlantUMLRelationParticipant(
                            identifier=item["source_id"],
                            cardinality=item["relation"].properties[0]["cardinality"],
                            aggregation_kind=item["relation"].properties[0][
                                "aggregationKind"
                            ],
                            participant_type="Class",
                        ),
                        target=PlantUMLRelationParticipant(
                            identifier=item["target_id"],
                            cardinality=item["relation"].properties[1]["cardinality"],
                            aggregation_kind=item["relation"].properties[1][
                                "aggregationKind"
                            ],
                            participant_type="Class",
                        ),
                        name=item["relation"].name,
                        stereotype=item["relation"].stereotype,
                    ),
                    associated_class=classviews_plantuml[item["associated_class"]],
                    association_label=item["association"].name,
                ),
            )
            for key, item in association_relationviews.items()
        ]
    )


def get_plantuml(arguments, thesaurus_link=None, thesaurus=None):
    """
    Gets all plant uml diagram codes for all diagrams which have `##publish##` in their
    description
    """
    # pylint:disable=[W1514]
    with open(arguments.input_file, "rt") as handle:
        project = json.load(handle)

    relations = _get_relations_from_project(project)

    diagrams_to_publish = _get_diagrams_to_publish(project)
    classviews = _get_classviews(
        diagrams_to_publish, _get_classes_from_project(project)
    )
    classviews_plantuml = _get_classviews_plantuml(
        classviews, thesaurus_link, thesaurus
    )

    generalizationviews = _get_generalizationviews(
        diagrams_to_publish, _get_generalizations_from_project(project), classviews
    )
    generalizationviews_plantuml = _get_generalizationviews_plantuml(
        generalizationviews
    )

    relationviews_plantuml = _get_relationviews_plantuml(
        _get_relationviews(diagrams_to_publish, relations, classviews)
    )
    association_relationviews_plantuml = _get_association_relationviews_plantuml(
        _get_association_relationviews(diagrams_to_publish, relations, classviews),
        classviews_plantuml,
    )
    return [
        PlantUMLDiagram(
            diagram["id"],
            diagram["name"],
            diagram["description"],
            [
                classviews_plantuml[item["id"]]
                for item in diagram["contents"]
                if "ClassView" == item["type"]
            ],
            [
                generalizationviews_plantuml[item["id"]]
                for item in diagram["contents"]
                if "GeneralizationView" == item["type"]
            ],
            [
                relationviews_plantuml[item["id"]]
                for item in diagram["contents"]
                if "RelationView" == item["type"]
                and item["id"] in relationviews_plantuml
            ],
            [
                association_relationviews_plantuml[item["id"]]
                for item in diagram["contents"]
                if "RelationView" == item["type"]
                and item["id"] in association_relationviews_plantuml
            ],
        )
        for diagram in diagrams_to_publish
    ]
