"""Met deze module testen we hoe eenvoudig het is om een ontoUML model, wat
is geexporteerd naar json via de ontoUML plugin uit Visual Paradigm, om te
zetten naar plantUML syntax"""

import argparse
import base64
import json
import os
import zlib
from typing import List

from src.factories.excel_to_thesaurus import from_ror_thesaurus
from src.factories.onto_uml_to_plant_uml import to_plant_uml_diagram
from src.factories.thesaurus_to_markdown import to_markdown
from src.markdown.markdown_plantuml import MarkdownPlantUml
from src.markdown.markdown_thesaurus import MarkdownThesaurus
from src.onto_uml.onto_uml_json_decoder import onto_uml_typed_object
from src.onto_uml.onto_uml_project import OntoUmlProject
from src.plant_uml.plant_uml_diagram import PlantUMLDiagram
from src.queried_onto_uml_json_importer import get_plantuml
from src.thesaurus.thesaurus import ThesaurusBase


def _import_onto_uml_json_project(input_file: str) -> OntoUmlProject:
    """Importeert uit de huidige folder het bestand
    "Regie op Registers Dure Geneesmiddelen.json"
    """

    # pylint: disable=[W1514]
    with open(input_file, "rt") as handle:
        return json.load(handle, object_hook=onto_uml_typed_object)


def _import_thesaurus(input_file: str):
    return from_ror_thesaurus(input_file)


# pylint: disable=[R0913]
def _save_to_file(
    arguments: argparse.Namespace,
    argument_flag: [str, str],
    argument_folder: [str, str],
    filename: str,
    callback,
    data_object,
):
    flag = getattr(arguments, argument_flag[1])
    if not flag:
        return None

    folder = getattr(arguments, argument_folder[1], None)
    if None is folder:
        raise argparse.ArgumentError(
            None,
            f"When {argument_flag[0]} is set, then the {argument_folder[0]} MUST be"
            + " provided",
        )

    if not os.path.exists(folder):
        os.makedirs(folder, mode=0o770)

    full_filename: str = _get_full_filename(folder, filename)
    filemode = "xt"  # By default, create a text file (xt)
    if os.path.exists(full_filename):
        filemode = "wt"  # If the file already exists, then overwrite the text file (wt)

    callback(data_object, full_filename, filemode)

    return full_filename


def _get_full_filename(folder, filename):
    return f"{folder}/{filename}"


def _json_dumper(data_object, filename, filemode):
    with open(filename, mode=filemode, encoding="UTF-8") as output_file:
        json.dump(data_object, output_file)


def _plain_text_dumper(data_object, filename, filemode):
    with open(filename, mode=filemode, encoding="UTF-8") as output_file:
        output_file.writelines(data_object)


def _save_metadata(arguments: argparse.Namespace, onto_uml_project: OntoUmlProject):
    _save_to_file(
        arguments,
        ["save-metadata", "save_metadata"],
        ["metadata-folder", "metadata_folder"],
        f"{onto_uml_project.name}.json",
        _json_dumper,
        onto_uml_project,
    )


def _generate_thesaurus_link(arguments):
    thesaurus_md_folder = getattr(arguments, "thesaurus_md_folder", None)
    if None is not thesaurus_md_folder:
        plant_uml_folder = getattr(arguments, "plant_uml_folder")
        thesaurus_link = os.path.relpath(thesaurus_md_folder, plant_uml_folder)
        base_url = getattr(arguments, "base_url", None)
        # If set, prepend thesaurus_link with the base url
        if None is not base_url:
            thesaurus_link = f"{base_url}/{thesaurus_link}"
        return f"{thesaurus_link}/thesaurus"
    return None


def _generate_plant_uml(
    arguments: argparse.Namespace,
    thesaurus: ThesaurusBase,
    onto_uml_project: OntoUmlProject = None,
    plant_uml_diagrams: List[PlantUMLDiagram] = None,
):
    thesaurus_link_stem = _generate_thesaurus_link(arguments)

    # Traverse over all diagrams in the project
    if None is not onto_uml_project:
        for diagram in [item for item in onto_uml_project.diagrams if item.publish]:
            _save_to_file(
                arguments,
                ["plant-uml", "plant_uml"],
                ["plant-uml-folder", "plant_uml_folder"],
                f"{diagram.identifier}.md",
                _plain_text_dumper,
                MarkdownPlantUml(
                    plant_uml_diagram=to_plant_uml_diagram(
                        diagram, thesaurus, thesaurus_link_stem
                    ),
                    diagram_name=diagram.name,
                    diagram_identifier=diagram.identifier,
                ).hugo_page,
            )
            if None is not getattr(arguments, "plant_uml_code", None):
                data = "\n".join(
                    MarkdownPlantUml(
                        plant_uml_diagram=to_plant_uml_diagram(
                            diagram, thesaurus, thesaurus_link_stem
                        ),
                        diagram_name=diagram.name,
                        diagram_identifier=diagram.identifier,
                    ).plant_uml_diagram.plant_uml
                ).splitlines(keepends=True)
                _save_to_file(
                    arguments,
                    ["plant-uml-code", "plant_uml_code"],
                    ["plant-uml-code-folder", "plant_uml_code_folder"],
                    f"{diagram.name}.plantuml",
                    _plain_text_dumper,
                    base64.urlsafe_b64encode(
                        zlib.compress(
                            "".join(data[6:-1]).encode("utf-8"), 9
                        )  # Compress plantuml
                    ).decode("ascii"),
                )
    elif None is not plant_uml_diagrams:
        for diagram in plant_uml_diagrams:
            _save_to_file(
                arguments=arguments,
                argument_flag=["plant-uml", "plant_uml"],
                argument_folder=["plant-uml-folder", "plant_uml_folder"],
                filename=f"{diagram.identifier}.md",
                callback=_plain_text_dumper,
                data_object=MarkdownPlantUml(
                    plant_uml_diagram=diagram,
                    diagram_name=diagram.name,
                    diagram_identifier=diagram.identifier,
                ).hugo_page,
            )
            if None is not getattr(arguments, "plant_uml_code", None):
                data = "\n".join(diagram.plant_uml).splitlines(keepends=True)
                _save_to_file(
                    arguments,
                    ["plant-uml-code", "plant_uml_code"],
                    ["plant-uml-code-folder", "plant_uml_code_folder"],
                    f"{diagram.name}.plantuml",
                    _plain_text_dumper,
                    base64.urlsafe_b64encode(
                        zlib.compress(
                            "".join(data[6:-1]).encode("utf-8"), 9
                        )  # Compress plantuml
                    ).decode("ascii"),
                )


def _generate_thesaurus_md(arguments: argparse.Namespace, thesaurus):
    marked_down_thesaurus: MarkdownThesaurus = to_markdown(thesaurus)
    frontmatter = [
        "---\n",
        "title: Thesaurus\n",
        "type: docs\n",
        "---\n",
    ]
    markdown = [line + "\n" for line in marked_down_thesaurus.markdown]
    frontmatter.extend(markdown)
    return _save_to_file(
        arguments,
        ["thesaurus-md", "thesaurus_md"],
        ["thesaurus-md-folder", "thesaurus_md_folder"],
        "thesaurus.md",
        _plain_text_dumper,
        frontmatter,
    )


def _setup_arguments() -> argparse.ArgumentParser:
    result = argparse.ArgumentParser(
        description="""Translate a JSON file (which is assumed to conform to the
        OntoUML JSON Schema) to an intermediate meta model, which allows it to be
        transformed to multiple output formats.

        Currently, only plantUML is supported."""
    )

    result.add_argument("--input-file", action="store")
    result.add_argument("--thesaurus-file", action="store")
    result.add_argument("--base-url", action="store")

    metadata_group = result.add_argument_group("Metadata")
    metadata_group.add_argument("--save-metadata", action="store_true")
    metadata_group.add_argument("--metadata-folder", action="store")

    plant_uml_group = result.add_argument_group("plantUML")
    plant_uml_group.add_argument("--plant-uml", action="store_true")
    plant_uml_group.add_argument("--plant-uml-folder", action="store")

    thesaurus_group = result.add_argument_group("thesaurus")
    thesaurus_group.add_argument("--thesaurus-md", action="store_true")
    thesaurus_group.add_argument("--thesaurus-md-folder", action="store")

    plantuml_code_group = result.add_argument_group("plantuml")
    plantuml_code_group.add_argument("--plant-uml-code", action="store_true")
    plantuml_code_group.add_argument("--plant-uml-code-folder", action="store")

    return result


def main(arguments: argparse.Namespace):
    """Generates plantuml from an ontoUML project exported in JSON format."""

    if None is getattr(arguments, "input_file", None):
        raise argparse.ArgumentError(None, "Mandatory --input-file argument not given")

    thesaurus_file = getattr(arguments, "thesaurus_file", None)
    thesaurus = None
    if None is not thesaurus_file:
        thesaurus = _import_thesaurus(thesaurus_file)
        _generate_thesaurus_md(arguments, thesaurus)

    #    onto_uml_project = _import_onto_uml_json_project(arguments.input_file)

    #    _save_metadata(arguments, onto_uml_project)

    _generate_plant_uml(
        arguments,
        thesaurus,
        onto_uml_project=None,
        plant_uml_diagrams=get_plantuml(
            arguments=arguments,
            thesaurus_link=_generate_thesaurus_link(arguments),
            thesaurus=thesaurus,
        ),
    )


if __name__ == "__main__":
    main(_setup_arguments().parse_args())
