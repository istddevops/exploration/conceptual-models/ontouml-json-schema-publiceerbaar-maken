"""
Helper methods for arguments. E.g. check mandatory arguments are set, typing, ...
"""


from typing import Any, Tuple


def _check_mandatory_set(checkable, message):
    if None is checkable:
        raise ValueError(message)


def _check_type(checkable, expected_type, message):
    if not isinstance(checkable, expected_type):
        raise TypeError(message)


def check_non_iterable(
    checkable: Tuple[Any, str],
    expected_type: Tuple[type, str] = None,
    mandatory: bool = True,
):
    """
    - If {checkable} is {mandatory}, check that {checkable} is set. If not, raise a
      ValueError with message specified in the second part of the {checkable} Tuple.
    - If {expected_type} is set, check if the non interable typed argument {checkable}
      is of {expected_type}. If not, raise a TypeError with message specified in the
      second part of {expected_type} Tuple.

    {checkable} is mandatory, and must be of type Tuple[Any, str]
    {expected_type} is mandatory, and must be of type Tuple[type, str]
    """

    _check_mandatory_set(checkable, "checkable")
    _check_type(checkable, Tuple, "checkable")
    if 2 != len(checkable):
        raise ValueError("checkable")
    _check_type(checkable[1], str, "checkable[1]")
    if None is not expected_type:
        _check_type(expected_type, Tuple, "expected_type")
        _check_mandatory_set(expected_type[0], "expected_type[0]")
        _check_type(expected_type[0], type, "expected_type[0]")
        _check_mandatory_set(expected_type[1], "expected_type[1]")
        _check_type(expected_type[1], str, "expected_type[1]")

    if mandatory:
        _check_mandatory_set(checkable[0], checkable[1])
    if None is not expected_type:
        _check_type(checkable[0], expected_type[0], expected_type[1])


def check_iterable(
    checkable: Tuple[Any, str],
    expected_type: Tuple[type, str, bool],
    mandatory: bool = True,
):
    """
    - If {checkable} is {mandatory}, check that {checkable} is set. If not, raise
      a ValueError with message specified in the second part of the {checkable} Tuple.
    - Check if the iterable typed argument {checkable} is indeed iterable. If not,
      raise a TypeError with message specified in the second part of {expected_type}
      Tuple.
    - If {checkable} is {mandatory}, check that {checkable} contains any items. If not,
      raise a ValueError with message specified in the second part of the {checkable}
      Tuple.
    - If the third part of the {expected_type} Tuple is True, check that all items of
      {checkable} are of {expected_type}. If not, raise a TypeError with message
      specified in the second part of the {expectecd_type} Tuple
    """

    _check_mandatory_set(checkable, "checkable")
    _check_type(checkable, Tuple, "checkable")
    if 2 != len(checkable):
        raise ValueError("checkable")
    _check_mandatory_set(checkable[1], "checkable[1]")
    _check_type(checkable[1], str, "checkable[1]")

    _check_mandatory_set(expected_type, "expected_type")
    _check_type(expected_type, Tuple, "expected_type")
    if 3 != len(expected_type):
        raise ValueError("expected_type")
    _check_mandatory_set(expected_type[0], "expected_type[0]")
    # _check_type(expected_type[0], type, "expected_type[0]")
    _check_mandatory_set(expected_type[1], "expected_type[1]")
    _check_type(expected_type[1], str, "expected_type[1]")
    _check_mandatory_set(expected_type[2], "expected_type[2]")
    _check_type(expected_type[2], bool, "expected_type[2]")

    if mandatory and None is checkable[0]:
        raise ValueError(checkable[1])

    try:
        iter(checkable[0])
    except TypeError as exc:
        raise TypeError(expected_type[1]) from exc

    if mandatory and 0 == len(checkable[0]):
        raise ValueError(checkable[1])

    if expected_type[2] and not all(
        isinstance(item, expected_type[0]) for item in checkable[0]
    ):
        raise TypeError(expected_type[1])
