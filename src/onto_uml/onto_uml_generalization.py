"""The interface for an OntoUML generalization as exported by Visual Paradigm"""

from dataclasses import dataclass

from src.onto_uml.onto_uml_object_instance import OntoUmlObjectInstance
from src.onto_uml.onto_uml_objects import (
    OntoUmlObject,
    _OntoUmlObjectBase,
    _OntoUmlObjectDefaultsBase,
)


class OntoUmlGeneralizationParty(OntoUmlObjectInstance):
    """Interface for OntoUML generalization party as exported by Visual Paradigm.
    A generalization has two parties, the specific and the general one. This
    class represent either one."""

    @property
    def party(self):
        """The object referenced by this generalization party"""
        return super().object_instance

    @property
    def generalization_party_type(self):
        """The type of the object which is a party in the generalization in which
        this instance is present."""
        return super().object_type


@dataclass
class _OntoUmlGeneralizationBase(_OntoUmlObjectBase):
    general: OntoUmlGeneralizationParty
    """The general party of the generalization"""
    specific: OntoUmlGeneralizationParty
    """The specific party of the generalization"""


@dataclass
class _OntoUmlGeneralizationDefaultsBase(_OntoUmlObjectDefaultsBase):
    pass


@dataclass
class OntoUmlGeneralization(
    _OntoUmlGeneralizationDefaultsBase, OntoUmlObject, _OntoUmlGeneralizationBase
):
    """Interface for OntoUML generalization as exported by Visual Paradigm"""
