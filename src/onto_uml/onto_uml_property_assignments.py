"""The interface for an OntoUML model as exported by Visual Paradigm"""


from dataclasses import dataclass
from typing import Any


@dataclass
class OntoUmlPropertyAssignments:
    """Interface for OntoUML property assignments as exported by Visual
    Paradigm"""

    thesaurus_imar_id: Any
    """The thesaurus IMAR ID assigned to the current object"""
