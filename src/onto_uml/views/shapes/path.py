"""Interface for an OntoUML path shape as exported by Visual Paradigm"""


from dataclasses import dataclass, field
from typing import Any, List

from src.onto_uml.views.shapes.shape import Shape


@dataclass
class Point:
    """Interface for a point"""

    x_coordinate: Any
    """The x coordinate of the point"""
    y_coordinate: Any
    """The y coordinate of the point"""


@dataclass
class Path(Shape):
    """Interface for an OntoUML path shape as exported by Visual Paradigm"""

    points: List[Point] = field(default_factory=list)
    """The points over which the path flows"""
