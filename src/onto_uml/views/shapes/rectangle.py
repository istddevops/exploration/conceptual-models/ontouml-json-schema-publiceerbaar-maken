"""Interface for an OntoUML rectangle shape as exported by Visual Paradigm"""


from dataclasses import dataclass
from typing import Any

from src.onto_uml.views.shapes.shape import Shape


@dataclass
class Rectangle(Shape):
    """Interface for an OntoUML rectangle shape as exported by Visual Paradigm"""

    x_coordinate: Any
    """The x coordinate of the box"""
    y_coordinate: Any
    """The y coordinate of the box"""
    width: Any
    """The width of the box"""
    height: Any
    """The height of the box"""
