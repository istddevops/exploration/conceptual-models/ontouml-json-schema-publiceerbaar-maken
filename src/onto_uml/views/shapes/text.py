"""Interface for an OntoUML text shape as exported by Visual Paradigm"""


from dataclasses import dataclass

from src.onto_uml.views.shapes.rectangle import Rectangle


@dataclass
class Text(Rectangle):
    """Interface for an OntoUML text shape as exported by Visual Paradigm"""

    value: str
    """The value of the text shape"""
