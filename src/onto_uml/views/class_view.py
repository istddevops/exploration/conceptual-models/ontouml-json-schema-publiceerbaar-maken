"""The interface for an OntoUML class view as exported by Visual Paradigm"""


from dataclasses import dataclass

from src.onto_uml.views.object_view import (
    ObjectView,
    ObjectViewBase,
    ObjectViewDefaultsBase,
)
from src.onto_uml.views.shapes.rectangle import Rectangle


@dataclass
class ClassViewDefaultsBase(ObjectViewDefaultsBase):
    """
    Interface for the optional OntoUML ClassView members
    """


@dataclass
class ClassViewBase(ObjectViewBase):
    """
    Interface for the mandatory OntoUML ClassView members
    """

    shape: Rectangle


@dataclass
class ClassView(ClassViewDefaultsBase, ObjectView, ClassViewBase):
    """Interface for an OntoUML ClassView as exported by Visual Paradigm"""
