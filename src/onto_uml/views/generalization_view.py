"""The interface for an OntoUML generalization view as exported by Visual Paradigm"""


from dataclasses import dataclass

from src.onto_uml.views.relation_view import RelationView


@dataclass
class GeneralizationView(RelationView):
    """Interface for an OntoUML GeneralizationView as exported by Visual Paradigm"""
