"""The interface for an OntoUML relation view as exported by Visual Paradigm"""


from dataclasses import dataclass

from src.onto_uml.onto_uml_model_element import OntoUmlModelElement
from src.onto_uml.views.object_view import (
    ObjectView,
    ObjectViewBase,
    ObjectViewDefaultsBase,
)
from src.onto_uml.views.shapes.path import Path


@dataclass
class RelationViewDefaultsBase(ObjectViewDefaultsBase):
    """
    Base interface for the optional fields of an OntoUML RelationView
    """


@dataclass
class RelationViewBase(ObjectViewBase):
    """
    Base interface for the mandatory fields of an OntoUML RelationView
    """

    shape: Path
    """The path used to represent the relation"""
    source: OntoUmlModelElement
    """The source element of the viewed relation"""
    target: OntoUmlModelElement
    """The target element of the viewed relation"""


@dataclass
class RelationView(RelationViewDefaultsBase, ObjectView, RelationViewBase):
    """
    Interface for an OntoUML RelationView as exported by Visual Paradigm
    """
