"""The interface for an OntoUML generalization set view as exported by Visual
Paradigm"""


from dataclasses import dataclass

from src.onto_uml.views.object_view import (
    ObjectView,
    ObjectViewBase,
    ObjectViewDefaultsBase,
)
from src.onto_uml.views.shapes.text import Text


@dataclass
class GeneralizationSetViewDefaultsBase(ObjectViewDefaultsBase):
    """
    Interface for the optional members of an OntoUML GeneralizationSetView
    """


@dataclass
class GeneralizationSetViewBase(ObjectViewBase):
    """
    Interface for the mandatory members of an OntoUml GeneralizationSetView
    """

    shape: Text


@dataclass
class GeneralizationSetView(
    GeneralizationSetViewDefaultsBase, ObjectView, GeneralizationSetViewBase
):
    """Interface for an OntoUML GeneralizationsetView as exported by Visual Paradigm"""
