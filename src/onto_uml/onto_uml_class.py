"""The interface for an OntoUML class as exported by Visual Paradigm"""


from dataclasses import dataclass, field
from typing import List

import src.onto_uml.helpers.decoded_generalizations as d
from src.onto_uml.onto_uml_objects import (
    OntoUmlDerivableObject,
    _OntoUmlDerivableObjectBase,
    _OntoUmlDerivableObjectDefaultsBase,
)


@dataclass
class _OntoUmlClassBase(_OntoUmlDerivableObjectBase):
    """"""

    is_extensional: bool
    """Indication wether the class is extensional"""
    is_powertype: bool
    """Indication wether the class is a powertype"""


@dataclass
class _OntoUmlClassDefaultsBase(_OntoUmlDerivableObjectDefaultsBase):
    """"""

    literals: list = field(default_factory=list)
    """Literals of the class"""
    order: str = None
    """Order of the class"""
    restricted_to: List[str] = field(default_factory=list)
    """Objects to which use of this class is restricted"""


@dataclass
class OntoUmlClass(
    _OntoUmlClassDefaultsBase, OntoUmlDerivableObject, _OntoUmlClassBase
):
    """Interface for OntoUML class as exported by Visual Paradigm"""

    @staticmethod
    def get_html_color_code(onto_uml_class):
        """
        Gives the HTML color code for use in a diagram representation. The color code
        is dependent on the class stereotype:
        - <No stereotype> -> None
        - <<kind>>, <<quantity>> -> FF99A3 (roze)
        - <<subkind>>, <<role>>, <<category>> welke GEEN generalization is van
          <<relator>> -> FFDADD (lichtroze)
        - <<mode>>, <<quality>> -> 70D7FF (blauw)
        - <<phase>> welke specification is van <<mode>> of <<quality>>
          -> C0EDFF (lichtblauw)
        - <<relator>> -> 99FF99 (groen)
        - <<phase>> welke specification is van <<relator>>, <<category>> welke
          generalization is van <<relator>> -> D3FFD3 (lichtgroen)
        """

        generalizations = d.get_generalizations()
        generalization = [
            value
            for value in generalizations.values()
            if onto_uml_class in (value.general.party, value.specific.party)
        ]

        result = None

        match onto_uml_class.stereotype:
            case "kind" | "quantity":
                result = "FF99A3"
            case "subkind" | "role":
                result = "FFDADD"
            case "category":
                result = "FFDADD"
                if (
                    1 == len(generalization)
                    and onto_uml_class == generalization[0].general.party
                    and "relator" == generalization[0].specific.party.stereotype
                ):
                    result = "D3FFD3"
            case "mode" | "quality":
                result = "70D7FF"
            case "phase":
                if (
                    1 == len(generalization)
                    and onto_uml_class == generalization[0].specific.party
                ):
                    match generalization[0].general.party.stereotype:
                        case "mode" | "quality":
                            result = "C0EDFF"
                        case "relator":
                            result = "D3FFD3"
            case "relator":
                result = "99FF99"

        return result

    @property
    def html_color_code(self):
        """
        Gives the HTML color code for use in a diagram representation.
        """
        return OntoUmlClass.get_html_color_code(self)
