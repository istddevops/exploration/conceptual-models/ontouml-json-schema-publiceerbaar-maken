"""
Base interface for an OntoUML object instance
"""


from dataclasses import InitVar, dataclass, field
from typing import Any


@dataclass
class _OntoUmlObjectInstanceDefaultsBase:
    _instance: Any = field(default=None, compare=False)
    _dict: dict = field(default_factory=dict, compare=False)
    reference: InitVar(Any) = None


@dataclass
class _OntoUmlObjectInstanceBase:
    identifier: str
    """The identifier given by Visual Paradigm for the object"""
    object_type: str
    """The type of the object"""


@dataclass
class OntoUmlObjectInstance(
    _OntoUmlObjectInstanceDefaultsBase, _OntoUmlObjectInstanceBase
):
    """
    Base interface for an OntoUML object instance
    """

    def __post_init__(self, reference):
        if None is not reference:
            self._instance = reference

    @property
    def object_instance(self):
        """The referenced object"""
        if None is self._instance and self.identifier in self._dict:
            self._instance = self._dict[self.identifier]
        return self._instance
