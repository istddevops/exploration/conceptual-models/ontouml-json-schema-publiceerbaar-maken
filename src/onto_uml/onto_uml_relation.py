"""The interface for an OntoUML relation as exported by Visual Paradigm"""

from dataclasses import dataclass

from src.onto_uml.onto_uml_objects import OntoUmlDerivableObject


@dataclass
class OntoUmlRelation(OntoUmlDerivableObject):
    """Interface for OntoUML relation as exported by Visual Paradigm"""
