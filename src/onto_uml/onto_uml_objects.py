"""UmlObject base classes"""

from dataclasses import dataclass, field
from typing import Any, List

from src.onto_uml.onto_uml_property_assignments import OntoUmlPropertyAssignments


@dataclass
class _IdentifiableObjectBase:
    identifier: str
    """The identifier of the object exported by the OntoUML plugin"""
    name: str
    """The name of the object exported by the OntoUML plugin"""


@dataclass
class _IdentifiableObjectDefaultsBase:
    description: str = field(default=None)
    """The optional description of the object exported by the OntoUML plugin"""


@dataclass
class IdentifiableObject(_IdentifiableObjectDefaultsBase, _IdentifiableObjectBase):
    """The interface of an object exported by the OntoUML plugin"""


@dataclass
class _OntoUmlObjectBase(_IdentifiableObjectBase):
    pass


@dataclass
class _OntoUmlObjectDefaultsBase(_IdentifiableObjectDefaultsBase):
    property_assignments: OntoUmlPropertyAssignments = field(default=None)
    """The optional set of user defined properties assigned to the UML object"""
    _owner: Any = field(default=None, compare=False)
    _package_dict: dict = field(default_factory=dict, compare=False)


@dataclass
class OntoUmlObject(_OntoUmlObjectDefaultsBase, IdentifiableObject, _OntoUmlObjectBase):
    """The interface of an UML object exported by the OntoUML plugin"""

    @property
    def owner(self):
        """The owning package of this object"""
        if None is self._owner:
            owning = list(
                filter(
                    lambda o: None is not o.contents and self in o.contents,
                    self._package_dict.values(),
                )
            )
            if 1 == len(owning):
                self._owner = owning[0]

        return self._owner


@dataclass
class OntoUmlPropertyType:
    """Interface for OntoUML relation property type as exported by Visual Paradigm"""

    identifier: str
    """The identifier of the property type"""
    related_type: Any
    """The type of the object to which this property refers"""


# pylint: disable=[R0902]
@dataclass
class _OntoUmlPropertyBase(_OntoUmlObjectBase):
    stereotype: str
    """The stereotype of the UML property"""
    is_derived: bool
    """Indication wether this UML property is derived"""
    is_read_only: bool
    """Indication wether this UML property is read-only"""
    is_ordered: bool
    """Indication wether this UML property is ordered"""
    cardinality: str
    """The cardinality of this UML property"""
    property_type: OntoUmlPropertyType
    """The UML type of this UML property"""
    subsetted_properties: Any
    """The UML properties subsetted by this UML property"""
    redefined_properties: Any
    """The UML properties being redefined by this UML property"""
    aggregation_kind: str
    """The aggregation kind"""


@dataclass
class _OntoUmlPropertyDefaultsBase(_OntoUmlObjectDefaultsBase):
    pass


@dataclass
class OntoUmlProperty(
    _OntoUmlPropertyDefaultsBase, OntoUmlObject, _OntoUmlPropertyBase
):
    """Interface for OntoUML relation property as exported by Visual Paradigm"""


@dataclass
class _OntoUmlDerivableObjectBase(_OntoUmlObjectBase):
    stereotype: str
    """The stereotype of the derivable UML object"""
    is_abstract: bool
    """Indication wether the derivable UML object is abstract"""
    is_derived: bool
    """Indication wether the derivable UML object itself is derived"""


@dataclass
class _OntoUmlDerivableObjectDefaultsBase(_OntoUmlObjectDefaultsBase):
    properties: List[OntoUmlProperty] = field(default_factory=list)
    """The UML properties which apply to the derivable UML object"""


@dataclass
class OntoUmlDerivableObject(
    _OntoUmlDerivableObjectDefaultsBase, OntoUmlObject, _OntoUmlDerivableObjectBase
):
    """
    The interface of an UML object from which other objects can be derived.
    """
