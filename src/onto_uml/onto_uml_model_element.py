"""Interface for an OntoUML ModelElement as exported by Visual Paradigm"""


class OntoUmlModelElement:
    """Interface for an OntoUML ModelElement as exported by Visual Paradigm"""

    def __init__(self, model_element_id: str, model_element_type: str, element=None):
        self._id = model_element_id
        self._type = model_element_type
        self._dict = {}
        self._element = element

    def __eq__(self, other):
        if None is other:
            return False
        return (
            self.model_element_id == other.model_element_id
            and self.model_element_type == other.model_element_type
        )

    @property
    def model_element(self):
        """The object referenced by this model element"""
        if None is self._element and self._id in self._dict:
            self._element = self._dict[self._id]
        return self._element

    @property
    def model_element_id(self):
        """The id of the model element which is reflected by the owning view of the
        object"""
        return self._id

    @property
    def model_element_type(self):
        """The type of the model element which is reflected by the owning view of
        the object"""
        return self._type
