"""The interface for an OntoUML diagram as exported by Visual Paradigm"""


from dataclasses import dataclass, field
from typing import Any

from src.onto_uml.onto_uml_content_container import (
    OntoUmlContentContainer,
    _OntoUmlContentContainerBase,
    _OntoUmlContentContainerDefaultsBase,
)
from src.onto_uml.onto_uml_object_instance import OntoUmlObjectInstance


@dataclass
class _OntoUmlDiagramOwnerBase:
    pass


@dataclass
class _OntoUmlDiagramOwnerDefaultsBases:
    pass


@dataclass
class OntoUmlDiagramOwner(
    _OntoUmlDiagramOwnerDefaultsBases, OntoUmlObjectInstance, _OntoUmlDiagramOwnerBase
):
    """Interface for the owner of the OntoUML diagram as exported by Visual
    Paradigm"""


@dataclass
class _OntoUmlDiagramBase(_OntoUmlContentContainerBase):
    pass


@dataclass
class _OntoUmlDiagramDefaultsBase(_OntoUmlContentContainerDefaultsBase):
    owner: OntoUmlDiagramOwner = None
    """
    The owner of the diagram, that is, the object under which the diagram is present
    """
    publish: bool = False
    """Indication whether the diagram needs to be published or not."""

    _project: Any = field(default=None, compare=False)


@dataclass
class OntoUmlDiagram(
    _OntoUmlDiagramDefaultsBase, OntoUmlContentContainer, _OntoUmlDiagramBase
):
    """Interface for OntoUML diagram as exported by Visual Paradigm"""
