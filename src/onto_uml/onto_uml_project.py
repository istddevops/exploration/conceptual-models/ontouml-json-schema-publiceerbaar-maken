"""The interface for an OntoUML project as exported by Visual Paradigm"""

from dataclasses import dataclass
from typing import List

from src.onto_uml.onto_uml_diagram import OntoUmlDiagram
from src.onto_uml.onto_uml_objects import (
    IdentifiableObject,
    _IdentifiableObjectBase,
    _IdentifiableObjectDefaultsBase,
)
from src.onto_uml.onto_uml_package import OntoUmlPackage


@dataclass
class _OntoUmlProjectBase(_IdentifiableObjectBase):
    model: OntoUmlPackage
    """The root model of the project"""
    diagrams: List[OntoUmlDiagram]
    """The diagrams present in the project"""


@dataclass
class _OntoUmlProjectDefaultsBase(_IdentifiableObjectDefaultsBase):
    pass


@dataclass
class OntoUmlProject(
    _OntoUmlProjectDefaultsBase, IdentifiableObject, _OntoUmlProjectBase
):
    """Interface (incl. serializer) for OntoUML project as exported by Visual
    Paradigm"""

    def __post_init__(self):
        if None is not self.diagrams:
            try:
                for diagram in self.diagrams:
                    diagram._project = self  # pylint:disable=[W0212]
            except TypeError as type_error:
                raise TypeError("diagrams") from type_error
            except AttributeError as attribute_error:
                raise TypeError("diagrams") from attribute_error
