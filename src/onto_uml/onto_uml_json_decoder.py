"""Decoder and helpers for Onto UML json file"""

from typing import List

from src.onto_uml.onto_uml_class import OntoUmlClass
from src.onto_uml.onto_uml_diagram import OntoUmlDiagram, OntoUmlDiagramOwner
from src.onto_uml.onto_uml_generalization import (
    OntoUmlGeneralization,
    OntoUmlGeneralizationParty,
)
from src.onto_uml.onto_uml_generalization_set import (
    OntoUmlGeneralizationSet,
    OntoUmlGeneralizationSetItem,
)
from src.onto_uml.onto_uml_model_element import OntoUmlModelElement
from src.onto_uml.onto_uml_objects import OntoUmlProperty, OntoUmlPropertyType
from src.onto_uml.onto_uml_package import OntoUmlPackage
from src.onto_uml.onto_uml_project import OntoUmlProject
from src.onto_uml.onto_uml_property_assignments import OntoUmlPropertyAssignments
from src.onto_uml.onto_uml_relation import OntoUmlRelation
from src.onto_uml.views.class_view import ClassView
from src.onto_uml.views.generalization_set_view import GeneralizationSetView
from src.onto_uml.views.generalization_view import GeneralizationView
from src.onto_uml.views.relation_view import RelationView
from src.onto_uml.views.shapes.path import Path, Point
from src.onto_uml.views.shapes.rectangle import Rectangle
from src.onto_uml.views.shapes.text import Text

_classes = {}
_class_views = {}
_diagrams = {}
generalizations = {}
_generalization_sets = {}
_generalization_set_views = {}
_generalization_views = {}
_packages = {}
_relations = {}
_relation_views = {}


def _check_attributes(obj, attributes: List[str]) -> bool:
    for attribute in attributes:
        try:
            if attribute not in obj:
                return False
        except TypeError:
            if not hasattr(obj, attribute):
                return False
    return True


def _class(obj):
    if not _check_attributes(
        obj,
        [
            "id",
            "name",
            "description",
            "propertyAssignments",
            "stereotype",
            "isAbstract",
            "isDerived",
            "properties",
            "isExtensional",
            "isPowertype",
            "order",
            "literals",
            "restrictedTo",
        ],
    ):
        return obj

    worker = OntoUmlClass(
        identifier=obj["id"],
        name=obj["name"],
        description=obj["description"],
        property_assignments=_property_assignments(obj["propertyAssignments"]),
        stereotype=obj["stereotype"],
        is_abstract=obj["isAbstract"],
        is_derived=obj["isDerived"],
        properties=obj["properties"],
        is_extensional=obj["isExtensional"],
        is_powertype=obj["isPowertype"],
        order=obj["order"],
        literals=obj["literals"],
        restricted_to=obj["restrictedTo"],
    )
    _classes[obj["id"]] = worker
    worker._package_dict = _packages  # pylint:disable=[W0212]
    return worker


def _class_view(obj):
    if not _check_attributes(obj, ["id", "modelElement", "shape"]):
        return obj

    worker = ClassView(
        identifier=obj["id"],
        model_element=_model_element(obj["modelElement"]),
        shape=_rectangle(obj["shape"]),
    )
    _class_views[obj["id"]] = worker
    return worker


def _diagram(obj):
    if not _check_attributes(obj, ["id", "name", "description", "owner", "contents"]):
        return obj

    description: str = obj["description"]
    worker = OntoUmlDiagram(
        identifier=obj["id"],
        name=obj["name"],
        description=description,
        owner=_diagram_owner(obj["owner"]),
        contents=obj["contents"],
    )
    if None is not description:
        has_publish: bool = "##publish##" in description.lower()
        if has_publish:
            # ##publish## uit de description wissen. Ook eventuele enter die hierna
            # komt.
            worker.description = description.replace("##publish##\n", "").replace(
                "##publish##", ""
            )
        worker.publish = has_publish
    _diagrams[obj["id"]] = worker
    return worker


def _set_dict(target, obj_type):
    # pylint:disable=[W0212]
    match obj_type:
        case "Class":
            target._dict = _classes
        case "ClassView":
            target._dict = _class_views
        case "Diagram":
            target._dict = _diagrams
        case "Generalization":
            target._dict = generalizations
        case "GeneralizationSet":
            target._dict = _generalization_sets
        case "GeneralizationSetView":
            target._dict = _generalization_set_views
        case "GeneralizationView":
            target._dict = _generalization_views
        case "Package":
            target._dict = _packages
        case "Relation":
            target._dict = _relations
        case "RelationView":
            target._dict = _relation_views


def _diagram_owner(obj):
    if obj is None:
        return None
    if not _check_attributes(obj, ["id", "type"]):
        return None

    worker = OntoUmlDiagramOwner(identifier=obj["id"], object_type=obj["type"])
    _set_dict(worker, obj["type"])
    return worker


def _generalization(obj):
    if not _check_attributes(
        obj, ["id", "name", "description", "propertyAssignments", "general", "specific"]
    ):
        return obj

    worker = OntoUmlGeneralization(
        identifier=obj["id"],
        name=obj["name"],
        description=obj["description"],
        property_assignments=_property_assignments(obj["propertyAssignments"]),
        general=_generalization_party(obj["general"]),
        specific=_generalization_party(obj["specific"]),
    )
    generalizations[obj["id"]] = worker
    return worker


def _generalization_party(obj):
    if None is obj:
        return None
    if not _check_attributes(obj, ["id", "type"]):
        return None

    worker = OntoUmlGeneralizationParty(identifier=obj["id"], object_type=obj["type"])
    _set_dict(worker, obj["type"])
    return worker


def _generalization_set(obj):
    if not _check_attributes(
        obj,
        [
            "id",
            "name",
            "description",
            "propertyAssignments",
            "isDisjoint",
            "isComplete",
            "categorizer",
            "generalizations",
        ],
    ):
        return obj

    worker = OntoUmlGeneralizationSet(
        identifier=obj["id"],
        name=obj["name"],
        description=obj["description"],
        property_assignments=_property_assignments(obj["propertyAssignments"]),
        is_disjoint=obj["isDisjoint"],
        is_complete=obj["isComplete"],
        categorizer=obj["categorizer"],
        generalizations=[_generalization_set_item(g) for g in obj["generalizations"]],
    )
    _generalization_sets[obj["id"]] = worker
    worker._package_dict = _packages  # pylint:disable=[W0212]
    return worker


def _generalization_set_item(obj):
    if None is obj:
        return None
    if not _check_attributes(obj, ["id", "type"]):
        return None

    worker = OntoUmlGeneralizationSetItem(identifier=obj["id"], item_type=obj["type"])
    _set_dict(worker, obj["type"])
    return worker


def _generalization_set_view(obj):
    if not _check_attributes(obj, ["id", "modelElement", "shape"]):
        return obj

    worker = GeneralizationSetView(
        identifier=obj["id"],
        model_element=_model_element(obj["modelElement"]),
        shape=_text(obj["shape"]),
    )
    _generalization_set_views[obj["id"]] = worker
    return worker


def _generalization_view(obj):
    if not _check_attributes(obj, ["id", "modelElement", "shape", "source", "target"]):
        return obj

    worker = GeneralizationView(
        identifier=obj["id"],
        model_element=_model_element(obj["modelElement"]),
        shape=_path(obj["shape"]),
        source=_model_element(obj["source"]),
        target=_model_element(obj["target"]),
    )
    _generalization_views[obj["id"]] = worker
    return worker


def _model_element(obj):
    if None is obj:
        return None
    if not _check_attributes(obj, ["id", "type"]):
        return None

    worker = OntoUmlModelElement(
        model_element_id=obj["id"], model_element_type=obj["type"]
    )
    _set_dict(worker, obj["type"])
    return worker


def _package(obj):
    if not _check_attributes(
        obj, ["id", "name", "description", "propertyAssignments", "contents"]
    ):
        return obj

    worker = OntoUmlPackage(
        identifier=obj["id"],
        name=obj["name"],
        description=obj["description"],
        property_assignments=_property_assignments(obj["propertyAssignments"]),
        contents=obj["contents"],
    )
    _packages[obj["id"]] = worker
    return worker


def _path(obj):
    if None is obj:
        return None
    if not _check_attributes(obj, ["id", "type", "points"]):
        return None

    return Path(
        identifier=obj["id"],
        shape_type=obj["type"],
        points=[_point(p) for p in obj["points"]],
    )


def _point(obj):
    if None is obj:
        return None
    if not _check_attributes(obj, ["x", "y"]):
        return None

    return Point(x_coordinate=obj["x"], y_coordinate=obj["y"])


def _project(obj):
    if not _check_attributes(obj, ["id", "name", "description", "model", "diagrams"]):
        return obj

    model = _package(obj["model"])
    result = OntoUmlProject(
        identifier=obj["id"],
        name=obj["name"],
        description=obj["description"],
        model=model,
        diagrams=[_diagram(d) for d in obj["diagrams"]],
    )
    model._owner = result  # pylint:disable=[W0212]

    return result


def _property(obj):
    if not _check_attributes(
        obj,
        [
            "id",
            "name",
            "description",
            "propertyAssignments",
            "stereotype",
            "isDerived",
            "isReadOnly",
            "isOrdered",
            "cardinality",
            "propertyType",
            "subsettedProperties",
            "redefinedProperties",
            "aggregationKind",
        ],
    ):
        return obj

    return OntoUmlProperty(
        identifier=obj["id"],
        name=obj["name"],
        description=obj["description"],
        property_assignments=_property_assignments(obj["propertyAssignments"]),
        stereotype=obj["stereotype"],
        is_derived=obj["isDerived"],
        is_read_only=obj["isReadOnly"],
        is_ordered=obj["isOrdered"],
        cardinality=obj["cardinality"],
        property_type=_property_type(obj["propertyType"]),
        subsetted_properties=obj["subsettedProperties"],
        redefined_properties=obj["redefinedProperties"],
        aggregation_kind=obj["aggregationKind"],
    )


def _property_type(obj):
    if None is obj:
        return None

    worker = OntoUmlPropertyType(identifier=obj["id"], related_type=obj["type"])
    _set_dict(worker, obj["type"])
    return worker


def _property_assignments(obj):
    if None is obj:
        return None
    subkey = "ThesaurusIMAR_ID"
    if subkey not in obj:
        return None

    return OntoUmlPropertyAssignments(thesaurus_imar_id=obj[subkey])


def _rectangle(obj):
    if None is obj:
        return None
    if not _check_attributes(obj, ["id", "type", "x", "y", "width", "height"]):
        return None

    return Rectangle(
        identifier=obj["id"],
        shape_type=obj["type"],
        x_coordinate=obj["x"],
        y_coordinate=obj["y"],
        width=obj["width"],
        height=obj["height"],
    )


def _relation(obj):
    if not _check_attributes(
        obj,
        [
            "id",
            "name",
            "description",
            "propertyAssignments",
            "stereotype",
            "isAbstract",
            "isDerived",
            "properties",
        ],
    ):
        return obj

    worker = OntoUmlRelation(
        identifier=obj["id"],
        name=obj["name"],
        description=obj["description"],
        property_assignments=_property_assignments(obj["propertyAssignments"]),
        stereotype=obj["stereotype"],
        is_abstract=obj["isAbstract"],
        is_derived=obj["isDerived"],
        properties=obj["properties"],
    )
    _relations[obj["id"]] = worker
    return worker


def _relation_view(obj):
    if not _check_attributes(obj, ["id", "modelElement", "shape", "source", "target"]):
        return obj

    worker = RelationView(
        identifier=obj["id"],
        model_element=_model_element(obj["modelElement"]),
        shape=_path(obj["shape"]),
        source=_model_element(obj["source"]),
        target=_model_element(obj["target"]),
    )
    _relation_views[obj["id"]] = worker
    return worker


def _text(obj):
    if None is obj:
        return None
    if not _check_attributes(obj, ["id", "type", "x", "y", "width", "height", "value"]):
        return None

    return Text(
        identifier=obj["id"],
        shape_type=obj["type"],
        x_coordinate=obj["x"],
        y_coordinate=obj["y"],
        width=obj["width"],
        height=obj["height"],
        value=obj["value"],
    )


def onto_uml_typed_object(obj):
    """Object hook for Onto UML json decoding"""
    if "type" not in obj:
        return obj

    result = obj
    match obj["type"]:
        case "Class":
            result = _class(obj)
        case "ClassView":
            result = _class_view(obj)
        case "Diagram":
            result = _diagram(obj)
        case "Generalization":
            result = _generalization(obj)
        case "GeneralizationSet":
            result = _generalization_set(obj)
        case "GeneralizationSetView":
            result = _generalization_set_view(obj)
        case "GeneralizationView":
            result = _generalization_view(obj)
        case "Package":
            result = _package(obj)
        case "Path":
            result = _path(obj)
        case "Project":
            result = _project(obj)
        case "Property":
            result = _property(obj)
        case "Rectangle":
            result = _rectangle(obj)
        case "Relation":
            result = _relation(obj)
        case "RelationView":
            result = _relation_view(obj)
        case "Text":
            result = _text(obj)

    return result
