"""Base class for containers which can hold content, such as Packages"""


from dataclasses import dataclass, field

from src.onto_uml.onto_uml_objects import (
    OntoUmlObject,
    _OntoUmlObjectBase,
    _OntoUmlObjectDefaultsBase,
)


@dataclass
class _OntoUmlContentContainerBase(_OntoUmlObjectBase):
    pass


@dataclass
class _OntoUmlContentContainerDefaultsBase(_OntoUmlObjectDefaultsBase):
    contents: list = field(default_factory=list)
    """The contents of the container"""


@dataclass
class OntoUmlContentContainer(
    _OntoUmlContentContainerDefaultsBase, OntoUmlObject, _OntoUmlContentContainerBase
):
    """Interface for OntoUML objects which are basically containers holding other
    content."""
