"""The interface for an OntoUML generalization set as exported by Visual Paradigm"""

from dataclasses import InitVar, dataclass, field
from typing import Any, List

from src.onto_uml.onto_uml_objects import (
    OntoUmlObject,
    _OntoUmlObjectBase,
    _OntoUmlObjectDefaultsBase,
)


@dataclass
class OntoUmlGeneralizationSetItemDefaultsBase:
    """
    Interface with default fields for the base dataclass of
    `OntoUmlGeneralizationSetItem`
    """

    generalization: InitVar(Any) = None
    _generalization: Any = None
    """The generalization object referenced by this generalization set item"""
    _dict: dict = field(default_factory=dict, compare=False)

    def __post_init__(self, generalization):
        if None is not generalization:
            self._generalization = generalization


@dataclass
class OntoUmlGeneralizationSetItemBase:
    """Interface for the base dataclass of `OntoUmlGeneralizationSetItem`"""

    identifier: str
    """The identifier of the generalization which is part of the set"""
    item_type: str
    """The type of the generalization (...) which is part of the set"""


@dataclass
class OntoUmlGeneralizationSetItem(
    OntoUmlGeneralizationSetItemDefaultsBase, OntoUmlGeneralizationSetItemBase
):
    """Interface for OntoUML generalization set item (generalization) as exported by
    Visual Paradigm."""

    @property
    def generalization(self):
        """The generalization object referenced by this generalization set item"""
        if None is self._generalization and 0 < len(self._dict):
            if self.identifier in self._dict:
                self._generalization = self._dict[self.identifier]
        return self._generalization


@dataclass
class _OntoUmlGeneralizationSetBase(_OntoUmlObjectBase):
    generalizations: List[OntoUmlGeneralizationSetItem]
    """The generalizations which make up the generalization set"""


@dataclass
class _OntoUmlGeneralizationSetDefaultsBase(_OntoUmlObjectDefaultsBase):
    is_disjoint: bool = False
    """Indication wether the generalization set is disjoint"""
    is_complete: bool = False
    """Indication wether the generalization set is complete"""
    categorizer: str = None
    """The categorizer of the generalization set"""


# pylint: disable=[R0902]
@dataclass
class OntoUmlGeneralizationSet(
    _OntoUmlGeneralizationSetDefaultsBase, OntoUmlObject, _OntoUmlGeneralizationSetBase
):
    """Interface for OntoUML generalization set as exported by Visual Paradigm.
    A generalization set involves multiple generalizatoins."""
