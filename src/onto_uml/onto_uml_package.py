"""The interface for an OntoUML package as exported by Visual Paradigm"""

from dataclasses import dataclass

from src.onto_uml.onto_uml_content_container import OntoUmlContentContainer


@dataclass
class OntoUmlPackage(OntoUmlContentContainer):
    """Interface for OntoUML package as exported by Visual Paradigm"""
