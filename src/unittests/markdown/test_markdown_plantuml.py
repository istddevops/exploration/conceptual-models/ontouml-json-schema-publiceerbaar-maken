"""
Test suites for the `src.markdown.markdown.plantuml` module
"""


from copy import deepcopy
from unittest import TestCase
from unittest.mock import PropertyMock, patch

from src.markdown.markdown_plantuml import MarkdownPlantUml
from src.plant_uml.plant_uml_diagram import PlantUMLDiagram


class TestMarkdownPlantUml(TestCase):
    """
    Test suite for the `MarkdownPlantUml` class
    """

    def test_generate_plant_uml_markdown(self):
        # pylint:disable=[C0301]
        """
        Test suite for the `_generate_plant_uml_markdown` method
        Invariants:
        | `shortcode_open` | `shortcode_close` | `self.plant_uml_diagram` | `self.plant_uml_diagram.plant_uml` | result                                                                                  |
        | set              | set               | set                      | yields results                     |                                                                                         |
        | ================ | ================= | ======================== | ================================== | ========================================================= |
        | yes              | yes               | yes                      | yes                                | `shortcode_open`                                          |
        |                  |                   |                          |                                    | `self.plant_uml_diagram.plant_uml` separated by `\n`      |
        |                  |                   |                          |                                    | `shortcode_close`                                         |
        |                  |                   |                          |                                    |                                                           |
        | no               | yes               | yes                      | yes                                | `None`                                                    |
        | yes              | no                | yes                      | yes                                | `None`                                                    |
        | no               | no                | yes                      | yes                                | `None`                                                    |
        | yes              | yes               | no                       | n/a                                | `shortcode_open`                                          |
        |                  |                   |                          |                                    | ' No data available for diagram `self.diagram_identifier` |
        |                  |                   |                          |                                    | `shortcode_close`                                         |
        |                  |                   |                          |                                    |                                                           |
        | no               | yes               | no                       | n/a                                | `None`                                                    |
        | yes              | no                | no                       | n/a                                | `None`                                                    |
        | no               | no                | no                       | n/a                                | `None`                                                    |
        | yes              | yes               | yes                      | no                                 | `shortcode_open`                                          |
        |                  |                   |                          |                                    | ' No data available for diagram `self.diagram_identifier` |
        |                  |                   |                          |                                    | `shortcode_close`                                         |
        |                  |                   |                          |                                    |                                                           |
        | no               | yes               | yes                      | no                                 | `None`                                                    |
        | yes              | no                | yes                      | no                                 | `None`                                                    |
        | no               | no                | yes                      | no                                 | `None`                                                    |
        """  # noqa=E501

        template = MarkdownPlantUml(
            plant_uml_diagram=PlantUMLDiagram(
                identifier="FakeDiagramIdentifier",
                name="FakeDiagramName",
                description="FakeDiagramDescription",
                classes=[],
                generalizations=[],
                relations=[],
                association_relations=[],
            ),
            diagram_name="FakeDiagramName",
            diagram_identifier="FakeDiagramIdentifier",
        )
        with self.subTest(
            """
            | `shortcode_open` | `shortcode_close` | `self.plant_uml_diagram` | `self.plant_uml_diagram.plant_uml` | result                                                                                  |
            | set              | set               | set                      | yields results                     |                                                                                         |
            | ================ | ================= | ======================== | ================================== | ========================================================= |
            | yes              | yes               | yes                      | yes                                | `shortcode_open`                                          |
            |                  |                   |                          |                                    | `self.plant_uml_diagram.plant_uml` separated by `\n`      |
            |                  |                   |                          |                                    | `shortcode_close`                                         |
            |                  |                   |                          |                                    |                                                           |
            """  # noqa=E501
        ):
            with patch.object(
                PlantUMLDiagram,
                "plant_uml",
                new_callable=PropertyMock(return_value=["1", "2", "3"]),
            ):
                testee = deepcopy(template)
                expected = [
                    "FakeShortCodeOpen\n",
                    "1\n",
                    "2\n",
                    "3\n",
                    "FakeShortCodeClose\n",
                ]
                # pylint:disable=[W0212]
                self.assertEqual(
                    testee._generate_plant_uml_markdown(
                        "FakeShortCodeOpen", "FakeShortCodeClose"
                    ),
                    expected,
                )

        with self.subTest(
            """
            | `shortcode_open` | `shortcode_close` | `self.plant_uml_diagram` | `self.plant_uml_diagram.plant_uml` | result                                                                                  |
            | set              | set               | set                      | yields results                     |                                                                                         |
            | ================ | ================= | ======================== | ================================== | ========================================================= |
            | no               | yes               | yes                      | yes                                | `None`                                                    |
            """  # noqa=E501
        ):
            with patch.object(
                PlantUMLDiagram,
                "plant_uml",
                new_callable=PropertyMock(return_value=["1", "2", "3"]),
            ):
                testee = deepcopy(template)
                # pylint:disable=[W0212]
                self.assertIsNone(
                    testee._generate_plant_uml_markdown(None, "FakeShortCodeClose")
                )

        with self.subTest(
            """
            | `shortcode_open` | `shortcode_close` | `self.plant_uml_diagram` | `self.plant_uml_diagram.plant_uml` | result                                                                                  |
            | set              | set               | set                      | yields results                     |                                                                                         |
            | ================ | ================= | ======================== | ================================== | ========================================================= |
            | yes              | no                | yes                      | yes                                | `None`                                                    |
            """  # noqa=E501
        ):
            with patch.object(
                PlantUMLDiagram,
                "plant_uml",
                new_callable=PropertyMock(return_value=["1", "2", "3"]),
            ):
                testee = deepcopy(template)
                # pylint:disable=[W0212]
                self.assertIsNone(
                    testee._generate_plant_uml_markdown("FakeShortCodeOpen", None)
                )

        with self.subTest(
            """
            | `shortcode_open` | `shortcode_close` | `self.plant_uml_diagram` | `self.plant_uml_diagram.plant_uml` | result                                                                                  |
            | set              | set               | set                      | yields results                     |                                                                                         |
            | ================ | ================= | ======================== | ================================== | ========================================================= |
            | no               | no                | yes                      | yes                                | `None`                                                    |
            """  # noqa=E501
        ):
            with patch.object(
                PlantUMLDiagram,
                "plant_uml",
                new_callable=PropertyMock(return_value=["1", "2", "3"]),
            ):
                testee = deepcopy(template)
                # pylint:disable=[W0212]
                self.assertIsNone(testee._generate_plant_uml_markdown(None, None))

        with self.subTest(
            """
            | `shortcode_open` | `shortcode_close` | `self.plant_uml_diagram` | `self.plant_uml_diagram.plant_uml` | result                                                                                  |
            | set              | set               | set                      | yields results                     |                                                                                         |
            | ================ | ================= | ======================== | ================================== | ========================================================= |
            | yes              | yes               | no                       | n/a                                | `shortcode_open`                                          |
            |                  |                   |                          |                                    | ' No data available for diagram `self.diagram_identifier` |
            |                  |                   |                          |                                    | `shortcode_close`                                         |
            |                  |                   |                          |                                    |                                                           |
            """  # noqa=E501
        ):
            testee = MarkdownPlantUml(
                plant_uml_diagram=None,
                diagram_name="FakeDiagramName",
                diagram_identifier="FakeDiagramIdentifier",
            )
            expected = [
                "FakeShortCodeOpen\n",
                "' No data available for diagram FakeDiagramIdentifier",
                "FakeShortCodeClose\n",
            ]
            # pylint:disable=[W0212]
            self.assertEqual(
                testee._generate_plant_uml_markdown(
                    "FakeShortCodeOpen", "FakeShortCodeClose"
                ),
                expected,
            )

        with self.subTest(
            """
            | `shortcode_open` | `shortcode_close` | `self.plant_uml_diagram` | `self.plant_uml_diagram.plant_uml` | result                                                                                  |
            | set              | set               | set                      | yields results                     |                                                                                         |
            | ================ | ================= | ======================== | ================================== | ========================================================= |
            | no               | yes               | no                       | n/a                                | `None`                                                    |
            """  # noqa=E501
        ):
            testee = MarkdownPlantUml(
                plant_uml_diagram=None,
                diagram_name="FakeDiagramName",
                diagram_identifier="FakeDiagramIdentifier",
            )
            # pylint:disable=[W0212]
            self.assertIsNone(
                testee._generate_plant_uml_markdown(None, "FakeShortCodeClose")
            )

        with self.subTest(
            """
            | `shortcode_open` | `shortcode_close` | `self.plant_uml_diagram` | `self.plant_uml_diagram.plant_uml` | result                                                                                  |
            | set              | set               | set                      | yields results                     |                                                                                         |
            | ================ | ================= | ======================== | ================================== | ========================================================= |
            | yes              | no                | no                       | n/a                                | `None`                                                    |
            """  # noqa=E501
        ):
            testee = MarkdownPlantUml(
                plant_uml_diagram=None,
                diagram_name="FakeDiagramName",
                diagram_identifier="FakeDiagramIdentifier",
            )
            # pylint:disable=[W0212]
            self.assertIsNone(
                testee._generate_plant_uml_markdown("FakeShortCodeOpen", None)
            )

        with self.subTest(
            """
            | `shortcode_open` | `shortcode_close` | `self.plant_uml_diagram` | `self.plant_uml_diagram.plant_uml` | result                                                                                  |
            | set              | set               | set                      | yields results                     |                                                                                         |
            | ================ | ================= | ======================== | ================================== | ========================================================= |
            | no               | no                | no                       | n/a                                | `None`                                                    |
            """  # noqa=E501
        ):
            testee = MarkdownPlantUml(
                plant_uml_diagram=None,
                diagram_name="FakeDiagramName",
                diagram_identifier="FakeDiagramIdentifier",
            )
            # pylint:disable=[W0212]
            self.assertIsNone(testee._generate_plant_uml_markdown(None, None))

        with self.subTest(
            """
            | `shortcode_open` | `shortcode_close` | `self.plant_uml_diagram` | `self.plant_uml_diagram.plant_uml` | result                                                                                  |
            | set              | set               | set                      | yields results                     |                                                                                         |
            | ================ | ================= | ======================== | ================================== | ========================================================= |
            | yes              | yes               | yes                      | no                                 | `shortcode_open`                                          |
            |                  |                   |                          |                                    | ' No data available for diagram `self.diagram_identifier` |
            |                  |                   |                          |                                    | `shortcode_close`                                         |
            |                  |                   |                          |                                    |                                                           |
            """  # noqa=E501
        ):
            with patch.object(
                PlantUMLDiagram, "plant_uml", new_callable=PropertyMock(return_value=[])
            ):
                testee = deepcopy(template)
                expected = [
                    "FakeShortCodeOpen\n",
                    "' No data available for diagram FakeDiagramIdentifier",
                    "FakeShortCodeClose\n",
                ]
                # pylint:disable=[W0212]
                self.assertEqual(
                    testee._generate_plant_uml_markdown(
                        "FakeShortCodeOpen", "FakeShortCodeClose"
                    ),
                    expected,
                )

        with self.subTest(
            """
            | `shortcode_open` | `shortcode_close` | `self.plant_uml_diagram` | `self.plant_uml_diagram.plant_uml` | result                                                                                  |
            | set              | set               | set                      | yields results                     |                                                                                         |
            | ================ | ================= | ======================== | ================================== | ========================================================= |
            | no               | yes               | yes                      | no                                 | `None`                                                    |
            """  # noqa=E501
        ):
            with patch.object(
                PlantUMLDiagram, "plant_uml", new_callable=PropertyMock(return_value=[])
            ):
                testee = deepcopy(template)
                # pylint:disable=[W0212]
                self.assertIsNone(
                    testee._generate_plant_uml_markdown(None, "FakeShortCodeClose")
                )

        with self.subTest(
            """
            | `shortcode_open` | `shortcode_close` | `self.plant_uml_diagram` | `self.plant_uml_diagram.plant_uml` | result                                                                                  |
            | set              | set               | set                      | yields results                     |                                                                                         |
            | ================ | ================= | ======================== | ================================== | ========================================================= |
            | yes              | no                | yes                      | no                                 | `None`                                                    |
            """  # noqa=E501
        ):
            with patch.object(
                PlantUMLDiagram, "plant_uml", new_callable=PropertyMock(return_value=[])
            ):
                testee = deepcopy(template)
                # pylint:disable=[W0212]
                self.assertIsNone(
                    testee._generate_plant_uml_markdown("FakeShortCodeOpen", None)
                )

        with self.subTest(
            """
            | `shortcode_open` | `shortcode_close` | `self.plant_uml_diagram` | `self.plant_uml_diagram.plant_uml` | result                                                                                  |
            | set              | set               | set                      | yields results                     |                                                                                         |
            | ================ | ================= | ======================== | ================================== | ========================================================= |
            | no               | no                | yes                      | no                                 | `None`                                                    |
            """  # noqa=E501
        ):
            with patch.object(
                PlantUMLDiagram, "plant_uml", new_callable=PropertyMock(return_value=[])
            ):
                testee = deepcopy(template)
                # pylint:disable=[W0212]
                self.assertIsNone(testee._generate_plant_uml_markdown(None, None))

    def test_frontmatter(self):
        """
        Check that the property `self.diagram_name` is properly included in the result
        """
        expected = [
            "---\n",
            "title: FakeName\n",
            "type: docs\n",
            "bookToC: false\n",
            "---\n",
        ]
        self.assertEqual(
            MarkdownPlantUml(
                PlantUMLDiagram(
                    identifier="FakeDiagramIdentifier",
                    name="FakeDiagramName",
                    description="FakeDiagramDescription",
                    classes=[],
                    generalizations=[],
                    relations=[],
                    association_relations=[],
                ),
                diagram_name="FakeName",
                diagram_identifier="FakeDiagramIdentifier",
            ).frontmatter,
            expected,
        )

    def test_hugo_snippet(self):
        # pylint:disable=[C0301]
        """
        Unittest for `hugo_snippet` property.
        This property uses the _generate_plant_uml_markdown method with the following
        arguments:
        - `shortcode_open` =
          "{{< plantuml id='`self.diagram_identifier`' name='`self.diagram_name`' >}}"
        - `shortcode_close` =
          "{{< /plantuml >}}"
        We only test that the correct shortcodes are placed around the actual plantUML
        code; that's tested by `test_generate_plant_uml_markdown`. We DO however check that
        `_generate_plant_uml_markdown` gets called
        Invariants:
        | diagram_identifier | diagram_name | resulting `shortcode_open`                     | `shortcode_close`   |
        | FakeId             | FakeName     | {{< plantuml id="FakeId" name="FakeName" >}}\n | {{< /plantuml >}}\n |
        |                    | FakeName     | {{< plantuml id="" name="FakeName" >}}\n       | {{< /plantuml >}}\n |
        | `None`             | FakeName     | {{< plantuml id="" name="FakeName" >}}\n       | {{< /plantuml >}}\n |
        | FakeId             |              | {{< plantuml id="FakeId" name="" >}}\n         | {{< /plantuml >}}\n |
        |                    |              | {{< plantuml id="" name="" >}}\n               | {{< /plantuml >}}\n |
        | `None`             |              | {{< plantuml id="" name="" >}}\n               | {{< /plantuml >}}\n |
        | FakeId             | `None`       | {{< plantuml id="FakeId" name="" >}}\n         | {{< /plantuml >}}\n |
        |                    | `None`       | {{< plantuml id="" name="" >}}\n               | {{< /plantuml >}}\n |
        | `None`             | `None`       | {{< plantuml id="" name="" >}}\n               | {{< /plantuml >}}\n |
        """  # noqa=E501

        template = MarkdownPlantUml(
            plant_uml_diagram=PlantUMLDiagram(
                identifier="FakePlantUMLDiagramIdentifier",
                name="FakePlantUMLDiagramName",
                description="FakePlantUMLDiagramDescription",
                classes=[],
                generalizations=[],
                relations=[],
                association_relations=[],
            ),
            diagram_name="FakeName",
            diagram_identifier="FakeId",
        )
        expected_close = "{{< /plantuml >}}\n"

        def change_value_and_assert(
            arguments_and_values, expected_open, expected_close, template
        ):
            testee = deepcopy(template)
            if None is not arguments_and_values:
                for arg_and_value in arguments_and_values:
                    setattr(testee, arg_and_value[0], arg_and_value[1])
            result = testee.hugo_snippet
            self.assertEqual(result[0], expected_open)
            self.assertEqual(result[-1], expected_close)

        with self.subTest(
            # pylint:disable=[C0301]
            """
            | diagram_identifier | diagram_name | resulting `shortcode_open`                     | `shortcode_close`   |
            | FakeId             | FakeName     | {{< plantuml id="FakeId" name="FakeName" >}}\n | {{< /plantuml >}}\n |
            """  # noqa=E501
        ):
            change_value_and_assert(
                None,
                """{{< plantuml id="FakeId" name="FakeName" >}}\n""",
                expected_close,
                template,
            )

        with self.subTest(
            # pylint:disable=[C0301]
            """
            | diagram_identifier | diagram_name | resulting `shortcode_open`                     | `shortcode_close`   |
            |                    | FakeName     | {{< plantuml id="" name="FakeName" >}}\n       | {{< /plantuml >}}\n |
            """  # noqa=E501
        ):
            change_value_and_assert(
                [("diagram_identifier", "")],
                """{{< plantuml id="" name="FakeName" >}}\n""",
                expected_close,
                template,
            )

        with self.subTest(
            # pylint:disable=[C0301]
            """
            | diagram_identifier | diagram_name | resulting `shortcode_open`                     | `shortcode_close`   |
            | `None`             | FakeName     | {{< plantuml id="" name="FakeName" >}}\n       | {{< /plantuml >}}\n |
            """  # noqa=E501
        ):
            change_value_and_assert(
                [("diagram_identifier", None)],
                """{{< plantuml id="" name="FakeName" >}}\n""",
                expected_close,
                template,
            )

        with self.subTest(
            # pylint:disable=[C0301]
            """
            | diagram_identifier | diagram_name | resulting `shortcode_open`                     | `shortcode_close`   |
            | FakeId             |              | {{< plantuml id="FakeId" name="" >}}\n         | {{< /plantuml >}}\n |
            """  # noqa=E501
        ):
            change_value_and_assert(
                [("diagram_name", "")],
                """{{< plantuml id="FakeId" name="" >}}\n""",
                expected_close,
                template,
            )

        with self.subTest(
            # pylint:disable=[C0301]
            """
            | diagram_identifier | diagram_name | resulting `shortcode_open`                     | `shortcode_close`   |
            |                    |              | {{< plantuml id="" name="" >}}\n               | {{< /plantuml >}}\n |
            """  # noqa=E501
        ):
            change_value_and_assert(
                [("diagram_identifier", ""), ("diagram_name", "")],
                """{{< plantuml id="" name="" >}}\n""",
                expected_close,
                template,
            )

        with self.subTest(
            # pylint:disable=[C0301]
            """
            | diagram_identifier | diagram_name | resulting `shortcode_open`                     | `shortcode_close`   |
            | `None`             |              | {{< plantuml id="" name="" >}}\n               | {{< /plantuml >}}\n |
            """  # noqa=E501
        ):
            change_value_and_assert(
                [("diagram_identifier", None), ("diagram_name", "")],
                """{{< plantuml id="" name="" >}}\n""",
                expected_close,
                template,
            )

        with self.subTest(
            # pylint:disable=[C0301]
            """
            | diagram_identifier | diagram_name | resulting `shortcode_open`                     | `shortcode_close`   |
            | FakeId             | `None`       | {{< plantuml id="FakeId" name="" >}}\n         | {{< /plantuml >}}\n |
            """  # noqa=E501
        ):
            change_value_and_assert(
                [("diagram_name", None)],
                """{{< plantuml id="FakeId" name="" >}}\n""",
                expected_close,
                template,
            )

        with self.subTest(
            # pylint:disable=[C0301]
            """
            | diagram_identifier | diagram_name | resulting `shortcode_open`                     | `shortcode_close`   |
            |                    | `None`       | {{< plantuml id="" name="" >}}\n               | {{< /plantuml >}}\n |
            """  # noqa=E501
        ):
            change_value_and_assert(
                [("diagram_identifier", ""), ("diagram_name", None)],
                """{{< plantuml id="" name="" >}}\n""",
                expected_close,
                template,
            )

        with self.subTest(
            # pylint:disable=[C0301]
            """
            | diagram_identifier | diagram_name | resulting `shortcode_open`                     | `shortcode_close`   |
            | `None`             | `None`       | {{< plantuml id="" name="" >}}\n               | {{< /plantuml >}}\n |
            """  # noqa=E501
        ):
            change_value_and_assert(
                [("diagram_identifier", None), ("diagram_name", None)],
                """{{< plantuml id="" name="" >}}\n""",
                expected_close,
                template,
            )

    def test_hugo_page(self):
        """
        The `hugo_page` property relies on both `frontmatter` and `hugo_snippet`
        properties.
        The result should be first the `frontmatter` output, followed by `hugo_snippet`
        output; returned in a single array.
        """

        with patch.multiple(
            MarkdownPlantUml,
            frontmatter=PropertyMock(return_value=["frontmatter 1", "frontmatter 2"]),
            hugo_snippet=PropertyMock(
                return_value=["hugo_snippet 1", "hugo_snippet 2"]
            ),
        ):
            testee = MarkdownPlantUml(
                plant_uml_diagram=PlantUMLDiagram(
                    identifier="FakePlantUMLDiagramIdentifier",
                    name="FakePlantUMLDiagramName",
                    description="FakePlantUMLDiagramDescription",
                    classes=[],
                    generalizations=[],
                    relations=[],
                    association_relations=[],
                ),
                diagram_name="FakeName",
                diagram_identifier="FakeId",
            )
            expected = [
                "frontmatter 1",
                "frontmatter 2",
                "hugo_snippet 1",
                "hugo_snippet 2",
            ]
            self.assertEqual(testee.hugo_page, expected)

    def test_gitlab_snippet(self):
        # pylint:disable=[C0301]
        """
        Unittest for `gitlab_snippet` property.
        This property uses the _generate_plant_uml_markdown method with the following
        arguments:
        - `shortcode_open` = ```plantuml
        - `shortcode_close` = ```
        We only test that the correct shortcodes are placed around the actual plantUML
        code; that's tested by `test_generate_plant_uml_markdown`.
        Invariants:
        | diagram_identifier | diagram_name | resulting `shortcode_open` | `shortcode_close` |
        | FakeId             | FakeName     | ```plantuml                | ```               |
        |                    | FakeName     | ```plantuml                | ```               |
        | `None`             | FakeName     | ```plantuml                | ```               |
        | FakeId             |              | ```plantuml                | ```               |
        |                    |              | ```plantuml                | ```               |
        | `None`             |              | ```plantuml                | ```               |
        | FakeId             | `None`       | ```plantuml                | ```               |
        |                    | `None`       | ```plantuml                | ```               |
        | `None`             | `None`       | ```plantuml                | ```               |
        """  # noqa=E501

        template = MarkdownPlantUml(
            plant_uml_diagram=PlantUMLDiagram(
                identifier="FakePlantUMLDiagramIdentifier",
                name="FakePlantUMLDiagramName",
                description="FakePlantUMLDiagramDescription",
                classes=[],
                generalizations=[],
                relations=[],
                association_relations=[],
            ),
            diagram_name="FakeName",
            diagram_identifier="FakeId",
        )
        expected_open = "```plantuml\n"
        expected_close = "```\n"

        def change_value_and_assert(
            arguments_and_values, expected_open, expected_close, template
        ):
            testee = deepcopy(template)
            if None is not arguments_and_values:
                for arg_and_value in arguments_and_values:
                    setattr(testee, arg_and_value[0], arg_and_value[1])
            result = testee.gitlab_snippet
            self.assertEqual(result[0], expected_open)
            self.assertEqual(result[-1], expected_close)

        with self.subTest(
            # pylint:disable=[C0301]
            """
            | diagram_identifier | diagram_name | resulting `shortcode_open` | `shortcode_close` |
            | FakeId             | FakeName     | ```plantuml                | ```               |
            """  # noqa=E501
        ):
            change_value_and_assert(None, expected_open, expected_close, template)

        with self.subTest(
            # pylint:disable=[C0301]
            """
            | diagram_identifier | diagram_name | resulting `shortcode_open` | `shortcode_close` |
            |                    | FakeName     | ```plantuml                | ```               |
            """  # noqa=E501
        ):
            change_value_and_assert(
                [("diagram_identifier", "")], expected_open, expected_close, template
            )

        with self.subTest(
            # pylint:disable=[C0301]
            """
            | diagram_identifier | diagram_name | resulting `shortcode_open` | `shortcode_close` |
            | `None`             | FakeName     | ```plantuml                | ```               |
            """  # noqa=E501
        ):
            change_value_and_assert(
                [("diagram_identifier", None)], expected_open, expected_close, template
            )

        with self.subTest(
            # pylint:disable=[C0301]
            """
            | diagram_identifier | diagram_name | resulting `shortcode_open` | `shortcode_close` |
            | FakeId             |              | ```plantuml                | ```               |
            """  # noqa=E501
        ):
            change_value_and_assert(
                [("diagram_name", "")], expected_open, expected_close, template
            )

        with self.subTest(
            # pylint:disable=[C0301]
            """
            | diagram_identifier | diagram_name | resulting `shortcode_open` | `shortcode_close` |
            |                    |              | ```plantuml                | ```               |
            """  # noqa=E501
        ):
            change_value_and_assert(
                [("diagram_identifier", ""), ("diagram_name", "")],
                expected_open,
                expected_close,
                template,
            )

        with self.subTest(
            # pylint:disable=[C0301]
            """
            | diagram_identifier | diagram_name | resulting `shortcode_open` | `shortcode_close` |
            | `None`             |              | ```plantuml                | ```               |
            """  # noqa=E501
        ):
            change_value_and_assert(
                [("diagram_identifier", None), ("diagram_name", "")],
                expected_open,
                expected_close,
                template,
            )

        with self.subTest(
            # pylint:disable=[C0301]
            """
            | diagram_identifier | diagram_name | resulting `shortcode_open` | `shortcode_close` |
            | FakeId             | `None`       | ```plantuml                | ```               |
            """  # noqa=E501
        ):
            change_value_and_assert(
                [("diagram_name", None)], expected_open, expected_close, template
            )

        with self.subTest(
            # pylint:disable=[C0301]
            """
            | diagram_identifier | diagram_name | resulting `shortcode_open` | `shortcode_close` |
            |                    | `None`       | ```plantuml                | ```               |
            """  # noqa=E501
        ):
            change_value_and_assert(
                [("diagram_identifier", ""), ("diagram_name", None)],
                expected_open,
                expected_close,
                template,
            )

        with self.subTest(
            # pylint:disable=[C0301]
            """
            | diagram_identifier | diagram_name | resulting `shortcode_open` | `shortcode_close` |
            | `None`             | `None`       | ```plantuml                | ```               |
            """  # noqa=E501
        ):
            change_value_and_assert(
                [("diagram_identifier", None), ("diagram_name", None)],
                expected_open,
                expected_close,
                template,
            )

    def test_gitlab_page(self):
        """
        The `gitlab_page` property relies on both `frontmatter` and `gitlab_snippet`
        properties.
        The result should be first the `frontmatter` output, followed by
        `gitlab_snippet` output; returned in a single array.
        """

        with patch.multiple(
            MarkdownPlantUml,
            frontmatter=PropertyMock(return_value=["frontmatter 1", "frontmatter 2"]),
            gitlab_snippet=PropertyMock(
                return_value=["gitlab_snippet 1", "gitlab_snippet 2"]
            ),
        ):
            testee = MarkdownPlantUml(
                plant_uml_diagram=PlantUMLDiagram(
                    identifier="FakePlantUMLDiagramIdentifier",
                    name="FakePlantUMLDiagramName",
                    description="FakePlantUMLDiagramDescription",
                    classes=[],
                    generalizations=[],
                    relations=[],
                    association_relations=[],
                ),
                diagram_name="FakeName",
                diagram_identifier="FakeId",
            )
            expected = [
                "frontmatter 1",
                "frontmatter 2",
                "gitlab_snippet 1",
                "gitlab_snippet 2",
            ]
            self.assertEqual(testee.gitlab_page, expected)
