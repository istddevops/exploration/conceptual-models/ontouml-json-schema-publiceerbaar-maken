"""
Test suites for module `src.markdown.markdown_thesaurus_item`
"""


from unittest import TestCase

from src.markdown.markdown_thesaurus_item import MarkdownThesaurusItem


class TestMarkdownThesaurusItem(TestCase):
    """
    Testsuite for class `MarkdownThesaurusItem`
    We test the properties `reference` and `markdown`.
    `reference`: "[`identifier`]: [#`concept`]  "
                 where `concept` is set to lowercase, and any spaces (` `), opening and
                 closing brackets (`(` and `)` respectively) are replaced by
                 dashes (`-`). Followed by two whitespaces (`  `)
    `markdown`:
    "## [`concept`][`identifier`]  "
    ""
    "`meaning`  "
    ""
    if `meaning` is not set (or set to empty string), then `markdown`:
    "## [`concept`][`identifier`]  "
    ""
    """

    def test_init(self):
        """
        Unit test for the constructor.
        Cases:
        | `identifier` | `concept` | `meaning` |                          |
        | set          | set       | set       | expected result          |
        | ============ | ========= | ========= | ======================== |
        | yes          | yes       | yes       | no error                 |
        | no           | yes       | yes       | ValueError("identifier") |
        | yes          | no        | yes       | ValueError("concept")    |
        | no           | no        | yes       | ValueError("identifier") |
        | yes          | yes       | no        | no error                 |
        | no           | yes       | no        | ValueError("identifier") |
        | yes          | no        | no        | ValueError("concept")    |
        | no           | no        | no        | ValueError("identifier") |
        """

        with self.subTest(
            """
            Cases:
            | `identifier` | `concept` | `meaning` |                          |
            | set          | set       | set       | expected result          |
            | ============ | ========= | ========= | ======================== |
            | yes          | yes       | yes       | no error                 |
            """
        ):
            MarkdownThesaurusItem("FakeIdentifier", "FakeConcept", "FakeMeaning")

        with self.subTest(
            """
            Cases:
            | `identifier` | `concept` | `meaning` |                          |
            | set          | set       | set       | expected result          |
            | ============ | ========= | ========= | ======================== |
            | no           | yes       | yes       | ValueError("identifier") |
            Invariant: `identifier` is `None`
            """
        ):
            with self.assertRaisesRegex(ValueError, "identifier"):
                MarkdownThesaurusItem(None, "FakeConcept", "FakeMeaning")

        with self.subTest(
            """
            Cases:
            | `identifier` | `concept` | `meaning` |                          |
            | set          | set       | set       | expected result          |
            | ============ | ========= | ========= | ======================== |
            | no           | yes       | yes       | ValueError("identifier") |
            Invariant: `identifier` is set to empty string
            """
        ):
            with self.assertRaisesRegex(ValueError, "identifier"):
                MarkdownThesaurusItem("", "FakeConcept", "FakeMeaning")

        with self.subTest(
            """
            Cases:
            | `identifier` | `concept` | `meaning` |                          |
            | set          | set       | set       | expected result          |
            | ============ | ========= | ========= | ======================== |
            | yes          | no        | yes       | ValueError("concept")    |
            Invariant: `concept` is `None`
            """
        ):
            with self.assertRaisesRegex(ValueError, "concept"):
                MarkdownThesaurusItem("FakeIdentifier", None, "FakeMeaning")

        with self.subTest(
            """
            Cases:
            | `identifier` | `concept` | `meaning` |                          |
            | set          | set       | set       | expected result          |
            | ============ | ========= | ========= | ======================== |
            | yes          | no        | yes       | ValueError("concept")    |
            Invariant: `concept` is set to empty string
            """
        ):
            with self.assertRaisesRegex(ValueError, "concept"):
                MarkdownThesaurusItem("FakeIdentifier", "", "FakeMeaning")

        with self.subTest(
            """
            Cases:
            | `identifier` | `concept` | `meaning` |                          |
            | set          | set       | set       | expected result          |
            | ============ | ========= | ========= | ======================== |
            | no           | no        | yes       | ValueError("identifier") |
            """
        ):
            with self.assertRaisesRegex(ValueError, "identifier"):
                MarkdownThesaurusItem(None, None, "FakeMeaning")

        with self.subTest(
            """
            Cases:
            | `identifier` | `concept` | `meaning` |                          |
            | set          | set       | set       | expected result          |
            | ============ | ========= | ========= | ======================== |
            | yes          | yes       | no        | no error                 |
            invariant: `meaning` is `None`
            """
        ):
            MarkdownThesaurusItem("FakeIdentifier", "FakeConcept", None)

        with self.subTest(
            """
            Cases:
            | `identifier` | `concept` | `meaning` |                          |
            | set          | set       | set       | expected result          |
            | ============ | ========= | ========= | ======================== |
            | yes          | yes       | no        | no error                 |
            invariant: `meaning` is set to empty string
            """
        ):
            MarkdownThesaurusItem("FakeIdentifier", "FakeConcept", "")

        with self.subTest(
            """
            Cases:
            | `identifier` | `concept` | `meaning` |                          |
            | set          | set       | set       | expected result          |
            | ============ | ========= | ========= | ======================== |
            | no           | yes       | no        | ValueError("identifier") |
            """
        ):
            with self.assertRaisesRegex(ValueError, "identifier"):
                MarkdownThesaurusItem(None, "FakeConcept", None)

        with self.subTest(
            """
            Cases:
            | `identifier` | `concept` | `meaning` |                          |
            | set          | set       | set       | expected result          |
            | ============ | ========= | ========= | ======================== |
            | yes          | no        | no        | ValueError("concept")    |
            """
        ):
            with self.assertRaisesRegex(ValueError, "concept"):
                MarkdownThesaurusItem("FakeIdentifier", None, None)

        with self.subTest(
            """
            Cases:
            | `identifier` | `concept` | `meaning` |                          |
            | set          | set       | set       | expected result          |
            | ============ | ========= | ========= | ======================== |
            | no           | no        | no        | ValueError("identifier") |
            """
        ):
            with self.assertRaisesRegex(ValueError, "identifier"):
                MarkdownThesaurusItem(None, None, None)

    def test_reference(self):
        """
        Unit test for the reference property.
        `reference`: "[`identifier`]: [#`concept`]  "
                     where `concept` is set to lowercase, and any spaces (` `), opening
                     and closing brackets (`(` and `)` respectively) are replaced by
                     dashes (`-`). Followed by two whitespaces (`  `)
        `identifier` and `concept` are always set, as otherwise the constructor raised
        an error.
        Testcases:
        `identifier` will be set to "FakeIdentifier"
        | `concept` has | `concept` has    | `concept` has    |
        | whitespaces   | opening brackets | closing brackets |
        | ============= | ================ | ================ |
        | yes           | yes              | yes              |
        | no            | yes              | yes              |
        | yes           | no               | yes              |
        | no            | no               | yes              |
        | yes           | yes              | no               |
        | no            | yes              | no               |
        | yes           | no               | no               |
        | no            | no               | no               |
        """

        identifier = "FakeIdentifier"

        with self.subTest(
            """
            | `concept` has | `concept` has    | `concept` has    |
            | whitespaces   | opening brackets | closing brackets |
            | ============= | ================ | ================ |
            | yes           | yes              | yes              |
            """
        ):
            concept = "Fake Concept with spaces (and with opening and closing brackets)"
            expected = [
                (
                    "[FakeIdentifier]: [#fake-concept-with-spaces--and-with-opening-and"
                    + "-closing-brackets-]  "
                )
            ]
            self.assertEqual(
                MarkdownThesaurusItem(identifier, concept, "").reference, expected
            )

        with self.subTest(
            """
            | `concept` has | `concept` has    | `concept` has    |
            | whitespaces   | opening brackets | closing brackets |
            | ============= | ================ | ================ |
            | no            | yes              | yes              |
            """
        ):
            concept = "FakeConceptWithoutspaces(ButWithOpeningAndClosingBrackets)"
            expected = [
                (
                    "[FakeIdentifier]: [#fakeconceptwithoutspaces-"
                    + "butwithopeningandclosingbrackets-]  "
                )
            ]
            self.assertEqual(
                MarkdownThesaurusItem(identifier, concept, "").reference, expected
            )

        with self.subTest(
            """
            | `concept` has | `concept` has    | `concept` has    |
            | whitespaces   | opening brackets | closing brackets |
            | ============= | ================ | ================ |
            | yes           | no               | yes              |
            """
        ):
            concept = (
                "Fake Concept with spaces and closing brackets) but without opening "
                + "brackets"
            )
            expected = [
                (
                    "[FakeIdentifier]: [#fake-concept-with-spaces-and-closing-brackets"
                    + "--but-without-opening-brackets]  "
                )
            ]
            self.assertEqual(
                MarkdownThesaurusItem(identifier, concept, "").reference, expected
            )

        with self.subTest(
            """
            | `concept` has | `concept` has    | `concept` has    |
            | whitespaces   | opening brackets | closing brackets |
            | ============= | ================ | ================ |
            | no            | no               | yes              |
            """
        ):
            concept = (
                "FakeConceptWithoutSpacesButWithClosingBrackets)"
                + "AlsoWithoutOpeningBrackets"
            )
            expected = [
                (
                    "[FakeIdentifier]: [#fakeconceptwithoutspacesbutwithclosingbrackets"
                    + "-alsowithoutopeningbrackets]  "
                )
            ]
            self.assertEqual(
                MarkdownThesaurusItem(identifier, concept, "").reference, expected
            )

        with self.subTest(
            """
            | `concept` has | `concept` has    | `concept` has    |
            | whitespaces   | opening brackets | closing brackets |
            | ============= | ================ | ================ |
            | yes           | yes              | no               |
            """
        ):
            concept = (
                "Fake Concept With Spaces (And Opening Brackets But Without "
                + "Closing Brackets"
            )
            expected = [
                (
                    "[FakeIdentifier]: [#fake-concept-with-spaces--and-opening-brackets"
                    + "-but-without-closing-brackets]  "
                )
            ]
            self.assertEqual(
                MarkdownThesaurusItem(identifier, concept, "").reference, expected
            )

        with self.subTest(
            """
            | `concept` has | `concept` has    | `concept` has    |
            | whitespaces   | opening brackets | closing brackets |
            | ============= | ================ | ================ |
            | no            | yes              | no               |
            """
        ):
            concept = (
                "FakeConceptWithoutSpaces(ButWithOpeningBracketsButWithout"
                + "ClosingBrackets"
            )
            expected = [
                (
                    "[FakeIdentifier]: [#fakeconceptwithoutspaces-"
                    + "butwithopeningbracketsbutwithoutclosingbrackets]  "
                )
            ]
            self.assertEqual(
                MarkdownThesaurusItem(identifier, concept, "").reference, expected
            )

        with self.subTest(
            """
            | `concept` has | `concept` has    | `concept` has    |
            | whitespaces   | opening brackets | closing brackets |
            | ============= | ================ | ================ |
            | yes           | no               | no               |
            """
        ):
            concept = "Fake Concept With Spaces But Without Opening Or Closing Brackets"
            expected = [
                (
                    "[FakeIdentifier]: [#fake-concept-with-spaces-but-without-opening"
                    + "-or-closing-brackets]  "
                )
            ]
            self.assertEqual(
                MarkdownThesaurusItem(identifier, concept, "").reference, expected
            )

        with self.subTest(
            """
            | `concept` has | `concept` has    | `concept` has    |
            | whitespaces   | opening brackets | closing brackets |
            | ============= | ================ | ================ |
            | no            | no               | no               |
            """
        ):
            concept = "FakeConceptWithoutSpacesOpeningBracketsAndClosingBrackets"
            expected = [
                (
                    "[FakeIdentifier]: "
                    + "[#fakeconceptwithoutspacesopeningbracketsandclosingbrackets]  "
                )
            ]
            self.assertEqual(
                MarkdownThesaurusItem(identifier, concept, "").reference, expected
            )
