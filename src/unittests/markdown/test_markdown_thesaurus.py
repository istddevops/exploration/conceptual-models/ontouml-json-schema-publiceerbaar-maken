"""
Test suites for module `src.markdown.markdown_thesaurus`
"""


from copy import deepcopy
from unittest import TestCase

from src.markdown.markdown_thesaurus import MarkdownThesaurus


class TestMarkdownThesaurus(TestCase):
    """
    Testsuite for class `MarkdownThesaurus`
    We test the constructor as well as the property `markdown`.
    The constructor tests that all required members are provided at construction. It
    takes the following arguments:
    - `title`: mandatory, type `str`
    - `contents`: mandatory, type `list` for which each items MUST have the
        attributes `markdown` and `reference`.
    These will be tested separately, to keep the unittest methods small and concise.
    The `markdown` property created a markdown layout for a thesaurus:
    "# `title`  "
    ""
    <for each `item`>
    "`item.markdown`"
    "`item.reference'"
    """

    def test_init_title(self):
        # pylint:disable=[C0301]
        """
        1. `title` not set => ValueError("title")
        2. `title` set, but not of type `str` => TypeError("title")
        3. `title` set to empty `str` => ValueError("title")
        4. `title` set to `str` => no error
        """  # noqa=E501

        fakeContents = type(
            "FakeItem",
            (object,),
            {"markdown": "FakeMarkdown", "reference": "FakeReference"},
        )
        with self.subTest("""1. `title` not set => ValueError("title")"""):
            with self.assertRaisesRegex(ValueError, "title"):
                MarkdownThesaurus(None, [fakeContents])

        with self.subTest(
            """2. `title` set, but not of type `str` => TypeError("title")"""
        ):
            with self.assertRaisesRegex(TypeError, "title"):
                MarkdownThesaurus(1, [fakeContents])

        with self.subTest("""3. `title` set to empty `str` => ValueError("title")"""):
            with self.assertRaisesRegex(ValueError, "title"):
                MarkdownThesaurus("", [fakeContents])

        with self.subTest("""4. `title` set to `str` => no error"""):
            MarkdownThesaurus("FakeTitle", [fakeContents])

    def test_init_contents(self):
        """
        1. `content` not set => ValueError("content")
        2. `content` set, but not a `list` => TypeError("content")
        3. `content` set to `list`, but not all items have members `markdown` and
           `reference` => ValueError("content")
        4. `content` set to `list`, and all items have members `markdown` and
           `reference` => no error
        """

        fake_title = "FakeTitle"

        with self.subTest("""1. `content` not set => ValueError("content")"""):
            with self.assertRaisesRegex(ValueError, "content"):
                MarkdownThesaurus(fake_title, None)

        with self.subTest(
            """2. `content` set, but not a `list` => TypeError("content")"""
        ):
            with self.assertRaisesRegex(TypeError, "content"):
                MarkdownThesaurus(fake_title, 1)

        with self.subTest(
            """
            3. `content` set to `list`, but not all items have members `markdown` and
               `reference` => ValueError("content")
            Invariant: no items at all have either `markdown` nor `reference` members
            """
        ):
            with self.assertRaisesRegex(ValueError, "content"):
                MarkdownThesaurus(fake_title, [1, 2])

        with self.subTest(
            """
            3. `content` set to `list`, but not all items have members `markdown` and
               `reference` => ValueError("content")
            Invariant: items only have `markdown` members
            """
        ):
            fake_content_item_1 = type(
                "FakeItemType",
                (object,),
                {"markdown": "FakeMarkup", "Foo": "FakeThing"},
            )
            fake_content_item_2 = deepcopy(fake_content_item_1)
            fake_content_item_2.markdown = "AnotherFakeMarkdown"
            with self.assertRaisesRegex(ValueError, "content"):
                MarkdownThesaurus(
                    fake_title, [fake_content_item_1, fake_content_item_2]
                )

        with self.subTest(
            """
            3. `content` set to `list`, but not all items have members `markdown` and
               `reference` => ValueError("content")
            Invariant: items only have `reference` members
            """
        ):
            fake_content_item_1 = type(
                "FakeItemType",
                (object,),
                {"reference": "FakeReference", "Foo": "FakeThing"},
            )
            fake_content_item_2 = deepcopy(fake_content_item_1)
            fake_content_item_2.reference = "AnotherFakeReference"
            with self.assertRaisesRegex(ValueError, "content"):
                MarkdownThesaurus(
                    fake_title, [fake_content_item_1, fake_content_item_2]
                )

        with self.subTest(
            """
            3. `content` set to `list`, but not all items have members `markdown` and
               `reference` => ValueError("content")
            Invariant: one item only has `markdown`, other item only has `reference`
            """
        ):
            fake_content_item_1 = type(
                "FakeItemType",
                (object,),
                {"markdown": "FakeMarkdown", "Foo": "FakeThing"},
            )
            fake_content_item_2 = type(
                "FakeItemType",
                (object,),
                {"reference": "FakeReference", "Foo": "FakeThing"},
            )
            with self.assertRaisesRegex(ValueError, "content"):
                MarkdownThesaurus(
                    fake_title, [fake_content_item_1, fake_content_item_2]
                )

        with self.subTest(
            """
            4. `content` set to `list`, and all items have members `markdown` and
               `reference` => no error
            """
        ):
            fake_content_item_1 = type(
                "FakeItemType",
                (object,),
                {"markdown": "FakeMarkup", "reference": "FakeReference"},
            )
            fake_content_item_2 = deepcopy(fake_content_item_1)
            fake_content_item_2.markdown = "AnotherFakeMarkup"
            fake_content_item_2.reference = "AnotherFakeReference"
            MarkdownThesaurus(fake_title, [fake_content_item_1, fake_content_item_2])

    def test_markdown(self):
        """
        The markdown property yields the following lines:
        "# `title`  "
        [repeated for all items in `contents`]:
        "`item.markdown`"
        [repeated for all items in `contents`]:
        "`item.reference`"

        The constructor already guards that `title` and `contents` is set, and that
        each `contents` item has at least a `markdown` and `reference` property. So no
        need to test that here.
        """

        expected = [
            "# FakeTitle  ",
            "",
            "FakeMarkdown 1",
            "FakeMarkdown 2",
            "FakeReference 1",
            "FakeReference 2",
        ]

        content_item_1 = type(
            "FakeItemType",
            (object,),
            {
                "markdown": ["FakeMarkdown 1"],
                "reference": ["FakeReference 1"],
            },
        )
        content_item_2 = type(
            "FakeItemType",
            (object,),
            {
                "markdown": ["FakeMarkdown 2"],
                "reference": ["FakeReference 2"],
            },
        )
        result = MarkdownThesaurus(
            "FakeTitle", [content_item_1, content_item_2]
        ).markdown
        self.assertEqual(result, expected)
