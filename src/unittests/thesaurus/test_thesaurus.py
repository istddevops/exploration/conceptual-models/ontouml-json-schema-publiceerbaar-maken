"""
Test suites for the `src.thesaurus.thesaurus.py` module
"""


from unittest import TestCase

from src.thesaurus.thesaurus import ThesaurusBase


class TestThesaurusBase(TestCase):
    """
    Test suite for the `ThesaurusBase` class
    """

    def test_init(self):
        # pylint:disable=[C0301]
        """
        Test case for the constructor. The constructor takes two arguments:
        1. `title`
           - This is a mandatory argument. If not set, ValueError("title") will be
             raised.
        2. `items`
           - This is a mandatory argument. If not set, ValueError("items") will be
             raised.
           - `items` needs to be a iterable, for which each item needs to have at least
             the attributes `identifier`, `concept` and `meaning`. If it is not an
             iterable, TypeError("items") will be raised. If not all item have
             attributes `identifier`, `concept` and `meaning`, TypeError("items") will
             be raised.
        Test cases:
        | `title` | `title` | `items` | `items`  | `items`    |                                  |
        | set     | is str  | set     | is       | each have  |                                  |
        |         |         |         | iterable | attributes | result                           |
        | ======= | ======= | ======= | ======== | ========== | ================================ |
        | yes     | yes     | yes     | yes      | yes        | no error                         |
        | no      | n/a     | yes     | yes      | yes        | ValueError("title")              |
        | yes     | no      | yes     | yes      | yes        | TypeError("title")               |
        | yes     | yes     | no      | n/a      | n/a        | ValueError("items")              |
        | no      | n/a     | no      | n/a      | n/a        | ValueError("title")              |
        | yes     | no      | no      | n/a      | n/a        | TypeError("title")               |
        | yes     | yes     | yes     | no       | n/a        | TypeError("... is not iterable") |
        | no      | n/a     | yes     | no       | n/a        | ValueError("title")              |
        | yes     | no      | yes     | no       | n/a        | TypeError("title")               |
        | yes     | yes     | yes     | yes      | no         | TypeError("items")               |
        | no      | n/a     | yes     | yes      | no         | ValueError("title")              |
        | yes     | no      | yes     | yes      | no         | TypeError("title")               |
        """  # noqa=E501

        with self.subTest(
            """
            | `title` | `title` | `items` | `items`  | `items`    |                     |
            | set     | is str  | set     | is       | each have  |                     |
            |         |         |         | iterable | attributes | result              |
            | ======= | ======= | ======= | ======== | ========== | =================== |
            | yes     | yes     | yes     | yes      | yes        | no error            |
            """  # noqa=E501
        ):
            ThesaurusBase(
                "FakeTitle",
                [
                    type(
                        "FakeType",
                        (object,),
                        {
                            "identifier": "FakeIdentifier",
                            "concept": "FakeConcept",
                            "meaning": "FakeMeaning",
                        },
                    )
                ],
            )

        with self.subTest(
            """
            | `title` | `title` | `items` | `items`  | `items`    |                     |
            | set     | is str  | set     | is       | each have  |                     |
            |         |         |         | iterable | attributes | result              |
            | ======= | ======= | ======= | ======== | ========== | =================== |
            | no      | n/a     | yes     | yes      | yes        | ValueError("title") |
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(ValueError, "title"):
                ThesaurusBase(
                    None,
                    [
                        type(
                            "FakeType",
                            (object,),
                            {
                                "identifier": "FakeIdentifier",
                                "concept": "FakeConcept",
                                "meaning": "FakeMeaning",
                            },
                        )
                    ],
                )

        with self.subTest(
            """
            | `title` | `title` | `items` | `items`  | `items`    |                     |
            | set     | is str  | set     | is       | each have  |                     |
            |         |         |         | iterable | attributes | result              |
            | ======= | ======= | ======= | ======== | ========== | =================== |
            | yes     | no      | yes     | yes      | yes        | TypeError("title")  |
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(TypeError, "title"):
                ThesaurusBase(
                    1,
                    [
                        type(
                            "FakeType",
                            (object,),
                            {
                                "identifier": "FakeIdentifier",
                                "concept": "FakeConcept",
                                "meaning": "FakeMeaning",
                            },
                        )
                    ],
                )

        with self.subTest(
            """
            | `title` | `title` | `items` | `items`  | `items`    |                     |
            | set     | is str  | set     | is       | each have  |                     |
            |         |         |         | iterable | attributes | result              |
            | ======= | ======= | ======= | ======== | ========== | =================== |
            | yes     | yes     | no      | n/a      | n/a        | ValueError("items") |
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(ValueError, "items"):
                ThesaurusBase("FakeTitle", None)

        with self.subTest(
            """
            | `title` | `title` | `items` | `items`  | `items`    |                     |
            | set     | is str  | set     | is       | each have  |                     |
            |         |         |         | iterable | attributes | result              |
            | ======= | ======= | ======= | ======== | ========== | =================== |
            | no      | n/a     | no      | n/a      | n/a        | ValueError("title") |
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(ValueError, "title"):
                ThesaurusBase(None, None)

        with self.subTest(
            """
            | `title` | `title` | `items` | `items`  | `items`    |                     |
            | set     | is str  | set     | is       | each have  |                     |
            |         |         |         | iterable | attributes | result              |
            | ======= | ======= | ======= | ======== | ========== | =================== |
            | yes     | no      | no      | n/a      | n/a        | TypeError("title")  |
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(TypeError, "title"):
                ThesaurusBase(1, None)

        with self.subTest(
            """
            | `title` | `title` | `items` | `items`  | `items`    |                               |
            | set     | is str  | set     | is       | each have  |                               |
            |         |         |         | iterable | attributes | result                        |
            | ======= | ======= | ======= | ======== | ========== | ============================= |
            | yes     | yes     | yes     | no       | n/a        | TypeError("is not iterable")  |
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(TypeError, "is not iterable"):
                ThesaurusBase("FakeTitle", 1)

        with self.subTest(
            """
            | `title` | `title` | `items` | `items`  | `items`    |                     |
            | set     | is str  | set     | is       | each have  |                     |
            |         |         |         | iterable | attributes | result              |
            | ======= | ======= | ======= | ======== | ========== | =================== |
            | no      | n/a     | yes     | no       | n/a        | ValueError("title") |
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(ValueError, "title"):
                ThesaurusBase(None, "items")

        with self.subTest(
            """
            | `title` | `title` | `items` | `items`  | `items`    |                     |
            | set     | is str  | set     | is       | each have  |                     |
            |         |         |         | iterable | attributes | result              |
            | ======= | ======= | ======= | ======== | ========== | =================== |
            | yes     | no      | yes     | no       | n/a        | TypeError("title")  |
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(TypeError, "title"):
                ThesaurusBase(1, 1)

        with self.subTest(
            """
            | `title` | `title` | `items` | `items`  | `items`    |                     |
            | set     | is str  | set     | is       | each have  |                     |
            |         |         |         | iterable | attributes | result              |
            | ======= | ======= | ======= | ======== | ========== | =================== |
            | yes     | yes     | yes     | yes      | no         | TypeError("items")  |
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(TypeError, "items"):
                ThesaurusBase("FakeTitle", [1, 2, 3])

        with self.subTest(
            """
            | `title` | `title` | `items` | `items`  | `items`    |                     |
            | set     | is str  | set     | is       | each have  |                     |
            |         |         |         | iterable | attributes | result              |
            | ======= | ======= | ======= | ======== | ========== | =================== |
            | no      | n/a     | yes     | yes      | no         | ValueError("title") |
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(ValueError, "title"):
                ThesaurusBase(None, [1, 2, 3])

        with self.subTest(
            """
            | `title` | `title` | `items` | `items`  | `items`    |                     |
            | set     | is str  | set     | is       | each have  |                     |
            |         |         |         | iterable | attributes | result              |
            | ======= | ======= | ======= | ======== | ========== | =================== |
            | yes     | no      | yes     | yes      | no         | TypeError("title")  |
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(TypeError, "title"):
                ThesaurusBase(1, [1, 2, 3, 4])
