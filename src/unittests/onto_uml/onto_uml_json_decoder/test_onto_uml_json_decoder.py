"""Test suites for onto_uml.onto_uml_json_decoder, non-views"""

from unittest import TestCase
from unittest.mock import DEFAULT, patch

from src.onto_uml import onto_uml_json_decoder
from src.onto_uml.onto_uml_class import OntoUmlClass
from src.onto_uml.onto_uml_diagram import OntoUmlDiagram, OntoUmlDiagramOwner
from src.onto_uml.onto_uml_generalization import (
    OntoUmlGeneralization,
    OntoUmlGeneralizationParty,
)
from src.onto_uml.onto_uml_generalization_set import (
    OntoUmlGeneralizationSet,
    OntoUmlGeneralizationSetItem,
)
from src.onto_uml.onto_uml_model_element import OntoUmlModelElement
from src.onto_uml.onto_uml_objects import OntoUmlProperty, OntoUmlPropertyType
from src.onto_uml.onto_uml_package import OntoUmlPackage
from src.onto_uml.onto_uml_project import OntoUmlProject
from src.onto_uml.onto_uml_relation import OntoUmlRelation


# pylint: disable=[W0212]
class TestOntoUmlJsonDecoder(TestCase):
    """Testsuite for OntoUmlJsonDecoder methods"""

    def test_correct_handler_method_called(self):
        """Check that onto_uml_typed_object hands the object down to the correct
        specific handler method.
        1.   No "type" attribute in the deserialized object, then return the object
             itself.
        2.   "type" attribute in the deserialized object
        2.a. "type" == "Class" -> call to _class
        2.b. "type" == "ClassView" -> call to _class_view
        2.c. "type" == "Diagram" -> call to _diagram
        2.d. "type" == "Generalization" -> call to _generalization
        2.e. "type" == "GeneralizationSet" -> call to _generalization_set
        2.f. "type" == "GeneralizationSetView" -> call to _generalization_set_view
        2.g. "type" == "Package" -> call to _package
        2.h. "type" == "Path" -> call to _path
        2.i. "type" == "Project" -> call to _project
        2.j. "type" == "Property" -> call to _property
        2.k. "type" == "Rectangle" -> call to _rectangle
        2.l. "type" == "Relation" -> call to _relation
        2.m. "type" == "RelationView" -> call to _relation_view
        2.n. "type" == "Text" -> call to _text
        2.o. "type" is other -> return the object itself
        """

        # 1.   No "type" attribute in the deserialized object, then return the object
        #      itself.
        with self.subTest(
            """1.   No "type" attribute in the deserialized object, then
        return the object itself"""
        ):
            fake_json = {"fake": 1}
            self.assertEqual(
                onto_uml_json_decoder.onto_uml_typed_object(fake_json), fake_json
            )

        handler_test_setups = [
            {
                "description": """2.a. "type" == "Class" -> call to _class""",
                "fake_json": {"type": "Class"},
                "handler": "_class",
            },
            {
                "description": """2.b. "type" == "ClassView" -> call to _class_view""",
                "fake_json": {"type": "ClassView"},
                "handler": "_class_view",
            },
            {
                "description": """2.c. "type" == "Diagram" -> call to _diagram""",
                "fake_json": {"type": "Diagram"},
                "handler": "_diagram",
            },
            {
                "description": """2.d. "type" == "Generalization" -> call to
                _generalization""",
                "fake_json": {"type": "Generalization"},
                "handler": "_generalization",
            },
            {
                "description": """2.e. "type" == "GeneralizationSet" -> call to
                    _generalization_set""",
                "fake_json": {"type": "GeneralizationSet"},
                "handler": "_generalization_set",
            },
            {
                "description": """2.f. "type" == "GeneralizationSetView" -> call to
                    _generalization_set_view""",
                "fake_json": {"type": "GeneralizationSetView"},
                "handler": "_generalization_set_view",
            },
            {
                "description": """2.g. "type" == "Package" -> call to _package""",
                "fake_json": {"type": "Package"},
                "handler": "_package",
            },
            {
                "description": """2.h. "type" == "Path" -> call to _path""",
                "fake_json": {"type": "Path"},
                "handler": "_path",
            },
            {
                "description": """2.i. "type" == "Project" -> call to _project""",
                "fake_json": {"type": "Project"},
                "handler": "_project",
            },
            {
                "description": """2.j. "type" == "Property" -> call to _property""",
                "fake_json": {"type": "Property"},
                "handler": "_property",
            },
            {
                "description": """2.k. "type" == "Rectangle" -> call to _rectangle""",
                "fake_json": {"type": "Rectangle"},
                "handler": "_rectangle",
            },
            {
                "description": """2.l. "type" == "Relation" -> call to _relation""",
                "fake_json": {"type": "Relation"},
                "handler": "_relation",
            },
            {
                "description": """2.m. "type" == "RelationView" -> call to
                _relation_view""",
                "fake_json": {"type": "RelationView"},
                "handler": "_relation_view",
            },
            {
                "description": """2.n. "type" == "Text" -> call to _text""",
                "fake_json": {"type": "Text"},
                "handler": "_text",
            },
        ]

        # 2.   "type" attribute in the deserialized object
        for testable in handler_test_setups:
            with self.subTest(testable["description"]):
                fake_json = testable["fake_json"]
                with patch.object(
                    onto_uml_json_decoder, testable["handler"]
                ) as mocked_call:
                    onto_uml_json_decoder.onto_uml_typed_object(fake_json)
                mocked_call.assert_called_once()

        # 2.o. "type" is other -> return the object itself
        with self.subTest("""2.o. "type" is other -> return the object itself"""):
            fake_json = {type: "Fake"}
            self.assertEqual(
                onto_uml_json_decoder.onto_uml_typed_object(fake_json), fake_json
            )
        # 2.o. "type" is other -> also check none of the handlers are called
        with self.subTest(
            """2.o. "type" is other -> also check none of the handlers are called"""
        ):
            with patch.multiple(
                onto_uml_json_decoder,
                _class=DEFAULT,
                _class_view=DEFAULT,
                _diagram=DEFAULT,
                _generalization=DEFAULT,
                _generalization_set=DEFAULT,
                _generalization_set_view=DEFAULT,
                _package=DEFAULT,
                _path=DEFAULT,
                _project=DEFAULT,
                _property=DEFAULT,
                _rectangle=DEFAULT,
                _relation=DEFAULT,
                _relation_view=DEFAULT,
                _text=DEFAULT,
            ) as handlers:
                fake_json = {type: "Fake"}
                onto_uml_json_decoder.onto_uml_typed_object(fake_json)
                handlers["_class"].assert_not_called()

    def test_check_attributes(self):
        """Unittest voor _check_attributes:
        1.  Wanneer een van de attributen uit de lijst niet aanwezig zijn in het
            object, dan False
        1.a Geen van de attributen van de lijst zijn aanwezig in het object -> False
        1.b Eén van de attributen van de lijst is aanwezig in het object -> False
        1.c Sommige van de attributen van de lijst zijn aanwezig in het object -> False
        1.d Op één na alle attributen van de lijst zijn aanwezig in het object -> False
        2. Anders True
        2.a Alle attributen van de lijst zijn aanwezig in het object -> True
        2.b Alle attributen van de lijst zijn aanwezig in het object, maar het object
            heeft nog één ander attribuut die niet in de lijst staat -> True
        2.c Alle attributen van de lijst zijn aanwezig in het object, maar het object
            heeft nog een aantal andere attributen die niet in de lijst staan -> True
        """

        with self.subTest(
            """1.a Geen van de attributen van de lijst zijn aanwezig in het object ->
            False"""
        ):
            # pylint: disable=[R0903]
            class FakeObjectOneAAndB:
                """Fake object for test cases 1.a. and 1.b."""

                def __init__(self):
                    self.fake_attribute_one = "1"
                    self.fake_attribute_two = "2"

            self.assertFalse(
                onto_uml_json_decoder._check_attributes(
                    FakeObjectOneAAndB(), ["id", "type", "fake"]
                )
            )

        with self.subTest(
            """1.b Eén van de attributen van de lijst is aanwezig in het object ->
            False"""
        ):
            self.assertFalse(
                onto_uml_json_decoder._check_attributes(
                    FakeObjectOneAAndB(), ["id", "type", "fake", "fake_attribute_one"]
                )
            )

        with self.subTest(
            """1.c Sommige van de attributen van de lijst zijn aanwezig in het object ->
            False"""
        ):
            # pylint: disable=[R0903]
            class FakeObjectOneCAndDAndTwo:
                """Fake object for test cases 1.c. and 1.d."""

                def __init__(self):
                    self.attribute_a = "a"
                    self.attribute_b = "b"
                    self.attribute_c = "c"
                    self.attribute_d = "d"
                    self.attribute_e = "e"
                    self.attribute_f = "f"

            self.assertFalse(
                onto_uml_json_decoder._check_attributes(
                    FakeObjectOneCAndDAndTwo(),
                    ["id", "type", "attribute_a", "attribute_b"],
                )
            )

        with self.subTest(
            """1.d Op één na alle attributen van de lijst zijn aanwezig in het object ->
            False"""
        ):
            self.assertFalse(
                onto_uml_json_decoder._check_attributes(
                    FakeObjectOneCAndDAndTwo(),
                    [
                        "attribute_a",
                        "attribute_b",
                        "attribute_c",
                        "attribute_d",
                        "fake",
                    ],
                )
            )

        with self.subTest(
            """2.a Alle attributen van de lijst zijn aanwezig in het object -> True"""
        ):
            self.assertTrue(
                onto_uml_json_decoder._check_attributes(
                    FakeObjectOneCAndDAndTwo(),
                    [
                        "attribute_a",
                        "attribute_b",
                        "attribute_c",
                        "attribute_d",
                        "attribute_e",
                        "attribute_f",
                    ],
                )
            )

        with self.subTest(
            """2.b Alle attributen van de lijst zijn aanwezig in het object, maar het
            object heeft nog één ander attribuut die niet in de lijst staat -> True"""
        ):
            self.assertTrue(
                onto_uml_json_decoder._check_attributes(
                    FakeObjectOneCAndDAndTwo(),
                    [
                        "attribute_a",
                        "attribute_b",
                        "attribute_c",
                        "attribute_d",
                        "attribute_e",
                    ],
                )
            )

        with self.subTest(
            """2.c Alle attributen van de lijst zijn aanwezig in het object, maar het
            object heeft nog een aantal andere attributen die niet in de lijst staan ->
            True"""
        ):
            self.assertTrue(
                onto_uml_json_decoder._check_attributes(
                    FakeObjectOneCAndDAndTwo(),
                    [
                        "attribute_a",
                        "attribute_b",
                        "attribute_d",
                        "attribute_e",
                    ],
                )
            )

    def test_class(self):
        """Unittest voor _class handler:
        1. Wanneer niet alle verwachte attributen aanwezig zijn in het object,
           dan het object as-is teruggeven.
        2. Wanneer wel alle verwachte attributen aanwezig zijn in het object,
           dan controleren dat deze correct zijn overgenomen in het OntoUmlClass
           object."""

        with self.subTest(
            """1. Wanneer niet alle verwachte attributen aanwezig zijn in het object,
           dan het object as-is teruggeven."""
        ):
            fake_json = {"id": "FakeId", "type": "Class", "fake": "Dummy"}
            self.assertEqual(onto_uml_json_decoder._class(fake_json), fake_json)

        with self.subTest(
            """2. Wanneer wel alle verwachte attributen aanwezig zijn in
            het object, dan controleren dat deze correct zijn overgenomen in het
            OntoUmlClass object."""
        ):
            fake_json = {
                "id": "z_zwRAGFYFAUAxq_",
                "name": "Fake",
                "description": None,
                "type": "Class",
                "propertyAssignments": None,
                "stereotype": "kind",
                "isAbstract": False,
                "isDerived": False,
                "properties": None,
                "isExtensional": None,
                "isPowertype": None,
                "order": None,
                "literals": None,
                "restrictedTo": ["functional-complex"],
            }
            expected_class = OntoUmlClass(
                identifier=fake_json["id"],
                name=fake_json["name"],
                description=fake_json["description"],
                property_assignments=fake_json["propertyAssignments"],
                stereotype=fake_json["stereotype"],
                is_abstract=fake_json["isAbstract"],
                is_derived=fake_json["isDerived"],
                properties=fake_json["properties"],
                is_extensional=fake_json["isExtensional"],
                is_powertype=fake_json["isPowertype"],
                order=fake_json["order"],
                literals=fake_json["literals"],
                restricted_to=fake_json["restrictedTo"],
            )
            self.assertEqual(onto_uml_json_decoder._class(fake_json), expected_class)

    def test_diagram(self):
        """Unittest voor _diagram handler:
        1. Wanneer niet alle verwachte attributen aanwezig zijn in het object, dan het
           object as-is teruggeven.
        2. Wanneer wel alle verwachte attributen aanwezig zijn in het object, dan
           controleren dat deze correct zijn overgenomen in het OntoUmlDiagram object.
        """

        with self.subTest(
            """1. Wanneer niet alle verwachte attributen aanwezig zijn in het object,
            dan het object as-is teruggeven."""
        ):
            fake_json = {"id": "FakeId", "type": "Diagram", "fake": "Dummy"}
            self.assertEqual(onto_uml_json_decoder._diagram(fake_json), fake_json)

        with self.subTest(
            """2. Wanneer wel alle verwachte attributen aanwezig zijn in het object, dan
            controleren dat deze correct zijn overgenomen in het OntoUmlDiagram
            object."""
        ):
            fake_json = {
                "id": "3aD58QGGAqAAjA61",
                "name": "Fake",
                "description": None,
                "type": "Diagram",
                "owner": {"id": "NaeIigGFYEGQJAx4", "type": "Package"},
                "contents": [],
            }
            expected_class = OntoUmlDiagram(
                identifier=fake_json["id"],
                name=fake_json["name"],
                description=fake_json["description"],
                owner=OntoUmlDiagramOwner(
                    identifier=fake_json["owner"]["id"],
                    object_type=fake_json["owner"]["type"],
                ),
                contents=[],
            )
            generated_class = onto_uml_json_decoder._diagram(fake_json)
            self.assertTrue(generated_class == expected_class)
            self.assertEqual(generated_class, expected_class)

    def test_diagram_owner(self):
        """Unittest voor _diagram_owner handler:
        1.a. Wanneer geen object is meegegeven, dan None teruggeven
        1.b. Wanneer niet alle verwachte attributen aanwezig zijn in het object, dan
             None teruggeven.
        2. Wanneer wel alle verwachte attributen aanwezig zijn in het object, dan
           controleren dat deze correct zijn overgenomen in het OntoUmlDiagramOwner
           object.
        """

        with self.subTest(
            """1.a. Wanneer geen object is meegegeven, dan None teruggeven"""
        ):
            self.assertIsNone(onto_uml_json_decoder._diagram_owner(None))

        with self.subTest(
            """1.b. Wanneer niet alle verwachte attributen aanwezig zijn in het object,
            dan None teruggeven."""
        ):
            fake_json = {"fake": "Dummy", "anotherFake": "Dummy"}
            self.assertIsNone(onto_uml_json_decoder._diagram_owner(fake_json))

        with self.subTest(
            """2. Wanneer wel alle verwachte attributen aanwezig zijn in het object, dan
            controleren dat deze correct zijn overgenomen in het OntoUmlDiagramOwner
            object."""
        ):
            fake_json = {"id": "NaeIigGFYEGQJAx4", "type": "Package"}
            expected_class = OntoUmlDiagramOwner(
                identifier=fake_json["id"], object_type=fake_json["type"]
            )
            self.assertEqual(
                onto_uml_json_decoder._diagram_owner(fake_json), expected_class
            )

    def test_generalization(self):
        """Unittest voor _generalization handler:
        1. Wanneer niet alle verwachte attributen aanwezig zijn in het object, dan het
           object as-is teruggeven.
        2. Wanneer wel alle verwachte attributen aanwezig zijn in het object, dan
           controleren dat deze correct zijn overgenomen in het OntoUmlGeneralization
           object.
        """

        with self.subTest(
            """1. Wanneer niet alle verwachte attributen aanwezig zijn in het object,
            dan het object as-is teruggeven."""
        ):
            fake_json = {"id": "FakeId", "type": "Diagram", "fake": "Dummy"}
            self.assertEqual(
                onto_uml_json_decoder._generalization(fake_json), fake_json
            )

        with self.subTest(
            """2. Wanneer wel alle verwachte attributen aanwezig zijn in het object, dan
            controleren dat deze correct zijn overgenomen in het OntoUmlGeneralization
            object."""
        ):
            fake_json = {
                "id": "9s7wRAGFYFAUAxrY",
                "name": None,
                "description": None,
                "type": "Generalization",
                "propertyAssignments": None,
                "general": {"id": "z_zwRAGFYFAUAxq_", "type": "Class"},
                "specific": {"id": "gyrThAGFYFAUAxbh", "type": "Class"},
            }
            expected_class = OntoUmlGeneralization(
                identifier=fake_json["id"],
                name=fake_json["name"],
                description=fake_json["description"],
                property_assignments=None,
                general=OntoUmlGeneralizationParty(
                    identifier=fake_json["general"]["id"],
                    object_type=fake_json["general"]["type"],
                ),
                specific=OntoUmlGeneralizationParty(
                    identifier=fake_json["specific"]["id"],
                    object_type=fake_json["specific"]["type"],
                ),
            )
            self.assertEqual(
                onto_uml_json_decoder._generalization(fake_json), expected_class
            )

    def test_generalization_party(self):
        """Unittest voor _generalization_party handler:
        1.a. Wanneer geen object is meegegeven, dan None teruggeven
        1.b. Wanneer niet alle verwachte attributen aanwezig zijn in het object, dan
             None teruggeven.
        2. Wanneer wel alle verwachte attributen aanwezig zijn in het object, dan
           controleren dat deze correct zijn overgenomen in het
           OntoUmlGeneralizationParty object.
        """

        with self.subTest(
            """1.a. Wanneer geen object is meegegeven, dan None teruggeven"""
        ):
            self.assertIsNone(onto_uml_json_decoder._generalization_party(None))

        with self.subTest(
            """1.b. Wanneer niet alle verwachte attributen aanwezig zijn in het object,
            dan None teruggeven."""
        ):
            fake_json = {"fake": "Dummy", "anotherFake": "Dummy"}
            self.assertIsNone(onto_uml_json_decoder._generalization_party(fake_json))

        with self.subTest(
            """2. Wanneer wel alle verwachte attributen aanwezig zijn in het object, dan
            controleren dat deze correct zijn overgenomen in het
            OntoUmlGeneralizationParty object."""
        ):
            fake_json = {"id": "NaeIigGFYEGQJAx4", "type": "Package"}
            expected_class = OntoUmlGeneralizationParty(
                identifier=fake_json["id"], object_type=fake_json["type"]
            )
            self.assertEqual(
                onto_uml_json_decoder._generalization_party(fake_json), expected_class
            )

    def test_generalization_set(self):
        """Unittest voor _generalization_set handler:
        1. Wanneer niet alle verwachte attributen aanwezig zijn in het object, dan het
           object as-is teruggeven.
        2. Wanneer wel alle verwachte attributen aanwezig zijn in het object, dan
           controleren dat deze correct zijn overgenomen in het OntoUmlGeneralizationSet
           object.
        """

        with self.subTest(
            """1. Wanneer niet alle verwachte attributen aanwezig zijn in het object,
            dan het object as-is teruggeven."""
        ):
            fake_json = {"id": "FakeId", "type": "Diagram", "fake": "Dummy"}
            self.assertEqual(
                onto_uml_json_decoder._generalization_set(fake_json), fake_json
            )

        with self.subTest(
            """2. Wanneer wel alle verwachte attributen aanwezig zijn in het object, dan
            controleren dat deze correct zijn overgenomen in het
            OntoUmlGeneralizationSet object."""
        ):
            fake_json = {
                "id": "jKx40gGGAqAAjAu0",
                "name": "Fake",
                "description": None,
                "type": "GeneralizationSet",
                "propertyAssignments": None,
                "isDisjoint": True,
                "isComplete": False,
                "categorizer": None,
                "generalizations": [
                    {"id": "_oVaNAGFYFAUAyKF", "type": "Generalization"},
                    {"id": "QI1aNAGFYFAUAyKW", "type": "Generalization"},
                    {"id": "IKlaNAGFYFAUAyJ0", "type": "Generalization"},
                ],
            }
            expected_class = OntoUmlGeneralizationSet(
                identifier=fake_json["id"],
                name=fake_json["name"],
                description=fake_json["description"],
                property_assignments=fake_json["propertyAssignments"],
                is_disjoint=fake_json["isDisjoint"],
                is_complete=fake_json["isComplete"],
                categorizer=fake_json["categorizer"],
                generalizations=[
                    OntoUmlGeneralizationSetItem(g["id"], g["type"])
                    for g in fake_json["generalizations"]
                ],
            )
            self.assertEqual(
                onto_uml_json_decoder._generalization_set(fake_json), expected_class
            )

    def test_generalization_set_item(self):
        """Unittest voor _generalization_set_item handler:
        1.a. Wanneer geen object is meegegeven, dan None teruggeven
        1.b. Wanneer niet alle verwachte attributen aanwezig zijn in het object, dan
             None teruggeven.
        2. Wanneer wel alle verwachte attributen aanwezig zijn in het object, dan
           controleren dat deze correct zijn overgenomen in het
           OntoUmlGeneralizationSetItem object.
        """

        with self.subTest(
            """1.a. Wanneer geen object is meegegeven, dan None teruggeven"""
        ):
            self.assertIsNone(onto_uml_json_decoder._generalization_set_item(None))

        with self.subTest(
            """1.b. Wanneer niet alle verwachte attributen aanwezig zijn in het object,
            dan None teruggeven."""
        ):
            fake_json = {"fake": "Dummy", "anotherFake": "Dummy"}
            self.assertIsNone(onto_uml_json_decoder._generalization_set_item(fake_json))

        with self.subTest(
            """2. Wanneer wel alle verwachte attributen aanwezig zijn in het object, dan
            controleren dat deze correct zijn overgenomen in het
            OntoUmlGeneralizationSetItem object."""
        ):
            fake_json = {"id": "NaeIigGFYEGQJAx4", "type": "Generalization"}
            expected_class = OntoUmlGeneralizationSetItem(
                identifier=fake_json["id"], item_type=fake_json["type"]
            )
            self.assertEqual(
                onto_uml_json_decoder._generalization_set_item(fake_json),
                expected_class,
            )

    def test_model_element(self):
        """Unittest voor _model_element handler:
        1.a. Wanneer geen object is meegegeven, dan None teruggeven
        1.b. Wanneer niet alle verwachte attributen aanwezig zijn in het object, dan
             None teruggeven.
        2. Wanneer wel alle verwachte attributen aanwezig zijn in het object, dan
           controleren dat deze correct zijn overgenomen in het
           OntoUmlModelElement object.
        """

        with self.subTest(
            """1.a. Wanneer geen object is meegegeven, dan None teruggeven"""
        ):
            self.assertIsNone(onto_uml_json_decoder._model_element(None))

        with self.subTest(
            """1.b. Wanneer niet alle verwachte attributen aanwezig zijn in het object,
            dan None teruggeven."""
        ):
            fake_json = {"fake": "Dummy", "anotherFake": "Dummy"}
            self.assertIsNone(onto_uml_json_decoder._model_element(fake_json))

        with self.subTest(
            """2. Wanneer wel alle verwachte attributen aanwezig zijn in het object, dan
            controleren dat deze correct zijn overgenomen in het
            OntoUmlModelElement object."""
        ):
            fake_json = {"id": "NaeIigGFYEGQJAx4", "type": "Generalization"}
            expected_class = OntoUmlModelElement(
                model_element_id=fake_json["id"], model_element_type=fake_json["type"]
            )
            self.assertEqual(
                onto_uml_json_decoder._model_element(fake_json), expected_class
            )

    def test_package(self):
        """Unittest voor _package handler:
        1. Wanneer niet alle verwachte attributen aanwezig zijn in het object, dan het
           object as-is teruggeven.
        2. Wanneer wel alle verwachte attributen aanwezig zijn in het object, dan
           controleren dat deze correct zijn overgenomen in het OntoUmlPackage
           object.
        """

        with self.subTest(
            """1. Wanneer niet alle verwachte attributen aanwezig zijn in het object,
            dan het object as-is teruggeven."""
        ):
            fake_json = {"id": "FakeId", "type": "Diagram", "fake": "Dummy"}
            self.assertEqual(onto_uml_json_decoder._package(fake_json), fake_json)

        with self.subTest(
            """2. Wanneer wel alle verwachte attributen aanwezig zijn in het object, dan
            controleren dat deze correct zijn overgenomen in het OntoUmlPackage
            object."""
        ):
            fake_json = {
                "id": "sLYThAGFYFAUAxIL_root",
                "name": "Fake",
                "description": None,
                "type": "Package",
                "propertyAssignments": None,
                "contents": None,
            }
            expected_class = OntoUmlPackage(
                identifier=fake_json["id"],
                name=fake_json["name"],
                description=fake_json["description"],
                property_assignments=None,
                contents=None,
            )
            self.assertEqual(onto_uml_json_decoder._package(fake_json), expected_class)

    def test_project(self):
        """Unittest voor _project handler:
        1. Wanneer niet alle verwachte attributen aanwezig zijn in het object, dan het
           object as-is teruggeven.
        2. Wanneer wel alle verwachte attributen aanwezig zijn in het object, dan
           controleren dat deze correct zijn overgenomen in het OntoUmlProject
           object.
        """

        with self.subTest(
            """1. Wanneer niet alle verwachte attributen aanwezig zijn in het object,
            dan het object as-is teruggeven."""
        ):
            fake_json = {"id": "FakeId", "type": "Diagram", "fake": "Dummy"}
            self.assertEqual(onto_uml_json_decoder._project(fake_json), fake_json)

        with self.subTest(
            """2. Wanneer wel alle verwachte attributen aanwezig zijn in het object, dan
            controleren dat deze correct zijn overgenomen in het OntoUmlProject
            object."""
        ):
            fake_json = {
                "id": "sLYThAGFYFAUAxIL",
                "name": "Fake project",
                "description": None,
                "type": "Project",
                "model": {
                    "id": "sLYThAGFYFAUAxIL_root",
                    "name": "Fake model",
                    "description": None,
                    "type": "Package",
                    "propertyAssignments": None,
                    "contents": None,
                },
                "diagrams": [
                    {
                        "id": "3aD58QGGAqAAjA61",
                        "name": "FakeDiagramName",
                        "description": None,
                        "type": "Diagram",
                        "owner": {"id": "NaeIigGFYEGQJAx4", "type": "Package"},
                        "contents": None,
                    }
                ],
            }
            expected_class = OntoUmlProject(
                identifier=fake_json["id"],
                name=fake_json["name"],
                description=fake_json["description"],
                model=OntoUmlPackage(
                    identifier=fake_json["model"]["id"],
                    name=fake_json["model"]["name"],
                    description=fake_json["model"]["description"],
                    property_assignments=None,
                    contents=None,
                ),
                diagrams=[
                    OntoUmlDiagram(
                        identifier=fake_json["diagrams"][0]["id"],
                        name=fake_json["diagrams"][0]["name"],
                        description=fake_json["diagrams"][0]["description"],
                        owner=OntoUmlDiagramOwner(
                            identifier=fake_json["diagrams"][0]["owner"]["id"],
                            object_type=fake_json["diagrams"][0]["owner"]["type"],
                        ),
                        contents=None,
                    )
                ],
            )
            self.assertEqual(onto_uml_json_decoder._project(fake_json), expected_class)

    def test_property(self):
        """Unittest voor _property handler:
        1. Wanneer niet alle verwachte attributen aanwezig zijn in het object, dan het
           object as-is teruggeven.
        2. Wanneer wel alle verwachte attributen aanwezig zijn in het object, dan
           controleren dat deze correct zijn overgenomen in het OntoUmlRelationProperty
           object.
        """

        with self.subTest(
            """1. Wanneer niet alle verwachte attributen aanwezig zijn in het object,
            dan het object as-is teruggeven."""
        ):
            fake_json = {"id": "FakeId", "type": "Diagram", "fake": "Dummy"}
            self.assertEqual(onto_uml_json_decoder._property(fake_json), fake_json)

        with self.subTest(
            """2. Wanneer wel alle verwachte attributen aanwezig zijn in het object, dan
            controleren dat deze correct zijn overgenomen in het OntoUmlRelationProperty
            object."""
        ):
            fake_json = {
                "id": "yPj0wcGGAqAEDTfr",
                "name": None,
                "description": None,
                "type": "Property",
                "propertyAssignments": None,
                "stereotype": None,
                "isDerived": False,
                "isReadOnly": False,
                "isOrdered": False,
                "cardinality": None,
                "propertyType": {"id": "AD90wcGGAqAEDTfT", "type": "Class"},
                "subsettedProperties": None,
                "redefinedProperties": None,
                "aggregationKind": "NONE",
            }
            expected_class = OntoUmlProperty(
                identifier=fake_json["id"],
                name=fake_json["name"],
                description=fake_json["description"],
                property_assignments=None,
                stereotype=fake_json["stereotype"],
                is_derived=fake_json["isDerived"],
                is_read_only=fake_json["isReadOnly"],
                is_ordered=fake_json["isOrdered"],
                cardinality=fake_json["cardinality"],
                property_type=OntoUmlPropertyType(
                    identifier=fake_json["propertyType"]["id"],
                    related_type=fake_json["propertyType"]["type"],
                ),
                subsetted_properties=fake_json["subsettedProperties"],
                redefined_properties=fake_json["redefinedProperties"],
                aggregation_kind=fake_json["aggregationKind"],
            )
            self.assertEqual(onto_uml_json_decoder._property(fake_json), expected_class)

    def test_relation(self):
        """Unittest voor _relation handler:
        1. Wanneer niet alle verwachte attributen aanwezig zijn in het object, dan het
           object as-is teruggeven.
        2. Wanneer wel alle verwachte attributen aanwezig zijn in het object, dan
           controleren dat deze correct zijn overgenomen in het OntoUmlRelation
           object.
        """

        with self.subTest(
            """1. Wanneer niet alle verwachte attributen aanwezig zijn in het object,
            dan het object as-is teruggeven."""
        ):
            fake_json = {"id": "FakeId", "type": "Diagram", "fake": "Dummy"}
            self.assertEqual(onto_uml_json_decoder._relation(fake_json), fake_json)

        with self.subTest(
            """2. Wanneer wel alle verwachte attributen aanwezig zijn in het object, dan
            controleren dat deze correct zijn overgenomen in het OntoUmlRelation
            object."""
        ):
            fake_json = {
                "id": "SPj0wcGGAqAEDTfq",
                "name": None,
                "description": None,
                "type": "Relation",
                "propertyAssignments": None,
                "stereotype": None,
                "isAbstract": False,
                "isDerived": False,
                "properties": None,
            }
            expected_class = OntoUmlRelation(
                identifier=fake_json["id"],
                name=fake_json["name"],
                description=fake_json["description"],
                property_assignments=None,
                stereotype=fake_json["stereotype"],
                is_abstract=fake_json["isAbstract"],
                is_derived=fake_json["isDerived"],
                properties=fake_json["properties"],
            )
            self.assertEqual(onto_uml_json_decoder._relation(fake_json), expected_class)
