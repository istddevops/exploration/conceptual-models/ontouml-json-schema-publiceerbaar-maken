"""Test suites for onto_uml.onto_uml_json_decoder, views only"""

from unittest import TestCase

from src.onto_uml import onto_uml_json_decoder
from src.onto_uml.onto_uml_model_element import OntoUmlModelElement
from src.onto_uml.views.class_view import ClassView
from src.onto_uml.views.generalization_set_view import GeneralizationSetView
from src.onto_uml.views.generalization_view import GeneralizationView
from src.onto_uml.views.relation_view import RelationView
from src.onto_uml.views.shapes.path import Path, Point
from src.onto_uml.views.shapes.rectangle import Rectangle
from src.onto_uml.views.shapes.text import Text


# pylint: disable=[W0212]
class TestOntoUmlJsonDecoder(TestCase):
    """Testsuite for OntoUmlJsonDecoder methods"""

    def test_class_view(self):
        """Unittest voor _class_view handler:
        1. Wanneer niet alle verwachte attributen aanwezig zijn in het object, dan het
           object as-is teruggeven.
        2. Wanneer wel alle verwachte attributen aanwezig zijn in het object, dan
           controleren dat deze correct zijn overgenomen in het ClassView object."""

        with self.subTest(
            """1. Wanneer niet alle verwachte attributen aanwezig zijn in het object,
            dan het object as-is teruggeven."""
        ):
            fake_json = {"id": "FakeId", "type": "ClassView", "fake": "Dummy"}
            self.assertEqual(onto_uml_json_decoder._class_view(fake_json), fake_json)

        with self.subTest(
            """2. Wanneer wel alle verwachte attributen aanwezig zijn in het object, dan
           controleren dat deze correct zijn overgenomen in het ClassView object."""
        ):
            fake_json = {
                "id": "QGRcmQGGAqAAjC7X",
                "type": "ClassView",
                "modelElement": {"id": "tgtbrUGGAqAAkCQE", "type": "Class"},
                "shape": {
                    "id": "QGRcmQGGAqAAjC7X_shape",
                    "type": "Rectangle",
                    "x": 395,
                    "y": 142,
                    "width": 80,
                    "height": 40,
                },
            }
            expected_class = ClassView(
                fake_json["id"],
                OntoUmlModelElement(
                    fake_json["modelElement"]["id"], fake_json["modelElement"]["type"]
                ),
                Rectangle(
                    fake_json["shape"]["id"],
                    fake_json["shape"]["type"],
                    fake_json["shape"]["x"],
                    fake_json["shape"]["y"],
                    fake_json["shape"]["width"],
                    fake_json["shape"]["height"],
                ),
            )
            self.assertEqual(
                onto_uml_json_decoder._class_view(fake_json), expected_class
            )

    def test_generalization_set_view(self):
        """Unittest voor _generalization_set_view handler:
        1. Wanneer niet alle verwachte attributen aanwezig zijn in het object, dan het
           object as-is teruggeven.
        2. Wanneer wel alle verwachte attributen aanwezig zijn in het object, dan
           controleren dat deze correct zijn overgenomen in het GeneralizationSetView
           object.
        """

        with self.subTest(
            """1. Wanneer niet alle verwachte attributen aanwezig zijn in het object,
            dan het object as-is teruggeven."""
        ):
            fake_json = {"id": "FakeId", "type": "Diagram", "fake": "Dummy"}
            self.assertEqual(
                onto_uml_json_decoder._generalization_set_view(fake_json), fake_json
            )

        with self.subTest(
            """2. Wanneer wel alle verwachte attributen aanwezig zijn in het object, dan
            controleren dat deze correct zijn overgenomen in het
            GeneralizationSetView object."""
        ):
            fake_json = {
                "id": "xqx40gGGAqAAjAu2",
                "type": "GeneralizationSetView",
                "modelElement": {"id": "jKx40gGGAqAAjAu0", "type": "GeneralizationSet"},
                "shape": {
                    "id": "xqx40gGGAqAAjAu2_shape",
                    "type": "Text",
                    "x": 1010,
                    "y": 1188,
                    "width": 64,
                    "height": 40,
                    "value": "",
                },
            }
            expected_class = GeneralizationSetView(
                fake_json["id"],
                OntoUmlModelElement(
                    fake_json["modelElement"]["id"], fake_json["modelElement"]["type"]
                ),
                Text(
                    fake_json["shape"]["id"],
                    fake_json["shape"]["type"],
                    fake_json["shape"]["x"],
                    fake_json["shape"]["y"],
                    fake_json["shape"]["width"],
                    fake_json["shape"]["height"],
                    fake_json["shape"]["value"],
                ),
            )
            self.assertEqual(
                onto_uml_json_decoder._generalization_set_view(fake_json),
                expected_class,
            )

    def test_generalization_view(self):
        """Unittest voor _generalization_view handler:
        1. Wanneer niet alle verwachte attributen aanwezig zijn in het object, dan het
           object as-is teruggeven.
        2. Wanneer wel alle verwachte attributen aanwezig zijn in het object, dan
           controleren dat deze correct zijn overgenomen in het GeneralizationView
           object.
        """

        with self.subTest(
            """1. Wanneer niet alle verwachte attributen aanwezig zijn in het object,
            dan het object as-is teruggeven."""
        ):
            fake_json = {"id": "FakeId", "type": "Diagram", "fake": "Dummy"}
            self.assertEqual(
                onto_uml_json_decoder._generalization_view(fake_json), fake_json
            )

        with self.subTest(
            """2. Wanneer wel alle verwachte attributen aanwezig zijn in het object, dan
            controleren dat deze correct zijn overgenomen in het GeneralizationView
            object."""
        ):
            fake_json = {
                "id": "Nal1yQGGAqAAjBet",
                "type": "GeneralizationView",
                "modelElement": {"id": "LuXV8QGGAqAAjBIH", "type": "Generalization"},
                "shape": {
                    "id": "Nal1yQGGAqAAjBet_path",
                    "type": "Path",
                    "points": [{"x": 153, "y": 333}, {"x": 131, "y": 333}],
                },
                "source": {"id": "CqF1yQGGAqAAjBeW", "type": "ClassView"},
                "target": {"id": "Fal1yQGGAqAAjBer", "type": "ClassView"},
            }
            expected_class = GeneralizationView(
                fake_json["id"],
                OntoUmlModelElement(
                    fake_json["modelElement"]["id"], fake_json["modelElement"]["type"]
                ),
                Path(
                    fake_json["shape"]["id"],
                    fake_json["shape"]["type"],
                    [Point(p["x"], p["y"]) for p in fake_json["shape"]["points"]],
                ),
                OntoUmlModelElement(
                    fake_json["source"]["id"], fake_json["source"]["type"]
                ),
                OntoUmlModelElement(
                    fake_json["target"]["id"], fake_json["target"]["type"]
                ),
            )
            self.assertEqual(
                onto_uml_json_decoder._generalization_view(fake_json), expected_class
            )

    def test_path(self):
        """Unittest voor _path handler:
        1.a. Wanneer geen object is meegegeven, dan None teruggeven
        1.b. Wanneer niet alle verwachte attributen aanwezig zijn in het object, dan
             None teruggeven.
        2. Wanneer wel alle verwachte attributen aanwezig zijn in het object, dan
           controleren dat deze correct zijn overgenomen in het Path object.
        """

        with self.subTest(
            """1.a. Wanneer geen object is meegegeven, dan None teruggeven"""
        ):
            self.assertIsNone(onto_uml_json_decoder._path(None))

        with self.subTest(
            """1.b. Wanneer niet alle verwachte attributen aanwezig zijn in het object,
            dan None teruggeven."""
        ):
            fake_json = {"fake": "Dummy", "anotherFake": "Dummy"}
            self.assertIsNone(onto_uml_json_decoder._path(fake_json))

        with self.subTest(
            """2. Wanneer wel alle verwachte attributen aanwezig zijn in het object, dan
            controleren dat deze correct zijn overgenomen in het Path object."""
        ):
            fake_json = {
                "id": "sjT6mQGGAqAAjDOO_path",
                "type": "Path",
                "points": [{"x": 394, "y": 342}, {"x": 328, "y": 342}],
            }
            expected_class = Path(
                fake_json["id"],
                fake_json["type"],
                [Point(p["x"], p["y"]) for p in fake_json["points"]],
            )
            self.assertEqual(onto_uml_json_decoder._path(fake_json), expected_class)

    def test_point(self):
        """Unittest voor _point handler:
        1.a. Wanneer geen object is meegegeven, dan None teruggeven
        1.b. Wanneer niet alle verwachte attributen aanwezig zijn in het object, dan
             None teruggeven.
        2. Wanneer wel alle verwachte attributen aanwezig zijn in het object, dan
           controleren dat deze correct zijn overgenomen in het Point object.
        """

        with self.subTest(
            """1.a. Wanneer geen object is meegegeven, dan None teruggeven"""
        ):
            self.assertIsNone(onto_uml_json_decoder._point(None))

        with self.subTest(
            """1.b. Wanneer niet alle verwachte attributen aanwezig zijn in het object,
            dan None teruggeven."""
        ):
            fake_json = {"fake": "Dummy", "anotherFake": "Dummy"}
            self.assertIsNone(onto_uml_json_decoder._point(fake_json))

        with self.subTest(
            """2. Wanneer wel alle verwachte attributen aanwezig zijn in het object, dan
            controleren dat deze correct zijn overgenomen in het Point object."""
        ):
            fake_json = {
                "x": 395,
                "y": 426,
            }
            expected_class = Point(
                fake_json["x"],
                fake_json["y"],
            )
            self.assertEqual(onto_uml_json_decoder._point(fake_json), expected_class)

    def test_rectangle(self):
        """Unittest voor _rectangle handler:
        1.a. Wanneer geen object is meegegeven, dan None teruggeven
        1.b. Wanneer niet alle verwachte attributen aanwezig zijn in het object, dan
             None teruggeven.
        2. Wanneer wel alle verwachte attributen aanwezig zijn in het object, dan
           controleren dat deze correct zijn overgenomen in het Rectangle object.
        """

        with self.subTest(
            """1.a. Wanneer geen object is meegegeven, dan None teruggeven"""
        ):
            self.assertIsNone(onto_uml_json_decoder._rectangle(None))

        with self.subTest(
            """1.b. Wanneer niet alle verwachte attributen aanwezig zijn in het object,
            dan None teruggeven."""
        ):
            fake_json = {"fake": "Dummy", "anotherFake": "Dummy"}
            self.assertIsNone(onto_uml_json_decoder._rectangle(fake_json))

        with self.subTest(
            """2. Wanneer wel alle verwachte attributen aanwezig zijn in het object, dan
            controleren dat deze correct zijn overgenomen in het Rectangle object."""
        ):
            fake_json = {
                "id": "eNdxKQGGAqAAjCBq_shape",
                "type": "Rectangle",
                "x": 395,
                "y": 426,
                "width": 80,
                "height": 40,
            }
            expected_class = Rectangle(
                fake_json["id"],
                fake_json["type"],
                fake_json["x"],
                fake_json["y"],
                fake_json["width"],
                fake_json["height"],
            )
            self.assertEqual(
                onto_uml_json_decoder._rectangle(fake_json), expected_class
            )

    def test_relation_view(self):
        """Unittest voor _relation_view handler:
        1. Wanneer niet alle verwachte attributen aanwezig zijn in het object, dan het
           object as-is teruggeven.
        2. Wanneer wel alle verwachte attributen aanwezig zijn in het object, dan
           controleren dat deze correct zijn overgenomen in het RelationView
           object.
        """

        with self.subTest(
            """1. Wanneer niet alle verwachte attributen aanwezig zijn in het object,
            dan het object as-is teruggeven."""
        ):
            fake_json = {"id": "FakeId", "type": "Diagram", "fake": "Dummy"}
            self.assertEqual(onto_uml_json_decoder._relation_view(fake_json), fake_json)

        with self.subTest(
            """2. Wanneer wel alle verwachte attributen aanwezig zijn in het object, dan
            controleren dat deze correct zijn overgenomen in het RelationView
            object."""
        ):
            fake_json = {
                "id": "sjT6mQGGAqAAjDOO",
                "type": "RelationView",
                "modelElement": {"id": "XDT6mQGGAqAAjDOJ", "type": "Relation"},
                "shape": {
                    "id": "sjT6mQGGAqAAjDOO_path",
                    "type": "Path",
                    "points": [{"x": 394, "y": 342}, {"x": 328, "y": 342}],
                },
                "source": {"id": "cM6xKQGGAqAAjB_6", "type": "ClassView"},
                "target": {"id": "r7VxKQGGAqAAjCBM", "type": "ClassView"},
            }
            expected_class = RelationView(
                fake_json["id"],
                OntoUmlModelElement(
                    fake_json["modelElement"]["id"], fake_json["modelElement"]["type"]
                ),
                Path(
                    fake_json["shape"]["id"],
                    fake_json["shape"]["type"],
                    [Point(p["x"], p["y"]) for p in fake_json["shape"]["points"]],
                ),
                OntoUmlModelElement(
                    fake_json["source"]["id"], fake_json["source"]["type"]
                ),
                OntoUmlModelElement(
                    fake_json["target"]["id"], fake_json["target"]["type"]
                ),
            )
            self.assertEqual(
                onto_uml_json_decoder._relation_view(fake_json), expected_class
            )

    def test_text(self):
        """Unittest voor _text handler:
        1.a. Wanneer geen object is meegegeven, dan None teruggeven
        1.b. Wanneer niet alle verwachte attributen aanwezig zijn in het object, dan
             None teruggeven.
        2. Wanneer wel alle verwachte attributen aanwezig zijn in het object, dan
           controleren dat deze correct zijn overgenomen in het Text object.
        """

        with self.subTest(
            """1.a. Wanneer geen object is meegegeven, dan None teruggeven"""
        ):
            self.assertIsNone(onto_uml_json_decoder._text(None))

        with self.subTest(
            """1.b. Wanneer niet alle verwachte attributen aanwezig zijn in het object,
            dan None teruggeven."""
        ):
            fake_json = {"fake": "Dummy", "anotherFake": "Dummy"}
            self.assertIsNone(onto_uml_json_decoder._text(fake_json))

        with self.subTest(
            """2. Wanneer wel alle verwachte attributen aanwezig zijn in het object, dan
            controleren dat deze correct zijn overgenomen in het Rectangle object."""
        ):
            fake_json = {
                "id": "eNdxKQGGAqAAjCBq_shape",
                "type": "Rectangle",
                "x": 395,
                "y": 426,
                "width": 80,
                "height": 40,
                "value": "FakeText",
            }
            expected_class = Text(
                fake_json["id"],
                fake_json["type"],
                fake_json["x"],
                fake_json["y"],
                fake_json["width"],
                fake_json["height"],
                fake_json["value"],
            )
            self.assertEqual(onto_uml_json_decoder._text(fake_json), expected_class)
