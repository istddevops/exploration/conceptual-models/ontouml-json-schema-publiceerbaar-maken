"""Test suites for onto_uml.onto_uml_generalization_set"""


from copy import deepcopy
from unittest import TestCase
from unittest.mock import patch

from src.onto_uml.onto_uml_generalization_set import (
    OntoUmlGeneralizationSet,
    OntoUmlGeneralizationSetItem,
)
from src.onto_uml.onto_uml_property_assignments import OntoUmlPropertyAssignments
from src.unittests.helpers.invariant_generator import (
    invariant_does_not_equal_other,
    invariant_equals_other,
    setup_equals_matrix,
)


class TestOntoUmlGeneralizationSetItem(TestCase):
    """
    Test suite for OntoUML generalization set item interface
    - Constructor (__init__) is not tested as it simply assigns arguments to members
    - comparer (__eq__) is tested
    - properties are not tested as they simply return the hidden member values, except
      * generalization property
    """

    def test_eq(self):
        """
        Test the comparison method.
        1. Other is None -> return False
        | #  | identifier equal? | item_type equal? | generalization equal? | result |
        | 2. | yes               | yes              | yes                   | True   |
        | 3. | no                | yes              | yes                   | False  |
        | 4. | yes               | no               | yes                   | False  |
        | 5. | no                | no               | yes                   | False  |
        | 6. | yes               | yes              | no                    | False  |
        | 7. | no                | yes              | no                    | False  |
        | 8. | yes               | no               | no                    | False  |
        | 9. | no                | no               | no                    | False  |
        """

        testcases = setup_equals_matrix(["identifier", "item_type", "_generalization"])

        template: OntoUmlGeneralizationSetItem = OntoUmlGeneralizationSetItem(
            identifier="FakeId",
            item_type="FakeType",
            generalization="FakeGeneralization",
        )

        with self.subTest(
            """
            1. Other is None -> return False
            """
        ):
            first = deepcopy(template)
            other: OntoUmlGeneralizationSetItem = None
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        for testcase in testcases:
            with self.subTest(testcase.description):
                # First, setup the testcase
                first = deepcopy(template)
                other = deepcopy(template)
                if all(
                    item.invariant == invariant_equals_other
                    for item in testcase.single_invariants
                ):
                    # No need to change any attributes, just compare the two and assert
                    # they are equal
                    self.assertTrue(first == other)
                    self.assertEqual(first, other)
                else:
                    # First, change the different attributes, then compare the two and
                    # assert they are not equal
                    for testable in [
                        item
                        for item in testcase.single_invariants
                        if item.invariant == invariant_does_not_equal_other
                    ]:
                        worker = getattr(other, testable.condition.identifier)
                        worker = f"Another{worker}"
                        setattr(other, testable.condition.identifier, worker)
                    self.assertFalse(first == other)
                    self.assertNotEqual(first, other)

    def test_generalization(self):
        """
        The `generalization` property returns the `_generalization` member.
        This member is optionally filled in the constructor.
        If it is not set (None), then it'll be looked up in the `_dict` member by `_id`,
        and then set in `_generalization`. If it is set, then the set value is returned.

        Testcases:
        | #  | _generalization set | _id in _dict.keys | _generalization reset |
        | 1. | yes                 | yes               | no                    |
        | 2. | no                  | yes               | yes                   |
        | 3. | yes                 | no                | no                    |
        | 4. | no                  | no                | no                    |
        """

        fake_reference = "FakeGeneralizationReference"
        orig_reference = "FakeGeneralization"
        with self.subTest(
            """
            | #  | _generalization set | _id in _dict.keys | _generalization reset |
            | 1. | yes                 | yes               | no                    |
            """
        ):
            testee: OntoUmlGeneralizationSetItem = OntoUmlGeneralizationSetItem(
                identifier="FakeId", item_type="FakeType", generalization=orig_reference
            )
            # pylint: disable=[W0212, W0104]
            with patch.dict(testee._dict, values={"FakeId": fake_reference}):
                test_value = testee.generalization
                self.assertNotEqual(test_value, fake_reference)
                self.assertEqual(test_value, orig_reference)

        with self.subTest(
            """
            | #  | _generalization set | _id in _dict.keys | _generalization reset |
            | 2. | no                  | yes               | yes                   |
            """
        ):
            testee: OntoUmlGeneralizationSetItem = OntoUmlGeneralizationSetItem(
                identifier="FakeId", item_type="FakeType"
            )
            # pylint: disable=[W0212, W0104]
            with patch.dict(testee._dict, values={"FakeId": fake_reference}):
                test_value = testee.generalization
                self.assertEqual(test_value, fake_reference)

        with self.subTest(
            """
            | #  | _generalization set | _id in _dict.keys | _generalization reset |
            | 3. | yes                 | no                | no                    |
            """
        ):
            testee: OntoUmlGeneralizationSetItem = OntoUmlGeneralizationSetItem(
                identifier="FakeId",
                item_type="FakeType",
                generalization="FakeGeneralization",
            )
            # pylint: disable=[W0212, W0104]
            with patch.dict(testee._dict, values={"AnotherFakeId": fake_reference}):
                test_value = testee.generalization
                self.assertEqual(test_value, orig_reference)

        with self.subTest(
            """
            | #  | _generalization set | _id in _dict.keys | _generalization reset |
            | 4. | no                  | no                | no                    |
            """
        ):
            testee: OntoUmlGeneralizationSetItem = OntoUmlGeneralizationSetItem(
                identifier="FakeId", item_type="FakeType"
            )
            # pylint: disable=[W0212, W0104]
            with patch.dict(testee._dict, values={"AnotherFakeId": fake_reference}):
                test_value = testee.generalization
                self.assertIsNone(test_value)


class TestOntoUmlGeneralizationSet(TestCase):
    """
    Test suite for OntoUML generalization set interface
    - Constructor (__init__) is not tested as it simply assigns arguments to members
    - comparer (__eq__) is tested
    - properties are not tested as they simply return the hidden member values
    """

    # pylint:disable=[R0915]
    def test_eq(self):
        """
        Test the comparison method.
         1. Other is None -> return False
         2. Inherited members equal, specific items equal -> True
         3. One inherited member differs, specific items equal -> False
         4. Multiple inherited members differ, specific items equal -> False
         5. All inherited members differ, specific items equal -> False
         6. Inherited members equal, one specific item differs -> False
         7. One inherited member differs, one specific item differs -> False
         8. Multiple inherited members differ, one specific item differs -> False
         9. All inherited members differ, one specific item differs -> False
        10. Inherited members equal, multiple specific items differ -> False
        11. One inherited member differs, multiple specific items differ -> False
        12. Multiple inherited members differ, multiple specific items differ -> False
        13. All inherited members differ, multiple specific items differ -> False
        14. Inherited members equal, all specific items differ -> False
        15. One inherited member differs, all specific items differ -> False
        16. Multiple inherited members differ, all specific items differ -> False
        17. All inherited members differ, all specific items differ -> False
        """

        template: OntoUmlGeneralizationSet = OntoUmlGeneralizationSet(
            identifier="FakeId",
            name="FakeName",
            description="FakeDescription",
            is_complete=True,
            categorizer="FakeCategorizer",
            generalizations=[
                OntoUmlGeneralizationSetItem(
                    "FakeItemId_1", "FakeItemType_1", "FakeGeneralization_1"
                ),
                OntoUmlGeneralizationSetItem(
                    "FakeItemId_2", "FakeItemType_2", "FakeGeneralization_2"
                ),
                OntoUmlGeneralizationSetItem(
                    "FakeItemId_3", "FakeItemType_4", "FakeGeneralization_3"
                ),
            ],
        )

        with self.subTest(
            """
             1. Other is None -> return False
            """
        ):
            first = deepcopy(template)
            other: OntoUmlGeneralizationSet = None
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
             2. Inherited members equal, specific items equal -> True
            """
        ):
            first = deepcopy(template)
            other = deepcopy(template)
            self.assertTrue(first == other)
            self.assertEqual(first, other)

        with self.subTest(
            """
             3. One inherited member differs, specific items equal -> False
            """
        ):
            first = deepcopy(template)
            other = deepcopy(template)
            other.description = "OtherFakeDescription"
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
             4. Multiple inherited members differ, specific items equal -> False
            """
        ):
            first = deepcopy(template)
            other = deepcopy(template)
            other.identifier = "OtherFakeId"
            other.description = "OtherFakeDescription"
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
             5. All inherited members differ, specific items equal -> False
            """
        ):
            first = deepcopy(template)
            other = deepcopy(template)
            other.identifier = "OtherFakeId"
            other.name = "OtherFakeName"
            other.description = "OtherFakeDescription"
            other.property_assignments = OntoUmlPropertyAssignments(-1)
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
             6. Inherited members equal, one specific item differs -> False
            """
        ):
            first = deepcopy(template)
            other = deepcopy(template)
            other.is_disjoint = not other.is_disjoint
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
             7. One inherited member differs, one specific item differs -> False
            """
        ):
            first = deepcopy(template)
            other = deepcopy(template)
            other.description = "AnotherFakeDescription"
            other.is_disjoint = not other.is_disjoint
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
             8. Multiple inherited members differ, one specific item differs -> False
            """
        ):
            first = deepcopy(template)
            other = deepcopy(template)
            other.identifier = "OtherFakeId"
            other.description = "OtherFakeDescription"
            other.is_disjoint = not other.is_disjoint
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
             9. All inherited members differ, one specific item differs -> False
            """
        ):
            first = deepcopy(template)
            other = deepcopy(template)
            other.identifier = "OtherFakeId"
            other.name = "OtherFakeName"
            other.description = "OtherFakeDescription"
            other.property_assignments = OntoUmlPropertyAssignments(-1)
            other.is_disjoint = not other.is_disjoint
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
            10. Inherited members equal, multiple specific items differ -> False
            """
        ):
            first = deepcopy(template)
            other = deepcopy(template)
            other.is_disjoint = not other.is_disjoint
            other.categorizer = "AnotherFakeCategorizer"
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
            11. One inherited member differs, multiple specific items differ -> False
            """
        ):
            first = deepcopy(template)
            other = deepcopy(template)
            other.description = "AnotherFakeDescription"
            other.is_disjoint = not other.is_disjoint
            other.categorizer = "AnotherFakeCategorizer"
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
            12. Multiple inherited members differ, multiple specific items differ
                -> False
            """
        ):
            first = deepcopy(template)
            other = deepcopy(template)
            other.identifier = "OtherFakeId"
            other.description = "OtherFakeDescription"
            other.is_disjoint = not other.is_disjoint
            other.categorizer = "AnotherFakeCategorizer"
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
            13. All inherited members differ, multiple specific items differ -> False
            """
        ):
            first = deepcopy(template)
            other = deepcopy(template)
            other.identifier = "OtherFakeId"
            other.name = "OtherFakeName"
            other.description = "OtherFakeDescription"
            other.property_assignments = OntoUmlPropertyAssignments(-1)
            other.is_disjoint = not other.is_disjoint
            other.categorizer = "AnotherFakeCategorizer"
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
            14. Inherited members equal, all specific items differ -> False
            """
        ):
            first = deepcopy(template)
            other = deepcopy(template)
            other.is_disjoint = not other.is_disjoint
            other.is_complete = not other.is_complete
            other.categorizer = "AnotherFakeCategorizer"
            other.generalizations = [
                OntoUmlGeneralizationSetItem(
                    "FakeItemId_4", "FakeItemType_4", "FakeGeneralization_4"
                ),
                OntoUmlGeneralizationSetItem(
                    "FakeItemId_5", "FakeItemType_5", "FakeGeneralization_5"
                ),
            ]
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
            15. One inherited member differs, all specific items differ -> False
            """
        ):
            first = deepcopy(template)
            other = deepcopy(template)
            other.description = "AnotherFakeDescription"
            other.is_disjoint = not other.is_disjoint
            other.is_complete = not other.is_complete
            other.categorizer = "AnotherFakeCategorizer"
            other.generalizations = [
                OntoUmlGeneralizationSetItem(
                    "FakeItemId_4", "FakeItemType_4", "FakeGeneralization_4"
                ),
                OntoUmlGeneralizationSetItem(
                    "FakeItemId_5", "FakeItemType_5", "FakeGeneralization_5"
                ),
            ]
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
            16. Multiple inherited members differ, all specific items differ -> False
            """
        ):
            first = deepcopy(template)
            other = deepcopy(template)
            other.identifier = "OtherFakeId"
            other.description = "OtherFakeDescription"
            other.is_disjoint = not other.is_disjoint
            other.is_complete = not other.is_complete
            other.categorizer = "AnotherFakeCategorizer"
            other.generalizations = [
                OntoUmlGeneralizationSetItem(
                    "FakeItemId_4", "FakeItemType_4", "FakeGeneralization_4"
                ),
                OntoUmlGeneralizationSetItem(
                    "FakeItemId_5", "FakeItemType_5", "FakeGeneralization_5"
                ),
            ]
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
            17. All inherited members differ, all specific items differ -> False
            """
        ):
            first = deepcopy(template)
            other = deepcopy(template)
            other.identifier = "OtherFakeId"
            other.name = "OtherFakeName"
            other.description = "OtherFakeDescription"
            other.property_assignments = OntoUmlPropertyAssignments(-1)
            other.is_disjoint = not other.is_disjoint
            other.is_complete = not other.is_complete
            other.categorizer = "AnotherFakeCategorizer"
            other.generalizations = [
                OntoUmlGeneralizationSetItem(
                    "FakeItemId_4", "FakeItemType_4", "FakeGeneralization_4"
                ),
                OntoUmlGeneralizationSetItem(
                    "FakeItemId_5", "FakeItemType_5", "FakeGeneralization_5"
                ),
            ]
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)
