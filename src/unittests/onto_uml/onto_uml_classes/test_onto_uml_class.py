"""Test suites for onto_uml.onto_uml_class"""

from copy import deepcopy
from unittest import TestCase
from unittest.mock import mock_open, patch

from src.onto_uml.onto_uml_class import OntoUmlClass
from src.onto_uml_json_importer import _import_onto_uml_json_project


class TestOntoUmlClass(TestCase):
    """
    Test suite for OntoUML class interface
    - Constructor (__init__) is not tested as it simply assigns arguments to members
    - comparer (__eq__) is tested
    - properties are not tested as they simply return the hidden member values
    """

    def test_eq(self):
        """
        Test the comparison method.
        1. The other OntoUMLClass is None -> return False
        2. The super equals other.super, and all specific members are equal too
           -> return True
        3. The super equals other.super, and one specific member is not equal -> return
           False
        4. The super equals other.super, and more specific members are not equal ->
           return False
        5. The super does not equal other.super, but all specific members are equal
           -> return False
        6. The super does not equal other.super, and one specific member is not equal
           -> return False
        7. The super does not equal other.super, and more specific members are not
           equal -> return False
        """

        template: OntoUmlClass = OntoUmlClass(
            identifier="FakeId",
            name="FakeName",
            description=None,
            property_assignments=None,
            stereotype="FakeStereoType",
            is_abstract=False,
            is_derived=True,
            properties=None,
            is_extensional=False,
            is_powertype=True,
            order=None,
            literals=None,
            restricted_to=None,
        )
        with self.subTest(
            """
            1. The other OntoUMLClass is None -> return False
            """
        ):
            first = deepcopy(template)
            second: OntoUmlClass = None
            self.assertFalse(first == second)
            self.assertNotEqual(first, second)

        with self.subTest(
            """
            2. The super equals other.super, and all specific members are equal too
               -> return True
            """
        ):
            first = deepcopy(template)
            second = deepcopy(template)
            self.assertTrue(first == second)
            self.assertEqual(first, second)

        with self.subTest(
            """
            3. The super equals other.super, and one specific member is not equal
               -> return False
            """
        ):
            first = deepcopy(template)
            second = deepcopy(template)
            second.is_extensional = not second.is_extensional  # Flip the value
            self.assertFalse(first == second)
            self.assertNotEqual(first, second)

        with self.subTest(
            """
            4. The super equals other.super, and more specific members are not equal ->
               return False
            """
        ):
            first = deepcopy(template)
            second = deepcopy(template)
            second.is_extensional = not second.is_extensional  # Flip the value
            second.order = "FakeOrder"
            self.assertFalse(first == second)
            self.assertNotEqual(first, second)

        with self.subTest(
            """
            5. The super does not equal other.super, but all specific members are equal
               -> return False
            """
        ):
            first = deepcopy(template)
            second = deepcopy(template)
            second.description = "FakeDescription"
            self.assertFalse(first == second)
            self.assertNotEqual(first, second)

        with self.subTest(
            """
            6. The super does not equal other.super, and one specific member is not
               equal -> return False
            """
        ):
            first = deepcopy(template)
            second = deepcopy(template)
            second.description = "FakeDescription"
            second.is_powertype = not second.is_powertype
            self.assertFalse(first == second)
            self.assertNotEqual(first, second)

        with self.subTest(
            """
            7. The super does not equal other.super, and more specific members are not
               equal -> return False
            """
        ):
            first = deepcopy(template)
            second = deepcopy(template)
            second.description = "FakeDescription"
            second.is_powertype = not second.is_powertype
            second.literals = ["FakeLiteral"]
            self.assertFalse(first == second)
            self.assertNotEqual(first, second)

    # pylint:disable=[R0915]
    def test_html_color_code(self):
        """
        Test for the html_color_code property.
        - <No stereotype> -> None
        - <<kind>>, <<quantity>> -> FF99A3 (roze)
        - <<subkind>>, <<role>>, <<category>> welke GEEN generalization is van
          <<relator>> -> FFDADD (lichtroze)
        - <<mode>>, <<quality>> -> 70D7FF (blauw)
        - <<phase>> welke specification is van <<mode>> of <<quality>>
          -> C0EDFF (lichtblauw)
        - <<relator>> -> 99FF99 (groen)
        - <<phase>> welke specification is van <<relator>>, <<category>> welke
          generalization is van <<relator>> -> D3FFD3 (lichtgroen)
        - Ander stereotype -> None
        """

        template_class = OntoUmlClass(
            identifier="FakeClassId",
            name="FakeClassName",
            stereotype=None,
            is_abstract=False,
            is_derived=False,
            is_extensional=False,
            is_powertype=False,
        )

        # First we test the cases without the need for generalizations.
        with self.subTest("""- <No stereotype> (None) -> None"""):
            testee = deepcopy(template_class)
            testee.stereotype = None
            self.assertIsNone(testee.html_color_code)

        with self.subTest("""- <No stereotype> ("") -> None"""):
            testee = deepcopy(template_class)
            testee.stereotype = ""
            self.assertIsNone(testee.html_color_code)

        with self.subTest("""- <<kind>> -> FF99A3 (roze)"""):
            expected = "FF99A3"
            testee = deepcopy(template_class)
            testee.stereotype = "kind"
            self.assertEqual(testee.html_color_code, expected)

        with self.subTest("""- <<quantity>> -> FF99A3 (roze)"""):
            expected = "FF99A3"
            testee = deepcopy(template_class)
            testee.stereotype = "quantity"
            self.assertEqual(testee.html_color_code, expected)

        with self.subTest("""- <<subkind>> -> FFDADD (lichtroze)"""):
            expected = "FFDADD"
            testee = deepcopy(template_class)
            testee.stereotype = "subkind"
            self.assertEqual(testee.html_color_code, expected)

        with self.subTest("""- <<role>> -> FFDADD (lichtroze)"""):
            expected = "FFDADD"
            testee = deepcopy(template_class)
            testee.stereotype = "role"
            self.assertEqual(testee.html_color_code, expected)

        with self.subTest("""<<mode>> -> 70D7FF (blauw)"""):
            expected = "70D7FF"
            testee = deepcopy(template_class)
            testee.stereotype = "mode"
            self.assertEqual(testee.html_color_code, expected)

        with self.subTest("""<<quality>> -> 70D7FF (blauw)"""):
            expected = "70D7FF"
            testee = deepcopy(template_class)
            testee.stereotype = "quality"
            self.assertEqual(testee.html_color_code, expected)

        with self.subTest("""<<relator>> -> 99FF99 (groen)"""):
            expected = "99FF99"
            testee = deepcopy(template_class)
            testee.stereotype = "relator"
            self.assertEqual(testee.html_color_code, expected)

        with self.subTest("""Ander stereotype -> None"""):
            testee = deepcopy(template_class)
            testee.stereotype = "mechanic"
            self.assertIsNone(testee.html_color_code)

        # Finally we test the cases for which we need generalizations.

        def import_fake_json(fake_json):
            with patch("builtins.open", mock_open(read_data=fake_json)):
                return _import_onto_uml_json_project("fake.json")

        # pylint:disable=[W0212]
        with self.subTest(
            """
            <<category>> welke GEEN generalization is van <<relator>> -> FFDADD
            (lichtroze)
            Hier testen we de <<category>> als generalization van <<kind>>.
            """
        ):
            fake_json = """
{
    "id" : "FakeJsonId",
    "name" : "FakeJsonName",
    "description": null,
    "type" : "Project",
    "model": {
        "id" : "FakePackageId",
        "name" : "FakePackageName",
        "description": null,
        "type" : "Package",
        "propertyAssignments": null,
        "contents": [
            {
                "id" : "FakeClassId_1",
                "name" : "FakeClassName_1",
                "description": null,
                "type" : "Class",
                "propertyAssignments": null,
                "stereotype": "category",
                "isAbstract": false,
                "isDerived": false,
                "properties": null,
                "isExtensional": null,
                "isPowertype": null,
                "order": null,
                "literals": null,
                "restrictedTo": null
            },
            {
                "id" : "FakeClassId_2",
                "name" : "FakeClassName_2",
                "description": null,
                "type" : "Class",
                "propertyAssignments": null,
                "stereotype": "kind",
                "isAbstract": false,
                "isDerived": false,
                "properties": null,
                "isExtensional": null,
                "isPowertype": null,
                "order": null,
                "literals": null,
                "restrictedTo": null
            },
            {
                "id" : "FakeGeneralizationId",
                "name" : null,
                "description" : null,
                "type" : "Generalization",
                "propertyAssignments" : null,
                "general" : {
                    "id" : "FakeClassId_1",
                    "type" : "Class"
                },
                "specific" : {
                    "id" : "FakeClassId_2",
                    "type" : "Class"
                }
            }
        ]
    },
    "diagrams" : []
}
"""

            project = import_fake_json(fake_json)
            testee: OntoUmlClass = [
                item
                for item in project.model.contents
                if (
                    isinstance(item, OntoUmlClass)
                    and "FakeClassId_1" == item.identifier
                )
            ][0]
            expected = "FFDADD"
            self.assertEqual(testee.html_color_code, expected)

        # pylint:disable=[W0212]
        with self.subTest(
            """
            <<category>> welke GEEN generalization is van <<relator>> -> FFDADD
            (lichtroze)
            Hier testen we de <<category>> als generalization van <<mode>>.
            """
        ):
            fake_json = """
{
    "id" : "FakeJsonId",
    "name" : "FakeJsonName",
    "description": null,
    "type" : "Project",
    "model": {
        "id" : "FakePackageId",
        "name" : "FakePackageName",
        "description": null,
        "type" : "Package",
        "propertyAssignments": null,
        "contents": [
            {
                "id" : "FakeClassId_1",
                "name" : "FakeClassName_1",
                "description": null,
                "type" : "Class",
                "propertyAssignments": null,
                "stereotype": "category",
                "isAbstract": false,
                "isDerived": false,
                "properties": null,
                "isExtensional": null,
                "isPowertype": null,
                "order": null,
                "literals": null,
                "restrictedTo": null
            },
            {
                "id" : "FakeClassId_2",
                "name" : "FakeClassName_2",
                "description": null,
                "type" : "Class",
                "propertyAssignments": null,
                "stereotype": "mode",
                "isAbstract": false,
                "isDerived": false,
                "properties": null,
                "isExtensional": null,
                "isPowertype": null,
                "order": null,
                "literals": null,
                "restrictedTo": null
            },
            {
                "id" : "FakeGeneralizationId",
                "name" : null,
                "description" : null,
                "type" : "Generalization",
                "propertyAssignments" : null,
                "general" : {
                    "id" : "FakeClassId_1",
                    "type" : "Class"
                },
                "specific" : {
                    "id" : "FakeClassId_2",
                    "type" : "Class"
                }
            }
        ]
    },
    "diagrams" : []
}
"""

            project = import_fake_json(fake_json)
            testee: OntoUmlClass = [
                item
                for item in project.model.contents
                if (
                    isinstance(item, OntoUmlClass)
                    and "FakeClassId_1" == item.identifier
                )
            ][0]
            expected = "FFDADD"
            self.assertEqual(testee.html_color_code, expected)

        # pylint:disable=[W0212]
        with self.subTest(
            """
            <<category>> welke GEEN generalization is van <<relator>> -> FFDADD
            (lichtroze)
            Hier testen we de <<category>> als losse class.
            """
        ):
            fake_json = """
{
    "id" : "FakeJsonId",
    "name" : "FakeJsonName",
    "description": null,
    "type" : "Project",
    "model": {
        "id" : "FakePackageId",
        "name" : "FakePackageName",
        "description": null,
        "type" : "Package",
        "propertyAssignments": null,
        "contents": [
            {
                "id" : "FakeClassId_1",
                "name" : "FakeClassName_1",
                "description": null,
                "type" : "Class",
                "propertyAssignments": null,
                "stereotype": "category",
                "isAbstract": false,
                "isDerived": false,
                "properties": null,
                "isExtensional": null,
                "isPowertype": null,
                "order": null,
                "literals": null,
                "restrictedTo": null
            },
            {
                "id" : "FakeClassId_2",
                "name" : "FakeClassName_2",
                "description": null,
                "type" : "Class",
                "propertyAssignments": null,
                "stereotype": "mode",
                "isAbstract": false,
                "isDerived": false,
                "properties": null,
                "isExtensional": null,
                "isPowertype": null,
                "order": null,
                "literals": null,
                "restrictedTo": null
            }
        ]
    },
    "diagrams" : []
}
"""

            project = import_fake_json(fake_json)
            testee: OntoUmlClass = [
                item
                for item in project.model.contents
                if (
                    isinstance(item, OntoUmlClass)
                    and "FakeClassId_1" == item.identifier
                )
            ][0]
            expected = "FFDADD"
            self.assertEqual(testee.html_color_code, expected)

        # pylint:disable=[W0212]
        with self.subTest(
            """
            <<category>> welke generalization is van <<quantity>> -> FFDADD (lichtroze)
            """
        ):
            fake_json = """
{
    "id" : "FakeJsonId",
    "name" : "FakeJsonName",
    "description": null,
    "type" : "Project",
    "model": {
        "id" : "FakePackageId",
        "name" : "FakePackageName",
        "description": null,
        "type" : "Package",
        "propertyAssignments": null,
        "contents": [
            {
                "id" : "FakeClassId_1",
                "name" : "FakeClassName_1",
                "description": null,
                "type" : "Class",
                "propertyAssignments": null,
                "stereotype": "category",
                "isAbstract": false,
                "isDerived": false,
                "properties": null,
                "isExtensional": null,
                "isPowertype": null,
                "order": null,
                "literals": null,
                "restrictedTo": null
            },
            {
                "id" : "FakeClassId_2",
                "name" : "FakeClassName_2",
                "description": null,
                "type" : "Class",
                "propertyAssignments": null,
                "stereotype": "quantity",
                "isAbstract": false,
                "isDerived": false,
                "properties": null,
                "isExtensional": null,
                "isPowertype": null,
                "order": null,
                "literals": null,
                "restrictedTo": null
            },
            {
                "id" : "FakeGeneralizationId",
                "name" : null,
                "description" : null,
                "type" : "Generalization",
                "propertyAssignments" : null,
                "general" : {
                    "id" : "FakeClassId_1",
                    "type" : "Class"
                },
                "specific" : {
                    "id" : "FakeClassId_2",
                    "type" : "Class"
                }
            }
        ]
    },
    "diagrams" : []
}
"""

            project = import_fake_json(fake_json)
            testee: OntoUmlClass = [
                item
                for item in project.model.contents
                if (
                    isinstance(item, OntoUmlClass)
                    and "FakeClassId_1" == item.identifier
                )
            ][0]
            expected = "FFDADD"
            self.assertEqual(testee.html_color_code, expected)

        # pylint:disable=[W0212]
        with self.subTest(
            """
            <<phase>> welke specification is van <<mode>> -> C0EDFF (lichtblauw)
            """
        ):
            fake_json = """
{
    "id" : "FakeJsonId",
    "name" : "FakeJsonName",
    "description": null,
    "type" : "Project",
    "model": {
        "id" : "FakePackageId",
        "name" : "FakePackageName",
        "description": null,
        "type" : "Package",
        "propertyAssignments": null,
        "contents": [
            {
                "id" : "FakeClassId_1",
                "name" : "FakeClassName_1",
                "description": null,
                "type" : "Class",
                "propertyAssignments": null,
                "stereotype": "mode",
                "isAbstract": false,
                "isDerived": false,
                "properties": null,
                "isExtensional": null,
                "isPowertype": null,
                "order": null,
                "literals": null,
                "restrictedTo": null
            },
            {
                "id" : "FakeClassId_2",
                "name" : "FakeClassName_2",
                "description": null,
                "type" : "Class",
                "propertyAssignments": null,
                "stereotype": "phase",
                "isAbstract": false,
                "isDerived": false,
                "properties": null,
                "isExtensional": null,
                "isPowertype": null,
                "order": null,
                "literals": null,
                "restrictedTo": null
            },
            {
                "id" : "FakeGeneralizationId",
                "name" : null,
                "description" : null,
                "type" : "Generalization",
                "propertyAssignments" : null,
                "general" : {
                    "id" : "FakeClassId_1",
                    "type" : "Class"
                },
                "specific" : {
                    "id" : "FakeClassId_2",
                    "type" : "Class"
                }
            }
        ]
    },
    "diagrams" : []
}
"""

            project = import_fake_json(fake_json)
            testee: OntoUmlClass = [
                item
                for item in project.model.contents
                if (
                    isinstance(item, OntoUmlClass)
                    and "FakeClassId_2" == item.identifier
                )
            ][0]
            expected = "C0EDFF"
            self.assertEqual(testee.html_color_code, expected)

        # pylint:disable=[W0212]
        with self.subTest(
            """
            <<phase>> welke specification is van <<quality>> -> C0EDFF (lichtblauw)
            """
        ):
            fake_json = """
{
    "id" : "FakeJsonId",
    "name" : "FakeJsonName",
    "description": null,
    "type" : "Project",
    "model": {
        "id" : "FakePackageId",
        "name" : "FakePackageName",
        "description": null,
        "type" : "Package",
        "propertyAssignments": null,
        "contents": [
            {
                "id" : "FakeClassId_1",
                "name" : "FakeClassName_1",
                "description": null,
                "type" : "Class",
                "propertyAssignments": null,
                "stereotype": "quality",
                "isAbstract": false,
                "isDerived": false,
                "properties": null,
                "isExtensional": null,
                "isPowertype": null,
                "order": null,
                "literals": null,
                "restrictedTo": null
            },
            {
                "id" : "FakeClassId_2",
                "name" : "FakeClassName_2",
                "description": null,
                "type" : "Class",
                "propertyAssignments": null,
                "stereotype": "phase",
                "isAbstract": false,
                "isDerived": false,
                "properties": null,
                "isExtensional": null,
                "isPowertype": null,
                "order": null,
                "literals": null,
                "restrictedTo": null
            },
            {
                "id" : "FakeGeneralizationId",
                "name" : null,
                "description" : null,
                "type" : "Generalization",
                "propertyAssignments" : null,
                "general" : {
                    "id" : "FakeClassId_1",
                    "type" : "Class"
                },
                "specific" : {
                    "id" : "FakeClassId_2",
                    "type" : "Class"
                }
            }
        ]
    },
    "diagrams" : []
}
"""

            project = import_fake_json(fake_json)
            testee: OntoUmlClass = [
                item
                for item in project.model.contents
                if (
                    isinstance(item, OntoUmlClass)
                    and "FakeClassId_2" == item.identifier
                )
            ][0]
            expected = "C0EDFF"
            self.assertEqual(testee.html_color_code, expected)

        # pylint:disable=[W0212]
        with self.subTest(
            """
            <<phase>> welke specification is van <<relator>> -> D3FFD3 (lichtgroen)
            """
        ):
            fake_json = """
{
    "id" : "FakeJsonId",
    "name" : "FakeJsonName",
    "description": null,
    "type" : "Project",
    "model": {
        "id" : "FakePackageId",
        "name" : "FakePackageName",
        "description": null,
        "type" : "Package",
        "propertyAssignments": null,
        "contents": [
            {
                "id" : "FakeClassId_1",
                "name" : "FakeClassName_1",
                "description": null,
                "type" : "Class",
                "propertyAssignments": null,
                "stereotype": "relator",
                "isAbstract": false,
                "isDerived": false,
                "properties": null,
                "isExtensional": null,
                "isPowertype": null,
                "order": null,
                "literals": null,
                "restrictedTo": null
            },
            {
                "id" : "FakeClassId_2",
                "name" : "FakeClassName_2",
                "description": null,
                "type" : "Class",
                "propertyAssignments": null,
                "stereotype": "phase",
                "isAbstract": false,
                "isDerived": false,
                "properties": null,
                "isExtensional": null,
                "isPowertype": null,
                "order": null,
                "literals": null,
                "restrictedTo": null
            },
            {
                "id" : "FakeGeneralizationId",
                "name" : null,
                "description" : null,
                "type" : "Generalization",
                "propertyAssignments" : null,
                "general" : {
                    "id" : "FakeClassId_1",
                    "type" : "Class"
                },
                "specific" : {
                    "id" : "FakeClassId_2",
                    "type" : "Class"
                }
            }
        ]
    },
    "diagrams" : []
}
"""

            project = import_fake_json(fake_json)
            testee: OntoUmlClass = [
                item
                for item in project.model.contents
                if (
                    isinstance(item, OntoUmlClass)
                    and "FakeClassId_2" == item.identifier
                )
            ][0]
            expected = "D3FFD3"
            self.assertEqual(testee.html_color_code, expected)

        # pylint:disable=[W0212]
        with self.subTest(
            """
            <<category>> welke generalization is van <<relator>> -> D3FFD3 (lichtgroen)
            """
        ):
            fake_json = """
{
    "id" : "FakeJsonId",
    "name" : "FakeJsonName",
    "description": null,
    "type" : "Project",
    "model": {
        "id" : "FakePackageId",
        "name" : "FakePackageName",
        "description": null,
        "type" : "Package",
        "propertyAssignments": null,
        "contents": [
            {
                "id" : "FakeClassId_1",
                "name" : "FakeClassName_1",
                "description": null,
                "type" : "Class",
                "propertyAssignments": null,
                "stereotype": "category",
                "isAbstract": false,
                "isDerived": false,
                "properties": null,
                "isExtensional": null,
                "isPowertype": null,
                "order": null,
                "literals": null,
                "restrictedTo": null
            },
            {
                "id" : "FakeClassId_2",
                "name" : "FakeClassName_2",
                "description": null,
                "type" : "Class",
                "propertyAssignments": null,
                "stereotype": "relator",
                "isAbstract": false,
                "isDerived": false,
                "properties": null,
                "isExtensional": null,
                "isPowertype": null,
                "order": null,
                "literals": null,
                "restrictedTo": null
            },
            {
                "id" : "FakeGeneralizationId",
                "name" : null,
                "description" : null,
                "type" : "Generalization",
                "propertyAssignments" : null,
                "general" : {
                    "id" : "FakeClassId_1",
                    "type" : "Class"
                },
                "specific" : {
                    "id" : "FakeClassId_2",
                    "type" : "Class"
                }
            }
        ]
    },
    "diagrams" : []
}
"""

            project = import_fake_json(fake_json)
            testee: OntoUmlClass = [
                item
                for item in project.model.contents
                if (
                    isinstance(item, OntoUmlClass)
                    and "FakeClassId_1" == item.identifier
                )
            ][0]
            expected = "D3FFD3"
            self.assertEqual(testee.html_color_code, expected)
