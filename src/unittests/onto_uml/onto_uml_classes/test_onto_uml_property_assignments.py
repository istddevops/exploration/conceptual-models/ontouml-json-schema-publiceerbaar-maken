"""Test suites for onto_uml.onto_uml_project"""

from copy import deepcopy
from unittest import TestCase

from src.onto_uml.onto_uml_property_assignments import OntoUmlPropertyAssignments
from src.unittests.helpers.class_helpers import assert_members


class TestOntoUmlProject(TestCase):
    """
    Test suite for OntoUML property assignments interface
    - Constructor (__init__) is not tested, as it simply copies arguments to internal
      members
    - comparer (__eq__) is tested
    - properties are not tested as they simply return the hidden member values
    """

    def test_members(self):
        """
        Verify only expected public members are exposed on `OntoUmlPropertyAssignments`
        type
        """
        assert_members(self, OntoUmlPropertyAssignments, ["thesaurus_imar_id"])

    def test_eq(self):
        """
        1. Other is None => False
        2. `self.thesaurus_imar_id` does not equal `other.thesaurus_imar_id` => False
        3. `self.thesaurus_imar_id` equals `other.thesaurus_imar_id` => True
        """

        template = OntoUmlPropertyAssignments(-1)
        with self.subTest("1. Other is None => False"):
            first = deepcopy(template)
            other = None
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
            2. `self.thesaurus_imar_id` does not equal `other.thesaurus_imar_id`
               => False
            """
        ):
            first = deepcopy(template)
            other = deepcopy(template)
            other.thesaurus_imar_id = -2  # pylint:disable=[W0212]
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
            3. `self.thesaurus_imar_id` does not equal `other.thesaurus_imar_id`
               => False
            """
        ):
            first = deepcopy(template)
            other = deepcopy(template)
            self.assertTrue(first == other)
            self.assertEqual(first, other)
