"""Test suites for onto_uml.onto_uml_generalization"""


from copy import deepcopy
from unittest import TestCase

from src.onto_uml.onto_uml_generalization import (
    OntoUmlGeneralization,
    OntoUmlGeneralizationParty,
)
from src.onto_uml.onto_uml_property_assignments import OntoUmlPropertyAssignments


class TestOntoUmlGeneralizationParty(TestCase):
    """
    Test suite for OntoUML generalization party interface
    - Constructor (`__init__`) relies on super (`OntoUmlObjectInstance`), will be tested
      there if needed.
    - comparer (`__eq__`) relies on super (`OntoUmlObjectInstance`), will be tested
      there.
    - properties are not tested as they simply return the hidden member values
    """


class TestOntoUmlGeneralization(TestCase):
    """
    Test suite for OntoUML generalization interface
    - Constructor (`__init__`) is not tested as it simply assigns arguments to members
    - comparer (`__eq__`) is tested
    - properties are not tested as they simply return the hidden member values
    """

    # pylint:disable=[R0915]
    def test_eq(self):
        """
        Test the comparison method.
         1. Other is None -> return False
         2. Inherited members equal, specific items equal -> True
         3. One inherited member differs, specific items equal -> False
         4. Multiple inherited members differ, specific items equal -> False
         5. All inherited members differ, specific items equal -> False
         6. Inherited members equal, one specific item differs -> False
         7. One inherited member differs, one specific item differs -> False
         8. Multiple inherited members differ, one specific item differs -> False
         9. All inherited members differ, one specific item differs -> False
        10. Inherited members equal, all specific items differ -> False
        11. One inherited member differs, all specific items differ -> False
        12. Multiple inherited members differ, all specific items differ -> False
        13. All inherited members differ, all specific items differ -> False
        """

        template: OntoUmlGeneralization = OntoUmlGeneralization(
            identifier="FakeId",
            name="FakeName",
            description="FakeDescription",
            property_assignments=OntoUmlPropertyAssignments(-1),
            general=OntoUmlGeneralizationParty(
                "FakePartyIdGeneral", "Class", "FakeGeneralClass"
            ),
            specific=OntoUmlGeneralizationParty(
                "FakePartyIdSpecific", "Class", "FakeSpecificClass"
            ),
        )

        with self.subTest(
            """
             1. Other is None -> return False
            """
        ):
            first = deepcopy(template)
            other: OntoUmlGeneralization = None
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
             2. Inherited members equal, specific items equal -> True
            """
        ):
            first = deepcopy(template)
            other = deepcopy(template)
            self.assertTrue(first == other)
            self.assertEqual(first, other)

        with self.subTest(
            """
             3. One inherited member differs, specific items equal -> False
            """
        ):
            first = deepcopy(template)
            other = deepcopy(template)
            other.description = "AnotherFakeDescription"
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
             4. Multiple inherited members differ, specific items equal -> False
            """
        ):
            first = deepcopy(template)
            other = deepcopy(template)
            other.description = "AnotherFakeDescription"
            other.property_assignments = None
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
             5. All inherited members differ, specific items equal -> False
            """
        ):
            first = deepcopy(template)
            other = deepcopy(template)
            other.identifier = "AnotherFakeId"
            other.name = "AnotherFakeName"
            other.description = "AnotherFakeDescription"
            other.property_assignments = None
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
             6. Inherited members equal, one specific item differs -> False
            """
        ):
            first = deepcopy(template)
            other = deepcopy(template)
            other.specific = OntoUmlGeneralizationParty(
                "AnotherSpecificFakeId", "Class", "AnotherFakeSpecificClass"
            )
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
             7. One inherited member differs, one specific item differs -> False
            """
        ):
            first = deepcopy(template)
            other = deepcopy(template)
            other.description = "AnotherFakeDescription"
            other.specific = OntoUmlGeneralizationParty(
                "AnotherSpecificFakeId", "Class", "AnotherFakeSpecificClass"
            )
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
             8. Multiple inherited members differ, one specific item differs -> False
            """
        ):
            first = deepcopy(template)
            other = deepcopy(template)
            other.description = "AnotherFakeDescription"
            other.property_assignments = None
            other.specific = OntoUmlGeneralizationParty(
                "AnotherSpecificFakeId", "Class", "AnotherFakeSpecificClass"
            )
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
             9. All inherited members differ, one specific item differs -> False
            """
        ):
            first = deepcopy(template)
            other = deepcopy(template)
            other.identifier = "AnotherFakeId"
            other.name = "AnotherFakeName"
            other.description = "AnotherFakeDescription"
            other.property_assignments = None
            other.specific = OntoUmlGeneralizationParty(
                "AnotherSpecificFakeId", "Class", "AnotherFakeSpecificClass"
            )
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
            10. Inherited members equal, all specific items differ -> False
            """
        ):
            first = deepcopy(template)
            other = deepcopy(template)
            other.specific = OntoUmlGeneralizationParty(
                "AnotherSpecificFakeId", "Class", "AnotherFakeSpecificClass"
            )
            other.general = OntoUmlGeneralizationParty(
                "AnotherGeneralFakeId", "Class", "AnotherFakeGeneralClass"
            )
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
            11. One inherited member differs, all specific items differ -> False
            """
        ):
            first = deepcopy(template)
            other = deepcopy(template)
            other.description = "AnotherFakeDescription"
            other.specific = OntoUmlGeneralizationParty(
                "AnotherSpecificFakeId", "Class", "AnotherFakeSpecificClass"
            )
            other.general = OntoUmlGeneralizationParty(
                "AnotherGeneralFakeId", "Class", "AnotherFakeGeneralClass"
            )
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
            12. Multiple inherited members differ, all specific items differ -> False
            """
        ):
            first = deepcopy(template)
            other = deepcopy(template)
            other.description = "AnotherFakeDescription"
            other.property_assignments = None
            other.specific = OntoUmlGeneralizationParty(
                "AnotherSpecificFakeId", "Class", "AnotherFakeSpecificClass"
            )
            other.general = OntoUmlGeneralizationParty(
                "AnotherGeneralFakeId", "Class", "AnotherFakeGeneralClass"
            )
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
            13. All inherited members differ, all specific items differ -> False
            """
        ):
            first = deepcopy(template)
            other = deepcopy(template)
            other.identifier = "AnotherFakeId"
            other.name = "AnotherFakeName"
            other.description = "AnotherFakeDescription"
            other.property_assignments = None
            other.specific = OntoUmlGeneralizationParty(
                "AnotherSpecificFakeId", "Class", "AnotherFakeSpecificClass"
            )
            other.general = OntoUmlGeneralizationParty(
                "AnotherGeneralFakeId", "Class", "AnotherFakeGeneralClass"
            )
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)
