"""Test suites for onto_uml.onto_uml_content_container"""

from copy import deepcopy
from unittest import TestCase

from src.onto_uml.onto_uml_content_container import OntoUmlContentContainer


class TestOntoUmlContentContainer(TestCase):
    """
    Test suite for OntoUML content container base interface
    - Constructor (__init__) is not tested as it simply assigns arguments to members
    - comparer (__eq__) is tested
    - properties are not tested as they simply return the hidden member values
    """

    def test_eq(self):
        """
        Test the comparison method.
        1. The other OntoUMLContentContainer is None -> return False
        2. The super equals other.super, and the specific member is equal too -> return
           True
        3. The super equals other.super, and the specific member is not equal -> return
           False
        4. The super does not equal other.super, but the specific member is equal
           -> return False
        5. The super does not equal other.super, and the specific member is not equal
           -> return False
        """

        template: OntoUmlContentContainer = OntoUmlContentContainer(
            identifier="FakeIdentifier",
            name="FakeName",
            description="FakeDescription",
            contents=["FakeContents"],
        )
        with self.subTest(
            """
            1. The other OntoUMLContentContainer is None -> return False
            """
        ):
            first = deepcopy(template)
            second: OntoUmlContentContainer = None
            self.assertFalse(first == second)
            self.assertNotEqual(first, second)

        with self.subTest(
            """
            2. The super equals other.super, and the specific member is equal too
               -> return True
            """
        ):
            first = deepcopy(template)
            second = deepcopy(template)
            self.assertTrue(first == second)
            self.assertEqual(first, second)

        with self.subTest(
            """
            3. The super equals other.super, and the specific member is not equal
               -> return False
            """
        ):
            first = deepcopy(template)
            second = deepcopy(template)
            second.contents = None
            self.assertFalse(first == second)
            self.assertNotEqual(first, second)

        with self.subTest(
            """
            4. The super does not equal other.super, but the specific member is equal
               -> return False
            """
        ):
            first = deepcopy(template)
            second = deepcopy(template)
            second.description = None
            self.assertFalse(first == second)
            self.assertNotEqual(first, second)

        with self.subTest(
            """
            5. The super does not equal other.super, and the specific member is not
               equal -> return False
            """
        ):
            first = deepcopy(template)
            second = deepcopy(template)
            second.description = None
            second.contents = ["FakeContentsTwo"]
            self.assertFalse(first == second)
            self.assertNotEqual(first, second)
