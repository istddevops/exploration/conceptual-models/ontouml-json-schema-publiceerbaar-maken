"""Test suites for onto_uml.onto_uml_model_element"""


from copy import deepcopy
from unittest import TestCase
from unittest.mock import patch

from src.onto_uml.onto_uml_model_element import OntoUmlModelElement


class TestOntoUmlModelElement(TestCase):
    """
    Test suite for OntoUML model element interface
    - Constructor (`__init__`) is not tested as it simply assigns arguments to members
    - comparer (`__eq__`) is tested
    - properties are not tested as they simply return the hidden member values, except:
      * model_element as it contains some logic
    """

    # pylint:disable=[R0915]
    def test_eq(self):
        # pylint:disable=[C0301]
        """
        Test the comparison method.
         1. Other is None -> False
         |  #  | `self.model_element_id` | `other.model_element_id` | `model_element_id` equal | `self.model_element_type` | `other.model_element_type` | `model_element_type` equal | result     |
         |  2. | set                     | set                      | yes                      | set                       | set                        | yes                        | True       |
         |  3. | not set                 | set                      | n/a                      | set                       | set                        | yes                        | False      |
         |  4. | set                     | not set                  | n/a                      | set                       | set                        | yes                        | False      |
         |  5. | not set                 | not set                  | n/a                      | set                       | set                        | yes                        | True       |
         |  6. | set                     | set                      | no                       | set                       | set                        | yes                        | False      |
         |  7. | set                     | set                      | yes                      | not set                   | set                        | n/a                        | False      |
         |  8. | not set                 | set                      | n/a                      | not set                   | set                        | n/a                        | False      |
         |  9. | set                     | not set                  | n/a                      | not set                   | set                        | n/a                        | False      |
         | 10. | not set                 | not set                  | n/a                      | not set                   | set                        | n/a                        | False      |
         | 11. | set                     | set                      | no                       | not set                   | set                        | n/a                        | False      |
         | 12. | set                     | set                      | yes                      | set                       | not set                    | n/a                        | False      |
         | 13. | not set                 | set                      | n/a                      | set                       | not set                    | n/a                        | False      |
         | 14. | set                     | not set                  | n/a                      | set                       | not set                    | n/a                        | False      |
         | 15. | not set                 | not set                  | n/a                      | set                       | not set                    | n/a                        | False      |
         | 16. | set                     | set                      | no                       | set                       | not set                    | n/a                        | False      |
         | 17. | set                     | set                      | yes                      | not set                   | not set                    | n/a                        | True       |
         | 18. | not set                 | set                      | n/a                      | not set                   | not set                    | n/a                        | False      |
         | 19. | set                     | not set                  | n/a                      | not set                   | not set                    | n/a                        | False      |
         | 20. | not set                 | not set                  | n/a                      | not set                   | not set                    | n/a                        | True       |
         | 21. | set                     | set                      | no                       | not set                   | not set                    | n/a                        | False      |
         | 22. | set                     | set                      | yes                      | set                       | set                        | no                         | False      |
         | 23. | not set                 | set                      | n/a                      | set                       | set                        | no                         | False      |
         | 24. | set                     | not set                  | n/a                      | set                       | set                        | no                         | False      |
         | 25. | not set                 | not set                  | n/a                      | set                       | set                        | no                         | False      |
         | 26. | set                     | set                      | no                       | set                       | set                        | no                         | False      |
        """  # noqa:E501

        frozen: OntoUmlModelElement = OntoUmlModelElement("FakeId", "FakeType")

        with self.subTest(
            """
             1. Other is None -> return False
            """
        ):
            first: OntoUmlModelElement = deepcopy(frozen)
            other: OntoUmlModelElement = None
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
            |  #  | `self.model_element_id` | `other.model_element_id` | `model_element_id` equal | `self.model_element_type` | `other.model_element_type` | `model_element_type` equal | result     |
            |  2. | set                     | set                      | yes                      | set                       | set                        | yes                        | True       |
            """  # noqa:E501
        ):
            first: OntoUmlModelElement = deepcopy(frozen)
            other = deepcopy(first)
            self.assertTrue(first == other)
            self.assertEqual(first, other)

        with self.subTest(
            """
            |  #  | `self.model_element_id` | `other.model_element_id` | `model_element_id` equal | `self.model_element_type` | `other.model_element_type` | `model_element_type` equal | result     |
            |  3. | not set                 | set                      | n/a                      | set                       | set                        | yes                        | False      |
            """  # noqa:E501
        ):
            first: OntoUmlModelElement = deepcopy(frozen)
            other = deepcopy(first)
            first._id = None  # pylint:disable=[W0212]
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
            |  #  | `self.model_element_id` | `other.model_element_id` | `model_element_id` equal | `self.model_element_type` | `other.model_element_type` | `model_element_type` equal | result     |
            |  4. | set                     | not set                  | n/a                      | set                       | set                        | yes                        | False      |
            """  # noqa:E501
        ):
            first: OntoUmlModelElement = deepcopy(frozen)
            other = deepcopy(first)
            other._id = None  # pylint:disable=[W0212]
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
            |  #  | `self.model_element_id` | `other.model_element_id` | `model_element_id` equal | `self.model_element_type` | `other.model_element_type` | `model_element_type` equal | result     |
            |  5. | not set                 | not set                  | n/a                      | set                       | set                        | yes                        | True       |
            """  # noqa:E501
        ):
            first: OntoUmlModelElement = deepcopy(frozen)
            other = deepcopy(first)
            first._id = None  # pylint:disable=[W0212]
            other._id = None  # pylint:disable=[W0212]
            self.assertTrue(first == other)
            self.assertEqual(first, other)

        with self.subTest(
            """
            |  #  | `self.model_element_id` | `other.model_element_id` | `model_element_id` equal | `self.model_element_type` | `other.model_element_type` | `model_element_type` equal | result     |
            |  6. | set                     | set                      | no                       | set                       | set                        | yes                        | False      |
            """  # noqa:E501
        ):
            first: OntoUmlModelElement = deepcopy(frozen)
            other = deepcopy(first)
            other._id = "AnotherFakeId"  # pylint:disable=[W0212]
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
            |  #  | `self.model_element_id` | `other.model_element_id` | `model_element_id` equal | `self.model_element_type` | `other.model_element_type` | `model_element_type` equal | result     |
            |  7. | set                     | set                      | yes                      | not set                   | set                        | n/a                        | False      |
            """  # noqa:E501
        ):
            first: OntoUmlModelElement = deepcopy(frozen)
            other = deepcopy(first)
            other._type = None  # pylint:disable=[W0212]
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
            |  #  | `self.model_element_id` | `other.model_element_id` | `model_element_id` equal | `self.model_element_type` | `other.model_element_type` | `model_element_type` equal | result     |
            |  8. | not set                 | set                      | n/a                      | not set                   | set                        | n/a                        | False      |
            """  # noqa:E501
        ):
            first: OntoUmlModelElement = deepcopy(frozen)
            other = deepcopy(first)
            first._id = None  # pylint:disable=[W0212]
            first._type = None  # pylint:disable=[W0212]
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
            |  #  | `self.model_element_id` | `other.model_element_id` | `model_element_id` equal | `self.model_element_type` | `other.model_element_type` | `model_element_type` equal | result     |
            |  9. | set                     | not set                  | n/a                      | not set                   | set                        | n/a                        | False      |
            """  # noqa:E501
        ):
            first: OntoUmlModelElement = deepcopy(frozen)
            other = deepcopy(first)
            other._id = None  # pylint: disable=[W0212]
            first._type = None  # pylint: disable=[W0212]
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
            |  #  | `self.model_element_id` | `other.model_element_id` | `model_element_id` equal | `self.model_element_type` | `other.model_element_type` | `model_element_type` equal | result     |
            | 10. | not set                 | not set                  | n/a                      | not set                   | set                        | n/a                        | False      |
            """  # noqa:E501
        ):
            first: OntoUmlModelElement = deepcopy(frozen)
            other = deepcopy(first)
            first._id = None  # pylint:disable=[W0212]
            other._id = None  # pylint:disable=[W0212]
            first._type = None  # pylint:disable=[W0212]
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
            |  #  | `self.model_element_id` | `other.model_element_id` | `model_element_id` equal | `self.model_element_type` | `other.model_element_type` | `model_element_type` equal | result     |
            | 11. | set                     | set                      | no                       | not set                   | set                        | n/a                        | False      |
            """  # noqa:E501
        ):
            first: OntoUmlModelElement = deepcopy(frozen)
            other = deepcopy(first)
            other._id = "AnotherFakeId"  # pylint:disable=[W0212]
            first._type = None  # pylint:disable=[W0212]
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
            |  #  | `self.model_element_id` | `other.model_element_id` | `model_element_id` equal | `self.model_element_type` | `other.model_element_type` | `model_element_type` equal | result     |
            | 12. | set                     | set                      | yes                      | set                       | not set                    | n/a                        | False      |
            """  # noqa:E501
        ):
            first: OntoUmlModelElement = deepcopy(frozen)
            other = deepcopy(first)
            other._type = None  # pylint:disable=[W0212]
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
            |  #  | `self.model_element_id` | `other.model_element_id` | `model_element_id` equal | `self.model_element_type` | `other.model_element_type` | `model_element_type` equal | result     |
            | 13. | not set                 | set                      | n/a                      | set                       | not set                    | n/a                        | False      |
            """  # noqa:E501
        ):
            first: OntoUmlModelElement = deepcopy(frozen)
            other = deepcopy(first)
            first._id = None  # pylint:disable=[W0212]
            other._type = None  # pylint:disable=[W0212]
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
            |  #  | `self.model_element_id` | `other.model_element_id` | `model_element_id` equal | `self.model_element_type` | `other.model_element_type` | `model_element_type` equal | result     |
            | 14. | set                     | not set                  | n/a                      | set                       | not set                    | n/a                        | False      |
            """  # noqa:E501
        ):
            first: OntoUmlModelElement = deepcopy(frozen)
            other = deepcopy(first)
            other._id = None  # pylint:disable=[W0212]
            other._type = None  # pylint:disable=[W0212]
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
            |  #  | `self.model_element_id` | `other.model_element_id` | `model_element_id` equal | `self.model_element_type` | `other.model_element_type` | `model_element_type` equal | result     |
            | 15. | not set                 | not set                  | n/a                      | set                       | not set                    | n/a                        | False      |
            """  # noqa:E501
        ):
            first: OntoUmlModelElement = deepcopy(frozen)
            other = deepcopy(first)
            first._id = None  # pylint:disable=[W0212]
            other._id = None  # pylint:disable=[W0212]
            other._type = None  # pylint:disable=[W0212]
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
            |  #  | `self.model_element_id` | `other.model_element_id` | `model_element_id` equal | `self.model_element_type` | `other.model_element_type` | `model_element_type` equal | result     |
            | 16. | set                     | set                      | no                       | set                       | not set                    | n/a                        | False      |
            """  # noqa:E501
        ):
            first: OntoUmlModelElement = deepcopy(frozen)
            other = deepcopy(first)
            other._id = "AnotherFakeId"  # pylint:disable=[W0212]
            other._type = None  # pylint:disable=[W0212]
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
            |  #  | `self.model_element_id` | `other.model_element_id` | `model_element_id` equal | `self.model_element_type` | `other.model_element_type` | `model_element_type` equal | result     |
            | 17. | set                     | set                      | yes                      | not set                   | not set                    | n/a                        | True       |
            """  # noqa:E501
        ):
            first: OntoUmlModelElement = deepcopy(frozen)
            other = deepcopy(first)
            first._type = None  # pylint:disable=[W0212]
            other._type = None  # pylint:disable=[W0212]
            self.assertTrue(first == other)
            self.assertEqual(first, other)

        with self.subTest(
            """
            |  #  | `self.model_element_id` | `other.model_element_id` | `model_element_id` equal | `self.model_element_type` | `other.model_element_type` | `model_element_type` equal | result     |
            | 18. | not set                 | set                      | n/a                      | not set                   | not set                    | n/a                        | False      |
            """  # noqa:E501
        ):
            first: OntoUmlModelElement = deepcopy(frozen)
            other = deepcopy(first)
            first._id = None  # pylint:disable=[W0212]
            first._type = None  # pylint:disable=[W0212]
            other._type = None  # pylint:disable=[W0212]
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
            |  #  | `self.model_element_id` | `other.model_element_id` | `model_element_id` equal | `self.model_element_type` | `other.model_element_type` | `model_element_type` equal | result     |
            | 19. | set                     | not set                  | n/a                      | not set                   | not set                    | n/a                        | False      |
            """  # noqa:E501
        ):
            first: OntoUmlModelElement = deepcopy(frozen)
            other = deepcopy(first)
            other._id = None  # pylint:disable=[W0212]
            first._type = None  # pylint:disable=[W0212]
            other._type = None  # pylint:disable=[W0212]
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
            |  #  | `self.model_element_id` | `other.model_element_id` | `model_element_id` equal | `self.model_element_type` | `other.model_element_type` | `model_element_type` equal | result     |
            | 20. | not set                 | not set                  | n/a                      | not set                   | not set                    | n/a                        | True       |
            """  # noqa:E501
        ):
            first: OntoUmlModelElement = deepcopy(frozen)
            other = deepcopy(first)
            first._id = None  # pylint:disable=[W0212]
            other._id = None  # pylint:disable=[W0212]
            first._type = None  # pylint:disable=[W0212]
            other._type = None  # pylint:disable=[W0212]
            self.assertTrue(first == other)
            self.assertEqual(first, other)

        with self.subTest(
            """
            |  #  | `self.model_element_id` | `other.model_element_id` | `model_element_id` equal | `self.model_element_type` | `other.model_element_type` | `model_element_type` equal | result     |
            | 21. | set                     | set                      | no                       | not set                   | not set                    | n/a                        | False      |
            """  # noqa:E501
        ):
            first: OntoUmlModelElement = deepcopy(frozen)
            other = deepcopy(first)
            other._id = "AnotherFakeId"  # pylint:disable=[W0212]
            first._type = None  # pylint:disable=[W0212]
            other._type = None  # pylint:disable=[W0212]
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
            |  #  | `self.model_element_id` | `other.model_element_id` | `model_element_id` equal | `self.model_element_type` | `other.model_element_type` | `model_element_type` equal | result     |
            | 22. | set                     | set                      | yes                      | set                       | set                        | no                         | False      |
            """  # noqa:E501
        ):
            first: OntoUmlModelElement = deepcopy(frozen)
            other = deepcopy(first)
            other._type = "AnotherFakeType"  # pylint:disable=[W0212]
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
            |  #  | `self.model_element_id` | `other.model_element_id` | `model_element_id` equal | `self.model_element_type` | `other.model_element_type` | `model_element_type` equal | result     |
            | 23. | not set                 | set                      | n/a                      | set                       | set                        | no                         | False      |
            """  # noqa:E501
        ):
            first: OntoUmlModelElement = deepcopy(frozen)
            other = deepcopy(first)
            first._id = None  # pylint:disable=[W0212]
            other._type = "AnotherFakeType"  # pylint:disable=[W0212]
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
            |  #  | `self.model_element_id` | `other.model_element_id` | `model_element_id` equal | `self.model_element_type` | `other.model_element_type` | `model_element_type` equal | result     |
            | 24. | set                     | not set                  | n/a                      | set                       | set                        | no                         | False      |
            """  # noqa:E501
        ):
            first: OntoUmlModelElement = deepcopy(frozen)
            other = deepcopy(first)
            other._id = None  # pylint:disable=[W0212]
            other._type = "AnotherFakeType"  # pylint:disable=[W0212]
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
            |  #  | `self.model_element_id` | `other.model_element_id` | `model_element_id` equal | `self.model_element_type` | `other.model_element_type` | `model_element_type` equal | result     |
            | 25. | not set                 | not set                  | n/a                      | set                       | set                        | no                         | False      |
            """  # noqa:E501
        ):
            first: OntoUmlModelElement = deepcopy(frozen)
            other = deepcopy(first)
            first._id = None  # pylint:disable=[W0212]
            other._id = None  # pylint:disable=[W0212]
            other._type = "AnotherFakeType"  # pylint:disable=[W0212]
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
            |  #  | `self.model_element_id` | `other.model_element_id` | `model_element_id` equal | `self.model_element_type` | `other.model_element_type` | `model_element_type` equal | result     |
            | 26. | set                     | set                      | no                       | set                       | set                        | no                         | False      |
            """  # noqa:E501
        ):
            first: OntoUmlModelElement = deepcopy(frozen)
            other = deepcopy(first)
            other._id = "AnotherFakeId"  # pylint:disable=[W0212]
            other._type = "AnotherFakeType"  # pylint:disable=[W0212]
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

    def test_model_element(self):
        """
        The `model_element` property returns the `_element` member.
        This member is optionally filled in the constructor.
        If it is not set (`None`), then it'll be looked up in the `_dict` member by
        `_id`, and then set in `_element`. If it is set, then the set value is returned.

        Testcases:
        | #  | `_element` set | `_id` in `_dict`.keys | `_element` reset |
        | 1. | yes            | yes                   | no               |
        | 2. | no             | yes                   | yes              |
        | 3. | yes            | no                    | no               |
        | 4. | no             | no                    | no               |
        """

        fake_element = "FakeElementReference"
        orig_element = "FakeElement"
        with self.subTest(
            """
            | #  | `_element` set | `_id` in `_dict`.keys | `_element` reset |
            | 1. | yes            | yes                   | no               |
            """
        ):
            testee: OntoUmlModelElement = OntoUmlModelElement(
                "FakeId", "FakeType", orig_element
            )
            # pylint: disable=[W0212, W0104]
            with patch.dict(testee._dict, values={"FakeId": fake_element}):
                test_value = testee.model_element
                self.assertNotEqual(test_value, fake_element)
                self.assertEqual(test_value, orig_element)

        with self.subTest(
            """
            | #  | `_element` set | `_id` in `_dict`.keys | `_element` reset |
            | 2. | no             | yes                   | yes              |
            """
        ):
            testee: OntoUmlModelElement = OntoUmlModelElement("FakeId", "FakeType")
            # pylint: disable=[W0212, W0104]
            with patch.dict(testee._dict, values={"FakeId": fake_element}):
                test_value = testee.model_element
                self.assertEqual(test_value, fake_element)

        with self.subTest(
            """
            | #  | `_element` set | `_id` in `_dict`.keys | `_element` reset |
            | 3. | yes            | no                    | no               |
            """
        ):
            testee: OntoUmlModelElement = OntoUmlModelElement(
                "FakeId", "FakeType", orig_element
            )
            # pylint: disable=[W0212, W0104]
            with patch.dict(testee._dict, values={"AnotherFakeId": fake_element}):
                test_value = testee.model_element
                self.assertEqual(test_value, orig_element)

        with self.subTest(
            """
            | #  | `_element` set | `_id` in `_dict`.keys | `_element` reset |
            | 4. | no             | no                    | no               |
            """
        ):
            testee: OntoUmlModelElement = OntoUmlModelElement("FakeId", "FakeType")
            # pylint: disable=[W0212, W0104]
            with patch.dict(testee._dict, values={"AnotherFakeId": fake_element}):
                test_value = testee.model_element
                self.assertIsNone(test_value)
