"""Test suites for onto_uml.onto_uml_objects.OntoUmlPropertyType"""


from copy import deepcopy
from unittest import TestCase

from src.onto_uml.onto_uml_objects import OntoUmlPropertyType


class TestOntoUmlPropertyType(TestCase):
    """
    Test suite for OntoUML property type interface
    - Constructor (`__init__`) is not tested as it simply assigns arguments to members
    - comparer (`__eq__`) is tested
    - properties are not tested as they simply return the hidden member values:
    """

    # pylint:disable=[R0915]
    def test_eq(self):
        # pylint:disable=[C0301]
        """
        Test the comparison method.
        0. Other is None -> False
        |  #  | `self.identifier` set | `other.identifier` set | `identifier` equals | `self.related_type` set | `other.related_type` set | `related_type` equals | result |
        |  1. | yes                   | yes                    | yes                 | yes                     | yes                      | yes                   | True   |
        |  2. | no                    | yes                    | n/a                 | yes                     | yes                      | yes                   | False  |
        |  3. | yes                   | no                     | n/a                 | yes                     | yes                      | yes                   | False  |
        |  4. | no                    | no                     | n/a                 | yes                     | yes                      | yes                   | True   |
        |  5. | yes                   | yes                    | yes                 | no                      | yes                      | n/a                   | False  |
        |  6. | no                    | yes                    | n/a                 | no                      | yes                      | n/a                   | False  |
        |  7. | yes                   | no                     | n/a                 | no                      | yes                      | n/a                   | False  |
        |  8. | no                    | no                     | n/a                 | no                      | yes                      | n/a                   | False  |
        |  9. | yes                   | yes                    | yes                 | yes                     | no                       | n/a                   | False  |
        | 10. | no                    | yes                    | n/a                 | yes                     | no                       | n/a                   | False  |
        | 11. | yes                   | no                     | n/a                 | yes                     | no                       | n/a                   | False  |
        | 12. | no                    | no                     | n/a                 | yes                     | no                       | n/a                   | False  |
        | 13. | yes                   | yes                    | yes                 | no                      | no                       | n/a                   | True   |
        | 14. | no                    | yes                    | n/a                 | no                      | no                       | n/a                   | False  |
        | 15. | yes                   | no                     | n/a                 | no                      | no                       | n/a                   | False  |
        | 16. | no                    | no                     | n/a                 | no                      | no                       | n/a                   | True   |
        | 17. | yes                   | yes                    | yes                 | yes                     | yes                      | no                    | False  |
        | 18. | no                    | yes                    | n/a                 | yes                     | yes                      | no                    | False  |
        | 19. | yes                   | no                     | n/a                 | yes                     | yes                      | no                    | False  |
        | 20. | no                    | no                     | n/a                 | yes                     | yes                      | no                    | False  |
        """  # noqa:E501

        frozen: OntoUmlPropertyType = OntoUmlPropertyType(
            identifier="FakeId", related_type="FakeType"
        )
        with self.subTest(
            """
            0. Other is None -> False
            """
        ):
            first = deepcopy(frozen)
            other = None
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
            |  #  | `self.identifier` set | `other.identifier` set | `identifier` equals | `self.related_type` set | `other.related_type` set | `related_type` equals | result |
            |  1. | yes                   | yes                    | yes                 | yes                     | yes                      | yes                   | True   |
            """  # noqa:E501
        ):
            first = deepcopy(frozen)
            other = deepcopy(frozen)
            self.assertTrue(first == other)
            self.assertEqual(first, other)

        with self.subTest(
            """
            |  #  | `self.identifier` set | `other.identifier` set | `identifier` equals | `self.related_type` set | `other.related_type` set | `related_type` equals | result |
            |  2. | no                    | yes                    | n/a                 | yes                     | yes                      | yes                   | False  |
            """  # noqa:E501
        ):
            first = deepcopy(frozen)
            other = deepcopy(frozen)
            first.identifier = None
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
            |  #  | `self.identifier` set | `other.identifier` set | `identifier` equals | `self.related_type` set | `other.related_type` set | `related_type` equals | result |
            |  3. | yes                   | no                     | n/a                 | yes                     | yes                      | yes                   | False  |
            """  # noqa:E501
        ):
            first = deepcopy(frozen)
            other = deepcopy(frozen)
            other.identifier = None
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
            |  #  | `self.identifier` set | `other.identifier` set | `identifier` equals | `self.related_type` set | `other.related_type` set | `related_type` equals | result |
            |  4. | no                    | no                     | n/a                 | yes                     | yes                      | yes                   | True   |
            """  # noqa:E501
        ):
            first = deepcopy(frozen)
            other = deepcopy(frozen)
            first.identifier = None
            other.identifier = None
            self.assertTrue(first == other)
            self.assertEqual(first, other)

        with self.subTest(
            """
            |  #  | `self.identifier` set | `other.identifier` set | `identifier` equals | `self.related_type` set | `other.related_type` set | `related_type` equals | result |
            |  5. | yes                   | yes                    | yes                 | no                      | yes                      | n/a                   | False  |
            """  # noqa:E501
        ):
            first = deepcopy(frozen)
            other = deepcopy(frozen)
            first.related_type = None
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
            |  #  | `self.identifier` set | `other.identifier` set | `identifier` equals | `self.related_type` set | `other.related_type` set | `related_type` equals | result |
            |  6. | no                    | yes                    | n/a                 | no                      | yes                      | n/a                   | False  |
            """  # noqa:E501
        ):
            first = deepcopy(frozen)
            other = deepcopy(frozen)
            first.identifier = None
            first.related_type = None
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
            |  #  | `self.identifier` set | `other.identifier` set | `identifier` equals | `self.related_type` set | `other.related_type` set | `related_type` equals | result |
            |  7. | yes                   | no                     | n/a                 | no                      | yes                      | n/a                   | False  |
            """  # noqa:E501
        ):
            first = deepcopy(frozen)
            other = deepcopy(frozen)
            other.identifier = None
            first.related_type = None
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
            |  #  | `self.identifier` set | `other.identifier` set | `identifier` equals | `self.related_type` set | `other.related_type` set | `related_type` equals | result |
            |  8. | no                    | no                     | n/a                 | no                      | yes                      | n/a                   | False  |
            """  # noqa:E501
        ):
            first = deepcopy(frozen)
            other = deepcopy(frozen)
            first.identifier = None
            other.identifier = None
            first.related_type = None
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
            |  #  | `self.identifier` set | `other.identifier` set | `identifier` equals | `self.related_type` set | `other.related_type` set | `related_type` equals | result |
            |  9. | yes                   | yes                    | yes                 | yes                     | no                       | n/a                   | False  |
            """  # noqa:E501
        ):
            first = deepcopy(frozen)
            other = deepcopy(frozen)
            other.related_type = None
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
            |  #  | `self.identifier` set | `other.identifier` set | `identifier` equals | `self.related_type` set | `other.related_type` set | `related_type` equals | result |
            | 10. | no                    | yes                    | n/a                 | yes                     | no                       | n/a                   | False  |
            """  # noqa:E501
        ):
            first = deepcopy(frozen)
            other = deepcopy(frozen)
            first.identifier = None
            other.related_type = None
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
            |  #  | `self.identifier` set | `other.identifier` set | `identifier` equals | `self.related_type` set | `other.related_type` set | `related_type` equals | result |
            | 11. | yes                   | no                     | n/a                 | yes                     | no                       | n/a                   | False  |
            """  # noqa:E501
        ):
            first = deepcopy(frozen)
            other = deepcopy(frozen)
            other.identifier = None
            other.related_type = None
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
            |  #  | `self.identifier` set | `other.identifier` set | `identifier` equals | `self.related_type` set | `other.related_type` set | `related_type` equals | result |
            | 12. | no                    | no                     | n/a                 | yes                     | no                       | n/a                   | False  |
            """  # noqa:E501
        ):
            first = deepcopy(frozen)
            other = deepcopy(frozen)
            first.identifier = None
            other.identifier = None
            other.related_type = None
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
            |  #  | `self.identifier` set | `other.identifier` set | `identifier` equals | `self.related_type` set | `other.related_type` set | `related_type` equals | result |
            | 13. | yes                   | yes                    | yes                 | no                      | no                       | n/a                   | True   |
            """  # noqa:E501
        ):
            first = deepcopy(frozen)
            other = deepcopy(frozen)
            first.related_type = None
            other.related_type = None
            self.assertTrue(first == other)
            self.assertEqual(first, other)

        with self.subTest(
            """
            |  #  | `self.identifier` set | `other.identifier` set | `identifier` equals | `self.related_type` set | `other.related_type` set | `related_type` equals | result |
            | 14. | no                    | yes                    | n/a                 | no                      | no                       | n/a                   | False  |
            """  # noqa:E501
        ):
            first = deepcopy(frozen)
            other = deepcopy(frozen)
            first.identifier = None
            first.related_type = None
            other.related_type = None
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
            |  #  | `self.identifier` set | `other.identifier` set | `identifier` equals | `self.related_type` set | `other.related_type` set | `related_type` equals | result |
            | 15. | yes                   | no                     | n/a                 | no                      | no                       | n/a                   | False  |
            """  # noqa:E501
        ):
            first = deepcopy(frozen)
            other = deepcopy(frozen)
            other.identifier = None
            first.related_type = None
            other.related_type = None
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
            |  #  | `self.identifier` set | `other.identifier` set | `identifier` equals | `self.related_type` set | `other.related_type` set | `related_type` equals | result |
            | 16. | no                    | no                     | n/a                 | no                      | no                       | n/a                   | True   |
            """  # noqa:E501
        ):
            first = deepcopy(frozen)
            other = deepcopy(frozen)
            first.identifier = None
            other.identifier = None
            first.related_type = None
            other.related_type = None
            self.assertTrue(first == other)
            self.assertEqual(first, other)

        with self.subTest(
            """
            |  #  | `self.identifier` set | `other.identifier` set | `identifier` equals | `self.related_type` set | `other.related_type` set | `related_type` equals | result |
            | 17. | yes                   | yes                    | yes                 | yes                     | yes                      | no                    | False  |
            """  # noqa:E501
        ):
            first = deepcopy(frozen)
            other = deepcopy(frozen)
            other.related_type = "AnotherFakeType"
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
            |  #  | `self.identifier` set | `other.identifier` set | `identifier` equals | `self.related_type` set | `other.related_type` set | `related_type` equals | result |
            | 18. | no                    | yes                    | n/a                 | yes                     | yes                      | no                    | False  |
            """  # noqa:E501
        ):
            first = deepcopy(frozen)
            other = deepcopy(frozen)
            first.identifier = None
            other.related_type = "AnotherFakeType"
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
            |  #  | `self.identifier` set | `other.identifier` set | `identifier` equals | `self.related_type` set | `other.related_type` set | `related_type` equals | result |
            | 19. | yes                   | no                     | n/a                 | yes                     | yes                      | no                    | False  |
            """  # noqa:E501
        ):
            first = deepcopy(frozen)
            other = deepcopy(frozen)
            other.identifier = None
            other.related_type = "AnotherFakeType"
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
            |  #  | `self.identifier` set | `other.identifier` set | `identifier` equals | `self.related_type` set | `other.related_type` set | `related_type` equals | result |
            | 20. | no                    | no                     | n/a                 | yes                     | yes                      | no                    | False  |
            """  # noqa:E501
        ):
            first = deepcopy(frozen)
            other = deepcopy(frozen)
            first.identifier = None
            other.identifier = None
            other.related_type = "AnotherFakeType"
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)
