"""Test suites for onto_uml.onto_uml_objects.OntoUmlObject"""


from copy import deepcopy
from unittest import TestCase

from src.onto_uml.onto_uml_objects import OntoUmlObject
from src.onto_uml.onto_uml_property_assignments import OntoUmlPropertyAssignments
from src.unittests.helpers.class_helpers import assert_members

# pylint:disable=[C0301]
from src.unittests.onto_uml.onto_uml_classes.onto_uml_objects.test_onto_uml_objects import (  # noqa=E501
    _prep_case,
)


class TestOntoUmlObject(TestCase):
    """
    Test suite for OntoUML object interface
    - Constructor (`__init__`) is not tested as it simply assigns arguments to members
    - comparer (`__eq__`) is tested
    - properties are not tested as they simply return the hidden member values, except:
      * owner as it contains some logic
    """

    def test_members(self):
        """
        Test that the expected public members exists on OntoUmlObject
        """
        assert_members(
            self,
            OntoUmlObject,
            ["owner", "identifier", "name", "description", "property_assignments"],
        )

    def test_eq(self):
        # pylint:disable=[C0301]
        """
        Test the comparison method.
         1. Other is None -> False
         For all members, the following testcases are tested (in combination):
         |  #  | member (self) set | member (other) set | members equal | partial result |
         |  1. | yes               | yes                | yes           | True           |
         |  2. | no                | yes                | n/a           | False          |
         |  3. | yes               | no                 | n/a           | False          |
         |  4. | no                | no                 | n/a           | True           |
         |  5. | yes               | yes                | no            | False          |
         Only when all of the partial results yield True, then True is the expected result.
         Otherwise, False is expected
        """  # noqa:E501

        testcases_for_member = {
            "1": {
                "description": "Self set, other set, both equal",
                "self_set": True,
                "other_set": True,
                "both_equal": True,
                "partial_expected": True,
            },
            "2": {
                "description": "Self not set, other set",
                "self_set": False,
                "other_set": True,
                "both_equal": None,
                "partial_expected": False,
            },
            "3": {
                "description": "Self set, other not set",
                "self_set": True,
                "other_set": False,
                "both_equal": None,
                "partial_expected": False,
            },
            "4": {
                "description": "Self not set, other not set",
                "self_set": False,
                "other_set": False,
                "both_equal": None,
                "partial_expected": True,
            },
            "5": {
                "description": "Self set, other set, both not equal",
                "self_set": True,
                "other_set": True,
                "both_equal": False,
                "partial_expected": False,
            },
        }

        members = {
            "identifier": {
                "attribute": "identifier",
                "value": "FakeId",
                "alternate_value": "AnotherFakeId",
            },
            "name": {
                "attribute": "name",
                "value": "FakeName",
                "alternate_value": "AnotherFakeName",
            },
            "description": {
                "attribute": "description",
                "value": "FakeDescription",
                "alternate_value": "AnotherFakeDescription",
            },
            "property_assignments": {
                "attribute": "property_assignments",
                "value": OntoUmlPropertyAssignments(-1),
                "alternate_value": OntoUmlPropertyAssignments(-2),
            },
        }

        frozen = OntoUmlObject(
            identifier=members["identifier"]["value"],
            name=members["name"]["value"],
            description=members["description"]["value"],
            property_assignments=members["property_assignments"]["value"],
        )

        with self.subTest(
            """
            1. Other is None -> False
            """
        ):
            first = deepcopy(frozen)
            other = None
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        for identifier_case in testcases_for_member.values():
            identifier_info = members["identifier"]
            for name_case in testcases_for_member.values():
                name_info = members["name"]
                for description_case in testcases_for_member.values():
                    description_info = members["description"]
                    for property_case in testcases_for_member.values():
                        property_info = members["property_assignments"]
                        first = deepcopy(frozen)
                        other = deepcopy(frozen)
                        _prep_case(first, other, identifier_case, identifier_info)
                        _prep_case(first, other, name_case, name_info)
                        _prep_case(first, other, description_case, description_info)
                        _prep_case(first, other, property_case, property_info)
                        test_description = f"""
                            identifier {identifier_case["description"]}
                            name {name_case["description"]}
                            description {description_case["description"]}
                            property_assignments {property_case["description"]}
                            """
                        with self.subTest(test_description):
                            if (
                                identifier_case["partial_expected"]
                                and name_case["partial_expected"]
                                and description_case["partial_expected"]
                                and property_case["partial_expected"]
                            ):
                                self.assertTrue(first == other, test_description)
                                self.assertEqual(first, other, test_description)
                            else:
                                self.assertFalse(first == other, test_description)
                                self.assertNotEqual(first, other, test_description)
