"""Test suites for onto_uml.onto_uml_objects"""


def _prep_case(first, other, testcase, member_info):
    if not testcase["self_set"]:
        setattr(first, member_info["attribute"], None)
    if not testcase["other_set"]:
        setattr(other, member_info["attribute"], None)
    if None is not testcase["both_equal"] and not testcase["both_equal"]:
        setattr(other, member_info["attribute"], member_info["alternate_value"])
