"""Test suites for onto_uml.onto_uml_objects.OntoUmlDerivableObject"""


from copy import deepcopy
from unittest import TestCase

from src.onto_uml.onto_uml_objects import (
    OntoUmlDerivableObject,
    OntoUmlProperty,
    OntoUmlPropertyType,
)
from src.onto_uml.onto_uml_property_assignments import OntoUmlPropertyAssignments
from src.unittests.helpers.invariant_generator import (
    invariant_does_not_equal_other,
    invariant_equals_other,
    setup_equals_matrix,
)


class TestOntoUmlDerivableObject(TestCase):
    """
    Test suite for OntoUML derivable object interface
    - Constructor (`__init__`) is not tested as it simply assigns arguments to members
    - comparer (`__eq__`) is tested
    - properties are not tested as they simply return the hidden member values, except:
      * owner as it contains some logic
    """

    def test_eq(self):
        # pylint:disable=[C0301]
        """
        Test the comparison method.
         1. Other is None -> False
         For all members, the following testcases are tested (in combination):
         |  #  | member (self) set | member (other) set | members equal | partial result |
         |  1. | yes               | yes                | yes           | True           |
         |  2. | no                | yes                | n/a           | False          |
         |  3. | yes               | no                 | n/a           | False          |
         |  4. | no                | no                 | n/a           | True           |
         |  5. | yes               | yes                | no            | False          |
         Only when all of the partial results yield True, then True is the expected result.
         Otherwise, False is expected
        """  # noqa:E501

        testcases = setup_equals_matrix(
            [
                "identifier",
                "name",
                "description",
                "property_assignments",
                "stereotype",
                "is_abstract",
                "is_derived",
                "properties",
            ]
        )

        template = OntoUmlDerivableObject(
            identifier="FakeIdentifier",
            name="FakeName",
            description="FakeDescription",
            property_assignments=OntoUmlPropertyAssignments(-1),
            stereotype="FakeStereotype",
            is_abstract=False,
            is_derived=False,
            properties=[
                OntoUmlProperty(
                    identifier="FakeOntoUmlPropertyIdentifier",
                    name="FakeOntoUmlPropertyName",
                    description="FakeOntoUmlPropertyDescription",
                    property_assignments=OntoUmlPropertyAssignments(-3),
                    stereotype="FakeOntoUmlPropertyStereotype",
                    is_derived=False,
                    is_read_only=False,
                    is_ordered=False,
                    cardinality="FakeOntoUmlPropertyCardinality",
                    property_type=OntoUmlPropertyType(
                        identifier="FakeOntoUmlPropertyTypeIdentifier",
                        related_type="FakeOntoUmlPropertyTypeRelationType",
                    ),
                    subsetted_properties=["FakeOntoUmlPropertySubsettedProperty"],
                    redefined_properties=["FakeOntoUmlPropertyRedefinedProperty"],
                    aggregation_kind="FakeOntoUmlPropertyAggregationKind",
                ),
            ],
        )

        with self.subTest("1. Other is None -> False"):
            first = deepcopy(template)
            other = None
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        for testcase in testcases:
            with self.subTest(testcase.description):
                # First, setup the testcase.
                first = deepcopy(template)
                other = deepcopy(template)
                if all(
                    item.invariant == invariant_equals_other
                    for item in testcase.single_invariants
                ):
                    # No need to change any attributes, just compare the two and assert
                    # they are equal
                    self.assertTrue(first == other)
                    self.assertEqual(first, other)
                else:
                    # First, change the different attributes, then compare the two and
                    # assert they are not equal
                    for testable in [
                        item
                        for item in testcase.single_invariants
                        if item.invariant == invariant_does_not_equal_other
                    ]:
                        match testable.condition.description:
                            case "is_abstract", "is_derived":
                                worker: bool = getattr(
                                    other, testable.condition.identifier
                                )
                                setattr(
                                    other, testable.condition.identifier, not worker
                                )
                            case "property_assignments":
                                setattr(
                                    other,
                                    testable.condition.identifier,
                                    OntoUmlPropertyAssignments(-2),
                                )
                            case "properties":
                                worker = getattr(other, testable.condition.identifier)
                                # pylint:disable=[W0212]
                                worker[0].identifier = f"Another{worker[0].identifier}"
                                setattr(other, testable.condition.identifier, worker)
                            case _:
                                worker = getattr(other, testable.condition.identifier)
                                worker = f"Another{worker}"
                                setattr(other, testable.condition.identifier, worker)
                    self.assertFalse(first == other)
                    self.assertNotEqual(first, other)
