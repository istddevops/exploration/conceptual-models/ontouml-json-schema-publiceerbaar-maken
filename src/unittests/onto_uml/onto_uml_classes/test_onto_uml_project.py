"""Test suites for onto_uml.onto_uml_project"""

from copy import deepcopy
from unittest import TestCase
from unittest.mock import Mock

from src.onto_uml.onto_uml_diagram import OntoUmlDiagram
from src.onto_uml.onto_uml_package import OntoUmlPackage
from src.onto_uml.onto_uml_project import OntoUmlProject
from src.unittests.helpers.class_helpers import assert_members
from src.unittests.helpers.invariant_generator import (
    invariant_does_not_equal_other,
    invariant_equals_other,
    setup_equals_matrix,
)


class TestOntoUmlProject(TestCase):
    """
    Test suite for OntoUML project interface
    - Constructor (__init__) is tested, as it sets the owner of owned diagrams to the
      OntoUML project
    - comparer (__eq__) is tested
    - properties are not tested as they simply return the hidden member values
    """

    def test_members(self):
        """
        Test to check that OntoUmlProject contains only the specified members
        """
        assert_members(
            self,
            OntoUmlProject,
            ["identifier", "name", "description", "model", "diagrams"],
        )

    # pylint:disable=[R0915]
    def test_init(self):
        """
        1. No arguments are set => no error
        2. diagrams is set but not a list => TypeError("diagrams")
        3. diagrams is set and a list, but not all items are of type OntoUmlDiagram
           => TypeError("diagrams")
        4. diagrams is set and a list of exclusively OntoUmlDiagram items, then each
           item will have it's `_project` member set to this `OntoUmlProject` instance.
        For completeness, also all combinations of arguments set / not set will be tested.
        None should yield an error
        | identifier set | name set | description set | model set | diagrams set | expected result |
        | yes            | yes      | yes             | yes       | yes          | no error        |
        | no             | yes      | yes             | yes       | yes          | no error        |
        | yes            | no       | yes             | yes       | yes          | no error        |
        | no             | no       | yes             | yes       | yes          | no error        |
        | yes            | yes      | no              | yes       | yes          | no error        |
        | no             | yes      | no              | yes       | yes          | no error        |
        | yes            | no       | no              | yes       | yes          | no error        |
        | no             | no       | no              | yes       | yes          | no error        |
        | yes            | yes      | yes             | no        | yes          | no error        |
        | no             | yes      | yes             | no        | yes          | no error        |
        | yes            | no       | yes             | no        | yes          | no error        |
        | no             | no       | yes             | no        | yes          | no error        |
        | yes            | yes      | no              | no        | yes          | no error        |
        | no             | yes      | no              | no        | yes          | no error        |
        | yes            | no       | no              | no        | yes          | no error        |
        | no             | no       | no              | no        | yes          | no error        |
        | yes            | yes      | yes             | yes       | no           | no error        |
        | no             | yes      | yes             | yes       | no           | no error        |
        | yes            | no       | yes             | yes       | no           | no error        |
        | no             | no       | yes             | yes       | no           | no error        |
        | yes            | yes      | no              | yes       | no           | no error        |
        | no             | yes      | no              | yes       | no           | no error        |
        | yes            | no       | no              | yes       | no           | no error        |
        | no             | no       | no              | yes       | no           | no error        |
        | yes            | yes      | yes             | no        | no           | no error        |
        | no             | yes      | yes             | no        | no           | no error        |
        | yes            | no       | yes             | no        | no           | no error        |
        | no             | no       | yes             | no        | no           | no error        |
        | yes            | yes      | no              | no        | no           | no error        |
        | no             | yes      | no              | no        | no           | no error        |
        | yes            | no       | no              | no        | no           | no error        |
        | no             | no       | no              | no        | no           | no error        |
        """  # noqa=E501

        with self.subTest(
            """
            1. No arguments are set => no error
            """
        ):
            OntoUmlProject(
                identifier=None, name=None, description=None, model=None, diagrams=None
            )

        with self.subTest(
            """
            2. diagrams is set but not a list => TypeError("diagrams")
            """
        ):
            with self.assertRaisesRegex(TypeError, "diagrams"):
                OntoUmlProject(
                    identifier=None, name=None, description=None, model=None, diagrams=1
                )

        with self.subTest(
            """
            3. diagrams is set and a list, but not all items are of type OntoUmlDiagram
               => TypeError("diagrams")
            """
        ):
            with self.assertRaisesRegex(TypeError, "diagrams"):
                OntoUmlProject(
                    identifier=None,
                    name=None,
                    description=None,
                    model=None,
                    diagrams=[
                        OntoUmlDiagram(
                            identifier=None,
                            name=None,
                            description=None,
                            property_assignments=None,
                            contents=None,
                        ),
                        "a",
                        "b",
                        3,
                    ],
                )

        with self.subTest(
            """
            4. diagrams is set and a list of exclusively OntoUmlDiagram items, then each
               item will have it's `_project` member set to this `OntoUmlProject`
               instance.
            """
        ):
            testee = OntoUmlProject(
                identifier=None,
                name=None,
                description=None,
                model=None,
                diagrams=[
                    OntoUmlDiagram(
                        identifier="1",
                        name=None,
                        description=None,
                        property_assignments=None,
                        contents=None,
                    ),
                    OntoUmlDiagram(
                        identifier="2",
                        name=None,
                        description=None,
                        property_assignments=None,
                        contents=None,
                    ),
                ],
            )
            self.assertTrue(
                all(
                    item._project == testee  # pylint:disable=[W0212]
                    for item in testee.diagrams
                )
            )

        # pylint:disable=[C0301]
        with self.subTest(
            """
            | identifier set | name set | description set | model set | diagrams set | expected result |
            | yes            | yes      | yes             | yes       | yes          | no error        |
            """  # noqa=E501
        ):
            OntoUmlProject(
                identifier="FakeIdentifier",
                name="FakeName",
                description="FakeDescription",
                model=OntoUmlPackage(
                    identifier="FakeIdentifier",
                    name="FakeName",
                    description="FakeDescription",
                    property_assignments=None,
                    contents=None,
                ),
                diagrams=[OntoUmlDiagram(identifier="FakeIdentifier", name="FakeName")],
            )

        # pylint:disable=[C0301]
        with self.subTest(
            """
            | identifier set | name set | description set | model set | diagrams set | expected result |
            | no             | yes      | yes             | yes       | yes          | no error        |
            """  # noqa=E501
        ):
            OntoUmlProject(
                identifier=None,
                name="FakeName",
                description="FakeDescription",
                model=OntoUmlPackage(
                    identifier="FakeIdentifier",
                    name="FakeName",
                    description="FakeDescription",
                ),
                diagrams=[OntoUmlDiagram(identifier="FakeIdentifier", name="FakeName")],
            )

        # pylint:disable=[C0301]
        with self.subTest(
            """
            | identifier set | name set | description set | model set | diagrams set | expected result |
            | yes            | no       | yes             | yes       | yes          | no error        |
            """  # noqa=E501
        ):
            OntoUmlProject(
                identifier="FakeIdentifier",
                name=None,
                description="FakeDescription",
                model=OntoUmlPackage(
                    identifier="FakeIdentifier",
                    name="FakeName",
                    description="FakeDescription",
                ),
                diagrams=[OntoUmlDiagram(identifier="FakeIdentifier", name="FakeName")],
            )

        # pylint:disable=[C0301]
        with self.subTest(
            """
            | identifier set | name set | description set | model set | diagrams set | expected result |
            | no             | no       | yes             | yes       | yes          | no error        |
            """  # noqa=E501
        ):
            OntoUmlProject(
                identifier=None,
                name=None,
                description="FakeDescription",
                model=OntoUmlPackage(
                    identifier="FakeIdentifier",
                    name="FakeName",
                    description="FakeDescription",
                ),
                diagrams=[OntoUmlDiagram(identifier="FakeIdentifier", name="FakeName")],
            )

        # pylint:disable=[C0301]
        with self.subTest(
            """
            | identifier set | name set | description set | model set | diagrams set | expected result |
            | yes            | yes      | no              | yes       | yes          | no error        |
            """  # noqa=E501
        ):
            OntoUmlProject(
                identifier="FakeIdentifier",
                name="FakeName",
                description=None,
                model=OntoUmlPackage(
                    identifier="FakeIdentifier",
                    name="FakeName",
                    description="FakeDescription",
                ),
                diagrams=[OntoUmlDiagram(identifier="FakeIdentifier", name="FakeName")],
            )

        # pylint:disable=[C0301]
        with self.subTest(
            """
            | identifier set | name set | description set | model set | diagrams set | expected result |
            | no             | yes      | no              | yes       | yes          | no error        |
            """  # noqa=E501
        ):
            OntoUmlProject(
                identifier=None,
                name="FakeName",
                description=None,
                model=OntoUmlPackage(
                    identifier="FakeIdentifier",
                    name="FakeName",
                    description="FakeDescription",
                ),
                diagrams=[OntoUmlDiagram(identifier="FakeIdentifier", name="FakeName")],
            )

        # pylint:disable=[C0301]
        with self.subTest(
            """
            | identifier set | name set | description set | model set | diagrams set | expected result |
            | yes            | no       | no              | yes       | yes          | no error        |
            """  # noqa=E501
        ):
            OntoUmlProject(
                identifier="FakeIdentifier",
                name=None,
                description=None,
                model=OntoUmlPackage(
                    identifier="FakeIdentifier",
                    name="FakeName",
                    description="FakeDescription",
                ),
                diagrams=[OntoUmlDiagram(identifier="FakeIdentifier", name="FakeName")],
            )

        # pylint:disable=[C0301]
        with self.subTest(
            """
            | identifier set | name set | description set | model set | diagrams set | expected result |
            | no             | no       | no              | yes       | yes          | no error        |
            """  # noqa=E501
        ):
            OntoUmlProject(
                identifier=None,
                name=None,
                description=None,
                model=OntoUmlPackage(
                    identifier="FakeIdentifier",
                    name="FakeName",
                    description="FakeDescription",
                ),
                diagrams=[OntoUmlDiagram(identifier="FakeIdentifier", name="FakeName")],
            )

        # pylint:disable=[C0301]
        with self.subTest(
            """
            | identifier set | name set | description set | model set | diagrams set | expected result |
            | yes            | yes      | yes             | no        | yes          | no error        |
            """  # noqa=E501
        ):
            OntoUmlProject(
                identifier="FakeIdentifier",
                name="FakeName",
                description="FakeDescription",
                model=None,
                diagrams=[OntoUmlDiagram(identifier="FakeIdentifier", name="FakeName")],
            )

        # pylint:disable=[C0301]
        with self.subTest(
            """
            | identifier set | name set | description set | model set | diagrams set | expected result |
            | no             | yes      | yes             | no        | yes          | no error        |
            """  # noqa=E501
        ):
            OntoUmlProject(
                identifier=None,
                name="FakeName",
                description="FakeDescription",
                model=None,
                diagrams=[OntoUmlDiagram(identifier="FakeIdentifier", name="FakeName")],
            )

        # pylint:disable=[C0301]
        with self.subTest(
            """
            | identifier set | name set | description set | model set | diagrams set | expected result |
            | yes            | no       | yes             | no        | yes          | no error        |
            """  # noqa=E501
        ):
            OntoUmlProject(
                identifier="FakeIdentifier",
                name=None,
                description="FakeDescription",
                model=None,
                diagrams=[OntoUmlDiagram(identifier="FakeIdentifier", name="FakeName")],
            )

        # pylint:disable=[C0301]
        with self.subTest(
            """
            | identifier set | name set | description set | model set | diagrams set | expected result |
            | no             | no       | yes             | no        | yes          | no error        |
            """  # noqa=E501
        ):
            OntoUmlProject(
                identifier=None,
                name=None,
                description="FakeDescription",
                model=None,
                diagrams=[OntoUmlDiagram(identifier="FakeIdentifier", name="FakeName")],
            )

        # pylint:disable=[C0301]
        with self.subTest(
            """
            | identifier set | name set | description set | model set | diagrams set | expected result |
            | yes            | yes      | no              | no        | yes          | no error        |
            """  # noqa=E501
        ):
            OntoUmlProject(
                identifier="FakeIdentifier",
                name="FakeName",
                description=None,
                model=None,
                diagrams=[OntoUmlDiagram(identifier="FakeIdentifier", name="FakeName")],
            )

        # pylint:disable=[C0301]
        with self.subTest(
            """
            | identifier set | name set | description set | model set | diagrams set | expected result |
            | no             | yes      | no              | no        | yes          | no error        |
            """  # noqa=E501
        ):
            OntoUmlProject(
                identifier=None,
                name="FakeName",
                description=None,
                model=None,
                diagrams=[OntoUmlDiagram(identifier="FakeIdentifier", name="FakeName")],
            )

        # pylint:disable=[C0301]
        with self.subTest(
            """
            | identifier set | name set | description set | model set | diagrams set | expected result |
            | yes            | no       | no              | no        | yes          | no error        |
            """  # noqa=E501
        ):
            OntoUmlProject(
                identifier="FakeIdentifier",
                name=None,
                description=None,
                model=None,
                diagrams=[OntoUmlDiagram(identifier="FakeIdentifier", name="FakeName")],
            )

        # pylint:disable=[C0301]
        with self.subTest(
            """
            | identifier set | name set | description set | model set | diagrams set | expected result |
            | no             | no       | no              | no        | yes          | no error        |
            """  # noqa=E501
        ):
            OntoUmlProject(
                identifier=None,
                name=None,
                description=None,
                model=None,
                diagrams=[OntoUmlDiagram(identifier="FakeIdentifier", name="FakeName")],
            )

        # pylint:disable=[C0301]
        with self.subTest(
            """
            | identifier set | name set | description set | model set | diagrams set | expected result |
            | yes            | yes      | yes             | yes       | no           | no error        |
            """  # noqa=E501
        ):
            OntoUmlProject(
                identifier="FakeIdentifier",
                name="FakeName",
                description="FakeDescription",
                model=OntoUmlPackage(
                    identifier="FakeIdentifier",
                    name="FakeName",
                    description="FakeDescription",
                ),
                diagrams=None,
            )

        # pylint:disable=[C0301]
        with self.subTest(
            """
            | identifier set | name set | description set | model set | diagrams set | expected result |
            | no             | yes      | yes             | yes       | no           | no error        |
            """  # noqa=E501
        ):
            OntoUmlProject(
                identifier=None,
                name="FakeName",
                description="FakeDescription",
                model=OntoUmlPackage(
                    identifier="FakeIdentifier",
                    name="FakeName",
                    description="FakeDescription",
                ),
                diagrams=None,
            )

        # pylint:disable=[C0301]
        with self.subTest(
            """
            | identifier set | name set | description set | model set | diagrams set | expected result |
            | yes            | no       | yes             | yes       | no           | no error        |
            """  # noqa=E501
        ):
            OntoUmlProject(
                identifier="FakeIdentifier",
                name=None,
                description="FakeDescription",
                model=OntoUmlPackage(
                    identifier="FakeIdentifier",
                    name="FakeName",
                    description="FakeDescription",
                ),
                diagrams=None,
            )

        # pylint:disable=[C0301]
        with self.subTest(
            """
            | identifier set | name set | description set | model set | diagrams set | expected result |
            | no             | no       | yes             | yes       | no           | no error        |
            """  # noqa=E501
        ):
            OntoUmlProject(
                identifier=None,
                name=None,
                description="FakeDescription",
                model=OntoUmlPackage(
                    identifier="FakeIdentifier",
                    name="FakeName",
                    description="FakeDescription",
                ),
                diagrams=None,
            )

        # pylint:disable=[C0301]
        with self.subTest(
            """
            | identifier set | name set | description set | model set | diagrams set | expected result |
            | yes            | yes      | no              | yes       | no           | no error        |
            """  # noqa=E501
        ):
            OntoUmlProject(
                identifier="FakeIdentifier",
                name="FakeName",
                description=None,
                model=OntoUmlPackage(
                    identifier="FakeIdentifier",
                    name="FakeName",
                    description="FakeDescription",
                ),
                diagrams=None,
            )

        # pylint:disable=[C0301]
        with self.subTest(
            """
            | identifier set | name set | description set | model set | diagrams set | expected result |
            | no             | yes      | no              | yes       | no           | no error        |
            """  # noqa=E501
        ):
            OntoUmlProject(
                identifier=None,
                name="FakeName",
                description=None,
                model=OntoUmlPackage(
                    identifier="FakeIdentifier",
                    name="FakeName",
                    description="FakeDescription",
                ),
                diagrams=None,
            )

        # pylint:disable=[C0301]
        with self.subTest(
            """
            | identifier set | name set | description set | model set | diagrams set | expected result |
            | yes            | no       | no              | yes       | no           | no error        |
            """  # noqa=E501
        ):
            OntoUmlProject(
                identifier="FakeIdentifier",
                name=None,
                description=None,
                model=OntoUmlPackage(
                    identifier="FakeIdentifier",
                    name="FakeName",
                    description="FakeDescription",
                ),
                diagrams=None,
            )

        # pylint:disable=[C0301]
        with self.subTest(
            """
            | identifier set | name set | description set | model set | diagrams set | expected result |
            | no             | no       | no              | yes       | no           | no error        |
            """  # noqa=E501
        ):
            OntoUmlProject(
                identifier=None,
                name=None,
                description=None,
                model=OntoUmlPackage(
                    identifier="FakeIdentifier",
                    name="FakeName",
                    description="FakeDescription",
                ),
                diagrams=None,
            )

        # pylint:disable=[C0301]
        with self.subTest(
            """
            | identifier set | name set | description set | model set | diagrams set | expected result |
            | yes            | yes      | yes             | no        | no           | no error        |
            """  # noqa=E501
        ):
            OntoUmlProject(
                identifier="FakeIdentifier",
                name="FakeName",
                description="FakeDescription",
                model=None,
                diagrams=None,
            )

        # pylint:disable=[C0301]
        with self.subTest(
            """
            | identifier set | name set | description set | model set | diagrams set | expected result |
            | no             | yes      | yes             | no        | no           | no error        |
            """  # noqa=E501
        ):
            OntoUmlProject(
                identifier=None,
                name="FakeName",
                description="FakeDescription",
                model=None,
                diagrams=None,
            )

        # pylint:disable=[C0301]
        with self.subTest(
            """
            | identifier set | name set | description set | model set | diagrams set | expected result |
            | yes            | no       | yes             | no        | no           | no error        |
            """  # noqa=E501
        ):
            OntoUmlProject(
                identifier="FakeIdentifier",
                name=None,
                description="FakeDescription",
                model=None,
                diagrams=None,
            )

        # pylint:disable=[C0301]
        with self.subTest(
            """
            | identifier set | name set | description set | model set | diagrams set | expected result |
            | no             | no       | yes             | no        | no           | no error        |
            """  # noqa=E501
        ):
            OntoUmlProject(
                identifier=None,
                name=None,
                description="FakeDescription",
                model=None,
                diagrams=None,
            )

        # pylint:disable=[C0301]
        with self.subTest(
            """
            | identifier set | name set | description set | model set | diagrams set | expected result |
            | yes            | yes      | no              | no        | no           | no error        |
            """  # noqa=E501
        ):
            OntoUmlProject(
                identifier="FakeIdentifier",
                name="FakeName",
                description=None,
                model=None,
                diagrams=None,
            )

        # pylint:disable=[C0301]
        with self.subTest(
            """
            | identifier set | name set | description set | model set | diagrams set | expected result |
            | no             | yes      | no              | no        | no           | no error        |
            """  # noqa=E501
        ):
            OntoUmlProject(
                identifier=None,
                name="FakeName",
                description=None,
                model=None,
                diagrams=None,
            )

        # pylint:disable=[C0301]
        with self.subTest(
            """
            | identifier set | name set | description set | model set | diagrams set | expected result |
            | yes            | no       | no              | no        | no           | no error        |
            """  # noqa=E501
        ):
            OntoUmlProject(
                identifier="FakeIdentifier",
                name=None,
                description=None,
                model=None,
                diagrams=None,
            )

        # pylint:disable=[C0301]
        with self.subTest(
            """
            | identifier set | name set | description set | model set | diagrams set | expected result |
            | no             | no       | no              | no        | no           | no error        |
            """  # noqa=E501
        ):
            OntoUmlProject(None, None, None, None, None)

    def test_eq(self):
        # pylint:disable=[C0301]
        """
        1. Is other is None => False
        For remaining cases, see table below.
        | identifier equal | name equal | description equal | model equal | diagrams equal | expected result |
        | yes              | yes        | yes               | yes         | yes            | True            |
        | no               | yes        | yes               | yes         | yes            | False           |
        | yes              | no         | yes               | yes         | yes            | False           |
        | no               | no         | yes               | yes         | yes            | False           |
        | yes              | yes        | no                | yes         | yes            | False           |
        | no               | yes        | no                | yes         | yes            | False           |
        | yes              | no         | no                | yes         | yes            | False           |
        | no               | no         | no                | yes         | yes            | False           |
        | yes              | yes        | yes               | no          | yes            | False           |
        | no               | yes        | yes               | no          | yes            | False           |
        | yes              | no         | yes               | no          | yes            | False           |
        | no               | no         | yes               | no          | yes            | False           |
        | yes              | yes        | no                | no          | yes            | False           |
        | no               | yes        | no                | no          | yes            | False           |
        | yes              | no         | no                | no          | yes            | False           |
        | no               | no         | no                | no          | yes            | False           |
        | yes              | yes        | yes               | yes         | no             | False           |
        | no               | yes        | yes               | yes         | no             | False           |
        | yes              | no         | yes               | yes         | no             | False           |
        | no               | no         | yes               | yes         | no             | False           |
        | yes              | yes        | no                | yes         | no             | False           |
        | no               | yes        | no                | yes         | no             | False           |
        | yes              | no         | no                | yes         | no             | False           |
        | no               | no         | no                | yes         | no             | False           |
        | yes              | yes        | yes               | no          | no             | False           |
        | no               | yes        | yes               | no          | no             | False           |
        | yes              | no         | yes               | no          | no             | False           |
        | no               | no         | yes               | no          | no             | False           |
        | yes              | yes        | no                | no          | no             | False           |
        | no               | yes        | no                | no          | no             | False           |
        | yes              | no         | no                | no          | no             | False           |
        | no               | no         | no                | no          | no             | False           |
        """  # noqa=E501

        equals_mock = Mock()
        equals_mock.__eq__ = Mock(return_value=True)
        not_equals_mock = Mock()
        not_equals_mock.__eq__ = Mock(return_value=False)
        template = OntoUmlProject(
            identifier="FakeProjectIdentifier",
            name="FakeProjectName",
            description="FakeProjectDescription",
            model=equals_mock,
            diagrams=[equals_mock],
        )

        with self.subTest("1. Is other is None => False"):
            first = deepcopy(template)
            other = None
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        testcases = setup_equals_matrix(
            [
                "identifier",
                "name",
                "description",
                "model",
                "diagrams",
            ]
        )

        for testcase in testcases:
            with self.subTest(testcase.description):
                # First, setup the testcase.
                first = deepcopy(template)
                other = deepcopy(template)
                if all(
                    item.invariant == invariant_equals_other
                    for item in testcase.single_invariants
                ):
                    # No need to change any attributes, just compare the two and assert
                    # they are equal
                    self.assertTrue(first == other, testcase.description)
                    self.assertEqual(first, other, testcase.description)
                else:
                    # First, change the different attributes, then compare the two and
                    # assert they are not equal
                    for testable in [
                        item
                        for item in testcase.single_invariants
                        if item.invariant == invariant_does_not_equal_other
                    ]:
                        match testable.condition.description:
                            case "model":  # OntoUmlPackage
                                # Set not_equals_mock on first, as the __eq__ method
                                # gets called on first
                                setattr(
                                    first,
                                    testable.condition.identifier,
                                    not_equals_mock,
                                )
                            case "diagrams":  # List[OntoUmlDiagram]
                                # Set not_equals_mock on first, as the __eq__ method
                                # gets called on first
                                setattr(
                                    first,
                                    testable.condition.identifier,
                                    [not_equals_mock],
                                )
                            case _:
                                worker = getattr(other, testable.condition.identifier)
                                worker = f"Another{worker}"
                                setattr(other, testable.condition.identifier, worker)
                    self.assertFalse(first == other, testcase.description)
                    self.assertNotEqual(first, other, testcase.description)
