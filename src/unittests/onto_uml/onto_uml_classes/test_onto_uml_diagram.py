"""Test suites for onto_uml.onto_uml_diagram"""

from copy import deepcopy
from unittest import TestCase

from src.onto_uml.onto_uml_diagram import OntoUmlDiagram, OntoUmlDiagramOwner


class TestOntoUmlDiagram(TestCase):
    """
    Test suite for OntoUML diagram interface
    - Constructor (__init__) is not tested as it simply assigns arguments to members
    - comparer (__eq__) is tested
    - properties are not tested as they simply return the hidden member values
    """

    # pylint: disable=[W0212]
    def test_eq(self):
        """
        Test the comparison method.
        1. The other OntoUMLDiagram is None -> return False
        2. The super equals other.super, and the specific member is equal too -> return
           True
        3. The super equals other.super, and the specific member is not equal -> return
           False
        4. The super does not equal other.super, but the specific member is equal
           -> return False
        5. The super does not equal other.super, and the specific member is not equal
           -> return False
        """

        template: OntoUmlDiagram = OntoUmlDiagram(
            identifier="FakeIdentifier",
            name="FakeName",
            description="FakeDescription",
            owner=OntoUmlDiagramOwner("FakeId_2", "Package", "FakeReference"),
            contents=["FakeContent"],
        )
        with self.subTest(
            """
            1. The other OntoUMLDiagram is None -> return False
            """
        ):
            first = deepcopy(template)
            second: OntoUmlDiagram = None
            self.assertFalse(first == second)
            self.assertNotEqual(first, second)

        with self.subTest(
            """
            2. The super equals other.super, and the specific member is equal too
               -> return True
            """
        ):
            first = deepcopy(template)
            second = deepcopy(template)
            self.assertTrue(first == second)
            self.assertEqual(first, second)

        with self.subTest(
            """
            3. The super equals other.super, and the specific member is not equal
               -> return False
            """
        ):
            first = deepcopy(template)
            second = deepcopy(template)
            second.owner = OntoUmlDiagramOwner(
                "FakeId_3", "Package", "OtherFakeReference"
            )
            self.assertFalse(first == second)
            self.assertNotEqual(first, second)

        with self.subTest(
            """
            4. The super does not equal other.super, but the specific member is equal
               -> return False
            """
        ):
            first = deepcopy(template)
            second = deepcopy(template)
            second.description = "AnotherFakeDescription"
            self.assertFalse(first == second)
            self.assertNotEqual(first, second)

        with self.subTest(
            """
            5. The super does not equal other.super, and the specific member is not
               equal -> return False
            """
        ):
            first = deepcopy(template)
            second = deepcopy(template)
            second.description = "AnotherFakeDescription"
            second.owner = OntoUmlDiagramOwner(
                "FakeId_4", "Package", "AnotherFakeReference"
            )
            self.assertFalse(first == second)
            self.assertNotEqual(first, second)
