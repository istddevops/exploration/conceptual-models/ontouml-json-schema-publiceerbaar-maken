"""
Test suites for the module `src.factories.excel_to_thesaurus`
"""


from copy import deepcopy
from dataclasses import dataclass, field
from typing import Any, Dict, List
from unittest import TestCase
from unittest.mock import patch

from src.factories.excel_to_thesaurus import from_ror_thesaurus
from src.thesaurus.regie_op_registers import ThesaurusRegieOpRegisters


@dataclass
class FakeIterator:
    """Fake iterator class"""

    iterable: Any = field(default_factory=list)
    _current_index: int = 0

    def __iter__(self):
        return self

    def __next__(self):
        if self._current_index < len(self.iterable):
            if isinstance(self.iterable, list):
                result = self.iterable[self._current_index]
            elif isinstance(self.iterable, dict):
                result = self.iterable[list(self.iterable.keys())[self._current_index]]
            else:
                raise TypeError("iterable")

            self._current_index += 1
            if isinstance(result, FakeCell):
                result.column = self._current_index
            return result

        raise StopIteration


@dataclass
class FakeCell:
    """
    Fake Excel cell
    """

    value: Any


@dataclass
class FakeRow:
    """
    Fake Excel row
    """

    cells: List[FakeCell] = field(default_factory=list)

    def __iter__(self):
        return FakeIterator(self.cells)

    def __getitem__(self, indexer):
        if isinstance(indexer, int):
            if indexer > len(self.cells):
                raise IndexError

            return self.cells[indexer]

        raise TypeError("indexer")


@dataclass
class FakeWorksheet:
    """
    Fake Excel worksheet
    """

    title: str
    rows: List[FakeRow] = field(default_factory=list)

    def __iter__(self):
        return FakeIterator(iterable=self.rows)


@dataclass
class FakeWorkbook:
    """
    Fake Excel workbook.
    """

    worksheets: Dict[str, FakeWorksheet] = field(default_factory=dict)
    _current_index: int = 0

    def __iter__(self):
        return self.worksheets

    def __next__(self):
        if self._current_index < len(self.worksheets):
            result = self.worksheets[list(self.worksheets.keys())[self._current_index]]
            self._current_index += 1
            return result

        raise StopIteration

    def __contains__(self, item):
        return item in self.worksheets

    def __getitem__(self, indexer):
        match type(indexer).__name__:
            case "int":
                if indexer > len(self.worksheets):
                    raise IndexError
                return self.worksheets.values()[indexer]
            case "str":
                result = [
                    item for item in self.worksheets.values() if item.title == indexer
                ]
                if 1 == len(result):
                    return result[0]
                raise IndexError
            case _:
                raise TypeError("indexer")


class TestFromRoRThesaurus(TestCase):
    """Test suite for the `from_ror_thesaurus` method"""

    def test_non_thesaurus_excel(self):
        """
        Tests the behaviour in case the `ror_thesaurus` Excel file is not a 'valid ROR
        Thesaurus'.
        1. The excel does not contain a worksheet named "Thesaurus RoR DG"
           => `None` returned
        2. The excel does contain a worksheet named "Thesaurus RoR DG", but has no rows
           => `None` returned
        3. The excel file does contain a worksheet named "Thesaurus RoR DG", but not
           all of the columns ["ThesaurusIMAR_ID", "Gekozen concept",
           "Concept ZIN (r21)", "Concept TZW", "Concept NTS (SNOMED)", "Concept ZiRA",
           "Toelichting NTS (SNOMED)", "Toelichting TZW", "Toelichting ZiRA",
           "Toelichting RoR"] are present on this worksheet
           => `None` returned
        4. The excel sheet does contain a worksheet named "Thesaurus RoR DG" and all
           of the columns ["ThesaurusIMAR_ID", "Gekozen concept", "Concept ZIN (r21)",
           "Concept TZW", "Concept NTS (SNOMED)", "Concept ZiRA",
           "Toelichting NTS (SNOMED)", "Toelichting TZW", "Toelichting ZiRA",
           "Toelichting RoR"] are present on this worksheet, followed by at least one
           data row => no error
        """

        with self.subTest(
            """
            1. The excel does not contain a worksheet named "Thesaurus RoR DG"
               => `None` returned
            """
        ):
            fake_workbook = FakeWorkbook({})
            with patch(
                "src.factories.excel_to_thesaurus.load_workbook",
                return_value=fake_workbook,
            ):
                self.assertIsNone(from_ror_thesaurus("fake.xlsx"))

        with self.subTest(
            """
            2. The excel does contain a worksheet named "Thesaurus RoR DG", but has no
               rows => `None` returned
            """
        ):
            fake_workbook = FakeWorkbook(
                {"Thesaurus RoR DG": FakeWorksheet("Thesaurus RoR DG")}
            )
            with patch(
                "src.factories.excel_to_thesaurus.load_workbook",
                return_value=fake_workbook,
            ):
                self.assertIsNone(from_ror_thesaurus("fake.xlsx"))

        with self.subTest(
            """
            3. The excel file does contain a worksheet named "Thesaurus RoR DG", but not
               all of the columns ["ThesaurusIMAR_ID", "Gekozen concept",
               "Concept ZIN (r21)", "Concept TZW", "Concept NTS (SNOMED)",
               "Concept ZiRA", "Toelichting NTS (SNOMED)", "Toelichting TZW",
               "Toelichting ZiRA", "Toelichting RoR"] are present on this worksheet
               => `None` returned
               Invariant: no header columns present at all
            """
        ):
            fake_workbook = FakeWorkbook(
                {"Thesaurus RoR DG": FakeWorksheet("Thesaurus RoR DG", [FakeRow()])}
            )
            with patch(
                "src.factories.excel_to_thesaurus.load_workbook",
                return_value=fake_workbook,
            ):
                self.assertIsNone(from_ror_thesaurus("fake.xlsx"))

        with self.subTest(
            """
            3. The excel file does contain a worksheet named "Thesaurus RoR DG", but not
               all of the columns ["ThesaurusIMAR_ID", "Gekozen concept",
               "Concept ZIN (r21)", "Concept TZW", "Concept NTS (SNOMED)",
               "Concept ZiRA", "Toelichting NTS (SNOMED)", "Toelichting TZW",
               "Toelichting ZiRA", "Toelichting RoR"] are present on this worksheet
               => `None` returned
               Invariant: a few header columns missing
            """
        ):
            fake_workbook = FakeWorkbook(
                {
                    "Thesaurus RoR DG": FakeWorksheet(
                        "Thesaurus RoR DG",
                        [
                            FakeRow(
                                [
                                    FakeCell("ThesaurusIMAR_ID"),
                                    FakeCell("Concept ZIN (r21)"),
                                    FakeCell("Concept NTS (SNOMED)"),
                                    FakeCell("Toelichting NTS (SNOMED)"),
                                    FakeCell("Toelichting ZiRA"),
                                ]
                            )
                        ],
                    )
                }
            )
            with patch(
                "src.factories.excel_to_thesaurus.load_workbook",
                return_value=fake_workbook,
            ):
                self.assertIsNone(from_ror_thesaurus("fake.xlsx"))

        with self.subTest(
            """
            4. The excel sheet does contain a worksheet named "Thesaurus RoR DG" and all
               of the columns ["ThesaurusIMAR_ID", "Gekozen concept",
               "Concept ZIN (r21)", "Concept TZW", "Concept NTS (SNOMED)",
               "Concept ZiRA", "Toelichting NTS (SNOMED)", "Toelichting TZW",
               "Toelichting ZiRA", "Toelichting RoR"] are present on this worksheet,
               followed by at least one data row => no error
               Invariant, only the header columns are present, no additional columns
            """
        ):
            fake_workbook = FakeWorkbook(
                {
                    "Thesaurus RoR DG": FakeWorksheet(
                        "Thesaurus RoR DG",
                        [
                            FakeRow(
                                [
                                    FakeCell("ThesaurusIMAR_ID"),
                                    FakeCell("Gekozen concept"),
                                    FakeCell("Concept ZIN (r21)"),
                                    FakeCell("Concept TZW"),
                                    FakeCell("Concept NTS (SNOMED)"),
                                    FakeCell("Concept ZiRA"),
                                    FakeCell("Toelichting NTS (SNOMED)"),
                                    FakeCell("Toelichting TZW"),
                                    FakeCell("Toelichting ZiRA"),
                                    FakeCell("Toelichting RoR"),
                                ]
                            ),
                            FakeRow(
                                [
                                    FakeCell("FakeId"),
                                    FakeCell("TZW"),
                                    FakeCell("FakeConceptZin"),
                                    FakeCell("FakeConceptTzw"),
                                    FakeCell("FakeConceptNtsSnomed"),
                                    FakeCell("FakeConceptZira"),
                                    FakeCell("FakeToelichtingNtsSnomed"),
                                    FakeCell("FakeToelichtingTzw"),
                                    FakeCell("FakeToelichtingZira"),
                                    FakeCell("FakeToelichtingRoR"),
                                ]
                            ),
                        ],
                    )
                }
            )
            with patch(
                "src.factories.excel_to_thesaurus.load_workbook",
                return_value=fake_workbook,
            ):
                from_ror_thesaurus("fake.xlsx")

        with self.subTest(
            """
            4. The excel sheet does contain a worksheet named "Thesaurus RoR DG" and all
               of the columns ["ThesaurusIMAR_ID", "Gekozen concept",
               "Concept ZIN (r21)", "Concept TZW", "Concept NTS (SNOMED)",
               "Concept ZiRA", "Toelichting NTS (SNOMED)", "Toelichting TZW",
               "Toelichting ZiRA", "Toelichting RoR"] are present on this worksheet,
               followed by at least one data row => no error
               Invariant, additional columns present besides the header columns
            """
        ):
            fake_workbook = FakeWorkbook(
                {
                    "Thesaurus RoR DG": FakeWorksheet(
                        "Thesaurus RoR DG",
                        [
                            FakeRow(
                                [
                                    FakeCell("ThesaurusIMAR_ID"),
                                    FakeCell("FakeHeader1"),
                                    FakeCell("Gekozen concept"),
                                    FakeCell("FakeHeader2"),
                                    FakeCell("Concept ZIN (r21)"),
                                    FakeCell("FakeHeader3"),
                                    FakeCell("Concept TZW"),
                                    FakeCell("FakeHeader4"),
                                    FakeCell("Concept NTS (SNOMED)"),
                                    FakeCell("FakeHeader5"),
                                    FakeCell("Concept ZiRA"),
                                    FakeCell("FakeHeader6"),
                                    FakeCell("Toelichting NTS (SNOMED)"),
                                    FakeCell("FakeHeader7"),
                                    FakeCell("Toelichting TZW"),
                                    FakeCell("FakeHeader8"),
                                    FakeCell("Toelichting ZiRA"),
                                    FakeCell("FakeHeader9"),
                                    FakeCell("Toelichting RoR"),
                                ]
                            ),
                            FakeRow(
                                [
                                    FakeCell("FakeId"),
                                    FakeCell("FakeValue1"),
                                    FakeCell("TZW"),
                                    FakeCell("FakeValue2"),
                                    FakeCell("FakeConceptZin"),
                                    FakeCell("FakeValue3"),
                                    FakeCell("FakeConceptTzw"),
                                    FakeCell("FakeValue4"),
                                    FakeCell("FakeConceptNtsSnomed"),
                                    FakeCell("FakeValue5"),
                                    FakeCell("FakeConceptZira"),
                                    FakeCell("FakeValue6"),
                                    FakeCell("FakeToelichtingNtsSnomed"),
                                    FakeCell("FakeValue7"),
                                    FakeCell("FakeToelichtingTzw"),
                                    FakeCell("FakeValue8"),
                                    FakeCell("FakeToelichtingZira"),
                                    FakeCell("FakeValue9"),
                                    FakeCell("FakeToelichtingRoR"),
                                ]
                            ),
                        ],
                    )
                }
            )
            with patch(
                "src.factories.excel_to_thesaurus.load_workbook",
                return_value=fake_workbook,
            ):
                from_ror_thesaurus("fake.xlsx")

    def test_thesaurus_excel_has_data(self):
        """
        Check to see that the thesaurus Excel file actually has any data rows.
        We expect the first row to be the header row (see `test_non_thesaurus_excel`),
        but the following rows should contain data.
        1. No data rows found => `None`
        2. Data rows found, but column "ThesaurusIMAR_ID" does not contain a value
           => `None`
        3. Data rows found => `ThesaurusRegieOpRegisters` object created succesfully
        """

        template_workbook = FakeWorkbook(
            {
                "Thesaurus RoR DG": FakeWorksheet(
                    "Thesaurus RoR DG",
                    [
                        FakeRow(
                            [
                                FakeCell("ThesaurusIMAR_ID"),
                                FakeCell("Gekozen concept"),
                                FakeCell("Concept ZIN (r21)"),
                                FakeCell("Concept TZW"),
                                FakeCell("Concept NTS (SNOMED)"),
                                FakeCell("Concept ZiRA"),
                                FakeCell("Toelichting NTS (SNOMED)"),
                                FakeCell("Toelichting TZW"),
                                FakeCell("Toelichting ZiRA"),
                                FakeCell("Toelichting RoR"),
                            ]
                        )
                    ],
                )
            }
        )

        with self.subTest("""1. No data rows found => `None`"""):
            fake_workbook = deepcopy(template_workbook)
            with patch(
                "src.factories.excel_to_thesaurus.load_workbook",
                return_value=fake_workbook,
            ):
                self.assertIsNone(from_ror_thesaurus("fake.xlsx"))

        with self.subTest(
            """
            2. Data rows found, but column "ThesaurusIMAR_ID" does not contain a value
               => `None`
               Invariant, cell not set (`None`)
            """
        ):
            fake_workbook = deepcopy(template_workbook)
            fake_workbook["Thesaurus RoR DG"].rows.append(
                FakeRow(
                    [
                        FakeCell(None),  # ThesaurusIMAR_ID
                        FakeCell("TZW"),  # "Gekozen concept
                        FakeCell(None),  # Concept ZIN (r21)
                        FakeCell("Fake concept"),  # Concept TZW
                        FakeCell("Fake concept SNOW"),  # Concept NTS (SNOMED)
                        FakeCell(None),  # Concept ZiRA
                        FakeCell(None),  # Toelichting NTS (SNOMED)
                        FakeCell("Fake toelichting"),  # Toelichting TZW
                        FakeCell(None),  # Toelichting ZiRA
                        FakeCell(None),  # Toelichting RoR
                    ]
                )
            )
            with patch(
                "src.factories.excel_to_thesaurus.load_workbook",
                return_value=fake_workbook,
            ):
                self.assertIsNone(from_ror_thesaurus("fake.xlsx"))

        with self.subTest(
            """
            2. Data rows found, but column "ThesaurusIMAR_ID" does not contain a value
               => `None`
               Invariant, cell set to empty string(`""`)
            """
        ):
            fake_workbook = deepcopy(template_workbook)
            fake_workbook["Thesaurus RoR DG"].rows.append(
                FakeRow(
                    [
                        FakeCell(""),  # ThesaurusIMAR_ID
                        FakeCell("TZW"),  # "Gekozen concept
                        FakeCell(None),  # Concept ZIN (r21)
                        FakeCell("Fake concept"),  # Concept TZW
                        FakeCell("Fake concept SNOW"),  # Concept NTS (SNOMED)
                        FakeCell(None),  # Concept ZiRA
                        FakeCell(None),  # Toelichting NTS (SNOMED)
                        FakeCell("Fake toelichting"),  # Toelichting TZW
                        FakeCell(None),  # Toelichting ZiRA
                        FakeCell(None),  # Toelichting RoR
                    ]
                )
            )
            with patch(
                "src.factories.excel_to_thesaurus.load_workbook",
                return_value=fake_workbook,
            ):
                self.assertIsNone(from_ror_thesaurus("fake.xlsx"))

        with self.subTest(
            """
            3. Data rows found => `ThesaurusRegieOpRegisters` object created succesfully
            """
        ):
            fake_workbook = deepcopy(template_workbook)
            with patch(
                "src.factories.excel_to_thesaurus.load_workbook",
                return_value=fake_workbook,
            ):
                fake_workbook["Thesaurus RoR DG"].rows.append(
                    FakeRow(
                        [
                            FakeCell(1),  # ThesaurusIMAR_ID
                            FakeCell("TZW"),  # "Gekozen concept
                            FakeCell(None),  # Concept ZIN (r21)
                            FakeCell("Fake concept"),  # Concept TZW
                            FakeCell("Fake concept SNOW"),  # Concept NTS (SNOMED)
                            FakeCell(None),  # Concept ZiRA
                            FakeCell(None),  # Toelichting NTS (SNOMED)
                            FakeCell("Fake toelichting"),  # Toelichting TZW
                            FakeCell(None),  # Toelichting ZiRA
                            FakeCell(None),  # Toelichting RoR
                        ]
                    )
                )
                self.assertIsInstance(
                    from_ror_thesaurus("fake.xlsx"), ThesaurusRegieOpRegisters
                )
