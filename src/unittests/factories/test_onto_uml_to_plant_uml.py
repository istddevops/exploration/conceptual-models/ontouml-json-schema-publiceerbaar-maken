"""
Test suites for the module `src.factories.onto_uml_to_plant_uml`
"""


from unittest import TestCase
from unittest.mock import mock_open, patch

from src.factories.onto_uml_to_plant_uml import _build_link, to_plant_uml_diagram
from src.onto_uml.onto_uml_diagram import OntoUmlDiagram
from src.onto_uml.onto_uml_property_assignments import OntoUmlPropertyAssignments
from src.onto_uml_json_importer import _import_onto_uml_json_project
from src.thesaurus.thesaurus import ThesaurusBase


class TestToPlantUmlDiagram(TestCase):
    """Test suite for factory method to_plant_uml_diagram"""

    def test_build_link_arguments(self):
        """
        Unit test for the `_build_link` method.
        - One or more of the arguments `property_assignments`, `thesaurus_file` or
          `thesaurus` not set => `None`
        - `property_assignments.thesaurus_imar_id` not in `thesaurus` => `None`
        - The resulting link is built as follows:
          `thesaurus_file`/#<concept>`property_assignments.thesaurus_imar_id`{betekenis}
          * <concept> is the name of the concept for the `thesaurus` item with ID
            `property_assignments.thesaurus_imar_id`, but with the following characters
            replaced by a dash (`-`):
            + space (` `)
            + open bracket (`(`)
            + closing bracket (`)`)
          * {betekenis} is the meaning of the concept for the `thesaurus` item with ID
            `property_assignments.thesaurus_imar_id`, but with newlines replaced by
            `<br />`
        Test cases:
        1. Arguments
        | property_assignments | thesaurus_file | thesaurus | return `None` |
        | ==================== | ============== | ========= | ============= |
        | set                  | set            | set       | no            |
        | not set              | set            | set       | yes           |
        | set                  | not set        | set       | yes           |
        | not set              | not set        | set       | yes           |
        | set                  | set            | not set   | yes           |
        | not set              | set            | not set   | yes           |
        | set                  | not set        | not set   | yes           |
        | not set              | not set        | not set   | yes           |
        2. Known `thesaurus_imar_id`
        | thesaurus_imar_id known | return `None` |
        | yes                     | no            |
        | no                      | yes           |
        3. Built link: check to see it has the described values
        Edge cases:
        | has space? | has open bracket? | has close bracket? |
        | yes        | yes               | yes                |
        | no         | yes               | yes                |
        | yes        | no                | yes                |
        | no         | no                | yes                |
        | yes        | yes               | no                 |
        | no         | yes               | no                 |
        | yes        | no                | no                 |
        | no         | no                | no                 |
        4. Built link: check to see if the description is added (only when set)
        | has description? | description has newline? |
        | yes              | yes                      |
        | no               | n/a                      |
        | yes              | no                       |
        """

        property_assignment = OntoUmlPropertyAssignments("1")
        thesaurus_template = type(
            "FakeThesaurusItem",
            (object,),
            {"identifier": "1", "concept": "FakeConcept", "meaning": None},
        )
        thesaurus_file = "fake_thesaurus.md"

        thesaurus = ThesaurusBase("FakeThesaurus", [thesaurus_template])

        with self.subTest(
            """
            1. Arguments
            | property_assignments | thesaurus_file | thesaurus | return `None` |
            | ==================== | ============== | ========= | ============= |
            | set                  | set            | set       | no            |
            """
        ):
            self.assertIsNotNone(
                _build_link(property_assignment, thesaurus_file, thesaurus)
            )

        with self.subTest(
            """
            1. Arguments
            | property_assignments | thesaurus_file | thesaurus | return `None` |
            | ==================== | ============== | ========= | ============= |
            | not set              | set            | set       | yes           |
            """
        ):
            self.assertIsNone(_build_link(None, thesaurus_file, thesaurus))

        with self.subTest(
            """
            1. Arguments
            | property_assignments | thesaurus_file | thesaurus | return `None` |
            | ==================== | ============== | ========= | ============= |
            | set                  | not set        | set       | yes           |
            """
        ):
            self.assertIsNone(_build_link(property_assignment, None, thesaurus))

        with self.subTest(
            """
            1. Arguments
            | property_assignments | thesaurus_file | thesaurus | return `None` |
            | ==================== | ============== | ========= | ============= |
            | not set              | not set        | set       | yes           |
            """
        ):
            self.assertIsNone(_build_link(None, None, thesaurus))

        with self.subTest(
            """
            1. Arguments
            | property_assignments | thesaurus_file | thesaurus | return `None` |
            | ==================== | ============== | ========= | ============= |
            | set                  | set            | not set   | yes           |
            """
        ):
            self.assertIsNone(_build_link(property_assignment, thesaurus_file, None))

        with self.subTest(
            """
            1. Arguments
            | property_assignments | thesaurus_file | thesaurus | return `None` |
            | ==================== | ============== | ========= | ============= |
            | not set              | set            | not set   | yes           |
            """
        ):
            self.assertIsNone(_build_link(None, thesaurus_file, None))

        with self.subTest(
            """
            1. Arguments
            | property_assignments | thesaurus_file | thesaurus | return `None` |
            | ==================== | ============== | ========= | ============= |
            | set                  | not set        | not set   | yes           |
            """
        ):
            self.assertIsNone(_build_link(property_assignment, None, None))

        with self.subTest(
            """
            1. Arguments
            | property_assignments | thesaurus_file | thesaurus | return `None` |
            | ==================== | ============== | ========= | ============= |
            | not set              | not set        | not set   | yes           |
            """
        ):
            self.assertIsNone(_build_link(None, None, None))

    def test_build_link_thesausur_imar_id_in_thesaurus(self):
        """
        Unit test for the `_build_link` method.
        - One or more of the arguments `property_assignments`, `thesaurus_file` or
          `thesaurus` not set => `None`
        - `property_assignments.thesaurus_imar_id` not in `thesaurus` => `None`
        - The resulting link is built as follows:
          `thesaurus_file`/#<concept>`property_assignments.thesaurus_imar_id`{betekenis}
          * <concept> is the name of the concept for the `thesaurus` item with ID
            `property_assignments.thesaurus_imar_id`, but with the following characters
            replaced by a dash (`-`):
            + space (` `)
            + open bracket (`(`)
            + closing bracket (`)`)
          * {betekenis} is the meaning of the concept for the `thesaurus` item with ID
            `property_assignments.thesaurus_imar_id`, but with newlines replaced by
            `<br />`
        Test cases:
        1. Arguments
        | property_assignments | thesaurus_file | thesaurus | return `None` |
        | ==================== | ============== | ========= | ============= |
        | set                  | set            | set       | no            |
        | not set              | set            | set       | yes           |
        | set                  | not set        | set       | yes           |
        | not set              | not set        | set       | yes           |
        | set                  | set            | not set   | yes           |
        | not set              | set            | not set   | yes           |
        | set                  | not set        | not set   | yes           |
        | not set              | not set        | not set   | yes           |
        2. Known `thesaurus_imar_id`
        | thesaurus_imar_id known | return `None` |
        | yes                     | no            |
        | no                      | yes           |
        3. Built link: check to see it has the described values
        Edge cases:
        | has space? | has open bracket? | has close bracket? |
        | yes        | yes               | yes                |
        | no         | yes               | yes                |
        | yes        | no                | yes                |
        | no         | no                | yes                |
        | yes        | yes               | no                 |
        | no         | yes               | no                 |
        | yes        | no                | no                 |
        | no         | no                | no                 |
        4. Built link: check to see if the description is added (only when set)
        | has description? | description has newline? |
        | yes              | yes                      |
        | no               | n/a                      |
        | yes              | no                       |
        """

        property_assignment = OntoUmlPropertyAssignments("1")
        thesaurus_template = type(
            "FakeThesaurusItem",
            (object,),
            {"identifier": "1", "concept": "FakeConcept", "meaning": None},
        )
        thesaurus_file = "fake_thesaurus.md"

        thesaurus = ThesaurusBase("FakeThesaurus", [thesaurus_template])

        with self.subTest(
            """
            2. Known `thesaurus_imar_id`
            | thesaurus_imar_id known | return `None` |
            | yes                     | no            |
            """
        ):
            self.assertIsNotNone(
                _build_link(property_assignment, thesaurus_file, thesaurus)
            )

        with self.subTest(
            """
            2. Known `thesaurus_imar_id`
            | thesaurus_imar_id known | return `None` |
            | no                      | yes           |
            """
        ):
            new_thesaurus = ThesaurusBase(
                "AnotherFakeThesaurus",
                [
                    type(
                        "AnotherFakeThesaurusItem",
                        (object,),
                        {
                            "identifier": "-1",
                            "concept": "Another fake concept",
                            "meaning": None,
                        },
                    )
                ],
            )
            self.assertIsNone(
                _build_link(property_assignment, thesaurus_file, new_thesaurus)
            )

    def test_build_link_format_of_concept(self):
        """
        Unit test for the `_build_link` method.
        - One or more of the arguments `property_assignments`, `thesaurus_file` or
          `thesaurus` not set => `None`
        - `property_assignments.thesaurus_imar_id` not in `thesaurus` => `None`
        - The resulting link is built as follows:
          `thesaurus_file`/#<concept>`property_assignments.thesaurus_imar_id`{betekenis}
          * <concept> is the name of the concept for the `thesaurus` item with ID
            `property_assignments.thesaurus_imar_id`, but with the following characters
            replaced by a dash (`-`):
            + space (` `)
            + open bracket (`(`)
            + closing bracket (`)`)
          * {betekenis} is the meaning of the concept for the `thesaurus` item with ID
            `property_assignments.thesaurus_imar_id`, but with newlines replaced by
            `<br />`
        Test cases:
        1. Arguments
        | property_assignments | thesaurus_file | thesaurus | return `None` |
        | ==================== | ============== | ========= | ============= |
        | set                  | set            | set       | no            |
        | not set              | set            | set       | yes           |
        | set                  | not set        | set       | yes           |
        | not set              | not set        | set       | yes           |
        | set                  | set            | not set   | yes           |
        | not set              | set            | not set   | yes           |
        | set                  | not set        | not set   | yes           |
        | not set              | not set        | not set   | yes           |
        2. Known `thesaurus_imar_id`
        | thesaurus_imar_id known | return `None` |
        | yes                     | no            |
        | no                      | yes           |
        3. Built link: check to see it has the described values
        Edge cases:
        | has space? | has open bracket? | has close bracket? |
        | yes        | yes               | yes                |
        | no         | yes               | yes                |
        | yes        | no                | yes                |
        | no         | no                | yes                |
        | yes        | yes               | no                 |
        | no         | yes               | no                 |
        | yes        | no                | no                 |
        | no         | no                | no                 |
        4. Built link: check to see if the description is added (only when set)
        | has description? | description has newline? |
        | yes              | yes                      |
        | no               | n/a                      |
        | yes              | no                       |
        """

        property_assignment = OntoUmlPropertyAssignments("1")
        thesaurus_file = "fake_thesaurus.md"

        with self.subTest(
            """
            3. Built link: check to see it has the described values
            Edge cases:
            | has space? | has open bracket? | has close bracket? |
            | yes        | yes               | yes                |
            """
        ):
            testee = ThesaurusBase(
                "AnotherFakeThesaurus",
                [
                    type(
                        "AnotherFakeThesaurusItem",
                        (object,),
                        {
                            "identifier": "1",
                            "concept": "Fake (Concept)",
                            "meaning": None,
                        },
                    )
                ],
            )
            expected = f"{thesaurus_file}/#fake--concept-1"
            self.assertEqual(
                _build_link(property_assignment, thesaurus_file, testee), expected
            )

        with self.subTest(
            """
            3. Built link: check to see it has the described values
            Edge cases:
            | has space? | has open bracket? | has close bracket? |
            | no         | yes               | yes                |
            """
        ):
            testee = ThesaurusBase(
                "AnotherFakeThesaurus",
                [
                    type(
                        "AnotherFakeThesaurusItem",
                        (object,),
                        {
                            "identifier": "1",
                            "concept": "Fake(Concept)",
                            "meaning": None,
                        },
                    )
                ],
            )
            expected = f"{thesaurus_file}/#fake-concept-1"
            self.assertEqual(
                _build_link(property_assignment, thesaurus_file, testee), expected
            )

        with self.subTest(
            """
            3. Built link: check to see it has the described values
            Edge cases:
            | has space? | has open bracket? | has close bracket? |
            | yes        | no                | yes                |
            """
        ):
            testee = ThesaurusBase(
                "AnotherFakeThesaurus",
                [
                    type(
                        "AnotherFakeThesaurusItem",
                        (object,),
                        {
                            "identifier": "1",
                            "concept": "Fake Concept)",
                            "meaning": None,
                        },
                    )
                ],
            )
            expected = f"{thesaurus_file}/#fake-concept-1"
            self.assertEqual(
                _build_link(property_assignment, thesaurus_file, testee), expected
            )

        with self.subTest(
            """
            3. Built link: check to see it has the described values
            Edge cases:
            | has space? | has open bracket? | has close bracket? |
            | no         | no                | yes                |
            """
        ):
            testee = ThesaurusBase(
                "AnotherFakeThesaurus",
                [
                    type(
                        "AnotherFakeThesaurusItem",
                        (object,),
                        {"identifier": "1", "concept": "FakeConcept)", "meaning": None},
                    )
                ],
            )
            expected = f"{thesaurus_file}/#fakeconcept-1"
            self.assertEqual(
                _build_link(property_assignment, thesaurus_file, testee), expected
            )

        with self.subTest(
            """
            3. Built link: check to see it has the described values
            Edge cases:
            | has space? | has open bracket? | has close bracket? |
            | yes        | yes               | no                 |
            """
        ):
            testee = ThesaurusBase(
                "AnotherFakeThesaurus",
                [
                    type(
                        "AnotherFakeThesaurusItem",
                        (object,),
                        {
                            "identifier": "1",
                            "concept": "Fake (Concept",
                            "meaning": None,
                        },
                    )
                ],
            )
            expected = f"{thesaurus_file}/#fake--concept1"
            self.assertEqual(
                _build_link(property_assignment, thesaurus_file, testee), expected
            )

        with self.subTest(
            """
            3. Built link: check to see it has the described values
            Edge cases:
            | has space? | has open bracket? | has close bracket? |
            | no         | yes               | no                 |
            """
        ):
            testee = ThesaurusBase(
                "AnotherFakeThesaurus",
                [
                    type(
                        "AnotherFakeThesaurusItem",
                        (object,),
                        {"identifier": "1", "concept": "Fake(Concept", "meaning": None},
                    )
                ],
            )
            expected = f"{thesaurus_file}/#fake-concept1"
            self.assertEqual(
                _build_link(property_assignment, thesaurus_file, testee), expected
            )

        with self.subTest(
            """
            3. Built link: check to see it has the described values
            Edge cases:
            | has space? | has open bracket? | has close bracket? |
            | yes        | no                | no                 |
            """
        ):
            testee = ThesaurusBase(
                "AnotherFakeThesaurus",
                [
                    type(
                        "AnotherFakeThesaurusItem",
                        (object,),
                        {"identifier": "1", "concept": "Fake Concept", "meaning": None},
                    )
                ],
            )
            expected = f"{thesaurus_file}/#fake-concept1"
            self.assertEqual(
                _build_link(property_assignment, thesaurus_file, testee), expected
            )

        with self.subTest(
            """
            3. Built link: check to see it has the described values
            Edge cases:
            | has space? | has open bracket? | has close bracket? |
            | no         | no                | no                 |
            """
        ):
            testee = ThesaurusBase(
                "AnotherFakeThesaurus",
                [
                    type(
                        "AnotherFakeThesaurusItem",
                        (object,),
                        {"identifier": "1", "concept": "FakeConcept", "meaning": None},
                    )
                ],
            )
            expected = f"{thesaurus_file}/#fakeconcept1"
            self.assertEqual(
                _build_link(property_assignment, thesaurus_file, testee), expected
            )

    def test_build_link_format_of_meaning(self):
        """
        Unit test for the `_build_link` method.
        - One or more of the arguments `property_assignments`, `thesaurus_file` or
          `thesaurus` not set => `None`
        - `property_assignments.thesaurus_imar_id` not in `thesaurus` => `None`
        - The resulting link is built as follows:
          `thesaurus_file`/#<concept>`property_assignments.thesaurus_imar_id`{betekenis}
          * <concept> is the name of the concept for the `thesaurus` item with ID
            `property_assignments.thesaurus_imar_id`, but with the following characters
            replaced by a dash (`-`):
            + space (` `)
            + open bracket (`(`)
            + closing bracket (`)`)
          * {betekenis} is the meaning of the concept for the `thesaurus` item with ID
            `property_assignments.thesaurus_imar_id`, but with newlines replaced by
            `<br />`
        Test cases:
        1. Arguments
        | property_assignments | thesaurus_file | thesaurus | return `None` |
        | ==================== | ============== | ========= | ============= |
        | set                  | set            | set       | no            |
        | not set              | set            | set       | yes           |
        | set                  | not set        | set       | yes           |
        | not set              | not set        | set       | yes           |
        | set                  | set            | not set   | yes           |
        | not set              | set            | not set   | yes           |
        | set                  | not set        | not set   | yes           |
        | not set              | not set        | not set   | yes           |
        2. Known `thesaurus_imar_id`
        | thesaurus_imar_id known | return `None` |
        | yes                     | no            |
        | no                      | yes           |
        3. Built link: check to see it has the described values
        Edge cases:
        | has space? | has open bracket? | has close bracket? |
        | yes        | yes               | yes                |
        | no         | yes               | yes                |
        | yes        | no                | yes                |
        | no         | no                | yes                |
        | yes        | yes               | no                 |
        | no         | yes               | no                 |
        | yes        | no                | no                 |
        | no         | no                | no                 |
        4. Built link: check to see if the description is added (only when set)
        | has description? | description has newline? |
        | yes              | yes                      |
        | no               | n/a                      |
        | yes              | no                       |
        """

        property_assignment = OntoUmlPropertyAssignments("1")
        thesaurus_file = "fake_thesaurus.md"

        with self.subTest(
            """
            4. Built link: check to see if the description is added (only when set)
            | has description? | description has newline? |
            | yes              | yes                      |
            """
        ):
            testee = ThesaurusBase(
                "AnotherFakeThesaurus",
                [
                    type(
                        "AnotherFakeThesaurusItem",
                        (object,),
                        {
                            "identifier": "1",
                            "concept": "FakeConcept",
                            "meaning": "Fake description\nNext line!",
                        },
                    )
                ],
            )
            expected = (
                f"{thesaurus_file}/#fakeconcept1" + "{Fake description<br />Next line!}"
            )
            self.assertEqual(
                _build_link(property_assignment, thesaurus_file, testee), expected
            )

        with self.subTest(
            """
            4. Built link: check to see if the description is added (only when set)
            | has description? | description has newline? |
            | no               | n/a                      |
            """
        ):
            testee = ThesaurusBase(
                "AnotherFakeThesaurus",
                [
                    type(
                        "AnotherFakeThesaurusItem",
                        (object,),
                        {"identifier": "1", "concept": "FakeConcept", "meaning": None},
                    )
                ],
            )
            expected = f"{thesaurus_file}/#fakeconcept1"
            self.assertEqual(
                _build_link(property_assignment, thesaurus_file, testee), expected
            )

        with self.subTest(
            """
            4. Built link: check to see if the description is added (only when set)
            | has description? | description has newline? |
            | yes              | no                       |
            """
        ):
            testee = ThesaurusBase(
                "AnotherFakeThesaurus",
                [
                    type(
                        "AnotherFakeThesaurusItem",
                        (object,),
                        {
                            "identifier": "1",
                            "concept": "FakeConcept",
                            "meaning": "Fake description",
                        },
                    )
                ],
            )
            expected = f"{thesaurus_file}/#fakeconcept1" + "{Fake description}"
            self.assertEqual(
                _build_link(property_assignment, thesaurus_file, testee), expected
            )

    def test_arguments(self):
        """
        Test the handling of the input arguments
        1. `onto_uml_diagram` is not set -> `ValueError`
        2. `onto_uml_diagram` is set but not of type `OntoUmlDiagram` -> `TypeError`
        3. `onto_uml_diagram` is set and of type `OntoUmlDiagram` -> no error
        """

        with self.subTest("""1. `onto_uml_diagram` is not set -> `ValueError`"""):
            with self.assertRaisesRegex(ValueError, "onto_uml_diagram"):
                to_plant_uml_diagram(None, None, None)

        with self.subTest(
            """
            2. `onto_uml_diagram` is set but not of type `OntoUmlDiagram` -> `TypeError`
            """
        ):
            with self.assertRaisesRegex(TypeError, "onto_uml_diagram"):
                to_plant_uml_diagram(1, None, None)

        with self.subTest(
            """
            3. `onto_uml_diagram` is set and of type `OntoUmlDiagram` -> no error
            """
        ):
            to_plant_uml_diagram(
                OntoUmlDiagram("FakeId", "FakeName", None, None, []), None, None
            )

    # pylint: disable=[W0212]
    def test_factory(self):
        """
        Test the actual factory working itself.
        1. `name` passed in the `OntoUmlDiagram` constructor -> result.`_name`
        2. `description` passed in the `OntoUmlDiagram` constructor
           -> result.`_description`
        3. `contents` passed in the `OntoUmlDiagram` constructor, where item is of type
           `OntoUmlClass` -> result.`_classes`
        4. `contents` passed in the `OntoUmlDiagram` constructor, where item is of type
           `OntoUmlGeneralization` -> result.`_generalizations`
        5. `contents` passed in the `OntoUmlDiagram` constructor, where item is of type
           `OntoUmlRelation` and both of the item `property` items have `type` "Class"
           -> result.`_relations`
        6. `contents` passed in the `OntoUmlDiagram` constructor, where item is of type
           `OntoUmlRelation` and one of the item `property` items has `type` "Class"
           and the other has `type` "Relation" -> result.`_association_relations`
        """

        with self.subTest(
            """
            1. `name` passed in the `OntoUmlDiagram` constructor -> result.`_name`
            """
        ):
            name = "FakeName"
            self.assertEqual(
                to_plant_uml_diagram(
                    OntoUmlDiagram("FakeId", name, None, None, []), None, None
                )._name,
                name,
            )

        with self.subTest(
            """
            2. `description` passed in the `OntoUmlDiagram` constructor
               -> result.`_description`
            """
        ):
            description = "FakeDescription"
            self.assertEqual(
                to_plant_uml_diagram(
                    OntoUmlDiagram("FakeId", "FakeName", description, None, []),
                    None,
                    None,
                )._description,
                description,
            )

    def test_generalization_in_relation(self):
        """
        Unittest for the bug in which a generalization ends up in a relation view.
        The bug is causing the error
        AttributeError: "OntoUmlGeneralization" object has not attribute "properties"
        This test checks to see this error no longer gets thrown.
        """

        fake_json = """
{
  "id" : "sLYThAGFYFAUAxIL",
  "name" : "FakeProject",
  "description" : null,
  "type" : "Project",
  "model" : {
    "id" : "sLYThAGFYFAUAxIL_root",
    "name" : "FakeModel",
    "description" : null,
    "type" : "Package",
    "propertyAssignments" : null,
    "contents" : [ {
      "id" : "NaeIigGFYEGQJAx4",
      "name" : "FakePackage",
      "description" : null,
      "type" : "Package",
      "propertyAssignments" : null,
      "contents" : [ {
        "id" : "iSmkswGGAqAoSCVg",
        "name" : null,
        "description" : null,
        "type" : "Generalization",
        "propertyAssignments" : null,
        "general" : {
          "id" : "ehSkswGGAqAoSCUi",
          "type" : "Class"
        },
        "specific" : {
          "id" : "xJFl8QGGAqAAjBBX",
          "type" : "Class"
        }
      }, {
        "id" : "ehSkswGGAqAoSCUi",
        "name" : "FakeClass_1",
        "description" : null,
        "type" : "Class",
        "propertyAssignments" : {
          "ThesaurusIMAR_ID" : "240"
        },
        "stereotype" : "category",
        "isAbstract" : false,
        "isDerived" : false,
        "properties" : null,
        "isExtensional" : false,
        "isPowertype" : null,
        "order" : null,
        "literals" : null,
        "restrictedTo" : [ "relator" ]
      }, {
        "id" : "xJFl8QGGAqAAjBBX",
        "name" : "FakeClass_2",
        "description" : null,
        "type" : "Class",
        "propertyAssignments" : {
        "ThesaurusIMAR_ID" : "253"
        },
        "stereotype" : "relator",
        "isAbstract" : false,
        "isDerived" : false,
        "properties" : null,
        "isExtensional" : null,
        "isPowertype" : null,
        "order" : null,
        "literals" : null,
        "restrictedTo" : [ "relator" ]
      } ]
    } ]
  },
  "diagrams" : [
  {
    "id" : "HIl58QGGAqAAjA6j",
    "name" : "FakeDiagram",
    "description" : null,
    "type" : "Diagram",
    "owner" : {
      "id" : "NaeIigGFYEGQJAx4",
      "type" : "Package"
    },
    "contents" : [
    {
      "id" : "YVInSwGGAqAoSDcv",
      "type" : "GeneralizationView",
      "modelElement" : {
        "id" : "iSmkswGGAqAoSCVg",
        "type" : "Generalization"
      },
      "shape" : {
        "id" : "YVInSwGGAqAoSDcv_path",
        "type" : "Path",
        "points" : [ {
          "x" : 278,
          "y" : 333
        }, {
          "x" : 251,
          "y" : 333
        } ]
      },
      "source" : {
        "id" : "PlInSwGGAqAoSDct",
        "type" : "ClassView"
      },
      "target" : {
        "id" : "CqF1yQGGAqAAjBeW",
        "type" : "ClassView"
      }
    }, {
      "id" : "PlInSwGGAqAoSDct",
      "type" : "ClassView",
      "modelElement" : {
        "id" : "ehSkswGGAqAoSCUi",
        "type" : "Class"
      },
      "shape" : {
        "id" : "PlInSwGGAqAoSDct_shape",
        "type" : "Rectangle",
        "x" : 279,
        "y" : 293,
        "width" : 89,
        "height" : 40
      }
    }, {
      "id" : "CqF1yQGGAqAAjBeW",
      "type" : "ClassView",
      "modelElement" : {
        "id" : "xJFl8QGGAqAAjBBX",
        "type" : "Class"
      },
      "shape" : {
        "id" : "CqF1yQGGAqAAjBeW_shape",
        "type" : "Rectangle",
        "x" : 154,
        "y" : 333,
        "width" : 96,
        "height" : 40
      }
    } ]
  } ]
}
"""

        with patch("builtins.open", mock_open(read_data=fake_json)):
            testable = _import_onto_uml_json_project("fake.json")
            plant_uml_output = to_plant_uml_diagram(testable.diagrams[0], None, None)
            self.assertIsNotNone(plant_uml_output)
