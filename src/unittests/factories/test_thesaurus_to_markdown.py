"""
Test suites for the `thesaurus_to_markdown` module.
"""


from unittest import TestCase

from src.factories.thesaurus_to_markdown import to_markdown
from src.markdown.markdown_thesaurus import MarkdownThesaurus
from src.thesaurus.thesaurus import ThesaurusBase


class TestToMarkdown(TestCase):
    """
    Test suite for the `to_markdown` method
    """

    def test_with_thesaurus_base(self):
        """
        Unit test to validate that a `MarkdownThesaurus` object is returned, when
        `thesaurus` is a `ThesaurusBase` object.
        1. `thesaurus` is of type `ThesaurusBase` => `MarkdownThesaurus` returned
        2. `thesaurus` is of type with base class `ThesaurusBase` => `MarkdownThesaurus`
           returned
        3. `thesaurus` is not of type `ThesaurusBase` => `None` returned
        """

        with self.subTest(
            "1. `thesaurus` is of type `ThesaurusBase` => `MarkdownThesaurus` returned"
        ):
            self.assertIsInstance(
                to_markdown(
                    ThesaurusBase(
                        "FakeTitle",
                        [
                            type(
                                "FakeThesaurusBaseItem",
                                (object,),
                                {
                                    "identifier": "1",
                                    "concept": "FakeConcept",
                                    "meaning": "FakeMeaning",
                                },
                            )
                        ],
                    )
                ),
                MarkdownThesaurus,
            )

        with self.subTest(
            """
            2. `thesaurus` is of type with base class `ThesaurusBase`
               => `MarkdownThesaurus` returned
            """
        ):
            self.assertIsInstance(
                to_markdown(
                    type(
                        "FakeThesaurusInheritedObject",
                        (ThesaurusBase,),
                        {
                            "title": "FakeThesaurusTitle",
                            "items": {
                                "1": {
                                    "identifier": "1",
                                    "concept": "AnotherFakeConcept",
                                    "meaning": "",
                                }
                            },
                        },
                    )
                ),
                MarkdownThesaurus,
            )

        with self.subTest(
            "3. `thesaurus` is not of type `ThesaurusBase` => `None` returned"
        ):
            self.assertIsNone(to_markdown(1))
