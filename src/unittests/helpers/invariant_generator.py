"""
Helper module with classes and methods which help to generate matrixes of test
cases, based on conditions or testables, and the invariants per condition / testable.
"""


import math
from typing import List

from src.helpers.argument_helpers import check_iterable, check_non_iterable


class Invariant:
    """Interface for an invariant"""

    def __init__(self, identifier, description, data):
        check_non_iterable((identifier, "identifier"))
        check_non_iterable((description, "description"), (str, "description"))
        check_non_iterable((data, "data"))

        self._identifier = identifier
        self._description = description
        self._data = data

    def __eq__(self, other):
        if None is other:
            return False
        identifiers = [self.identifier, other.identifier]
        identifier_equal = identifiers[0] == identifiers[1]
        description_equal = self.description == other.description
        data_equal = self.data == other.data
        return identifier_equal and description_equal and data_equal

    @property
    def identifier(self):
        """The identifier of this Invariant"""
        return self._identifier

    @property
    def description(self):
        """The description of this Invariant"""
        return self._description

    @property
    def data(self):
        """The data of the Invariant"""
        return self._data


class Condition:
    """
    Interface for a condition
    """

    def __init__(self, identifier, description, invariants: List[Invariant]):
        check_non_iterable((identifier, "identifier"))
        check_non_iterable((description, "description"), (str, "description"))
        check_iterable((invariants, "invariants"), (Invariant, "invariants", True))

        self._identifier = identifier
        self._description = description
        self._invariants = invariants

    def __eq__(self, other):
        if None is other:
            return False
        identifier_equal = self.identifier == other.identifier
        description_equal = self.description == other.description
        invariants_equal = self.invariants == other.invariants
        return identifier_equal and description_equal and invariants_equal

    @property
    def identifier(self):
        """The identifier of the test condition"""
        return self._identifier

    @property
    def description(self):
        """The description of the test condition"""
        return self._description

    @property
    def invariants(self):
        """The invariants belonging to the test condition"""
        return self._invariants


class ExpectedResult:
    """
    An expected outcome of a single Scenario
    """

    def __init__(self, description, callback):
        check_non_iterable((description, "description"), (str, "description"))
        check_non_iterable((callback, "callback"))

        self._description = description
        self._callback = callback

    def __eq__(self, other):
        if None is other:
            return False
        description_equal = self.description == other.description
        callback_equal = self.callback == other.callback
        return description_equal and callback_equal

    @property
    def description(self):
        """The description of the expected result"""
        return self._description

    @property
    def callback(self):
        """The callback function which will yield the expected result"""
        return self._callback


class SingleInvariant:
    """
    Interface for a single `Invariant` for a `Condition`
    """

    def __init__(
        self,
        condition: Condition,
        invariant_id=None,
        invariant: Invariant = None,
        callback=None,
    ):
        check_non_iterable((condition, "condition"), (Condition, "condition"))
        if None is invariant_id and None is invariant:
            raise ValueError("either `invariant_id` or `invariant` must be set")
        if None is not invariant_id and None is not invariant:
            raise ValueError(
                "both `invariant_id` or `invariant` cannot be set; use only one"
            )
        if None is not invariant_id:
            if 1 != len(
                [
                    item
                    for item in condition.invariants
                    if item.identifier == invariant_id
                ]
            ):
                raise KeyError("invariant_id")
        else:
            check_non_iterable(
                (invariant, "invariant"), (Invariant, "invariant"), False
            )
            if None is not invariant and invariant not in condition.invariants:
                raise KeyError("invariant")

        self._condition = condition
        self._invariant_id = invariant_id
        self._invariant = invariant
        self._callback = callback

    def __eq__(self, other):
        if None is other:
            return False
        condition_equal = self.condition == other.condition
        invariant_equal = self.invariant == other.invariant
        return condition_equal and invariant_equal

    @property
    def condition(self):
        """The `condition` on which the `invariant` applies"""
        return self._condition

    @property
    def invariant(self) -> Invariant:
        """The applied `invariant` for this `condition`"""
        if None is self._invariant:
            worker = [
                invariant
                for invariant in self.condition.invariants
                if self._invariant_id == invariant.identifier
            ]
            if 1 != len(worker):
                raise KeyError("invariant_id")
            self._invariant = worker[0]
        return self._invariant

    @property
    def callback(self):
        """
        Calls the callback, if set
        """
        if None is not self._callback:
            self._callback  # pylint:disable=[W0104]


class Scenario:
    """
    A single scenario. Meaning a given combination of a single invariant for all the
    conditions in the testset, and the expected result.
    """

    @staticmethod
    def _calculate_number_of_set_repetitions(
        conditions: List[Condition], current_index: int
    ) -> int:
        return math.prod(
            [
                len(item.invariants)
                for index, item in enumerate(conditions)
                if index > current_index
            ]
        )

    @staticmethod
    def _calculate_number_of_invariant_repetitions(
        conditions: List[Condition], current_index: int
    ) -> int:
        return math.prod(
            [
                len(item.invariants)
                for index, item in enumerate(conditions)
                if index < current_index
            ]
        )

    # pylint:disable=[E0602]
    @staticmethod
    def matrix(conditions: List[Condition]) -> list:  # noqa=F821
        """
        Generates a matrix of all conditions and invariant instances. This matrix is
        guaranteed to include all combinations of invariants per condition.
        """
        columns = []
        lines = -1
        for condition_index, condition in enumerate(conditions):
            column = (condition, [])
            repetitions = Scenario._calculate_number_of_invariant_repetitions(
                conditions, condition_index
            )
            multiplications = Scenario._calculate_number_of_set_repetitions(
                conditions, condition_index
            )
            repeated_worker = []
            for invariant in condition.invariants:
                worker = [invariant]
                worker = worker * repetitions
                repeated_worker.extend(worker)

            multiplied_worker = repeated_worker * multiplications
            lines = len(multiplied_worker)
            column[1].append(multiplied_worker)

            columns.append(column)

        result = []
        for line in range(lines):
            invariants = []
            for column in columns:
                invariants.append(
                    SingleInvariant(column[0], invariant=column[1][0][line])
                )
            result.append(Scenario(invariants))

        return result

    def __init__(
        self,
        single_invariants: List[SingleInvariant],
        expected_result: ExpectedResult = None,
    ):
        if None is single_invariants:
            raise ValueError("single_invariants")
        if not isinstance(single_invariants, list):
            raise TypeError("single_invariants")
        if not all(isinstance(item, SingleInvariant) for item in single_invariants):
            raise TypeError("single_invariants")
        if 0 == len(single_invariants):
            raise TypeError("single_invariants")
        self._single_invariants = single_invariants
        self._expected_result = expected_result

    def __eq__(self, other):
        if None is other:
            return False
        return (
            self.single_invariants == other.single_invariants
            and self.expected_result == other.expected_result
        )

    @property
    def description(self):
        """
        The description for the current scenario.
        Takes the form of
        <condition> '<invariant>'[, <condition> '<invariant>']* results in <expected
        result>
        """
        worker = [
            f"{item.condition.description} '{item.invariant.description}'"
            for item in self.single_invariants
        ]
        result = ", ".join(worker)
        if None is not self.expected_result:
            result = f"{result} results in {self.expected_result.description}"
        return result

    @property
    def single_invariants(self):
        """
        The list of single invariant for a given condition
        """
        return self._single_invariants

    @property
    def expected_result(self):
        """
        The result expected when the given set of `single_invariants` applies
        """
        return self._expected_result

    @expected_result.setter
    def expected_result(self, value):
        self._expected_result = value


invariant_equals_other = Invariant("yes", "equals other", True)
invariant_does_not_equal_other = Invariant("no", "does not equal other", False)


def setup_equals_matrix(members: List[str]):
    """
    Generate a test matrix for a set of `members`; for each members exactly two
    invariants apply: [`invariants_equals_other`, `invariant_does_not_equal_other`].

    `members` is a list of `Tuple`s of which the first item is the description of the
    member, and the second one is the internal attribute name for the type under test.
    If `members` is not set, then a `ValueError` will be raised with the `message`
    "members".
    If `members` is set, but is not a list consisting solely of `Tuple[str, str]` items,
    then a `TypeError` will be raised with the `message` "members".
    """
    # Check the `members` argument, and raise an error if needed
    check_iterable((members, "members"), (str, "members", False))

    # Prepare the invariants for the members equality, either "yes" or "no"
    invariants_equal = [invariant_equals_other, invariant_does_not_equal_other]

    # Setup conditions for each member; each condition has two invariants, yes
    # (member equals) or no (member is not equal)
    conditions = [Condition(member, member, invariants_equal) for member in members]

    # Prepare a matrix of ALL possible combinations of invariants for the given
    # conditions
    result = Scenario.matrix(conditions)

    # Setup expected result: __eq__ should yield "True" ONLY when all members
    # equal (the first (0) invariant in the set)
    for testcase in [
        testcase
        for testcase in result
        if all(
            item.invariant == invariants_equal[0]  # yes
            for item in testcase.single_invariants
        )
    ]:
        testcase.expected_result = ExpectedResult("True", lambda: True)

    # Setup expected result: __eq__ should yield "False" ONLY when not all members
    # equal (the second (1) invariant in the set)
    for testcase in [
        testcase
        for testcase in result
        if not all(
            item.invariant == invariants_equal[0]  # no
            for item in testcase.single_invariants
        )
    ]:
        testcase.expected_result = ExpectedResult("False", lambda: False)

    return result
