"""
Generic helper methods for testing class specifications
"""


import inspect
from typing import List
from unittest import TestCase

from src.helpers.argument_helpers import check_iterable, check_non_iterable


def assert_members(suite: TestCase, tested_type: type, expected: List[str]):
    """
    Check to see only the expected public members are defined on `{tested_type}`
    """

    check_non_iterable((suite, "test_case"), (TestCase, "test_case"))
    check_non_iterable((tested_type, "tested_type"), (type, "tested_type"))
    check_iterable((expected, "expected"), (str, "expected", True), True)

    inspected_members = inspect.getmembers(tested_type)
    members = []
    if 0 < len(
        [item for item in inspected_members if item[0] == "__dataclass_fields__"]
    ):
        # Dataclass, so get all dataclass fields
        members = [
            item.name
            for item in list(
                list(
                    filter(lambda x: x[0] == "__dataclass_fields__", inspected_members)
                )[0][1].values()
            )
            if not item.name.startswith("_")
        ]
    # Now, add all members not starting with an underscore, to fetch non-dataclass
    # members and / or properties.
    members.extend(
        [item[0] for item in inspected_members if not item[0].startswith("_")]
    )
    # Deduplicate the list
    members = list(dict.fromkeys(members))

    members_not_in_expected = [member for member in members if member not in expected]
    expected_not_specified = [
        expectee for expectee in expected if expectee not in members
    ]

    with suite.subTest(f"Check for unexpected `{str(tested_type)}` (public) members"):
        suite.assertEqual(
            0,
            len(members_not_in_expected),
            f"Unexpected members in `{str(tested_type)}`: {members_not_in_expected}",
        )

    with suite.subTest(f"Check for missing `{str(tested_type)}` (public) members"):
        suite.assertEqual(
            0,
            len(expected_not_specified),
            f"Missing members in `{str(tested_type)}`: {expected_not_specified}",
        )
