"""
Test suite for src.unittests.helpers.invariant_generator.ExpectedResult class
"""

from copy import deepcopy
from unittest import TestCase

from src.unittests.helpers.class_helpers import assert_members
from src.unittests.helpers.invariant_generator import ExpectedResult


class TestExpectedResult(TestCase):
    """
    Test suite for ExpectedResult class.
    """

    def test_members(self):
        """
        Check to see only the expected members are defined on `ExpectedResult`
        """
        assert_members(self, ExpectedResult, ["description", "callback"])

    def test_init(self):
        """
        | description set | description type str | callback set | expected result           |
        | yes             | yes                  | yes          | no error                  |
        | no              | n/a                  | yes          | ValueError("description") |
        | yes             | no                   | yes          | TypeError("description")  |
        | yes             | yes                  | no           | ValueError("callback")    |
        | no              | n/a                  | no           | ValueError("description") |
        | yes             | no                   | no           | TypeError("description")  |
        """  # noqa=E501

        with self.subTest(
            """
            | description set | description type str | callback set | expected result           |
            | yes             | yes                  | yes          | no error                  |
            """  # noqa=E501
        ):
            ExpectedResult("FakeDescription", assert_members)

        with self.subTest(
            """
            | description set | description type str | callback set | expected result           |
            | no              | n/a                  | yes          | ValueError("description") |
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(ValueError, "description"):
                ExpectedResult(None, assert_members)

        with self.subTest(
            """
            | description set | description type str | callback set | expected result           |
            | yes             | no                   | yes          | TypeError("description")  |
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(TypeError, "description"):
                ExpectedResult(1, assert_members)

        with self.subTest(
            """
            | description set | description type str | callback set | expected result           |
            | yes             | yes                  | no           | ValueError("callback")    |
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(ValueError, "callback"):
                ExpectedResult("FakeDescription", None)

        with self.subTest(
            """
            | description set | description type str | callback set | expected result           |
            | no              | n/a                  | no           | ValueError("description") |
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(ValueError, "description"):
                ExpectedResult(None, None)

        with self.subTest(
            """
            | description set | description type str | callback set | expected result           |
            | yes             | no                   | no           | TypeError("description")  |
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(TypeError, "description"):
                ExpectedResult(1, None)

    def test_eq(self):
        """
        1. If the other is None => False
        2.
        | description equal | callback equal | expected result |
        | yes               | yes            | True            |
        | no                | yes            | False           |
        | yes               | no             | False           |
        | no                | no             | False           |
        """

        frozen = ExpectedResult("FakeDescription", lambda: True)
        with self.subTest(
            """
            1. If the other is None => False
            """
        ):
            first = deepcopy(frozen)
            other = None
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
            | description equal | callback equal | expected result |
            | yes               | yes            | True            |
            """
        ):
            first = deepcopy(frozen)
            other = deepcopy(frozen)
            self.assertTrue(first == other)
            self.assertEqual(first, other)

        with self.subTest(
            """
            | description equal | callback equal | expected result |
            | no                | yes            | False           |
            """
        ):
            first = deepcopy(frozen)
            other = deepcopy(frozen)
            other._description = "AnotherDescription"  # pylint:disable=[W0212]
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
            | description equal | callback equal | expected result |
            | yes               | no             | False           |
            """
        ):
            first = deepcopy(frozen)
            other = deepcopy(frozen)
            other._callback = self.addCleanup  # pylint:disable=[W0212]
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
            | description equal | callback equal | expected result |
            | no                | no             | False           |
            """
        ):
            first = deepcopy(frozen)
            other = deepcopy(frozen)
            other._description = "AnotherFakeDescription"  # pylint:disable=[W0212]
            other._callback = self.addCleanup  # pylint:disable=[W0212]
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)
