"""
Test suite for src.unittests.helpers.invariant_generator.Condition class
"""


from copy import deepcopy
from unittest import TestCase

from src.unittests.helpers.class_helpers import assert_members
from src.unittests.helpers.invariant_generator import Condition, Invariant


class TestCondition(TestCase):
    """
    Test suite for Condition class.
    """

    def test_members(self):
        """
        Check to see only the expected members are defined on `Condition`
        """
        assert_members(self, Condition, ["identifier", "description", "invariants"])

    # pylint:disable=[R0915]
    def test_init(self):
        # pylint:disable=[C0301]
        """
        | identifier set | description set | description str | invariants set | invariants List[Invariant] | expected result           |
        | yes            | yes             | yes             | yes            | yes                        | no error                  |
        | no             | yes             | yes             | yes            | yes                        | ValueError("identifier")  |
        | yes            | no              | n/a             | yes            | yes                        | ValueError("description") |
        | no             | no              | n/a             | yes            | yes                        | ValueError("identifier")  |
        | yes            | yes             | no              | yes            | yes                        | TypeError("description")  |
        | no             | yes             | no              | yes            | yes                        | ValueError("identifier")  |
        | yes            | yes             | yes             | no             | n/a                        | ValueError("invariants")  |
        | no             | yes             | yes             | no             | n/a                        | ValueError("identifier")  |
        | yes            | no              | n/a             | no             | n/a                        | ValueError("description") |
        | no             | no              | n/a             | no             | n/a                        | ValueError("identifier")  |
        | yes            | yes             | no              | no             | n/a                        | TypeError("description")  |
        | no             | yes             | no              | no             | n/a                        | ValueError("identifier")  |
        | yes            | yes             | yes             | yes            | no                         | TypeError("invariants")   |
        | no             | yes             | yes             | yes            | no                         | ValueError("identifier")  |
        | yes            | no              | n/a             | yes            | no                         | ValueError("description") |
        | no             | no              | n/a             | yes            | no                         | ValueError("identifier")  |
        | yes            | yes             | no              | yes            | no                         | TypeError("description")  |
        | no             | yes             | no              | yes            | no                         | ValueError("identifier")  |
        """  # noqa=E501

        with self.subTest(
            """
            | identifier set | description set | description str | invariants set | invariants List[Invariant] | expected result           |
            | yes            | yes             | yes             | yes            | yes                        | no error                  |
            """  # noqa=E501
        ):
            Condition(
                "FakeIdentifier",
                "FakeDescription",
                [
                    Invariant("FakeId", "FakeDescription", {"data": "Fake"}),
                ],
            )

        with self.subTest(
            """
            | identifier set | description set | description str | invariants set | invariants List[Invariant] | expected result           |
            | no             | yes             | yes             | yes            | yes                        | ValueError("identifier")  |
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(ValueError, "identifier"):
                Condition(
                    None,
                    "FakeDescription",
                    [
                        Invariant("FakeId", "FakeDescription", {"data": "Fake"}),
                    ],
                )

        with self.subTest(
            """
            | identifier set | description set | description str | invariants set | invariants List[Invariant] | expected result           |
            | yes            | no              | n/a             | yes            | yes                        | ValueError("description") |
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(ValueError, "description"):
                Condition(1, None, [Invariant(2, "FakeDescription", 3)])

        with self.subTest(
            """
            | identifier set | description set | description str | invariants set | invariants List[Invariant] | expected result           |
            | no             | no              | n/a             | yes            | yes                        | ValueError("identifier")  |
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(ValueError, "identifier"):
                Condition(
                    None, None, [Invariant("FakeId", "FakeDesc", {"data": "Fake"})]
                )

        with self.subTest(
            """
            | identifier set | description set | description str | invariants set | invariants List[Invariant] | expected result           |
            | yes            | yes             | no              | yes            | yes                        | TypeError("description")  |
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(TypeError, "description"):
                Condition(1, 2, [Invariant(3, "FakeDescription", 4)])

        with self.subTest(
            """
            | identifier set | description set | description str | invariants set | invariants List[Invariant] | expected result           |
            | no             | yes             | no              | yes            | yes                        | ValueError("identifier")  |
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(ValueError, "identifier"):
                Condition(None, 1, [Invariant("FakeId", "FakeDescription", True)])

        with self.subTest(
            """
            | identifier set | description set | description str | invariants set | invariants List[Invariant] | expected result           |
            | yes            | yes             | yes             | no             | n/a                        | ValueError("invariants")  |
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(ValueError, "invariants"):
                Condition(1, "FakeDescription", None)

        with self.subTest(
            """
            | identifier set | description set | description str | invariants set | invariants List[Invariant] | expected result           |
            | no             | yes             | yes             | no             | n/a                        | ValueError("identifier")  |
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(ValueError, "identifier"):
                Condition(None, "FakeDescription", None)

        with self.subTest(
            """
            | identifier set | description set | description str | invariants set | invariants List[Invariant] | expected result           |
            | yes            | no              | n/a             | no             | n/a                        | ValueError("description") |
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(ValueError, "description"):
                Condition(1, None, None)

        with self.subTest(
            """
            | identifier set | description set | description str | invariants set | invariants List[Invariant] | expected result           |
            | no             | no              | n/a             | no             | n/a                        | ValueError("identifier")  |
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(ValueError, "identifier"):
                Condition(None, None, None)

        with self.subTest(
            """
            | identifier set | description set | description str | invariants set | invariants List[Invariant] | expected result           |
            | yes            | yes             | no              | no             | n/a                        | TypeError("description")  |
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(TypeError, "description"):
                Condition(1, 2, None)

        with self.subTest(
            """
            | identifier set | description set | description str | invariants set | invariants List[Invariant] | expected result           |
            | no             | yes             | no              | no             | n/a                        | ValueError("identifier")  |
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(ValueError, "identifier"):
                Condition(None, 2, None)

        with self.subTest(
            """
            | identifier set | description set | description str | invariants set | invariants List[Invariant] | expected result           |
            | yes            | yes             | yes             | yes            | no                         | TypeError("invariants")   |
            invariants not of list type at all
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(TypeError, "invariants"):
                Condition(1, "FakeDescription", 1)

        with self.subTest(
            """
            | identifier set | description set | description str | invariants set | invariants List[Invariant] | expected result           |
            | yes            | yes             | yes             | yes            | no                         | TypeError("invariants")   |
            invariants is list, but not all items of type Invariant
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(TypeError, "invariants"):
                Condition(
                    1, "FakeDescription", [1, Invariant(1, "FakeDescription", False), 3]
                )

        with self.subTest(
            """
            | identifier set | description set | description str | invariants set | invariants List[Invariant] | expected result           |
            | no             | yes             | yes             | yes            | no                         | ValueError("identifier")  |
            invariants not of list type at all
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(ValueError, "identifier"):
                Condition(None, "FakeDescription", 1)

        with self.subTest(
            """
            | identifier set | description set | description str | invariants set | invariants List[Invariant] | expected result           |
            | no             | yes             | yes             | yes            | no                         | ValueError("identifier")  |
            invariants is list, but not all items of type Invariant
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(ValueError, "identifier"):
                Condition(
                    None,
                    "FakeDescription",
                    [1, 2, Invariant("FakeId", "FakeDescr", {"member": "Fake"})],
                )

        with self.subTest(
            """
            | identifier set | description set | description str | invariants set | invariants List[Invariant] | expected result           |
            | yes            | no              | n/a             | yes            | no                         | ValueError("description") |
            invariants not of list type at all
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(ValueError, "description"):
                Condition(1, None, 3)

        with self.subTest(
            """
            | identifier set | description set | description str | invariants set | invariants List[Invariant] | expected result           |
            | yes            | no              | n/a             | yes            | no                         | ValueError("description") |
            invariants is list, but not all items of type Invariant
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(ValueError, "description"):
                Condition(1, None, ["a", Invariant(3, "FakeDescription", False)])

        with self.subTest(
            """
            | identifier set | description set | description str | invariants set | invariants List[Invariant] | expected result           |
            | no             | no              | n/a             | yes            | no                         | ValueError("identifier")  |
            invariants not of list type at all
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(ValueError, "identifier"):
                Condition(None, None, "a")

        with self.subTest(
            """
            | identifier set | description set | description str | invariants set | invariants List[Invariant] | expected result           |
            | no             | no              | n/a             | yes            | no                         | ValueError("identifier")  |
            invariants is list, but not all items of type Invariant
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(ValueError, "identifier"):
                Condition(None, None, ["a", "b", "c"])

        with self.subTest(
            """
            | identifier set | description set | description str | invariants set | invariants List[Invariant] | expected result           |
            | yes            | yes             | no              | yes            | no                         | TypeError("description")  |
            invariants not of list type at all
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(TypeError, "description"):
                Condition(1, 2, 3)

        with self.subTest(
            """
            | identifier set | description set | description str | invariants set | invariants List[Invariant] | expected result           |
            | yes            | yes             | no              | yes            | no                         | TypeError("description")  |
            invariants is list, but not all items of type Invariant
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(TypeError, "description"):
                Condition(1, 2, [3, 4, 5])

        with self.subTest(
            """
            | identifier set | description set | description str | invariants set | invariants List[Invariant] | expected result           |
            | no             | yes             | no              | yes            | no                         | ValueError("identifier")  |
            invariants not of list type at all
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(ValueError, "identifier"):
                Condition(None, 1, 3)

        with self.subTest(
            """
            | identifier set | description set | description str | invariants set | invariants List[Invariant] | expected result           |
            | no             | yes             | no              | yes            | no                         | ValueError("identifier")  |
            invariants is list, but not all items of type Invariant
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(ValueError, "identifier"):
                Condition(
                    None,
                    1,
                    [
                        Invariant(1, "FakeDescription", 1),
                        Invariant(2, "FakeDescription", 2),
                        3,
                    ],
                )

    # pylint:disable=[R0915]
    def test_eq(self):
        """
        1. If other is None => False
        2.
        | identifier equal | description equal | invariants equal | expected result |
        | yes              | yes               | yes              | True            |
        | no               | yes               | yes              | False           |
        | yes              | no                | yes              | False           |
        | no               | no                | yes              | False           |
        | yes              | yes               | no               | False           |
        | no               | yes               | no               | False           |
        | yes              | no                | no               | False           |
        | no               | no                | no               | False           |
        """

        with self.subTest(
            """
            1. If other is None => False
            """
        ):
            first = Condition(
                "FakeIdentifier",
                "fakeDescription",
                [Invariant("FakeInvariantId", "FakeInvariantDesc", 1)],
            )
            other = None
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        frozen = Condition(
            "FakeIdentifier",
            "fakeDescription",
            [Invariant("FakeInvariantId", "FakeInvariantDesc", 1)],
        )

        with self.subTest(
            """
            | identifier equal | description equal | invariants equal | expected result |
            | yes              | yes               | yes              | True            |
            """  # noqa=E501
        ):
            first = deepcopy(frozen)
            second = deepcopy(frozen)

            self.assertTrue(first == second)
            self.assertEqual(first, second)

        with self.subTest(
            """
            | identifier equal | description equal | invariants equal | expected result |
            | no               | yes               | yes              | False           |
            """  # noqa=E501
        ):
            first = deepcopy(frozen)
            second = deepcopy(frozen)
            second._identifier = "AnotherFakeId"  # pylint:disable=[W0212]

            self.assertFalse(first == second)
            self.assertNotEqual(first, second)

        with self.subTest(
            """
            | identifier equal | description equal | invariants equal | expected result |
            | yes              | no                | yes              | False           |
            """  # noqa=E501
        ):
            first = deepcopy(frozen)
            second = deepcopy(frozen)
            second._description = "AnotherFakeDescription"  # pylint:disable=[W0212]

            self.assertFalse(first == second)
            self.assertNotEqual(first, second)

        with self.subTest(
            """
            | identifier equal | description equal | invariants equal | expected result |
            | no               | no                | yes              | False           |
            """  # noqa=E501
        ):
            first = deepcopy(frozen)
            second = deepcopy(frozen)
            second._identifier = "AnotherFakeIdentifier"  # pylint:disable=[W0212]
            second._description = "AnotherFakeDescription"  # pylint:disable=[W0212]

            self.assertFalse(first == second)
            self.assertNotEqual(first, second)

        with self.subTest(
            """
            | identifier equal | description equal | invariants equal | expected result |
            | yes              | yes               | no               | False           |
            """  # noqa=E501
        ):
            first = deepcopy(frozen)
            second = deepcopy(frozen)
            second._invariants = [  # pylint:disable=[W0212]
                Invariant("FakeInvariantId", "FakeInvariantDescription", {"diff": True})
            ]

            self.assertFalse(first == second)
            self.assertNotEqual(first, second)

        with self.subTest(
            """
            | identifier equal | description equal | invariants equal | expected result |
            | no               | yes               | no               | False           |
            """  # noqa=E501
        ):
            first = deepcopy(frozen)
            second = deepcopy(frozen)
            second._identifier = "AnotherFakeId"  # pylint:disable=[W0212]
            second._invariants = [  # pylint:disable=[W0212]
                Invariant("FakeInvariantId", "FakeInvariantDescription", {"diff": True})
            ]

            self.assertFalse(first == second)
            self.assertNotEqual(first, second)

        with self.subTest(
            """
            | identifier equal | description equal | invariants equal | expected result |
            | yes              | no                | no               | False           |
            """  # noqa=E501
        ):
            first = deepcopy(frozen)
            second = deepcopy(frozen)
            second._description = "AnotherFakeDescription"  # pylint:disable=[W0212]
            second._invariants = [  # pylint:disable=[W0212]
                Invariant("FakeInvariantId", "FakeInvariantDescription", {"diff": True})
            ]

            self.assertFalse(first == second)
            self.assertNotEqual(first, second)

        with self.subTest(
            """
            | identifier equal | description equal | invariants equal | expected result |
            | no               | no                | no               | False           |
            """  # noqa=E501
        ):
            first = deepcopy(frozen)
            second = deepcopy(frozen)
            second._identifier = "AnotherFakeIdentifier"  # pylint:disable=[W0212]
            second._description = "AnotherFakeDescription"  # pylint:disable=[W0212]
            second._invariants = [  # pylint:disable=[W0212]
                Invariant("FakeInvariantId", "FakeInvariantDescription", {"diff": True})
            ]

            self.assertFalse(first == second)
            self.assertNotEqual(first, second)
