"""
Test suite for src.unittests.helpers.invariant_generator.SingleInvariant class
"""


from copy import deepcopy
from unittest import TestCase

from src.unittests.helpers.class_helpers import assert_members
from src.unittests.helpers.invariant_generator import (
    Condition,
    Invariant,
    SingleInvariant,
)


class TestSingleInvariant(TestCase):
    """
    Test suite for SingleInvariant class.
    """

    def test_members(self):
        """
        Check to see only the expected members are defined on `SingleInvariant`
        """
        assert_members(self, SingleInvariant, ["condition", "invariant", "callback"])

    # pylint:disable=[R0915]
    def test_init(self):
        # pylint:disable=[C0301]
        """
        |           | condition |              | invariant_id         |           | invariant | invariant            | invariant.identifier |                                                                                     |
        | condition | typed     | invariant_id | in                   | invariant | typed     | in                   | equals               |                                                                                     |
        | set       | Condition | set          | condition.invariants | set       | Invariant | condition.invariants | invariant_id         | expected results                                                             |
        | --------- | --------- | ------------ | -------------------- | --------- | --------- | -------------------- | -------------------- | ---------------------------------------------------------------------------- |
        | yes       | yes       | yes          | yes                  | yes       | yes       | yes                  | yes                  | ValueError("both `invariant_id` or `invariant` cannot be set; use only one") |
        | no        | n/a       | yes          | n/a                  | yes       | yes       | n/a                  | yes                  | ValueError("condition")                                                      |
        | yes       | no        | yes          | n/a                  | yes       | yes       | n/a                  | yes                  | TypeError("condition")                                                       |
        | yes       | yes       | no           | n/a                  | yes       | yes       | yes                  | n/a                  | no error                                                                     |
        | no        | n/a       | no           | n/a                  | yes       | yes       | n/a                  | n/a                  | ValueError("condition")                                                      |
        | yes       | no        | no           | n/a                  | yes       | yes       | n/a                  | n/a                  | TypeError("condition")                                                       |
        | yes       | yes       | yes          | no                   | yes       | yes       | yes                  | n/a                  | ValueError("both `invariant_id` or `invariant` cannot be set; use only one") |
        | yes       | yes       | yes          | yes                  | no        | n/a       | n/a                  | n/a                  | no error                                                                     |
        | no        | n/a       | yes          | n/a                  | no        | n/a       | n/a                  | n/a                  | ValueError("condition")                                                      |
        | yes       | no        | yes          | n/a                  | no        | n/a       | n/a                  | n/a                  | TypeError("condition")                                                       |
        | yes       | yes       | no           | n/a                  | no        | n/a       | n/a                  | n/a                  | ValueError("either `invariant_id` or `invariant` must be set")               |
        | no        | n/a       | no           | n/a                  | no        | n/a       | n/a                  | n/a                  | ValueError("condition")                                                      |
        | yes       | no        | no           | n/a                  | no        | n/a       | n/a                  | n/a                  | TypeError("condition")                                                       |
        | yes       | yes       | yes          | no                   | no        | n/a       | n/a                  | n/a                  | KeyError("invariant_id")                                                     |
        | yes       | yes       | yes          | yes                  | yes       | no        | n/a                  | n/a                  | ValueError("both `invariant_id` or `invariant` cannot be set; use only one") |
        | no        | n/a       | yes          | n/a                  | yes       | no        | n/a                  | n/a                  | ValueError("condition")                                                      |
        | yes       | no        | yes          | n/a                  | yes       | no        | n/a                  | n/a                  | TypeError("condition")                                                       |
        | yes       | yes       | no           | n/a                  | yes       | no        | n/a                  | n/a                  | TypeError("invariant")                                                       |
        | no        | n/a       | no           | n/a                  | yes       | no        | n/a                  | n/a                  | ValueError("condition")                                                      |
        | yes       | no        | no           | n/a                  | yes       | no        | n/a                  | n/a                  | TypeError("condition")                                                       |
        | yes       | yes       | yes          | no                   | yes       | no        | n/a                  | n/a                  | ValueError("both `invariant_id` or `invariant` cannot be set; use only one") |
        | yes       | yes       | yes          | yes                  | yes       | yes       | yes                  | yes                  | ValueError("both `invariant_id` or `invariant` cannot be set; use only one") |
        | yes       | yes       | no           | n/a                  | yes       | yes       | yes                  | n/a                  | no error                                                                     |
        | yes       | yes       | yes          | no                   | yes       | yes       | yes                  | n/a                  | ValueError("both `invariant_id` or `invariant` cannot be set; use only one") |
        | yes       | yes       | yes          | yes                  | yes       | yes       | yes                  | yes                  | ValueError("both `invariant_id` or `invariant` cannot be set; use only one") |
        | no        | n/a       | yes          | n/a                  | yes       | yes       | n/a                  | yes                  | ValueError("condition")                                                      |
        | yes       | no        | yes          | n/a                  | yes       | yes       | n/a                  | yes                  | TypeError("condition")                                                       |
        | yes       | yes       | yes          | yes                  | yes       | yes       | yes                  | yes                  | ValueError("both `invariant_id` or `invariant` cannot be set; use only one") |
        """  # noqa=E501

        frozen_invariant = Invariant("FakeId", "Fake", ["a", "b", "c"])

        with self.subTest(
            """
            |           | condition |              | invariant_id         |           | invariant | invariant            | invariant.identifier |                                                                              |
            | condition | typed     | invariant_id | in                   | invariant | typed     | in                   | equals               |                                                                              |
            | set       | Condition | set          | condition.invariants | set       | Invariant | condition.invariants | invariant_id         | expected results                                                             |
            | yes       | yes       | yes          | yes                  | yes       | yes       | yes                  | yes                  | ValueError("both `invariant_id` or `invariant` cannot be set; use only one") |
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(
                ValueError,
                "both `invariant_id` or `invariant` cannot be set; use only one",
            ):
                invariant = deepcopy(frozen_invariant)
                SingleInvariant(
                    Condition("Fake", "Fake", [invariant]),
                    invariant_id="FakeId",
                    invariant=invariant,
                )

        with self.subTest(
            """
            |           | condition |              | invariant_id         |           | invariant | invariant            | invariant.identifier |                         |
            | condition | typed     | invariant_id | in                   | invariant | typed     | in                   | equals               |                         |
            | set       | Condition | set          | condition.invariants | set       | Invariant | condition.invariants | invariant_id         | expected results        |
            | no        | n/a       | yes          | n/a                  | yes       | yes       | n/a                  | yes                  | ValueError("condition") |
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(ValueError, "condition"):
                invariant = deepcopy(frozen_invariant)
                SingleInvariant(None, invariant_id="FakeId", invariant=invariant)

        with self.subTest(
            """
            |           | condition |              | invariant_id         |           | invariant | invariant            | invariant.identifier |                        |
            | condition | typed     | invariant_id | in                   | invariant | typed     | in                   | equals               |                        |
            | set       | Condition | set          | condition.invariants | set       | Invariant | condition.invariants | invariant_id         | expected results       |
            | yes       | no        | yes          | n/a                  | yes       | yes       | n/a                  | yes                  | TypeError("condition") |
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(TypeError, "condition"):
                invariant = deepcopy(frozen_invariant)
                SingleInvariant(1, invariant_id="FakeId", invariant=invariant)

        with self.subTest(
            """
            |           | condition |              | invariant_id         |           | invariant | invariant            | invariant.identifier |                  |
            | condition | typed     | invariant_id | in                   | invariant | typed     | in                   | equals               |                  |
            | set       | Condition | set          | condition.invariants | set       | Invariant | condition.invariants | invariant_id         | expected results |
            | yes       | yes       | no           | n/a                  | yes       | yes       | yes                  | n/a                  | no error         |
            """  # noqa=E501
        ):
            invariant = deepcopy(frozen_invariant)
            SingleInvariant(
                Condition("FakeConditionId", "FakeConditionDescription", [invariant]),
                None,
                invariant=invariant,
            )

        with self.subTest(
            """
            |           | condition |              | invariant_id         |           | invariant | invariant            | invariant.identifier |                         |
            | condition | typed     | invariant_id | in                   | invariant | typed     | in                   | equals               |                         |
            | set       | Condition | set          | condition.invariants | set       | Invariant | condition.invariants | invariant_id         | expected results        |
            | no        | n/a       | no           | n/a                  | yes       | yes       | n/a                  | n/a                  | ValueError("condition") |
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(ValueError, "condition"):
                invariant = deepcopy(frozen_invariant)
                SingleInvariant(None, None, invariant=invariant)

        with self.subTest(
            """
            |           | condition |              | invariant_id         |           | invariant | invariant            | invariant.identifier |                        |
            | condition | typed     | invariant_id | in                   | invariant | typed     | in                   | equals               |                        |
            | set       | Condition | set          | condition.invariants | set       | Invariant | condition.invariants | invariant_id         | expected results       |
            | yes       | no        | no           | n/a                  | yes       | yes       | n/a                  | n/a                  | TypeError("condition") |
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(TypeError, "condition"):
                invariant = deepcopy(frozen_invariant)
                SingleInvariant(1, None, invariant=invariant)

        with self.subTest(
            """
            |           | condition |              | invariant_id         |           | invariant | invariant            | invariant.identifier |                                                                              |
            | condition | typed     | invariant_id | in                   | invariant | typed     | in                   | equals               |                                                                              |
            | set       | Condition | set          | condition.invariants | set       | Invariant | condition.invariants | invariant_id         | expected results                                                             |
            | yes       | yes       | yes          | no                   | yes       | yes       | yes                  | n/a                  | ValueError("both `invariant_id` or `invariant` cannot be set; use only one") |
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(
                ValueError,
                "both `invariant_id` or `invariant` cannot be set; use only one",
            ):
                invariant = deepcopy(frozen_invariant)
                SingleInvariant(
                    Condition(
                        "FakeConditionIdentifier",
                        "FakeDescriptionIdentifier",
                        [invariant],
                    ),
                    "AnotherFakeId",
                    invariant=invariant,
                )

        with self.subTest(
            """
            |           | condition |              | invariant_id         |           | invariant | invariant            | invariant.identifier |                  |
            | condition | typed     | invariant_id | in                   | invariant | typed     | in                   | equals               |                  |
            | set       | Condition | set          | condition.invariants | set       | Invariant | condition.invariants | invariant_id         | expected results |
            | yes       | yes       | yes          | yes                  | no        | n/a       | n/a                  | n/a                  | no error         |
            """  # noqa=E501
        ):
            invariant = deepcopy(frozen_invariant)
            SingleInvariant(
                Condition(
                    "FakeConditionIdentifier", "FakeDescriptionIdentifier", [invariant]
                ),
                "FakeId",
                None,
            )

        with self.subTest(
            """
            |           | condition |              | invariant_id         |           | invariant | invariant            | invariant.identifier |                         |
            | condition | typed     | invariant_id | in                   | invariant | typed     | in                   | equals               |                         |
            | set       | Condition | set          | condition.invariants | set       | Invariant | condition.invariants | invariant_id         | expected results        |
            | no        | n/a       | yes          | n/a                  | no        | n/a       | n/a                  | n/a                  | ValueError("condition") |
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(ValueError, "condition"):
                invariant = deepcopy(frozen_invariant)
                SingleInvariant(None, "AnotherFakeId", None)

        with self.subTest(
            """
            |           | condition |              | invariant_id         |           | invariant | invariant            | invariant.identifier |                        |
            | condition | typed     | invariant_id | in                   | invariant | typed     | in                   | equals               |                        |
            | set       | Condition | set          | condition.invariants | set       | Invariant | condition.invariants | invariant_id         | expected results       |
            | yes       | no        | yes          | n/a                  | no        | n/a       | n/a                  | n/a                  | TypeError("condition") |
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(TypeError, "condition"):
                invariant = deepcopy(frozen_invariant)
                SingleInvariant(1, "AnotherFakeId", None)

        with self.subTest(
            """
            |           | condition |              | invariant_id         |           | invariant | invariant            | invariant.identifier |                                                                |
            | condition | typed     | invariant_id | in                   | invariant | typed     | in                   | equals               |                                                                |
            | set       | Condition | set          | condition.invariants | set       | Invariant | condition.invariants | invariant_id         | expected results                                               |
            | yes       | yes       | no           | n/a                  | no        | n/a       | n/a                  | n/a                  | ValueError("either `invariant_id` or `invariant` must be set") |
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(
                ValueError, "either `invariant_id` or `invariant` must be set"
            ):
                invariant = deepcopy(frozen_invariant)
                SingleInvariant(
                    Condition(
                        "FakeConditionIdentifier",
                        "FakeConditionDescription",
                        [invariant],
                    ),
                    None,
                    None,
                )

        with self.subTest(
            """
            |           | condition |              | invariant_id         |           | invariant | invariant            | invariant.identifier |                         |
            | condition | typed     | invariant_id | in                   | invariant | typed     | in                   | equals               |                         |
            | set       | Condition | set          | condition.invariants | set       | Invariant | condition.invariants | invariant_id         | expected results        |
            | no        | n/a       | no           | n/a                  | no        | n/a       | n/a                  | n/a                  | ValueError("condition") |
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(ValueError, "condition"):
                invariant = deepcopy(frozen_invariant)
                SingleInvariant(None, None, None)

        with self.subTest(
            """
            |           | condition |              | invariant_id         |           | invariant | invariant            | invariant.identifier |                        |
            | condition | typed     | invariant_id | in                   | invariant | typed     | in                   | equals               |                        |
            | set       | Condition | set          | condition.invariants | set       | Invariant | condition.invariants | invariant_id         | expected results       |
            | yes       | no        | no           | n/a                  | no        | n/a       | n/a                  | n/a                  | TypeError("condition") |
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(TypeError, "condition"):
                invariant = deepcopy(frozen_invariant)
                SingleInvariant(1, None, None)

        with self.subTest(
            """
            |           | condition |              | invariant_id         |           | invariant | invariant            | invariant.identifier |                          |
            | condition | typed     | invariant_id | in                   | invariant | typed     | in                   | equals               |                          |
            | set       | Condition | set          | condition.invariants | set       | Invariant | condition.invariants | invariant_id         | expected results         |
            | yes       | yes       | yes          | no                   | no        | n/a       | n/a                  | n/a                  | KeyError("invariant_id") |
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(KeyError, "invariant_id"):
                invariant = deepcopy(frozen_invariant)
                SingleInvariant(
                    Condition(
                        "FakeConditionIdentifier",
                        "FakeConditionDescription",
                        [invariant],
                    ),
                    "AnotherFakeId",
                    None,
                )

        with self.subTest(
            """
            |           | condition |              | invariant_id         |           | invariant | invariant            | invariant.identifier |                                                                              |
            | condition | typed     | invariant_id | in                   | invariant | typed     | in                   | equals               |                                                                              |
            | set       | Condition | set          | condition.invariants | set       | Invariant | condition.invariants | invariant_id         | expected results                                                             |
            | yes       | yes       | yes          | yes                  | yes       | no        | n/a                  | n/a                  | ValueError("both `invariant_id` or `invariant` cannot be set; use only one") |
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(
                ValueError,
                "both `invariant_id` or `invariant` cannot be set; use only one",
            ):
                invariant = deepcopy(frozen_invariant)
                SingleInvariant(
                    Condition(
                        "FakeConditionIdentifier",
                        "FakeConditionDescription",
                        [invariant],
                    ),
                    "FakeId",
                    1,
                )

        with self.subTest(
            """
            |           | condition |              | invariant_id         |           | invariant | invariant            | invariant.identifier |                         |
            | condition | typed     | invariant_id | in                   | invariant | typed     | in                   | equals               |                         |
            | set       | Condition | set          | condition.invariants | set       | Invariant | condition.invariants | invariant_id         | expected results        |
            | no        | n/a       | yes          | n/a                  | yes       | no        | n/a                  | n/a                  | ValueError("condition") |
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(ValueError, "condition"):
                invariant = deepcopy(frozen_invariant)
                SingleInvariant(None, "AnotherFakeId", 1)

        with self.subTest(
            """
            |           | condition |              | invariant_id         |           | invariant | invariant            | invariant.identifier |                         |
            | condition | typed     | invariant_id | in                   | invariant | typed     | in                   | equals               |                         |
            | set       | Condition | set          | condition.invariants | set       | Invariant | condition.invariants | invariant_id         | expected results        |
            | yes       | no        | yes          | n/a                  | yes       | no        | n/a                  | n/a                  | TypeError("condition")                                                             |
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(TypeError, "condition"):
                invariant = deepcopy(frozen_invariant)
                SingleInvariant(1, "AnotherFakeId", 1)

        with self.subTest(
            """
            |           | condition |              | invariant_id         |           | invariant | invariant            | invariant.identifier |                        |
            | condition | typed     | invariant_id | in                   | invariant | typed     | in                   | equals               |                        |
            | set       | Condition | set          | condition.invariants | set       | Invariant | condition.invariants | invariant_id         | expected results       |
            | yes       | yes       | no           | n/a                  | yes       | no        | n/a                  | n/a                  | TypeError("invariant") |
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(TypeError, "invariant"):
                invariant = deepcopy(frozen_invariant)
                SingleInvariant(
                    Condition(
                        "FakeConditionIdentifier",
                        "FakeConditionDescription",
                        [invariant],
                    ),
                    None,
                    1,
                )

        with self.subTest(
            """
            |           | condition |              | invariant_id         |           | invariant | invariant            | invariant.identifier |                         |
            | condition | typed     | invariant_id | in                   | invariant | typed     | in                   | equals               |                         |
            | set       | Condition | set          | condition.invariants | set       | Invariant | condition.invariants | invariant_id         | expected results        |
            | no        | n/a       | no           | n/a                  | yes       | no        | n/a                  | n/a                  | ValueError("condition") |
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(ValueError, "condition"):
                invariant = deepcopy(frozen_invariant)
                SingleInvariant(None, None, 1)

        with self.subTest(
            """
            |           | condition |              | invariant_id         |           | invariant | invariant            | invariant.identifier |                        |
            | condition | typed     | invariant_id | in                   | invariant | typed     | in                   | equals               |                        |
            | set       | Condition | set          | condition.invariants | set       | Invariant | condition.invariants | invariant_id         | expected results       |
            | yes       | no        | no           | n/a                  | yes       | no        | n/a                  | n/a                  | TypeError("condition") |
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(TypeError, "condition"):
                invariant = deepcopy(frozen_invariant)
                SingleInvariant(1, None, 1)

        with self.subTest(
            """
            |           | condition |              | invariant_id         |           | invariant | invariant            | invariant.identifier |                                                                              |
            | condition | typed     | invariant_id | in                   | invariant | typed     | in                   | equals               |                                                                              |
            | set       | Condition | set          | condition.invariants | set       | Invariant | condition.invariants | invariant_id         | expected results                                                             |
            | yes       | yes       | yes          | no                   | yes       | no        | n/a                  | n/a                  | ValueError("both `invariant_id` or `invariant` cannot be set; use only one") |
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(
                ValueError,
                "both `invariant_id` or `invariant` cannot be set; use only one",
            ):
                invariant = deepcopy(frozen_invariant)
                SingleInvariant(
                    Condition(
                        "FakeConditionIdentifier",
                        "FakeConditionDescription",
                        [invariant],
                    ),
                    -1,
                    1,
                )

        with self.subTest(
            """
            |           | condition |              | invariant_id         |           | invariant | invariant            | invariant.identifier |                                                                              |
            | condition | typed     | invariant_id | in                   | invariant | typed     | in                   | equals               |                                                                              |
            | set       | Condition | set          | condition.invariants | set       | Invariant | condition.invariants | invariant_id         | expected results                                                             |
            | yes       | yes       | yes          | yes                  | yes       | yes       | yes                  | yes                  | ValueError("both `invariant_id` or `invariant` cannot be set; use only one")       |
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(
                ValueError,
                "both `invariant_id` or `invariant` cannot be set; use only one",
            ):
                invariant = deepcopy(frozen_invariant)
                SingleInvariant(
                    Condition(
                        "FakeConditionIdentifier",
                        "FakeConditionDescription",
                        [invariant],
                    ),
                    "FakeId",
                    invariant,
                )

        with self.subTest(
            """
            |           | condition |              | invariant_id         |           | invariant | invariant            | invariant.identifier |                                                                              |
            | condition | typed     | invariant_id | in                   | invariant | typed     | in                   | equals               |                                                                              |
            | set       | Condition | set          | condition.invariants | set       | Invariant | condition.invariants | invariant_id         | expected results                                                             |
            | yes       | yes       | no           | n/a                  | yes       | yes       | yes                  | n/a                  | no error                                                                         |
            """  # noqa=E501
        ):
            invariant = deepcopy(frozen_invariant)
            SingleInvariant(
                Condition(
                    "FakeConditionIdentifier", "FakeConditionDescription", [invariant]
                ),
                None,
                invariant,
            )

        with self.subTest(
            """
            |           | condition |              | invariant_id         |           | invariant | invariant            | invariant.identifier |                                                                              |
            | condition | typed     | invariant_id | in                   | invariant | typed     | in                   | equals               |                                                                              |
            | set       | Condition | set          | condition.invariants | set       | Invariant | condition.invariants | invariant_id         | expected results                                                             |
            | yes       | yes       | yes          | no                   | yes       | yes       | yes                  | n/a                  | ValueError("both `invariant_id` or `invariant` cannot be set; use only one") |
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(
                ValueError,
                "both `invariant_id` or `invariant` cannot be set; use only one",
            ):
                invariant = deepcopy(frozen_invariant)
                SingleInvariant(
                    Condition(
                        "FakeConditionIdentifier",
                        "FakeConditionDescription",
                        [invariant],
                    ),
                    -2,
                    invariant,
                )

        with self.subTest(
            """
            |           | condition |              | invariant_id         |           | invariant | invariant            | invariant.identifier |                                                                              |
            | condition | typed     | invariant_id | in                   | invariant | typed     | in                   | equals               |                                                                              |
            | set       | Condition | set          | condition.invariants | set       | Invariant | condition.invariants | invariant_id         | expected results                                                             |
            | yes       | yes       | yes          | yes                  | yes       | yes       | yes                  | yes                  | ValueError("both `invariant_id` or `invariant` cannot be set; use only one") |
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(
                ValueError,
                "both `invariant_id` or `invariant` cannot be set; use only one",
            ):
                invariant = deepcopy(frozen_invariant)
                SingleInvariant(
                    Condition(
                        "FakeConditionIdentifier",
                        "FakeConditionDescription",
                        [invariant],
                    ),
                    "FakeId",
                    invariant,
                )

        with self.subTest(
            """
            |           | condition |              | invariant_id         |           | invariant | invariant            | invariant.identifier |                         |
            | condition | typed     | invariant_id | in                   | invariant | typed     | in                   | equals               |                         |
            | set       | Condition | set          | condition.invariants | set       | Invariant | condition.invariants | invariant_id         | expected results        |
            | no        | n/a       | yes          | n/a                  | yes       | yes       | n/a                  | yes                  | ValueError("condition") |
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(ValueError, "condition"):
                invariant = deepcopy(frozen_invariant)
                SingleInvariant(None, "FakeId", invariant)

        with self.subTest(
            """
            |           | condition |              | invariant_id         |           | invariant | invariant            | invariant.identifier |                        |
            | condition | typed     | invariant_id | in                   | invariant | typed     | in                   | equals               |                        |
            | set       | Condition | set          | condition.invariants | set       | Invariant | condition.invariants | invariant_id         | expected results       |
            | yes       | no        | yes          | n/a                  | yes       | yes       | n/a                  | yes                  | TypeError("condition") |
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(TypeError, "condition"):
                invariant = deepcopy(frozen_invariant)
                SingleInvariant(1, "FakeId", invariant)

        with self.subTest(
            """
            |           | condition |              | invariant_id         |           | invariant | invariant            | invariant.identifier |                                                                              |
            | condition | typed     | invariant_id | in                   | invariant | typed     | in                   | equals               |                                                                              |
            | set       | Condition | set          | condition.invariants | set       | Invariant | condition.invariants | invariant_id         | expected results                                                             |
            | yes       | yes       | yes          | yes                  | yes       | yes       | yes                  | yes                  | ValueError("both `invariant_id` or `invariant` cannot be set; use only one") |
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(
                ValueError,
                "both `invariant_id` or `invariant` cannot be set; use only one",
            ):
                invariant = deepcopy(frozen_invariant)
                SingleInvariant(
                    Condition(
                        "FakeConditionIdentifier",
                        "FakeConditionDescription",
                        [invariant],
                    ),
                    "FakeId",
                    invariant,
                )

    def test_eq(self):
        """
        1. If other is not set => False
        2. Invariants:
        | condition equal | invariant equal | result |
        | yes             | yes             | True   |
        | no              | yes             | False  |
        | yes             | no              | False  |
        | no              | no              | False  |
        """

        with self.subTest(
            """
            1. If other is not set => False
            """
        ):
            first = SingleInvariant(
                Condition(
                    "FakeConditionId",
                    "FakeConditionDescription",
                    [
                        Invariant("FakeInvariantId_1", "FakeInvariantDescription", 1),
                        Invariant("FakeInvariantId_2", "FakeInvariantDescription", 2),
                    ],
                ),
                invariant_id="FakeInvariantId_2",
            )
            other = None
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
            | condition equal | invariant equal | result |
            | yes             | yes             | True   |
            """
        ):
            first = SingleInvariant(
                Condition(
                    "FakeConditionId",
                    "FakeConditionDescription",
                    [
                        Invariant("FakeInvariantId_1", "FakeInvariantDescription", 1),
                        Invariant("FakeInvariantId_2", "FakeInvariantDescription", 2),
                    ],
                ),
                invariant_id="FakeInvariantId_2",
            )
            fake_invariant = Invariant(
                "FakeInvariantId_2", "FakeInvariantDescription", 2
            )
            other = deepcopy(first)
            self.assertTrue(first == other)
            self.assertEqual(first, other)

        with self.subTest(
            """
            | condition equal | invariant equal | result |
            | no              | yes             | False  |
            """
        ):
            first = SingleInvariant(
                Condition(
                    "FakeConditionId",
                    "FakeConditionDescription",
                    [
                        Invariant("FakeInvariantId_1", "FakeInvariantDescription", 1),
                    ],
                ),
                invariant_id="FakeInvariantId_1",
            )
            fake_invariant = Invariant(
                "FakeInvariantId_2", "FakeInvariantDescription", 2
            )
            other = deepcopy(first)
            # pylint:disable=[W0212]
            other._condition._description = "AnotherFakeConditionDescription"
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
            | condition equal | invariant equal | result |
            | yes             | no              | False  |
            """
        ):
            first = SingleInvariant(
                Condition(
                    "FakeConditionId",
                    "FakeConditionDescription",
                    [
                        Invariant("FakeInvariantId_1", "FakeInvariantDescription", 1),
                        Invariant("FakeInvariantId_2", "FakeInvariantDescription", 2),
                    ],
                ),
                invariant_id="FakeInvariantId_1",
            )
            # pylint:disable=[W0212]
            fake_invariant = deepcopy(first._condition._invariants[1])
            other = deepcopy(first)
            other._invariant = fake_invariant  # pylint:disable=[W0212]
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
            | condition equal | invariant equal | result |
            | no              | no              | False  |
            """
        ):
            first = SingleInvariant(
                Condition(
                    "FakeConditionId",
                    "FakeConditionDescription",
                    [
                        Invariant("FakeInvariantId_1", "FakeInvariantDescription", 1),
                        Invariant("FakeInvariantId_2", "FakeInvariantDescription", 2),
                    ],
                ),
                invariant_id="FakeInvariantId_1",
            )
            fake_invariant = Invariant(
                "FakeInvariantId_2", "FakeInvariantDescription", 2
            )
            other = SingleInvariant(
                Condition(
                    "AnotherFakeConditionId",
                    "AnotherFakeConditionDescription",
                    [
                        Invariant("FakeInvariantId_1", "FakeInvariantDescription", 1),
                        fake_invariant,
                    ],
                ),
                invariant=fake_invariant,
            )
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

    # pylint: disable=[C0301]
    def test_invariant(self):
        """
        Test for the `invariant` property.
        ! The constructor checks that either invariant or invariant id is set.
        ! The constructor checks that invariant and invariant id are NOT both set.
        | invariant set | invariant id set | invariant id in set of invariants | expected result                            |
        | yes           | n/a              | n/a                               | set invariant returned                     |
        | no            | yes              | yes                               | invariant set, to invariants[invariant id] |
        | no            | yes              | no                                | KeyError("invariant_id")                   |
        """  # noqa=E501

        with self.subTest(
            """
            | invariant set | invariant id set | invariant id in set of invariants | expected result                            |
            | yes           | n/a              | n/a                               | set invariant returned                     |
            """  # noqa=E501
        ):
            test_invariant = Invariant(2, "FakeInvariantDescription_2", 2)
            testee = SingleInvariant(
                Condition(
                    "FakeConditionId",
                    "FakeConditionDescription",
                    [
                        Invariant(1, "FakeInvariantDescription_1", 1),
                        test_invariant,
                    ],
                ),
                None,
                test_invariant,
            )
            self.assertEqual(testee.invariant, test_invariant)

        with self.subTest(
            """
            | invariant set | invariant id set | invariant id in set of invariants | expected result                            |
            | no            | yes              | yes                               | invariant set, to invariants[invariant id] |
            """  # noqa=E501
        ):
            test_invariant = Invariant(4, "FakeInvariantDescription_4", 4)
            testee = SingleInvariant(
                Condition(
                    "FakeConditionId",
                    "FakeConditionDescription",
                    [
                        Invariant(3, "FakeInvariantDescription_3", 3),
                        test_invariant,
                    ],
                ),
                4,
            )
            self.assertEqual(testee.invariant, test_invariant)
            # pylint: disable=[W0212]
            self.assertEqual(testee._invariant, test_invariant)

        with self.subTest(
            """
            | invariant set | invariant id set | invariant id in set of invariants | expected result                            |
            | no            | yes              | no                                | KeyError("invariant_id")                   |
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(KeyError, "invariant_id"):
                testee = SingleInvariant(
                    Condition(
                        "FakeConditionId",
                        "FakeConditionDescription",
                        [
                            Invariant(5, "FakeInvariantDescription_5", 5),
                            Invariant(6, "FakeInvariantDescription_6", 6),
                        ],
                    ),
                    7,
                ).invariant
