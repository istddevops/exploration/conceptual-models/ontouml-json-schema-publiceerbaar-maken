"""
Test suite for src.unittests.helpers.invariant_generator.Invariant class
"""

from copy import deepcopy
from unittest import TestCase

from src.unittests.helpers.class_helpers import assert_members
from src.unittests.helpers.invariant_generator import Invariant


class TestInvariant(TestCase):
    """
    Test suite for Invariant class.
    """

    def test_members(self):
        """
        Check to see only the expected members are defined on `Invariant`
        """
        assert_members(self, Invariant, ["identifier", "description", "data"])

    def test_init(self):
        # pylint:disable=[C0301]
        """
        | identifier set | description set | description str | data set | expected result           |
        | yes            | yes             | yes             | yes      | no error                  |
        | no             | yes             | yes             | yes      | ValueError("identifier")  |
        | yes            | no              | n/a             | yes      | ValueError("description") |
        | no             | no              | n/a             | yes      | ValueError("identifier")  |
        | yes            | yes             | no              | yes      | TypeError("description")  |
        | no             | yes             | no              | yes      | ValueError("identifier")  |
        | yes            | yes             | yes             | no       | ValueError("data")        |
        | no             | yes             | yes             | no       | ValueError("identifier")  |
        | yes            | no              | n/a             | no       | ValueError("description") |
        | no             | no              | n/a             | no       | ValueError("identifier")  |
        | yes            | yes             | no              | no       | TypeError("description")  |
        | no             | yes             | no              | no       | ValueError("identifier")  |
        """  # noqa=E501

        with self.subTest(
            """
            | identifier set | description set | description str | data set | expected result           |
            | yes            | yes             | yes             | yes      | no error                  |
            """  # noqa=E501
        ):
            Invariant("FakeIdentifier", "FakeDescription", {"data": 1})

        with self.subTest(
            """
            | identifier set | description set | description str | data set | expected result           |
            | no             | yes             | yes             | yes      | ValueError("identifier")  |
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(ValueError, "identifier"):
                Invariant(None, "FakeDescription", {"data": 1})

        with self.subTest(
            """
            | identifier set | description set | description str | data set | expected result           |
            | yes            | no              | n/a             | yes      | ValueError("description") |
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(ValueError, "description"):
                Invariant(1, None, {"data": 1})

        with self.subTest(
            """
            | identifier set | description set | description str | data set | expected result           |
            | no             | no              | n/a             | yes      | ValueError("identifier")  |
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(ValueError, "identifier"):
                Invariant(None, None, {"data": 1})

        with self.subTest(
            """
            | identifier set | description set | description str | data set | expected result           |
            | yes            | yes             | no              | yes      | TypeError("description")  |
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(TypeError, "description"):
                Invariant("FakeIdentifier", 1, {"data": 1})

        with self.subTest(
            """
            | identifier set | description set | description str | data set | expected result           |
            | no             | yes             | no              | yes      | ValueError("identifier")  |
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(ValueError, "identifier"):
                Invariant(None, 1, {"data": 1})

        with self.subTest(
            """
            | identifier set | description set | description str | data set | expected result           |
            | yes            | yes             | yes             | no       | ValueError("data")        |
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(ValueError, "data"):
                Invariant(1, "FakeDescription", None)

        with self.subTest(
            """
            | identifier set | description set | description str | data set | expected result           |
            | no             | yes             | yes             | no       | ValueError("identifier")  |
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(ValueError, "identifier"):
                Invariant(None, "FakeDescription", None)

        with self.subTest(
            """
            | identifier set | description set | description str | data set | expected result           |
            | yes            | no              | n/a             | no       | ValueError("description") |
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(ValueError, "description"):
                Invariant(1, None, None)

        with self.subTest(
            """
            | identifier set | description set | description str | data set | expected result           |
            | no             | no              | n/a             | no       | ValueError("identifier")  |
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(ValueError, "identifier"):
                Invariant(None, None, None)

        with self.subTest(
            """
            | identifier set | description set | description str | data set | expected result           |
            | yes            | yes             | no              | no       | TypeError("description")  |
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(TypeError, "description"):
                Invariant(1, 1, None)

        with self.subTest(
            """
            | identifier set | description set | description str | data set | expected result           |
            | no             | yes             | no              | no       | ValueError("identifier")  |
            """  # noqa=E501
        ):
            with self.assertRaisesRegex(ValueError, "identifier"):
                Invariant(None, 1, None)

    # pylint:disable=[R0915]
    def test_eq(self):
        """
        1. Is other is None => False
        2. Testables:
        | identifier equal | description equal | data equal | result |
        | yes              | yes               | yes        | True   |
        | no               | yes               | yes        | False  |
        | yes              | no                | yes        | False  |
        | no               | no                | yes        | False  |
        | yes              | yes               | no         | False  |
        | no               | yes               | no         | False  |
        | yes              | no                | no         | False  |
        | no               | no                | no         | False  |
        """

        frozen = Invariant("FakeIdentifier", "FakeDescription", {"fake_data": 1})
        with self.subTest("""1. Is other is None => False"""):
            first = deepcopy(frozen)
            other = None
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
            | identifier equal | description equal | data equal | result |
            | yes              | yes               | yes        | True   |
            """
        ):
            first = deepcopy(frozen)
            other = deepcopy(frozen)
            self.assertTrue(first == other)
            self.assertEqual(first, other)

        with self.subTest(
            """
            | identifier equal | description equal | data equal | result |
            | no               | yes               | yes        | False  |
            """
        ):
            first = deepcopy(frozen)
            other = deepcopy(frozen)
            other._identifier = f"Another{other._identifier}"  # pylint:disable=[W0212]
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
            | identifier equal | description equal | data equal | result |
            | yes              | no                | yes        | False  |
            """
        ):
            first = deepcopy(frozen)
            other = deepcopy(frozen)
            # pylint:disable=[W0212]
            other._description = f"Another{other._description}"
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
            | identifier equal | description equal | data equal | result |
            | no               | no                | yes        | False  |
            """
        ):
            first = deepcopy(frozen)
            other = deepcopy(frozen)
            # pylint:disable=[W0212]
            other._identifier = f"Another{other._identifier}"
            other._description = f"Another{other._description}"
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
            | identifier equal | description equal | data equal | result |
            | yes              | yes               | no         | False  |
            """
        ):
            first = deepcopy(frozen)
            other = deepcopy(frozen)
            # pylint:disable=[W0212]
            other._data = {"data": 2}
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
            | identifier equal | description equal | data equal | result |
            | no               | yes               | no         | False  |
            """
        ):
            first = deepcopy(frozen)
            other = deepcopy(frozen)
            # pylint:disable=[W0212]
            other._identifier = f"Another{other._identifier}"
            other._data = {"data": 2}
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
            | identifier equal | description equal | data equal | result |
            | yes              | no                | no         | False  |
            """
        ):
            first = deepcopy(frozen)
            other = deepcopy(frozen)
            # pylint:disable=[W0212]
            other._description = f"Another{other._description}"
            other._data = {"data": 2}
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)

        with self.subTest(
            """
            | identifier equal | description equal | data equal | result |
            | no               | no                | no         | False  |
            """
        ):
            first = deepcopy(frozen)
            other = deepcopy(frozen)
            # pylint:disable=[W0212]
            other._identifier = f"Another{other._identifier}"
            other._description = f"Another{other._description}"
            other._data = {"data": 2}
            self.assertFalse(first == other)
            self.assertNotEqual(first, other)
