"""
Test suite for src.unittests.helpers.invariant_generator.Scenario class
"""


from unittest import TestCase

from src.unittests.helpers.class_helpers import assert_members
from src.unittests.helpers.invariant_generator import (
    Condition,
    ExpectedResult,
    Invariant,
    Scenario,
    SingleInvariant,
)


class TestScenario(TestCase):
    """
    Test suite for Scenario class.
    """

    def test_members(self):
        """
        Check to see only the expected members are defined on `Scenario`
        """
        assert_members(
            self,
            Scenario,
            ["matrix", "expected_result", "single_invariants", "description"],
        )

    # pylint:disable=[W0212]
    def test_calculate_number_of_set_repetitions(self):
        # pylint:disable=[C0301]
        """
        The set of repetitions for a set of invariants equals the products of the
        number of invariants for the conditions FOLLOWING this one. When the condition
        is the last, the result should always be 1.

        Given the set of conditions and invariants
        | condition # | # of invariants |
        | 1           | 2               |
        | 2           | 5               |
        | 3           | 3               |
        | 4           | 8               |
        When the number of repetitions of the set of invariants is calculated for condition <condition>
        Then the result is <outcome>
        Examples:
        | condition | outcome |
        | 1         | 120     |
        | 2         | 24      |
        | 3         | 8       |
        | 4         | 1       |
        """  # noqa=E501

        testbase = [
            Condition(
                "1",
                "FakeDescription",
                [
                    Invariant("1.1", "FakeDescription", 1.1),
                    Invariant("1.2", "FakeDescription", 1.2),
                ],
            ),
            Condition(
                "2",
                "FakeDescription",
                [
                    Invariant("2.1", "FakeDescription", 2.1),
                    Invariant("2.2", "FakeDescription", 2.2),
                    Invariant("2.3", "FakeDescription", 2.3),
                    Invariant("2.4", "FakeDescription", 2.4),
                    Invariant("2.5", "FakeDescription", 2.5),
                ],
            ),
            Condition(
                "3",
                "FakeDescription",
                [
                    Invariant("3.1", "FakeDescription", 3.1),
                    Invariant("3.2", "FakeDescription", 3.2),
                    Invariant("3.3", "FakeDescription", 3.3),
                ],
            ),
            Condition(
                "4",
                "FakeDescription",
                [
                    Invariant("4.1", "FakeDescription", 4.1),
                    Invariant("4.2", "FakeDescription", 4.2),
                    Invariant("4.3", "FakeDescription", 4.3),
                    Invariant("4.4", "FakeDescription", 4.4),
                    Invariant("4.5", "FakeDescription", 4.5),
                    Invariant("4.6", "FakeDescription", 4.6),
                    Invariant("4.7", "FakeDescription", 4.7),
                    Invariant("4.8", "FakeDescription", 4.8),
                ],
            ),
        ]

        with self.subTest(
            """
            Given the set of conditions and invariants
            | condition # | # of invariants |
            | 1           | 2               |
            | 2           | 5               |
            | 3           | 3               |
            | 4           | 8               |
            When the number of repetitions of the set of invariants is calculated for condition <condition>
            Then the result is <outcome>
            Examples:
            | condition | outcome |
            | 1         | 120     |
            """  # noqa=E501
        ):
            self.assertEqual(
                Scenario._calculate_number_of_set_repetitions(testbase, 0), 120
            )

        with self.subTest(
            """
            Given the set of conditions and invariants
            | condition # | # of invariants |
            | 1           | 2               |
            | 2           | 5               |
            | 3           | 3               |
            | 4           | 8               |
            When the number of repetitions of the set of invariants is calculated for condition <condition>
            Then the result is <outcome>
            Examples:
            | condition | outcome |
            | 2         | 24      |
            """  # noqa=E501
        ):
            self.assertEqual(
                Scenario._calculate_number_of_set_repetitions(testbase, 1), 24
            )

        with self.subTest(
            """
            Given the set of conditions and invariants
            | condition # | # of invariants |
            | 1           | 2               |
            | 2           | 5               |
            | 3           | 3               |
            | 4           | 8               |
            When the number of repetitions of the set of invariants is calculated for condition <condition>
            Then the result is <outcome>
            Examples:
            | condition | outcome |
            | 3         | 8       |
            """  # noqa=E501
        ):
            self.assertEqual(
                Scenario._calculate_number_of_set_repetitions(testbase, 2), 8
            )

        with self.subTest(
            """
            Given the set of conditions and invariants
            | condition # | # of invariants |
            | 1           | 2               |
            | 2           | 5               |
            | 3           | 3               |
            | 4           | 8               |
            When the number of repetitions of the set of invariants is calculated for condition <condition>
            Then the result is <outcome>
            Examples:
            | condition | outcome |
            | 4         | 1       |
            """  # noqa=E501
        ):
            self.assertEqual(
                Scenario._calculate_number_of_set_repetitions(testbase, 3), 1
            )

    # pylint:disable=[W0212, C0301]
    def test_calculate_number_of_invariant_repetitions(self):
        """
        The number of repetitions for an invariants equals the products of the number of invariants for the conditions BEFORE this one. When the condition
        is the first, the result should always be 1.

        Given the set of conditions and invariants
        | condition # | # of invariants |
        | 1           | 2               |
        | 2           | 5               |
        | 3           | 3               |
        | 4           | 8               |
        When the number of repetitions of the invariant is calculated for condition <condition>
        Then the result is <outcome>
        Examples:
        | condition | outcome |
        | 1         | 1       |
        | 2         | 2       |
        | 3         | 10      |
        | 4         | 30      |
        """  # noqa=E501

        testbase = [
            Condition(
                "1",
                "FakeDescription",
                [
                    Invariant("1.1", "FakeDescription", 1.1),
                    Invariant("1.2", "FakeDescription", 1.2),
                ],
            ),
            Condition(
                "2",
                "FakeDescription",
                [
                    Invariant("2.1", "FakeDescription", 2.1),
                    Invariant("2.2", "FakeDescription", 2.2),
                    Invariant("2.3", "FakeDescription", 2.3),
                    Invariant("2.4", "FakeDescription", 2.4),
                    Invariant("2.5", "FakeDescription", 2.5),
                ],
            ),
            Condition(
                "3",
                "FakeDescription",
                [
                    Invariant("3.1", "FakeDescription", 3.1),
                    Invariant("3.2", "FakeDescription", 3.2),
                    Invariant("3.3", "FakeDescription", 3.3),
                ],
            ),
            Condition(
                "4",
                "FakeDescription",
                [
                    Invariant("4.1", "FakeDescription", 4.1),
                    Invariant("4.2", "FakeDescription", 4.2),
                    Invariant("4.3", "FakeDescription", 4.3),
                    Invariant("4.4", "FakeDescription", 4.4),
                    Invariant("4.5", "FakeDescription", 4.5),
                    Invariant("4.6", "FakeDescription", 4.6),
                    Invariant("4.7", "FakeDescription", 4.7),
                    Invariant("4.8", "FakeDescription", 4.8),
                ],
            ),
        ]

        with self.subTest(
            """
            The number of repetitions for an invariants equals the products of the
            number of invariants for the conditions BEFORE this one. When the condition
            is the first, the result should always be 1.
            
            Given the set of conditions and invariants
            | condition # | # of invariants |
            | 1           | 2               |
            | 2           | 5               |
            | 3           | 3               |
            | 4           | 8               |
            When the number of repetitions of the invariant is calculated for condition <condition>
            Then the result is <outcome>
            Examples:
            | condition | outcome |
            | 1         | 1       |
            """  # noqa=E501
        ):
            self.assertEqual(
                Scenario._calculate_number_of_invariant_repetitions(testbase, 0), 1
            )

        with self.subTest(
            """
            The number of repetitions for an invariants equals the products of the
            number of invariants for the conditions BEFORE this one. When the condition
            is the first, the result should always be 1.
            
            Given the set of conditions and invariants
            | condition # | # of invariants |
            | 1           | 2               |
            | 2           | 5               |
            | 3           | 3               |
            | 4           | 8               |
            When the number of repetitions of the invariant is calculated for condition <condition>
            Then the result is <outcome>
            Examples:
            | condition | outcome |
            | 2         | 2       |
            """  # noqa=E501
        ):
            self.assertEqual(
                Scenario._calculate_number_of_invariant_repetitions(testbase, 1), 2
            )

        with self.subTest(
            """
            The number of repetitions for an invariants equals the products of the
            number of invariants for the conditions BEFORE this one. When the condition
            is the first, the result should always be 1.
            
            Given the set of conditions and invariants
            | condition # | # of invariants |
            | 1           | 2               |
            | 2           | 5               |
            | 3           | 3               |
            | 4           | 8               |
            When the number of repetitions of the invariant is calculated for condition <condition>
            Then the result is <outcome>
            Examples:
            | condition | outcome |
            | 3         | 10      |
            """  # noqa=E501
        ):
            self.assertEqual(
                Scenario._calculate_number_of_invariant_repetitions(testbase, 2), 10
            )
        with self.subTest(
            """
            The number of repetitions for an invariants equals the products of the
            number of invariants for the conditions BEFORE this one. When the condition
            is the first, the result should always be 1.
            
            Given the set of conditions and invariants
            | condition # | # of invariants |
            | 1           | 2               |
            | 2           | 5               |
            | 3           | 3               |
            | 4           | 8               |
            When the number of repetitions of the invariant is calculated for condition <condition>
            Then the result is <outcome>
            Examples:
            | condition | outcome |
            | 4         | 30      |
            """  # noqa=E501
        ):
            self.assertEqual(
                Scenario._calculate_number_of_invariant_repetitions(testbase, 3), 30
            )

    def test_matrix(self):
        """
        Generates a matrix, in which each column represents a condition, and each line
        represents a combination of invariants for each condition, in such a way that
        each combination of invariants for all condtions are present.

        Given the set of conditions and invariants
        | condition              | invariant     |
        | fake condition         | invariant yes |
        | fake condition         | invariant no  |
        | another fake condition | black         |
        | another fake condition | white         |
        | another fake condition | gray          |
        | final fake condition   | 1             |
        | final fake condition   | 2             |
        | final fake condition   | 3             |

        When the testmatrix is generated
        Then the matrix contains these items
        | fake condition | another fake condition | final fake condition |
        | invariant yes  | black                  | 1                    |
        | invariant no   | black                  | 1                    |
        | invariant yes  | white                  | 1                    |
        | invariant no   | white                  | 1                    |
        | invariant yes  | gray                   | 1                    |
        | invariant no   | gray                   | 1                    |
        | invariant yes  | black                  | 2                    |
        | invariant no   | black                  | 2                    |
        | invariant yes  | white                  | 2                    |
        | invariant no   | white                  | 2                    |
        | invariant yes  | gray                   | 2                    |
        | invariant no   | gray                   | 2                    |
        | invariant yes  | black                  | 3                    |
        | invariant no   | black                  | 3                    |
        | invariant yes  | white                  | 3                    |
        | invariant no   | white                  | 3                    |
        | invariant yes  | gray                   | 3                    |
        | invariant no   | gray                   | 3                    |
        """

        fake_condition = Condition(
            "fake condition",
            "fake condition",
            [
                Invariant("invariant yes", "yes", True),
                Invariant("invariant no", "no", False),
            ],
        )
        another_fake_condition = Condition(
            "another fake condition",
            "another fake condition",
            [
                Invariant("black", "black", {"color": "black"}),
                Invariant("white", "white", {"color": "white"}),
                Invariant("gray", "gray", {"color": "gray"}),
            ],
        )
        final_fake_condition = Condition(
            "final fake condition",
            "final fake condition",
            [
                Invariant("1", "1", 1),
                Invariant("2", "2", 2),
                Invariant("3", "3", 3),
            ],
        )
        conditions = [fake_condition, another_fake_condition, final_fake_condition]

        expected = [
            Scenario(
                [
                    SingleInvariant(fake_condition, "invariant yes"),
                    SingleInvariant(another_fake_condition, "black"),
                    SingleInvariant(final_fake_condition, "1"),
                ]
            ),
            Scenario(
                [
                    SingleInvariant(fake_condition, "invariant no"),
                    SingleInvariant(another_fake_condition, "black"),
                    SingleInvariant(final_fake_condition, "1"),
                ]
            ),
            Scenario(
                [
                    SingleInvariant(fake_condition, "invariant yes"),
                    SingleInvariant(another_fake_condition, "white"),
                    SingleInvariant(final_fake_condition, "1"),
                ]
            ),
            Scenario(
                [
                    SingleInvariant(fake_condition, "invariant no"),
                    SingleInvariant(another_fake_condition, "white"),
                    SingleInvariant(final_fake_condition, "1"),
                ]
            ),
            Scenario(
                [
                    SingleInvariant(fake_condition, "invariant yes"),
                    SingleInvariant(another_fake_condition, "gray"),
                    SingleInvariant(final_fake_condition, "1"),
                ]
            ),
            Scenario(
                [
                    SingleInvariant(fake_condition, "invariant no"),
                    SingleInvariant(another_fake_condition, "gray"),
                    SingleInvariant(final_fake_condition, "1"),
                ]
            ),
            Scenario(
                [
                    SingleInvariant(fake_condition, "invariant yes"),
                    SingleInvariant(another_fake_condition, "black"),
                    SingleInvariant(final_fake_condition, "2"),
                ]
            ),
            Scenario(
                [
                    SingleInvariant(fake_condition, "invariant no"),
                    SingleInvariant(another_fake_condition, "black"),
                    SingleInvariant(final_fake_condition, "2"),
                ]
            ),
            Scenario(
                [
                    SingleInvariant(fake_condition, "invariant yes"),
                    SingleInvariant(another_fake_condition, "white"),
                    SingleInvariant(final_fake_condition, "2"),
                ]
            ),
            Scenario(
                [
                    SingleInvariant(fake_condition, "invariant no"),
                    SingleInvariant(another_fake_condition, "white"),
                    SingleInvariant(final_fake_condition, "2"),
                ]
            ),
            Scenario(
                [
                    SingleInvariant(fake_condition, "invariant yes"),
                    SingleInvariant(another_fake_condition, "gray"),
                    SingleInvariant(final_fake_condition, "2"),
                ]
            ),
            Scenario(
                [
                    SingleInvariant(fake_condition, "invariant no"),
                    SingleInvariant(another_fake_condition, "gray"),
                    SingleInvariant(final_fake_condition, "2"),
                ]
            ),
            Scenario(
                [
                    SingleInvariant(fake_condition, "invariant yes"),
                    SingleInvariant(another_fake_condition, "black"),
                    SingleInvariant(final_fake_condition, "3"),
                ]
            ),
            Scenario(
                [
                    SingleInvariant(fake_condition, "invariant no"),
                    SingleInvariant(another_fake_condition, "black"),
                    SingleInvariant(final_fake_condition, "3"),
                ]
            ),
            Scenario(
                [
                    SingleInvariant(fake_condition, "invariant yes"),
                    SingleInvariant(another_fake_condition, "white"),
                    SingleInvariant(final_fake_condition, "3"),
                ]
            ),
            Scenario(
                [
                    SingleInvariant(fake_condition, "invariant no"),
                    SingleInvariant(another_fake_condition, "white"),
                    SingleInvariant(final_fake_condition, "3"),
                ]
            ),
            Scenario(
                [
                    SingleInvariant(fake_condition, "invariant yes"),
                    SingleInvariant(another_fake_condition, "gray"),
                    SingleInvariant(final_fake_condition, "3"),
                ]
            ),
            Scenario(
                [
                    SingleInvariant(fake_condition, "invariant no"),
                    SingleInvariant(another_fake_condition, "gray"),
                    SingleInvariant(final_fake_condition, "3"),
                ]
            ),
        ]

        testcases = Scenario.matrix(conditions)
        self.assertEqual(testcases, expected)

    def test_init(self):
        """
        Background:
            Given a new `Scenario` instance is being created

        When `single_invariants` is set to a list of `SingleInvariant` items
            And `expected_result` is set to an `ExpectedResult` item
        Then the new `Scenario` instance will be created

        When `single_invariants` is set to a list of `SingleInvariant` items
            But `expected_result` is not set
        Then the new `Scenario` instance will be created

        When `single_invariants` is not set
        Then a `ValueError` is raised with message `single_invariants`

        When `single_invariants` is set
            But is not a list containing 0 or more items of type `SingleInvariant`
        Then a `TypeError` is raised with message `single_invariants`
        """

        fake_yes_no = [
            Invariant("FakeYesIdentifier", "FakeYesDescription", "yes"),
            Invariant("FakeNoIdentifier", "FakeNoDescription", "no"),
        ]
        condition = Condition(
            "FakeConditionIdentifier", "FakeConditionDescription", fake_yes_no
        )

        with self.subTest(
            """
            Background:
                Given a new `Scenario` instance is being created

            When `single_invariants` is set to a list of `SingleInvariant` items
                And `expected_result` is set to an `ExpectedResult` item
            Then the new `Scenario` instance will be created
            """
        ):
            Scenario(
                [
                    SingleInvariant(condition, invariant_id="FakeYesIdentifier"),
                    SingleInvariant(condition, invariant_id="FakeNoIdentifier"),
                ],
                ExpectedResult("FakeExpectedResultDescription", lambda: True),
            )

        with self.subTest(
            """
            Background:
                Given a new `Scenario` instance is being created

            When `single_invariants` is set to a list of `SingleInvariant` items
                But `expected_result` is not set
            Then the new `Scenario` instance will be created
            """
        ):
            Scenario(
                [
                    SingleInvariant(condition, invariant_id="FakeYesIdentifier"),
                    SingleInvariant(condition, invariant_id="FakeNoIdentifier"),
                ]
            )

        with self.subTest(
            """
            Background:
                Given a new `Scenario` instance is being created

            When `single_invariants` is not set
            Then a `ValueError` is raised with message `single_invariants`
            """
        ):
            with self.assertRaisesRegex(ValueError, "single_invariants"):
                Scenario(None)

        with self.subTest(
            """
            Background:
                Given a new `Scenario` instance is being created

            When `single_invariants` is set
                But is not a list containing 0 or more items of type `SingleInvariant`
            Then a `TypeError` is raised with message `single_invariants`

            `single_invariants` set to int
            """
        ):
            with self.assertRaisesRegex(TypeError, "single_invariants"):
                Scenario(1)

        with self.subTest(
            """
            Background:
                Given a new `Scenario` instance is being created

            When `single_invariants` is set
                But is not a list containing 0 or more items of type `SingleInvariant`
            Then a `TypeError` is raised with message `single_invariants`

            `single_invariants` set to list of strings
            """
        ):
            with self.assertRaisesRegex(TypeError, "single_invariants"):
                Scenario(["1", "2"])

        with self.subTest(
            """
            Background:
                Given a new `Scenario` instance is being created

            When `single_invariants` is set
                But is not a list containing 0 or more items of type `SingleInvariant`
            Then a `TypeError` is raised with message `single_invariants`

            `single_invariants` set to list of strings and `SingleInvariant` items
            """
        ):
            with self.assertRaisesRegex(TypeError, "single_invariants"):
                Scenario(
                    [
                        "1",
                        "2",
                        SingleInvariant(condition, invariant_id="FakeYesIdentifier"),
                    ]
                )

        with self.subTest(
            """
            Background:
                Given a new `Scenario` instance is being created

            When `single_invariants` is set
                But is not a list containing 0 or more items of type `SingleInvariant`
            Then a `TypeError` is raised with message `single_invariants`

            `single_invariants` set to an empty list
            """
        ):
            with self.assertRaisesRegex(TypeError, "single_invariants"):
                Scenario([])

    def test_description(self):
        """
        Description is generated by combining all descriptions of the `Condition` items
        followed by the description of its `invariant` member. Finally, the description
        of the `ExpectedResult` item for this `Scenario` is added.

        The constructors of respectively `Condition`, `Invariant` and `ExpectedResult`
        enforce that the `description` member is set. So we won't take that into account

        Given a Scenario

        If the Scenario has the following Condition items
        | condition.description | invariant.description |
        | Number of conditions  | two                   |
        | ExpectedResult set    | no                    |
        But no ExpectedResult is set
        When the description for this Scenario is generated
        Then it results in "Number of conditions 'two', ExpectedResult set 'no'"

        If the Scenario has the following Condition items
        | condition.description | invariant.description |
        | Number of conditions  | two                   |
        | ExpectedResult set    | yes                   |
        And the ExpectedResult has a description "formatted result"
        When the description for this Scenario is generated
        Then it results in "Number of conditions 'two', ExpectedResult set 'yes' results in formatted result"

        If the scenario has the following Condition items
        | condition.description | invariant.description |
        | Number of conditions  | one                   |
        But no ExpectedResult is set
        When the description for this Scenario is generated
        Then it results in "Number of conditions 'one'"

        If the scenario has the following Condition items
        | condition.description | invariant.description |
        | Number of conditions  | one                   |
        And the ExpectedResult has a description "formatted result"
        When the description for this Scenario is generated
        Then it results in "Number of conditions 'one' results in formatted result"
        """  # noqa=E501

        with self.subTest(
            """
            Given a Scenario

            If the Scenario has the following Condition items
            | condition.description | invariant.description |
            | Number of conditions  | two                   |
            | ExpectedResult set    | no                    |
            But no ExpectedResult is set
            When the description for this Scenario is generated
            Then it results in "Number of conditions 'two', ExpectedResult set 'no'"
            """  # noqa=E501
        ):
            invariant_number_of_conditions = Invariant("Invariant_1", "two", 2)
            invariant_expected_result_set = Invariant("Invariant_2", "no", False)
            conditions = [
                Condition(
                    "Condition_1",
                    "Number of conditions",
                    [invariant_number_of_conditions],
                ),
                Condition(
                    "Condition_2", "ExpectedResult set", [invariant_expected_result_set]
                ),
            ]
            single_invariants = [
                SingleInvariant(
                    conditions[0], invariant=invariant_number_of_conditions
                ),
                SingleInvariant(conditions[1], invariant=invariant_expected_result_set),
            ]
            testee = Scenario(single_invariants)
            expected = "Number of conditions 'two', ExpectedResult set 'no'"
            self.assertEqual(testee.description, expected)

        with self.subTest(
            """
            Given a Scenario

            If the Scenario has the following Condition items
            | condition.description | invariant.description |
            | Number of conditions  | two                   |
            | ExpectedResult set    | yes                   |
            And the ExpectedResult has a description "formatted result"
            When the description for this Scenario is generated
            Then it results in "Number of conditions 'two', ExpectedResult set 'yes' results in formatted result"
            """  # noqa=E501
        ):
            invariant_number_of_conditions = Invariant("Invariant_1", "two", 2)
            invariant_expected_result_set = Invariant("Invariant_2", "yes", False)
            conditions = [
                Condition(
                    "Condition_1",
                    "Number of conditions",
                    [invariant_number_of_conditions],
                ),
                Condition(
                    "Condition_2", "ExpectedResult set", [invariant_expected_result_set]
                ),
            ]
            single_invariants = [
                SingleInvariant(
                    conditions[0], invariant=invariant_number_of_conditions
                ),
                SingleInvariant(conditions[1], invariant=invariant_expected_result_set),
            ]
            testee = Scenario(
                single_invariants,
                ExpectedResult("formatted result", lambda: "Formatted result"),
            )
            expected = (
                "Number of conditions 'two', ExpectedResult set 'yes' results in "
                + "formatted result"
            )
            self.assertEqual(testee.description, expected)

        with self.subTest(
            """
            Given a Scenario

            If the scenario has the following Condition items
            | condition.description | invariant.description |
            | Number of conditions  | one                   |
            But no ExpectedResult is set
            When the description for this Scenario is generated
            Then it results in "Number of conditions 'one'"
            """  # noqa=E501
        ):
            invariant_number_of_conditions = Invariant("Invariant_1", "one", 2)
            conditions = [
                Condition(
                    "Condition_1",
                    "Number of conditions",
                    [invariant_number_of_conditions],
                ),
            ]
            single_invariants = [
                SingleInvariant(conditions[0], invariant=invariant_number_of_conditions)
            ]
            testee = Scenario(single_invariants)
            expected = "Number of conditions 'one'"
            self.assertEqual(testee.description, expected)

        with self.subTest(
            """
            Given a Scenario

            If the scenario has the following Condition items
            | condition.description | invariant.description |
            | Number of conditions  | one                   |
            And the ExpectedResult has a description "formatted result"
            When the description for this Scenario is generated
            Then it results in "Number of conditions 'one' results in formatted result"
            """  # noqa=E501
        ):
            invariant_number_of_conditions = Invariant("Invariant_1", "one", 2)
            conditions = [
                Condition(
                    "Condition_1",
                    "Number of conditions",
                    [invariant_number_of_conditions],
                ),
            ]
            single_invariants = [
                SingleInvariant(conditions[0], invariant=invariant_number_of_conditions)
            ]
            testee = Scenario(
                single_invariants,
                ExpectedResult("formatted result", lambda: "Formatted result"),
            )
            expected = "Number of conditions 'one' results in formatted result"
            self.assertEqual(testee.description, expected)
