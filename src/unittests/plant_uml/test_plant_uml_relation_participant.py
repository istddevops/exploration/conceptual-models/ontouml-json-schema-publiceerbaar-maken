"""Test suites for plant_uml.plant_uml_relation"""


from unittest import TestCase

from src.plant_uml.plant_uml_relation import PlantUMLRelationParticipant


class TestPlantUMLRelationParticipant(TestCase):
    """Test suite for `PlantUMLRelationParticipant`"""

    def test_init(self):
        """
        Test for `PlantUMLRelationParticipant` constructor
         1. If `identifier` is not set -> `ValueError`
         2. If `identifier` is set, but not of type `str` -> `TypeError`
         3. If `identifier` is set, and of type `str` -> no error
         4. If `cardinality` is set, but not of type `str` -> `TypeError`
         5. If `cardinality` is set, and of type `str` -> no error
         6. If `aggregation_kind` is not set -> no error
         7. If `aggregation_kind` is set, but not of type `str` -> `TypeError`
         8. If `aggregation_kind` is set, and of type `str`, but not with either "NONE",
            "COMPOSITE" or "SHARED" -> `ValueError`
         9. If `aggregation_kind` is set, with value "NONE" -> no error
        10. If `aggregation_kind` is set, with value "COMPOSITE" -> no error
        11. If `aggregation_kind` is set, with value "SHARED" -> no error
        12. If `participant_type` is not set -> `ValueError`
        13. If `participant_type` is set, but not of type `str` -> `TypeError`
        14. If `participant_type` is set, and of type `str` -> no error
        """

        with self.subTest("1. If `identifier` is not set -> `ValueError`"):
            with self.assertRaisesRegex(ValueError, "identifier"):
                PlantUMLRelationParticipant(None, None, "NONE", "Class")

        with self.subTest(
            "2. If `identifier` is set, but not of type `str` -> `TypeError`"
        ):
            with self.assertRaisesRegex(TypeError, "identifier"):
                PlantUMLRelationParticipant(1, None, "NONE", "Class")

        with self.subTest("3. If `identifier` is set, and of type `str` -> no error"):
            PlantUMLRelationParticipant("FakeIdentifier", None, "NONE", "Class")

        with self.subTest(
            "4. If `cardinality` is set, but not of type `str` -> `TypeError`"
        ):
            with self.assertRaisesRegex(TypeError, "cardinality"):
                PlantUMLRelationParticipant("FakeIdentifier", 1, "NONE", "Class")

        with self.subTest("5. If `cardinality` is set, and of type `str` -> no error"):
            PlantUMLRelationParticipant(
                "FakeIdentifier", "FakeCardinality", "NONE", "Class"
            )

        with self.subTest("6. If `aggregation_kind` is not set -> no error"):
            PlantUMLRelationParticipant(
                "FakeIdentifier", "FakeCardinality", None, "Class"
            )

        with self.subTest(
            "7. If `aggregation_kind` is set, but not of type `str` -> `TypeError`"
        ):
            with self.assertRaisesRegex(TypeError, "aggregation_kind"):
                PlantUMLRelationParticipant(
                    "FakeIdentifier", "FakeCardinality", 1, "Class"
                )

        with self.subTest(
            """
            8. If `aggregation_kind` is set, and of type `str`, but not with either
               "NONE", "COMPOSITE" or "SHARED" -> `ValueError`
            """
        ):
            with self.assertRaisesRegex(ValueError, "aggregation_kind"):
                PlantUMLRelationParticipant(
                    "FakeIdentifier", "FakeCardinality", "FakeAggregation", "Class"
                )

        with self.subTest(
            """9. If `aggregation_kind` is set, with value "NONE" -> no error"""
        ):
            PlantUMLRelationParticipant(
                "FakeIdentifier", "FakeCardinality", "NONE", "Class"
            )

        with self.subTest(
            """10. If `aggregation_kind` is set, with value "COMPOSITE" -> no error"""
        ):
            PlantUMLRelationParticipant(
                "FakeIdentifier", "FakeCardinality", "COMPOSITE", "Class"
            )

        with self.subTest(
            """11. If `aggregation_kind` is set, with value "SHARED" -> no error"""
        ):
            PlantUMLRelationParticipant(
                "FakeIdentifier", "FakeCardinality", "SHARED", "Class"
            )

        with self.subTest("""12. If `participant_type` is not set -> `ValueError`"""):
            with self.assertRaisesRegex(ValueError, "participant_type"):
                PlantUMLRelationParticipant(
                    "FakeIdentifier", "FakeCardinality", "SHARED", None
                )

        with self.subTest(
            """13. If `participant_type` is set, but not of type `str` -> `TypeError`"""
        ):
            with self.assertRaisesRegex(TypeError, "participant_type"):
                PlantUMLRelationParticipant(
                    "FakeIdentifier", "FakeCardinality", "SHARED", 1
                )

        with self.subTest(
            """14. If `participant_type` is set, and of type `str` -> no error"""
        ):
            PlantUMLRelationParticipant(
                "FakeIdentifier", "FakeCardinality", "SHARED", "FakeType"
            )

    def test_identifier(self):
        """
        Test for `identifier` property of `PlantUMLRelationParticipant` instance.
        1. If `PlantUMLRelationParticipant` was constructed with `identifier`
           containing no periods -> `identifier` value as passed to constructor.
        2. If `PlantUMLRelationParticipant` was constructed with `identifier`
           containing one period (`.`) -> `identifier` value, with the period (`.`)
           replaced with three underscores (`___`)
        3. If `PlantUMLRelationParticipant` was constructed with `identifier`
           containing multiple periods (`.`) -> `identifier` value, with each
           period (`.`) replaced with three underscores (`___`)
        """

        with self.subTest(
            """
            1. If `PlantUMLRelationParticipant` was constructed with `identifier`
               containing no periods -> `identifier` value as passed to constructor.
            """
        ):
            expected = "FakeIdentifier"
            self.assertEqual(
                PlantUMLRelationParticipant(expected, None, "NONE", "Class").identifier,
                expected,
            )

        with self.subTest(
            """
            2. If `PlantUMLRelationParticipant` was constructed with `identifier`
               containing one period (`.`) -> `identifier` value, with the period (`.`)
               replaced with three underscores (`___`)
            """
        ):
            expected = "Fake.Identifier"
            worker = PlantUMLRelationParticipant(expected, None, "NONE", "Class")
            expected = expected.replace(".", "___")
            self.assertEqual(worker.identifier, expected)

        with self.subTest(
            """
            3. If `PlantUMLRelationParticipant` was constructed with `identifier`
               containing multiple periods (`.`) -> `identifier` value, with each
               period (`.`) replaced with three underscores (`___`)
            """
        ):
            expected = "Fake.Ident.ifier"
            worker = PlantUMLRelationParticipant(expected, None, "NONE", "Class")
            expected = expected.replace(".", "___")
            self.assertEqual(worker.identifier, expected)

    def test_aggregation(self):
        """
        Test for `aggregation` property of `PlantUMLRelationParticipant` instance.
        1. If `PlantUMLRelationParticipant` was constructed with `aggregation_kind` set
           to "COMPOSITE" -> "*"
        2. If `PlantUMLRelationParticipant` was constructed with `aggregation_kind` set
           to "SHARED" -> "o"
        3. If `PlantUMLRelationParticipant` was constructed with `aggregation_kind` set
           to "NONE" -> ""
        4. If `PlantUMLRelationParticipant` was constructed with any other string value
           -> `ValueError`
        """

        with self.subTest(
            """
            1. If `PlantUMLRelationParticipant` was constructed with `aggregation_kind`
               set to "COMPOSITE" -> "*"
            """
        ):
            expected = "*"
            aggregation_input = "COMPOSITE"
            self.assertEqual(
                PlantUMLRelationParticipant(
                    "FakeId", None, aggregation_input, "Class"
                ).aggregation,
                expected,
            )

        with self.subTest(
            """
            2. If `PlantUMLRelationParticipant` was constructed with `aggregation_kind`
               set to "SHARED" -> "o"
            """
        ):
            expected = "o"
            aggregation_input = "SHARED"
            self.assertEqual(
                PlantUMLRelationParticipant(
                    "FakeId", None, aggregation_input, "Class"
                ).aggregation,
                expected,
            )

        with self.subTest(
            """
            3. If `PlantUMLRelationParticipant` was constructed with `aggregation_kind`
               set to "NONE" -> ""
            """
        ):
            expected = ""
            aggregation_input = "NONE"
            self.assertEqual(
                PlantUMLRelationParticipant(
                    "FakeId", None, aggregation_input, "Class"
                ).aggregation,
                expected,
            )

        with self.subTest(
            """
            4. If `PlantUMLRelationParticipant` was constructed with any other string
               value -> `ValueError`
            """
        ):
            with self.assertRaisesRegex(ValueError, "aggregation_kind"):
                aggregation_input = "FakeAggregation"
                # pylint: disable=[W0106]
                PlantUMLRelationParticipant(
                    "FakeId", None, aggregation_input, "Class"
                ).aggregation
