"""Test suites for plant_uml.plant_uml_relation"""


from unittest import TestCase

from src.plant_uml.plant_uml_relation import (
    PlantUMLRelation,
    PlantUMLRelationParticipant,
)


class TestPlantUMLRelation(TestCase):
    """Test suite for `PlantUMLRelation`"""

    def test_init(self):
        """
        Test the `PlantUMLRelation` constructor
        1. `source` not set -> ValueError
        2. `source` set but not of type `PlantUMLRelationParticipant` -> TypeError
        3. `target` not set -> ValueError
        4. `target` set but not of type `PlantUMLRelationParticipant` -> TypeError
        5. `name` set but not of type `str` -> TypeError
        6. `name` set and of type `str` -> no error
        7. `stereotype` set but not of type `str` -> TypeError
        8. `stereotype` set and of type `str` -> no error
        """

        with self.subTest(
            """
            1. `source` not set -> ValueError
            """
        ):
            with self.assertRaisesRegex(ValueError, "source"):
                PlantUMLRelation(None, None, None, None)

        with self.subTest(
            """
            2. `source` set but not of type `PlantUMLRelationParticipant` -> TypeError
            """
        ):
            with self.assertRaisesRegex(TypeError, "source"):
                PlantUMLRelation(1, None, None, None)

        with self.subTest(
            """
            3. `target` not set -> ValueError
            """
        ):
            with self.assertRaisesRegex(ValueError, "target"):
                PlantUMLRelation(
                    PlantUMLRelationParticipant(
                        "FakeSourceIdentifier", None, "NONE", "Class"
                    ),
                    None,
                    None,
                    None,
                )

        with self.subTest(
            """
            4. `target` set but not of type `PlantUMLRelationParticipant` -> TypeError
            """
        ):
            with self.assertRaisesRegex(TypeError, "target"):
                PlantUMLRelation(
                    PlantUMLRelationParticipant(
                        "FakeSourceIdentifier", None, "NONE", "Class"
                    ),
                    1,
                    None,
                    None,
                )

        with self.subTest(
            """
            5. `name` set but not of type `str` -> TypeError
            """
        ):
            with self.assertRaisesRegex(TypeError, "name"):
                PlantUMLRelation(
                    PlantUMLRelationParticipant(
                        "FakeSourceIdentifier", None, "NONE", "Class"
                    ),
                    PlantUMLRelationParticipant(
                        "FakeTargetIdentifier", None, "NONE", "Class"
                    ),
                    1,
                    None,
                )

        with self.subTest(
            """
            6. `name` set and of type `str` -> no error
            """
        ):
            PlantUMLRelation(
                PlantUMLRelationParticipant(
                    "FakeSourceIdentifier", None, "NONE", "Class"
                ),
                PlantUMLRelationParticipant(
                    "FakeTargetIdentifier", None, "NONE", "Class"
                ),
                "FakeName",
                None,
            )

        with self.subTest(
            """
            7. `stereotype` set but not of type `str` -> TypeError
            """
        ):
            with self.assertRaisesRegex(TypeError, "stereotype"):
                PlantUMLRelation(
                    PlantUMLRelationParticipant(
                        "FakeSourceIdentifier", None, "NONE", "Class"
                    ),
                    PlantUMLRelationParticipant(
                        "FakeTargetIdentifier", None, "NONE", "Class"
                    ),
                    "FakeName",
                    1,
                )

        with self.subTest(
            """
            8. `stereotype` set and of type `str` -> no error
            """
        ):
            PlantUMLRelation(
                PlantUMLRelationParticipant(
                    "FakeSourceIdentifier", None, "NONE", "Class"
                ),
                PlantUMLRelationParticipant(
                    "FakeTargetIdentifier", None, "NONE", "Class"
                ),
                "FakeName",
                "FakeDescription",
            )

    def test_stereotype(self):
        """
        Test the `stereotype` property.
        1.   `stereotype` set in `__init__` ->  `<<{stereotype}>>`
        2.   `stereotype` not set in `__init__` -> ``
        2.a. `stereotype` set to empty string in `__init__` -> ``
        """

        with self.subTest(
            """
            1. `stereotype` set in `__init__` ->  `<<{stereotype}>>`
            """
        ):
            expected = "<<FakeStereotype>>"
            self.assertEqual(
                PlantUMLRelation(
                    PlantUMLRelationParticipant("FakeSourceId", None, "NONE", "Class"),
                    PlantUMLRelationParticipant("FakeTargetId", None, "NONE", "Class"),
                    None,
                    "FakeStereotype",
                ).stereotype,
                expected,
            )

        with self.subTest(
            """
            2. `sterotype` not set in `__init__` -> ``
            """
        ):
            expected = ""
            self.assertEqual(
                PlantUMLRelation(
                    PlantUMLRelationParticipant("FakeSourceId", None, "NONE", "Class"),
                    PlantUMLRelationParticipant("FakeTargetId", None, "NONE", "Class"),
                    None,
                    None,
                ).stereotype,
                expected,
            )

        with self.subTest(
            """
            2.a. `stereotype` set to empty string in `__init__` -> ``
            """
        ):
            expected = ""
            self.assertEqual(
                PlantUMLRelation(
                    PlantUMLRelationParticipant("FakeSourceId", None, "NONE", "Class"),
                    PlantUMLRelationParticipant("FakeTargetId", None, "NONE", "Class"),
                    None,
                    "",
                ).stereotype,
                expected,
            )

    def test_plant_uml(self):
        # pylint: disable=[C0301]
        """
        Test `plant_uml` property.
        Invariants:
        1. `source.cardinality` is either set or not
        2. `target.cardinality` is either set or not
        3. `source.aggregation` values [`NONE`, `COMPOSITE`, `SHARED`]
        4. `target.aggregation` values [`NONE`, `COMPOSITE`, `SHARED`]
        5. `name` is either set or not
        6. `stereotype` is either set or not

        Result should be:
        `{source.identifier} "{source.cardinality}" {source.aggregation}--{target.aggregation} "{target.cardinality}" {target.identifier} : {stereotype}\n{name}`
        """  # noqa: E501

        # pylint: disable=[R1702]
        for s_c in [
            [' "SC"', "`source.cardinality` set", "SC"],
            ["", "`source.cardinality` not set", ""],
        ]:
            for t_c in [
                [' "TC"', "`target.cardinality` set", "TC"],
                ["", "`target.cardinality` not set", ""],
            ]:
                for s_a in [
                    [" ", "`source.aggregation` set to `NONE`", "NONE"],
                    [" *", "`source.aggregation` set to `COMPOSITE`", "COMPOSITE"],
                    [" o", "`source.aggregation` set to `SHARED`", "SHARED"],
                ]:
                    for t_a in [
                        ["", "`target.aggregation` set to `NONE`", "NONE"],
                        ["*", "`target.aggregation` set to `COMPOSITE`", "COMPOSITE"],
                        ["o", "`target.aggregation` set to `SHARED`", "SHARED"],
                    ]:
                        for name in [
                            [True, "FakeName", "`name` set", "FakeName"],
                            [False, "", "`name` not set", None],
                        ]:
                            for stereotype in [
                                [
                                    True,
                                    "<<FakeStereotype>>",
                                    "`stereotype` set",
                                    "FakeStereotype",
                                ],
                                [False, "", "`stereotype` not set", None],
                            ]:
                                delimeter = ""
                                line_break = ""
                                if name[0] or stereotype[0]:
                                    delimeter = " : "
                                if name[0] and stereotype[0]:
                                    line_break = "\\n"
                                expected = (
                                    f"FakeSourceId{s_c[0]}{s_a[0]}--{t_a[0]}{t_c[0]}"
                                    + f" FakeTargetId{delimeter}{stereotype[1]}"
                                    + f"{line_break}{name[1]}"
                                )
                                scenario = (
                                    f"{s_c[1]}, {t_c[1]}, {s_a[1]}, {t_a[1]}, {name[2]}"
                                    + f", {stereotype[2]}"
                                )
                                with self.subTest(scenario):
                                    self.assertEqual(
                                        PlantUMLRelation(
                                            PlantUMLRelationParticipant(
                                                "FakeSourceId", s_c[2], s_a[2], "Class"
                                            ),
                                            PlantUMLRelationParticipant(
                                                "FakeTargetId", t_c[2], t_a[2], "Class"
                                            ),
                                            name[3],
                                            stereotype[3],
                                        ).plant_uml,
                                        [expected],
                                    )
