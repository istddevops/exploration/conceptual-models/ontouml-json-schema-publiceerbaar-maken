"""Test suites for plant_uml.plant_uml_association_relation"""

from unittest import TestCase

from src.plant_uml.plant_uml_association_relation import PlantUMLAssociationRelation
from src.plant_uml.plant_uml_class import PlantUMLClass
from src.plant_uml.plant_uml_relation import (
    PlantUMLRelation,
    PlantUMLRelationParticipant,
)


class TestPlantUMLAssociationRelation(TestCase):
    """Test suite for PlantUMLAssociationRelation class"""

    def test_plant_uml(self):
        # pylint: disable=[C0301]
        """Test the plant_uml property. The returned string (rather, single string
        array) should be formatted thusly:
        ({relation_source_id}, {relation_target_id}) .. {associated_class_id}

        Invariants: both the relation_source and relation_target object has additional
        properties (besides identifier), which should not affect outcome of the test.
        Same goes for for associated class.

        1.a. - relation.source only has `identifier` and `participant_type` set
             - relation.target only has `identifier` and `participant_type` set
             - relation.name and relation.stereotype are not set
             - associated_class only has `identifier` and `name` set
        1.b. - relation.source has all attributes set
             - relation.target has all attributes set
             - relation.name and relation.stereotype are set
             - associated_class has all attributes set
        """

        with self.subTest(
            """1.a. - relation.source only has `identifier` and `participant_type` set
                    - relation.target only has `identifier` and `participant_type` set
                    - relation.name and relation.stereotype are not set
                    - associated_class only has `identifier` and `name` set"""
        ):
            fake = PlantUMLAssociationRelation(
                "FakeAssociationRelationId",
                PlantUMLRelation(
                    source=PlantUMLRelationParticipant(
                        "SourceFakeId", "", None, "Class"
                    ),
                    target=PlantUMLRelationParticipant(
                        "TargetFakeId", "", None, "Class"
                    ),
                    name=None,
                    stereotype=None,
                ),
                PlantUMLClass("ClassFakeId", "ClassFakeName"),
            )
            expected = ["(SourceFakeId, TargetFakeId) .. ClassFakeId"]
            self.assertEqual(fake.plant_uml, expected)
