"""Test suites for plant_uml.plant_uml_diagram"""

from typing import List
from unittest import TestCase
from unittest.mock import PropertyMock, patch

from src.plant_uml.plant_uml_association_relation import PlantUMLAssociationRelation
from src.plant_uml.plant_uml_class import PlantUMLClass
from src.plant_uml.plant_uml_diagram import PlantUMLDiagram
from src.plant_uml.plant_uml_generalization import PlantUMLGeneralization
from src.plant_uml.plant_uml_relation import (
    PlantUMLRelation,
    PlantUMLRelationParticipant,
)


# pylint: disable=[W0106]
class TestPlantUMLDiagram(TestCase):
    """Test suite for PlantUMLDiagram"""

    # pylint: disable=[R0915]
    def test_init(self):
        """
        Test the PlantUMLDiagram constructor.
         1. When `identifier` is not set -> ValueError
         2. When `identifier` is set but not typed `str` -> TypeError
         3. When `identifier` is set and typed `str` -> no error
         4. When `name` is not set -> no error
         5. When `name` is set but not typed `str` -> TypeError
         6. When `name` is set and typed `str` -> no error
         7. When `description` is not set -> no error
         8. When `description` is set but not typed `str` -> TypeError
         9. When `description` is set and typed `str` -> no error
        10. When `classes` is not set -> ValueError
        11. When `classes` is set but not typed `List[PlantUMLClass]` -> TypeError
        12. When `classes` is set and typed `List[PlantUMLClass]` but has no items
            -> no error
        13. When `generalizations` is not set -> ValueError
        14. When `generalizations` is set but not typed `List[PlantUMLGeneralization]`
            -> TypeError
        15. When `generalizations` is set and typed `List[PlantUMLGeneralization]` but
            has no items -> no error
        16. When `relations` is not set -> ValueError
        17. When `relations` is set but not typed `List[PlantUMLRelation]` -> TypeError
        18. When `relations` is set and typed `List[PlantUMLRelation]` but has no items
            -> no error
        19. When `_association_relations` is not set -> ValueError
        20. When `_association_relations` is set but not typed
            `List[PlantUMLAssociationRelation]` -> TypeError
        21. When `_association_relations` is set and typed
            `List[PlantUMLAssociationRelation]` but has no items
        """

        with self.subTest("1. When `identifier` is not set -> ValueError"):
            with self.assertRaisesRegex(ValueError, "identifier"):
                PlantUMLDiagram(None, None, None, None, None, None, None)

        with self.subTest(
            "2. When `identifier` is set but not typed `str` -> TypeError"
        ):
            with self.assertRaisesRegex(TypeError, "identifier"):
                PlantUMLDiagram(1, None, None, None, None, None, None)

        with self.subTest("3. When `identifier` is set and typed `str` -> no error"):
            PlantUMLDiagram("FakeIdentifier", None, None, [], [], [], [])

        with self.subTest("4. When `name` is not set -> no error"):
            PlantUMLDiagram("FakeIdentifier", None, None, [], [], [], [])

        with self.subTest("5. When `name` is set but not typed `str` -> TypeError"):
            with self.assertRaisesRegex(TypeError, "name"):
                PlantUMLDiagram("FakeIdentifier", 1, None, [], [], [], [])

        with self.subTest("6. When `name` is set and typed `str` -> no error"):
            PlantUMLDiagram("FakeIdentifier", "FakeName", None, [], [], [], [])

        with self.subTest("7. When `description` is not set -> no error"):
            PlantUMLDiagram("FakeIdentifier", None, None, [], [], [], [])

        with self.subTest(
            "8. When `description` is set but not typed `str` -> TypeError"
        ):
            with self.assertRaisesRegex(TypeError, "description"):
                PlantUMLDiagram("FakeIdentifier", None, 1, [], [], [], [])

        with self.subTest("9. When `description` is set and typed `str` -> no error"):
            PlantUMLDiagram("FakeIdentifier", None, "FakeDescription", [], [], [], [])

        with self.subTest("10. When `classes` is not set -> ValueError"):
            with self.assertRaisesRegex(ValueError, "classes"):
                PlantUMLDiagram("FakeIdentifier", None, None, None, [], [], [])

        with self.subTest(
            """
            11. When `classes` is set but not typed `List[PlantUMLClass]` -> TypeError
            """
        ):
            with self.assertRaisesRegex(TypeError, "classes"):
                PlantUMLDiagram(
                    "FakeIdentifier", None, None, ["a", "b", "c"], [], [], []
                )

        classes: List[PlantUMLClass] = []
        with self.subTest(
            """
            12. When `classes` is set and typed `List[PlantUMLClass]` but has no items
                -> no error
            """
        ):
            PlantUMLDiagram("FakeIdentifier", None, None, classes, [], [], [])

        with self.subTest("13. When `generalizations` is not set -> ValueError"):
            with self.assertRaisesRegex(ValueError, "generalizations"):
                PlantUMLDiagram("FakeIdentifier", None, None, classes, None, [], [])

        with self.subTest(
            """
            14. When `generalizations` is set but not typed
                `List[PlantUMLGeneralization]` -> TypeError
            """
        ):
            with self.assertRaisesRegex(TypeError, "generalizations"):
                PlantUMLDiagram(
                    "FakeIdentifier", None, None, classes, ["a", "b"], [], []
                )

        generalizations: List[PlantUMLGeneralization] = []
        with self.subTest(
            """
            15. When `generalizations` is set and typed `List[PlantUMLGeneralization]`
                but has no items -> no error
            """
        ):
            PlantUMLDiagram(
                "FakeIdentifier", None, None, classes, generalizations, [], []
            )

        with self.subTest("16. When `relations` is not set -> ValueError"):
            with self.assertRaisesRegex(ValueError, "relations"):
                PlantUMLDiagram(
                    "FakeIdentifier", None, None, classes, generalizations, None, []
                )

        with self.subTest(
            """
            17. When `relations` is set but not typed `List[PlantUMLRelation]`
                -> TypeError
            """
        ):
            with self.assertRaisesRegex(TypeError, "relations"):
                PlantUMLDiagram(
                    "FakeIdentifier",
                    None,
                    None,
                    classes,
                    generalizations,
                    ["a", "b"],
                    [],
                )

        relations: List[PlantUMLRelation] = []
        with self.subTest(
            """
            18. When `relations` is set and typed `List[PlantUMLRelation]` but has no
                items -> no error
            """
        ):
            PlantUMLDiagram(
                "FakeIdentifier", None, None, classes, generalizations, relations, []
            )

        with self.subTest("19. When `association_relations` is not set -> ValueError"):
            with self.assertRaisesRegex(ValueError, "association_relations"):
                PlantUMLDiagram(
                    "FakeIdentifier",
                    None,
                    None,
                    classes,
                    generalizations,
                    relations,
                    None,
                )

        with self.subTest(
            """
            20. When `association_relations` is set but not typed
                `List[PlantUMLAssociationRelation]` -> TypeError
            """
        ):
            with self.assertRaisesRegex(TypeError, "association_relations"):
                PlantUMLDiagram(
                    "FakeIdentifier",
                    None,
                    None,
                    classes,
                    generalizations,
                    relations,
                    ["a", "b"],
                )

        association_relations: List[PlantUMLAssociationRelation] = []
        with self.subTest(
            """
            21. When `association_relations` is set and typed
                `List[PlantUMLAssociationRelation]` but has no items
            """
        ):
            PlantUMLDiagram(
                "FakeIdentifier",
                None,
                None,
                classes,
                generalizations,
                relations,
                association_relations,
            )

    def test_title_and_description(self):
        """
        Test the title_and_description property.
        1. No `name` and `description` provided -> empty list
        2. Only `name` provided -> ["note as diagram_title", "<b><i>{name}</i></b>",
           "end note"]
        3. Only `description` provided -> ["note as diagram_title", "{description}",
           "end note"]
        4. Only `description` provided, newline in `description` ->
           ["note as diagram_title", "{description} line 1", "{description} line 2",
           "end note"]
        5. Both `name` and `description` provided -> ["note as diagram_title",
           "<b><i>{name}</i></b>", "{description} line 1", "{description} line 2",
           "end note"]
        """

        with self.subTest("1. No `name` and `description` provided -> empty list"):
            expected = []
            self.assertEqual(
                PlantUMLDiagram(
                    "FakeId", None, None, [], [], [], []
                ).title_and_description,
                expected,
            )

        with self.subTest(
            """
            2. Only `name` provided -> ["note as diagram_title", "<b><i>{name}</i></b>",
               "end note"]
            """
        ):
            expected = ["note as diagram_title", "<b><i>FakeName</i></b>", "end note"]
            self.assertEqual(
                PlantUMLDiagram(
                    "FakeId", "FakeName", None, [], [], [], []
                ).title_and_description,
                expected,
            )

        with self.subTest(
            """
            3. Only `description` provided -> ["note as diagram_title", "{description}",
               "end note"]
            """
        ):
            expected = ["note as diagram_title", "FakeDescription", "end note"]
            self.assertEqual(
                PlantUMLDiagram(
                    "FakeId", None, "FakeDescription", [], [], [], []
                ).title_and_description,
                expected,
            )

        with self.subTest(
            """
            4. Only `description` provided, newline in `description` ->
               ["note as diagram_title", "{description} line 1", "{description} line 2",
               "end note"]
            """
        ):
            description = """Dit is een test.
Met een zeer mooie newline er in."""
            expected = [
                "note as diagram_title",
                "Dit is een test.",
                "Met een zeer mooie newline er in.",
                "end note",
            ]
            self.assertEqual(
                PlantUMLDiagram(
                    "FakeId", None, description, [], [], [], []
                ).title_and_description,
                expected,
            )

        with self.subTest(
            """
            5. Both `name` and `description` provided -> ["note as diagram_title",
               "<b><i>{name}</i></b>", "{description}", "end note"]
            """
        ):
            expected = [
                "note as diagram_title",
                "<b><i>FakeName</i></b>",
                "FakeDescription",
                "end note",
            ]
            self.assertEqual(
                PlantUMLDiagram(
                    "FakeId", "FakeName", "FakeDescription", [], [], [], []
                ).title_and_description,
                expected,
            )

    def test_plant_uml(self):
        """
        Test the plant_uml property.
        Check the following:
        1. Only `identifier` is provided -> [`@startuml`, `' {identifier}`,
           `skinparam linetype ortho`, `@enduml`]
        2. For each provided `PlantUMLClass`, `PlantUMLClass.plant_uml` is called one
           time
        3. For each provided `PlantUMLGeneralization`,
           `PlantUMLGeneralization.plant_uml` is called one time
        4. For each provided `PlantUMLRelation`, `PlantUMLRelation.plant_uml` is called
           one time
        5. For each provided `PlantUMLAssociationRelation`,
           `PlantUMLAssociationRelation.plant_uml` is called one time
        """

        with self.subTest(
            """
            1. Only `identifier` is provided -> [`@startuml`, `' {identifier}`,
               `skinparam linetype ortho`, `@enduml`]
            """
        ):
            expected = [
                "@startuml",
                "' FakeIdentifier",
                "@enduml",
            ]
            self.assertEqual(
                PlantUMLDiagram("FakeIdentifier", None, None, [], [], [], []).plant_uml,
                expected,
            )

        with self.subTest(
            """
            2. For each provided `PlantUMLClass`, `PlantUMLClass.plant_uml` is called
               one time
            """
        ):
            expected = [
                "@startuml",
                "' FakeIdentifier",
                "@enduml",
            ]
            with patch.object(
                PlantUMLClass, "plant_uml", new_callable=PropertyMock
            ) as testee:
                fake_classes: List[PlantUMLClass] = [
                    PlantUMLClass("FakeClassId_1", "FakeClassName_1", None, None),
                    PlantUMLClass("FakeClassId_2", "FakeClassName_2", None, None),
                    PlantUMLClass("FakeClassId_3", "FakeClassName_3", None, None),
                    PlantUMLClass("FakeClassId_4", "FakeClassName_4", None, None),
                ]
                PlantUMLDiagram(
                    "FakeIdentifier", None, None, fake_classes, [], [], []
                ).plant_uml
                self.assertEqual(testee.call_count, 4)

        with self.subTest(
            """
            3. For each provided `PlantUMLGeneralization`,
               `PlantUMLGeneralization.plant_uml` is called one time
            """
        ):
            expected = [
                "@startuml",
                "' FakeIdentifier",
                "@enduml",
            ]
            with patch.object(
                PlantUMLGeneralization, "plant_uml", new_callable=PropertyMock
            ) as testee:
                fake_generalizations: List[PlantUMLGeneralization] = [
                    PlantUMLGeneralization(
                        "FakeSpecificId_1", "GeneralSpecificId_1", None, None
                    ),
                    PlantUMLGeneralization(
                        "FakeSpecificId_2", "GeneralSpecificId_2", None, None
                    ),
                    PlantUMLGeneralization(
                        "FakeSpecificId_3", "GeneralSpecificId_3", None, None
                    ),
                    PlantUMLGeneralization(
                        "FakeSpecificId_4", "GeneralSpecificId_4", None, None
                    ),
                ]
                PlantUMLDiagram(
                    "FakeIdentifier", None, None, [], fake_generalizations, [], []
                ).plant_uml
                self.assertEqual(testee.call_count, 4)

        with self.subTest(
            """
            4. For each provided `PlantUMLRelation`, `PlantUMLRelation.plant_uml` is
               called one time
            """
        ):
            expected = [
                "@startuml",
                "' FakeIdentifier",
                "@enduml",
            ]
            with patch.object(
                PlantUMLRelation, "plant_uml", new_callable=PropertyMock
            ) as testee:
                fake_relations: List[PlantUMLRelation] = [
                    PlantUMLRelation(
                        PlantUMLRelationParticipant(
                            "FakeSourceId_1", None, "NONE", "Class"
                        ),
                        PlantUMLRelationParticipant(
                            "FakeTargetId_1", None, "NONE", "Class"
                        ),
                        None,
                        None,
                    ),
                    PlantUMLRelation(
                        PlantUMLRelationParticipant(
                            "FakeSourceId_2", None, "NONE", "Class"
                        ),
                        PlantUMLRelationParticipant(
                            "FakeTargetId_2", None, "NONE", "Class"
                        ),
                        None,
                        None,
                    ),
                    PlantUMLRelation(
                        PlantUMLRelationParticipant(
                            "FakeSourceId_3", None, "NONE", "Class"
                        ),
                        PlantUMLRelationParticipant(
                            "FakeTargetId_3", None, "NONE", "Class"
                        ),
                        None,
                        None,
                    ),
                    PlantUMLRelation(
                        PlantUMLRelationParticipant(
                            "FakeSourceId_4", None, "NONE", "Class"
                        ),
                        PlantUMLRelationParticipant(
                            "FakeTargetId_4", None, "NONE", "Class"
                        ),
                        None,
                        None,
                    ),
                ]
                PlantUMLDiagram(
                    "FakeIdentifier", None, None, [], [], fake_relations, []
                ).plant_uml
                self.assertEqual(testee.call_count, 4)

        with self.subTest(
            """
            5. For each provided `PlantUMLAssociationRelation`,
               `PlantUMLAssociationRelation.plant_uml` is called one time
            """
        ):
            expected = [
                "@startuml",
                "' FakeIdentifier",
                "@enduml",
            ]
            with patch.object(
                PlantUMLAssociationRelation, "plant_uml", new_callable=PropertyMock
            ) as testee:
                fake_association_relations: List[PlantUMLAssociationRelation] = [
                    PlantUMLAssociationRelation(
                        "FakeAssociationRelationId_1",
                        PlantUMLClass("FakeSourceId_1", "FakeSourceName_1"),
                        PlantUMLClass("FakeTargetId_1", "FakeTargetName_1"),
                        PlantUMLClass("FakeClassId_1", "FakeClassName_1", None, None),
                    ),
                    PlantUMLAssociationRelation(
                        "FakeAssociationRelationId_2",
                        PlantUMLClass("FakeSourceId_2", "FakeSourceName_2"),
                        PlantUMLClass("FakeTargetId_2", "FakeTargetName_2"),
                        PlantUMLClass("FakeClassId_2", "FakeClassName_2", None, None),
                    ),
                    PlantUMLAssociationRelation(
                        "FakeAssociationRelationId_3",
                        PlantUMLClass("FakeSourceId_3", "FakeSourceName_3"),
                        PlantUMLClass("FakeTargetId_3", "FakeTargetName_3"),
                        PlantUMLClass("FakeClassId_3", "FakeClassName_3", None, None),
                    ),
                    PlantUMLAssociationRelation(
                        "FakeAssociationRelationId_4",
                        PlantUMLClass("FakeSourceId_4", "FakeSourceName_4"),
                        PlantUMLClass("FakeTargetId_4", "FakeTargetName_4"),
                        PlantUMLClass("FakeClassId_4", "FakeClassName_4", None, None),
                    ),
                ]
                PlantUMLDiagram(
                    "FakeIdentifier", None, None, [], [], [], fake_association_relations
                ).plant_uml
                self.assertEqual(testee.call_count, 4)
