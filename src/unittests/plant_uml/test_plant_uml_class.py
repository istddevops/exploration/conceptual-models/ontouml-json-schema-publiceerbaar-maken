"""Test suites for plant_uml.plant_uml_class"""

from copy import deepcopy
from unittest import TestCase

from src.plant_uml.plant_uml_class import PlantUMLClass


class TestPlantUMLClass(TestCase):
    """Test suite for TestPlantUMLClass class"""

    def test_plant_uml(self):
        # pylint: disable=[C0301]
        """Test the plant_uml property. The returned string (rather, single string
        array) should be formatted thusly:
        class "{name}" as {identifier} <<{stereotype}>> [[link to thesaurus item]] #color

        | # | `stereotype` set  | `color` set | `thesaurus_link` set | expected syntax                                                             |
        | 1 | FakeStereoType    | FakeColor   | thesaurus/#123       | class "FakeName" as FakeId <<FakeStereoType>> [[thesaurus/#123]] #FakeColor |
        | 2 | not set           | FakeColor   | thesaurus/#123       | class "FakeName" as FakeId [[thesaurus/#123]] #FakeColor                    |
        | 3 | FakeStereoType    | not set     | thesaurus/#123       | class "FakeName" as FakeId <<FakeStereoType>> [[thesaurus/#123]]            |
        | 4 | not set           | not set     | thesaurus/#123       | class "FakeName" as FakeId [[thesaurus/#123]]                               |
        | 5 | FakeStereoType    | FakeColor   | not set              | class "FakeName" as FakeId <<FakeStereoType>> #FakeColor                    |
        | 6 | not set           | FakeColor   | not set              | class "FakeName" as FakeId #FakeColor                                       |
        | 7 | FakeStereoType    | not set     | not set              | class "FakeName" as FakeId <<FakeStereoType>>                               |
        | 8 | not set           | not set     | not set              | class "FakeName" as FakeId                                                  |
        """  # noqa=E501

        template = PlantUMLClass(identifier="FakeId", name="FakeName")
        with self.subTest(
            """
            | # | `stereotype` set  | `color` set | `thesaurus_link` set | expected syntax                                                             |
            | 1 | FakeStereoType    | FakeColor   | thesaurus/#123       | class "FakeName" as FakeId <<FakeStereoType>> [[thesaurus/#123]] #FakeColor |
            """  # noqa=E501
        ):
            subject = deepcopy(template)
            subject.stereotype = "FakeStereoType"
            subject.color = "FakeColor"
            subject.thesaurus_link = "thesaurus/#123"
            expected = (
                """class "FakeName" as FakeId <<FakeStereoType>> [[thesaurus/#123]] """
                + """#FakeColor"""
            )
            self.assertEqual(subject.plant_uml, [expected])

        with self.subTest(
            """
            | # | `stereotype` set  | `color` set | `thesaurus_link` set | expected syntax                                                             |
            | 2 | not set           | FakeColor   | thesaurus/#123       | class "FakeName" as FakeId [[thesaurus/#123]] #FakeColor                    |
            """  # noqa=E501
        ):
            subject = deepcopy(template)
            subject.color = "FakeColor"
            subject.thesaurus_link = "thesaurus/#123"
            expected = """class "FakeName" as FakeId [[thesaurus/#123]] #FakeColor"""
            self.assertEqual(subject.plant_uml, [expected])

        with self.subTest(
            """
            | # | `stereotype` set  | `color` set | `thesaurus_link` set | expected syntax                                                             |
            | 3 | FakeStereoType    | not set     | thesaurus/#123       | class "FakeName" as FakeId <<FakeStereoType>> [[thesaurus/#123]]            |
            """  # noqa=E501
        ):
            subject = deepcopy(template)
            subject.stereotype = "FakeStereoType"
            subject.thesaurus_link = "thesaurus/#123"
            expected = (
                """class "FakeName" as FakeId <<FakeStereoType>> [[thesaurus/#123]]"""
            )
            self.assertEqual(subject.plant_uml, [expected])

        with self.subTest(
            """
            | # | `stereotype` set  | `color` set | `thesaurus_link` set | expected syntax                                                             |
            | 4 | not set           | not set     | thesaurus/#123       | class "FakeName" as FakeId [[thesaurus/#123]]                               |
            """  # noqa=E501
        ):
            subject = deepcopy(template)
            subject.thesaurus_link = "thesaurus/#123"
            expected = """class "FakeName" as FakeId [[thesaurus/#123]]"""
            self.assertEqual(subject.plant_uml, [expected])

        with self.subTest(
            """
            | # | `stereotype` set  | `color` set | `thesaurus_link` set | expected syntax                                                             |
            | 5 | FakeStereoType    | FakeColor   | not set              | class "FakeName" as FakeId <<FakeStereoType>> #FakeColor                    |
            """  # noqa=E501
        ):
            subject = deepcopy(template)
            subject.stereotype = "FakeStereoType"
            subject.color = "FakeColor"
            expected = """class "FakeName" as FakeId <<FakeStereoType>> #FakeColor"""
            self.assertEqual(subject.plant_uml, [expected])

        with self.subTest(
            """
            | # | `stereotype` set  | `color` set | `thesaurus_link` set | expected syntax                                                             |
            | 6 | not set           | FakeColor   | not set              | class "FakeName" as FakeId #FakeColor                                       |
            """  # noqa=E501
        ):
            subject = deepcopy(template)
            subject.color = "FakeColor"
            expected = """class "FakeName" as FakeId #FakeColor"""
            self.assertEqual(subject.plant_uml, [expected])

        with self.subTest(
            """
            | # | `stereotype` set  | `color` set | `thesaurus_link` set | expected syntax                                                             |
            | 7 | FakeStereoType    | not set     | not set              | class "FakeName" as FakeId <<FakeStereoType>>                               |
            """  # noqa=E501
        ):
            subject = deepcopy(template)
            subject.stereotype = "FakeStereoType"
            expected = """class "FakeName" as FakeId <<FakeStereoType>>"""
            self.assertEqual(subject.plant_uml, [expected])

        with self.subTest(
            """
            | # | `stereotype` set  | `color` set | `thesaurus_link` set | expected syntax                                                             |
            | 8 | not set           | not set     | not set              | class "FakeName" as FakeId                                                  |
            """  # noqa=E501
        ):
            subject = deepcopy(template)
            expected = """class "FakeName" as FakeId"""
            self.assertEqual(subject.plant_uml, [expected])

    def test_plant_uml_identifier_does_not_contain_period(self):
        """Test that the plant_uml property uses the identifier property, so the
        rendered class alias does not contain periods (`.`)"""
        testee = PlantUMLClass("Fake.Identifier", "FakeName")
        expected = "Fake___Identifier"
        self.assertEqual(testee.identifier, expected)

    def test_identifier(self):
        """Test the identifier property. This property replaces any periods (`.`) with
        three underscores (`___`).
        1. When constructed without period, return same value.
        2. When constructed with one period, return value where period is replaced by
           three underscores
        3. When constructed with multiple periods, return value where each period is
           replaced by three underscores
        """

        with self.subTest("""1. When constructed without period, return same value."""):
            expected = "FakeIdentifier"
            self.assertEqual(
                PlantUMLClass("FakeIdentifier", "FakeName").identifier,
                expected,
            )

        with self.subTest(
            """
            2. When constructed with one period, return value where period is replaced
               by three underscores
            """
        ):
            expected = "Fake___Identifier"
            self.assertEqual(
                PlantUMLClass("Fake.Identifier", "FakeName").identifier,
                expected,
            )

        with self.subTest(
            """
            3. When constructed with multiple periods, return value where each period is
               replaced by three underscores
            """
        ):
            expected = "Fake___Iden___tifier"
            self.assertEqual(
                PlantUMLClass("Fake.Iden.tifier", "FakeName").identifier,
                expected,
            )
