"""Test suites for plant_uml.plant_uml_generalization"""


from unittest import TestCase

from src.plant_uml.plant_uml_generalization import PlantUMLGeneralization


class TestPlantUMLGeneralization(TestCase):
    """Test suite for PlantUMLGeneralization"""

    def test_init(self):
        """
        Test the PlantUMLGeneralization constructor
        1. If `specific_identifier` not set -> ValueError
        2. If `specific_identifier` does not have type `str` -> TypeError
        3. If `general_identifier` not set -> ValueError
        4. If `general_identifier` does not have type `str` -> TypeError
        5. If `name` is set but does not have type `str` -> TypeError
        6. If `name` is set and has type `str` -> no error
        7. If `description` is set but does not have type `str` -> TypeError
        8. If `description` is set and has type `str` -> no error
        """

        with self.subTest("1. If `specific_identifier` not set -> ValueError"):
            with self.assertRaisesRegex(ValueError, "specific_identifier"):
                PlantUMLGeneralization(None, None, None, None)

        with self.subTest(
            "2. If `specific_identifier` does not have type `str` -> TypeError"
        ):
            with self.assertRaisesRegex(TypeError, "specific_identifier"):
                PlantUMLGeneralization(1, None, None, None)

        with self.subTest("3. If `general_identifier` not set -> ValueError"):
            with self.assertRaisesRegex(ValueError, "general_identifier"):
                PlantUMLGeneralization("FakeSpecificId", None, None, None)

        with self.subTest(
            "4. If `general_identifier` does not have type `str` -> TypeError"
        ):
            with self.assertRaisesRegex(TypeError, "general_identifier"):
                PlantUMLGeneralization("FakeSpecificId", 1, None, None)

        with self.subTest(
            "5. If `name` is set but does not have type `str` -> TypeError"
        ):
            with self.assertRaisesRegex(TypeError, "name"):
                PlantUMLGeneralization("FakeSpecificId", "FakeGeneralId", 1, None)

        with self.subTest("6. If `name` is set and has type `str` -> no error"):
            PlantUMLGeneralization("FakeSpecificId", "FakeGeneralId", "FakeName", None)

        with self.subTest(
            "7. If `description` is set but does not have type `str` -> TypeError"
        ):
            with self.assertRaisesRegex(TypeError, "description"):
                PlantUMLGeneralization("FakeSpecificId", "FakeGeneralId", None, 1)

        with self.subTest("8. If `description` is set and has type `str` -> no error"):
            PlantUMLGeneralization(
                "FakeSpecificId", "FakeGeneralId", None, "FakeDescription"
            )

    def test_specific_identifier(self):
        """
        Test the `specific_identifier` property. If any periods (`.`) have been set in
        `PlantUMLGeneralization.__init__` `specific_identifier` argument, these need
        to be replaced by three underscores (`___`).
        1. A `.` has been specified for the `specific_identifier` argument of
           `__init__` -> replaced by `___`
        2. Multiple `.` have been specified for the `specific_identifier` argument of
           `__init__` -> each replaced by `___`
        3. No `.` have been specified for the `specific_identifier` argument of
           `__init__ -> `specific_identifier` is returned as-is.
        """
        with self.subTest(
            """
            1. A `.` has been specified for the `specific_identifier` argument of
               `__init__` -> replaced by `___`
            """
        ):
            expected = "Fake___SpecificId"
            self.assertEqual(
                PlantUMLGeneralization(
                    "Fake.SpecificId", "FakeGeneralId", None, None
                ).specific_identifier,
                expected,
            )

        with self.subTest(
            """
            2. Multiple `.` have been specified for the `specific_identifier` argument
               of `__init__` -> each replaced by `___`
            """
        ):
            expected = "Fake___Specific___Id"
            self.assertEqual(
                PlantUMLGeneralization(
                    "Fake.Specific.Id", "FakeGeneralId", None, None
                ).specific_identifier,
                expected,
            )

        with self.subTest(
            """
            3. No `.` have been specified for the `specific_identifier` argument of
               `__init__ -> `specific_identifier` is returned as-is.
            """
        ):
            expected = "FakeSpecificId"
            self.assertEqual(
                PlantUMLGeneralization(
                    "FakeSpecificId", "FakeGeneralId", None, None
                ).specific_identifier,
                expected,
            )

    def test_general_identifier(self):
        """
        Test the `general_identifier` property. If any periods (`.`) have been set in
        `PlantUMLGeneralization.__init__` `general_identifier` argument, these need
        to be replaced by three underscores (`___`).
        1. A `.` has been specified for the `general_identifier` argument of
           `__init__` -> replaced by `___`
        2. Multiple `.` have been specified for the `general_identifier` argument of
           `__init__` -> each replaced by `___`
        3. No `.` have been specified for the `general_identifier` argument of
           `__init__ -> `general_identifier` is returned as-is.
        """
        with self.subTest(
            """
            1. A `.` has been specified for the `general_identifier` argument of
               `__init__` -> replaced by `___`
            """
        ):
            expected = "Fake___GeneralId"
            self.assertEqual(
                PlantUMLGeneralization(
                    "Fake.SpecificId", "Fake.GeneralId", None, None
                ).general_identifier,
                expected,
            )

        with self.subTest(
            """
            2. Multiple `.` have been specified for the `general_identifier` argument of
               `__init__` -> each replaced by `___`
            """
        ):
            expected = "Fake___General___Id"
            self.assertEqual(
                PlantUMLGeneralization(
                    "Fake.Specific.Id", "Fake.General.Id", None, None
                ).general_identifier,
                expected,
            )

        with self.subTest(
            """
            2. No `.` have been specified for the `general_identifier` argument of
               `__init__ -> `general_identifier` is returned as-is.
            """
        ):
            expected = "FakeGeneralId"
            self.assertEqual(
                PlantUMLGeneralization(
                    "Fake.Specific.Id", "FakeGeneralId", None, None
                ).general_identifier,
                expected,
            )

    def test_plant_uml(self):
        """
        Test the `plant_uml` property.
        1. No `name` and `description` set -> `{general} <|-- {specific}`
        2. `name` set, `description` not set -> `{general} <|-- {specific} : {name}`
        3. `name` not set, `description` set ->
           `{general} <|-- {specific} : {description}`
        4. `name` set, `description` set ->
           `{general} <|--{specific} : {name}<br />{description}`
        """

        with self.subTest(
            """
            1. No `name` and `description` set -> `{general} <|-- {specific}`
            """
        ):
            expected = ["GeneralId <|-- SpecificId"]
            self.assertEqual(
                PlantUMLGeneralization("SpecificId", "GeneralId", None, None).plant_uml,
                expected,
            )

        with self.subTest(
            """
            2. `name` set, `description` not set -> `{general} <|-- {specific} : {name}`
            """
        ):
            expected = ["GeneralId <|-- SpecificId : FakeName"]
            self.assertEqual(
                PlantUMLGeneralization(
                    "SpecificId", "GeneralId", "FakeName", None
                ).plant_uml,
                expected,
            )

        with self.subTest(
            """
            3. `name` not set, `description` set ->
               `{general} <|-- {specific} : {description}`
            """
        ):
            expected = ["GeneralId <|-- SpecificId : FakeDescription"]
            self.assertEqual(
                PlantUMLGeneralization(
                    "SpecificId", "GeneralId", None, "FakeDescription"
                ).plant_uml,
                expected,
            )

        with self.subTest(
            """
            4. `name` set, `description` set ->
               `{general} <|--{specific} : {name}<br />{description}`
            """
        ):
            expected = ["GeneralId <|-- SpecificId : FakeName<br />FakeDescription"]
            self.assertEqual(
                PlantUMLGeneralization(
                    "SpecificId", "GeneralId", "FakeName", "FakeDescription"
                ).plant_uml,
                expected,
            )
