---
title: Informatiemodel Aandoeningsregistratie
type: docs
BookToC: false
---

# Informatiemodel Aandoeningsregistratie  
{:.no_toc}

| Document | |
| --- | --- |
| Project | Regie op registers dure geneesmiddelen |
| Datum | 7 februari 2023 |
| Status | Concept |
| Versie | 0.10 |

## Colofon  
{:.no_toc}

| Colofon  | |
| --- | --- |
| Volgnummer | 2023002570 |
| Contactpersoon | Dhr. R.J. Morra (Reinier) <br> +31 (0)653 960510 <br> [rmorra@zinl.nl](mailto:rmorra@zinl.nl) |
| Auteur(s)	| Mw. D.T. Hochheimer (Davinia), Mw. E. Kampert (Elly), Dhr. R.J. Morra (Reinier), Mw. Q.R. Passchier (Quiri) |
| Afdeling | Fondsen & Informatiemanagement |
| Team | Projecten en Programma's Informatiemanagement |

## Inhoud  
{:.no_toc}

1. TOC
{:toc}

## Samenvatting  

> <streven 1 pagina, schrijven we op het laatst, plus groot de overzichtsplaat aandoeningsregistratie>  

## Waarover willen we met u het gesprek aangaan?  

In het Nederlandse zorgveld is sprake van een levendige dialoog over de wens voor meer samenhang in het secundair datagebruik. Met dit document wil het Zorginstituut de gedachtevorming stimuleren en ondersteunen. Het Zorginstituut gaat graag het gesprek aan met alle betrokken partijen en nodigt u uit te reageren op dit document. De contactgegevens vindt u in het colofon op pagina 1.  

Met dit document willen we ook een bijdrage leveren aan landelijke verkenningen van secundair datagebruik die momenteel [plaatsvinden in het zorgveld][voetnoot-1]. Wij geloven dat het raamwerk wenselijk en realistisch is en dat de tijd nu rijp is om gezamenlijk door te pakken. Alleen samen kunnen we de informatie die in het primaire proces al aanwezig is voor ons allemaal laten werken.  

**We zijn benieuwd naar uw perspectief!**  
- Hoe kijkt u als bestuurder aan tegen het raamwerk en hoe denkt u over het maken van afspraken binnen een aparte _governance_ voor secundair datagebruik?  
- Hoe kijkt u als beleidsmedewerker tegen het raamwerk als geheel aan?  
- Hoe kijkt u als architect aan tegen de uitwerkingen op inhoudelijk niveau waaronder datagedreven werken vanuit een kennismodel?  
- Hoe kijkt u als zorgverlener of IT-verantwoordelijke in de zorg tegen het raamwerk aan en wat is uw mening over gestructureerde registratie en data beschikbaar stellen met uniforme betekenis op basis van een kennismodel?  
- Hoe kijkt u persoonlijk als burger tegen het raamwerk aan? Op welke wijze maakt het voor u de zorg beter?  

Gedeelde architectuur?
Requirements analyse?  

## Inleiding  

> < Reinier Inleiding hoofdstuk: In dit hoofdstuk lichten we de uitdagingen in de zorg en het belang van secundair datagebruik / data & ICT kant regie op registers toe (definitie). Daarnaast beschrijven we het waarom en de urgentie ervan. Ook gaan we in op de rol van de overheid.>  

Boodschappen:  
- Dit document is de uitwerking van de dataset ror in een conceptueel model als doel om de samenhang inzichtelijk te maken.  
- Basis voor de discussie welke vragen met welk doel willen de stakeholders op de data stellen?  
  Registraties verzamelt data niet om het data verzamelen.  
  - Behoefte Gebruik / doelen van de dataset – stakeholders  

[Quiri][]  

Links: Allerlei vragende partijen met verschillende doelen behoeften  
Midden: 1 kennismodel waar data uit systemen op af te beelden is en waar alle partijen hun vragen op kunnen stellen  
Rechts: allerlei software systemen beelden data af op kennismodel en daardoor databeschikbaarheid met uniforme betekenis  
_Invoegen plaatje_  

### Programma Regie op registers Dure geneesmiddelen  

Reinier  

#### Dataset  

Reinier

### Waarom conceptueel modelleren?  

<Reinier aanzet, Quiri aanscherpen: Hier een stukje over achtergrond conceptueel modelleren  
Mens leesbaar model hoe begrippen met elkaar samenhangen als basis voor de te stellen vragen.>

### Hoe lees ik een conceptueel model?  

<Quiri: Hier een stukje over hoe een conceptueel model te lezen plus extra figuur legenda (stippel en streeplijn)>

{{< plantuml id="leeswijzerP" name="LeeswijzerP" file="/data/plantUML/LeeswijzerP.plantuml" />}}

Extra toegevoegd (buiten OntoUML):  
Gestreepte lijn : een concept dat zijn eigen detail uitwerking kent  
Stippellijn : concept ook op een andere plek gebruikt  

Uitgangspunten naamgeving  
- Concepten sluiten aan op de praktijk  
- Concepten kennen een eenduidige definitie  
  (die in deze set van modellen uniform worden gebruikt)  
- Als concepten vergelijkbaar zijn, echter toch inhoudelijk verschillen, dan moet de naamgeving dit duidelijk maken en de onderlinge relatie helder (gevisualiseerd) zijn  

Proces: gestart vanuit modellering vanuit de specifieke context en vervolgens kijken naar de samenhang / abstrahering met de andere modellen.  

### Van waaruit zijn we gestart?  

< Reinier (en Florien)  
Hier een stukje over data elementen / dataset RoR DG.>  
<Pilot, kik-v, cliënt informatie, bredere context>  

<uitwisselprofielen, matchingsproces Florien>  
<raamwerk, oude advies secundair data gebruik geboortezorg (matchingsproces)>  

Van Quiri (Kirí) Passchier aan iedereen:  12:23 PM
Context:

Individuele context burger
- Patiëntenleven (genen, geboorte, SES 
(wrr rapport), leefstijl, preventie, 0..* aandoeningen, 0..* totale behandelingen, kwaliteit van leven, overlijden, woonlocatie)

Zorgprofessional-patient context
- Behandel (plan) opstellen
- Uitvoering

Zorgaanbieder context
- Procesmaten (zie zorginzicht)
- Declareren

Meta context, populatie

systeemcontext
- Behandel effectiviteit (case studies)
- Kosten effectiviteit
- Pakketbeheer: Signaleren
- Toegankelijkheid

Onderzoekscontext
Plaatsbepaling
Niewe behandelingen

## Overzicht aandoeningsregistratie  

[Davinia] Vanuit de dataset van een aandoeningsregistratie hebben we een conceptueel model opgesteld zoals weergegeven in Figuur 1 - Overzicht aandoeningsregistratie. Dit is het overzichtsmodel, in de hoofdstukken hierna is het model gedetailleerd. 

{{< plantuml id="00-overzicht-aandoeningsregistratie" name="00. Overzicht aandoeningsregistratie" file="/data/plantUML/00. Aandoening Overview.plantuml" />}}

De persoon en diens aandoening (iets rechts uit het midden) staat centraal. Door mensen te volgen over het beloop van hun aandoening kan inzichtelijk worden hoe verschillende behandelkeuzes het leven met een bepaalde diagnose beinvloeden. Op basis van de behandelrichtlijnen en in de toekomst meer en meer <…> samen beslissen zal een, op de individuele patiënt toegespitst, behandelplan door de arts worden vastgelegd.  

Aan de linkerkant zijn de verschillende behandelopties en onderdelen terug te vinden die in loop van de gehele behandeling achtereenvolgens of parallel kunnen zijn ingezet. Daaronder bevindt zich een selectie van verrichtingen en middelen die hierbij van toepassing zijn voor de patiënt.  

Als voor behandeling is gekozen heeft dit diverse effecten hebben op de behandelde individu. Ook als patiënt en arts samen beslissen om van behandelen wordt afgezien, is het van belang om deze persoon te kunnen blijven volgen. Ook een natuurlijk beloop van een aandoening zal immers een effect hebben op een persoon die in uitkomstmetingen kan worden waargenomen. De gegevens van deze groep patiënten is een belangrijke bron van informatie ter controle.  

Aan de rechterkant zijn de uitkomstmaten weergegeven die nodig zijn om de effectiviteit van de behandeling (tot dan toe) te kunnen evalueren.  
Rechtsboven is te zien dat, om de toegepaste zorg te beoordelen op gepast gebruik, het eveneens noodzakelijk is om enerzijds de hoeveelheid geconsumeerde zorg en anderzijds de behaalde procesmaten in het organiseren hier van in beeld te brengen.  

In de volgende paragrafen zijn de volgende onderdelen verder uitgewerkt:  
….<de onderdelen benoemen><samen beslissen, Martijn en Domino>  

Een van de vragen rondom dure geneesmiddelen is de cumultatieve dosis. Om deze vraag goed te kunnen beantwoorden hebben we de verschillende varianten van een cumulatieve dosis uitgewerkt. <kosteneffectiviteit, Martijn>  

Daarnaast nemen wij als Zorginstituut pakketbesluiten. Om te verkennen hoe we deze pakketbesluiten kunnen monitoren, is het pakketbesluit uitgewerkt in een conceptueel model. Door de begripsvorming hieromtrent te koppelen aan de aandoeningsregistratie, is inzicht te krijgen hoe geneesmiddelen in de praktijk te monitoren in relatie tot het pakketbesluit. <Florien>
<Uitkomstgerichte zorg, Martijn>  

## Onderdelen aandoeningsregistratie  

### Behandeltraject  

### Samen beslissen  

### Zorgconsumptie  

### Kosteneffectiviteit  

### Cumulatieve dosis  

### Medicatie  

### Uitkomsten  

### Patient Reported Outcome Measures  

### Procesmaten  

## Indicatie pakket adviezen  

<!-- Identifiers for footnotes -->
[voetnoot-1]: javascript:void "Onder andere de zorgbrede implementatie-aanpak ‘eenmalig registreren en meervoudig gebruiken’ (VWS BIZ), de nationale visie en strategie het duurzaam informatiestelsel in de zorg (Nictiz en BIZ) en de routekaart voor secundair datagebruik (VWS DI)."
