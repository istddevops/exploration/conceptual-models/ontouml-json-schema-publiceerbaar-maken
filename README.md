# OntoUML json schema to plantUML converter

## Inleiding

Binnen Zorginstituut Nederland worden zogenaamde Conceptuele Modellen ontwikkeld waarbij gewerkt wordt met de OntoUML modelleer-methode. Deze methode heeft als voordeel dat het je dwingt om echt na te denken over concepten en niet over data (dat zit een abstractielaag dieper). Zie de [OntoUML specification][].  

Het Zorginstituut is bezig om publicaties niet meer op basis van gesloten technieken te realiseren (denk aan Word documenten, met hierin jpg plaatjes van modellen), maar met open technieken, waarbij alle input gezien wordt als te transformeren data.  
Hierin passen ook de Conceptuele Modellen; deze zijn feitelijk ook data, welke we ook middels open technieken publiceerbaar willen maken.

## Uitgangspunten / keuzes

### OntoUML JSON Schema

Er is vanuit het [OntoUML Server project][] een JSON Schema ontwikkeld waarmee OntoUML modellen uitgewisseld kunnen worden: het [OntoUML JSON Schema][].  
De tool die het Zorginstituut gebruikt om Conceptuele Modellen in te modelleren, Visual Paradigm met de OntoUML plugin, ondersteunt het exporteren van een OntoUML model middels dit schema.  

### GitLab als publicatieplatform

Vanuit het project DgPi is gekozen voor GitLab als platform waarmee we het datagedreven publiceren mogelijk maken. GitLab biedt de mogelijkheid om opgemaakte tekst te publiceren middels markdown, waarbij GitLab wat eigen keuzes heeft gemaakt. Zie [GitLab Flavored Markdown (GLFM)][].  
Een van de keuzes is, om out-of-the-box ondersteuning te bieden voor diagrammen en flowcharts middels [Mermaid][], [PlantUML][] en [Kroki][].  

### PlantUML als tool om het conceptueel (UML) diagram weer te geven  

Voor deze proof of concept is ervoor gekozen om het OntoUML model weer te geven middels [PlantUML][]; dit omdat PlantUML mogelijkheden biedt om vanuit objecten te referen naar andere pagina's.  

> Dit maakt het mogelijk om vanuit het weergegeven model een link op te nemen naar bijv. een thesaurus waarin het betreffende concept verder beschreven wordt.  

### Python als taal waarmee OntoUML JSON Schema compliant file wordt omgezet naar metamodel  

Deze proof of concept is ontwikkeld in Python. Hier is geen rationale voor. De code is *niet* testdriven opgezet, waardoor deze dan ook zeker niet als productiewaardig gezien moet worden.  

> Deze exercitie heeft als doel gehad om een werkend metamodel te maken, waarmee de vertaalslag gemaakt kan worden van [OntoUML JSON Schema][] naar een van de door GitLab ondersteunde diagram scripttalen ([Mermaid][], [PlantUML][] en [Kroki][]).  

### Engels als taal voor attributen  

Deze proof of concept gebruikt Engels als voertaal voor attributen en objecten, omdat we hiermee aansluiting kunnen houden bij zowel het [OntoUML JSON Schema][] alsook de (Engelstalige) scripts voor zowel [Mermaid][], [PlantUML][] en [Kroki][].

## Van [OntoUML JSON Schema][] naar [PlantUML][]

### Ontwerpkeuzes

#### De indeling in packages wordt genegeerd.  

In deze proof of concept worden geen packages weergegeven in een schema, ook worden deze niet gebruikt voor bijvoorbeeld een mappenstructuur.  

> Rationale: het zinvol gebruiken van een mappenstructuur, vereist ook dat we weten op welk platform de schema's uiteindelijk getoond worden, en welke engine hiervoor gebruikt zal worden. Dat weten we nu nog niet, dus vooralsnog is dit genegeerd.  

#### Volgordelijkheid bij het genereren van [PlantUML][] script voor een [Diagram](#diagram)  

De verschillende objecttypes worden in een vaste volgorde verwerkt. Dit omdat je zo maximale controle hebt over hoe het uiteindelijk in [PlantUML][] weergegeven schema er uit ziet.  

   1. Alle Classes naar [PlantUML][]  
   1. Alle Generalizations naar [PlantUML][]  
   1. Alle Relations naar [PlantUML][]  

### Visual Paradigm OntoUML plugin extentie op [OntoUML JSON Schema][]

Naast de definitie zoals beschreven in [OntoUML JSON Schema][], exporteert de OntoUML plugin voor Visual Paradigm nog wat extra types, welke gebruikt worden om objecten aan een plaat te koppelen. Deze worden in de volgende paragrafen beschreven.  

#### ClassView  

* id  
  Technische identificatie van de ClassView  
* modelElement  
  Het element wat door deze ClassView weergegeven wordt  

#### RelationView  

* id  
  Technische identificatie van de RelationView  
* modelElement  
  Het element wat door deze RelationView weergegeven wordt  
* source  
  Het bron object waar vanuit de relatie, welke wordt weergegeven in deze RelationView, start  
* target  
  Het doel object waar naartoe de relatie, welke wordt weergegeven in deze RelationView, gaat  

#### GeneralizationView  

* id  
  Technische identificatie van de GeneralizationView  
* modelElement  
  Het element wat door deze GeneralizationView weergegeven wordt  
* source  
  Het generieke element van deze generalisatie  
* target
  Het specifieke element van deze generalisatie  

#### GeneralizationSetView  

* id  
  Technische identificatie van de GeneralizationSetView  
* type  
  Het type van de GeneralizationSetView  
* modelElement  
  Het element wat door deze GeneralizationSetView weergegeven wordt  

#### Diagram  
* id  
  Technische identificatie van het Diagram  
* name  
  De naam van het Diagram  
* description  
  De beschrijving van het Diagram  
* owner  
  Het object waaronder dit Diagram valt  
* contents  
  De inhoud van het Diagram.  
  Dit kunnen 0 of meer objecten zijn van de volgende typen:  
  * [ClassView](#classview)  
  * [RelationView](#relationview)  
  * [GeneralizationView](#generalizationview)  
  * [GeneralizationSetView](#generalizationsetview)  

[OntoUML Specification]: https://ontouml.readthedocs.io/en/latest/
[OntoUML Server project]: https://github.com/OntoUML/ontouml-server
[OntoUML JSON Schema]: https://github.com/OntoUML/ontouml-schema
[GitLab Flavored Markdown (GLFM)]: https://docs.gitlab.com/ee/user/markdown.html
[Mermaid]: https://mermaid.js.org/
[PlantUML]: https://plantuml.com
[Kroki]: https://docs.kroki.io/kroki/

```plantuml
Bob -> Alice: Hi
Alice -> Bob: Hi there!
```