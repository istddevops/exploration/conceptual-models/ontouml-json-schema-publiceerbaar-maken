"""
Contains hooks for behave tests
"""


from pathlib import Path


def after_step(context, step):
    """
    Hook which fires after a step has been executed
    """
    if "failed" == step.status:
        if hasattr(context, "browser"):
            folder = (
                f"./testresults/{context.feature.name}/{context.scenario.name}/"
            ).replace('"', "")
            worker = Path(folder)
            if not worker.exists():
                worker.mkdir(parents=True, exist_ok=True)
            filename = (f"{folder}{step.name}.png").replace('"', "")
            context.browser.save_screenshot(filename)
