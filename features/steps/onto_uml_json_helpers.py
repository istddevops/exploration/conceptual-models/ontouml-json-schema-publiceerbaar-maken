"""
Module met helper methods om json output te genereren voor diverse UML objecten; dit
conform het OntoUML Json schema.
"""


from typing import Tuple
from uuid import uuid4


def maak_class_dict(naam, stereotype=None) -> dict:
    """
    Maak een `dict` aan die een uml class representeert
    :param naam: De naam van de class
    :param stereotype: het optionele stereotype van de class
    :returns: een `dict` waarmee een class wordt uitgedrukt.
    """
    return {
        "id": str(uuid4()).replace("-", ""),
        "name": naam,
        "description": None,
        "type": "Class",
        "propertyAssignments": None,
        "stereotype": stereotype,
        "isAbstract": False,
        "isDerived": False,
        "properties": None,
        "isExtensional": False,
        "isPowertype": None,
        "order": None,
        "literals": None,
        "restrictedTo": None,
    }


def maak_class_en_classview(
    context, naam: str, stereotype: str = None
) -> Tuple[dict, dict]:
    """
    Maakt de class en weergave van dezelfde class op een diagram aan en voegt deze toe
    aan de test `context`.
    :param context: De test context
    :param naam: De naam van de class
    :param stereotype: het optionele stereotype van de class
    :returns: een `Tuple` met hierin de `dict` die de class representeert en de `dict`
              die de weergave van dezelfde class representeert.
    """
    uml_class = maak_class_dict(naam, stereotype)
    uml_classview = maak_classview_dict(uml_class)
    voeg_object_toe_aan_model(context, uml_class)
    voeg_objectview_toe_aan_diagram(context, uml_classview)
    return uml_class, uml_classview


def maak_classview_dict(class_dict: dict) -> dict:
    """
    Maak een `dict` aan die een class weergave op een diagram representeert
    :param class_dict: een `dict` zoals aangemaakt door `_maak_class_dict`
    :returns: een `dict` waarmee een class zoals die op een view wordt weergegeven
              wordt uitgedrukt.
    """
    return {
        "id": str(uuid4()).replace("-", ""),
        "type": "ClassView",
        "modelElement": {"id": class_dict["id"], "type": "Class"},
        "shape": {
            "id": str(uuid4()).replace("-", ""),
            "type": "Rectangle",
            "x": 0,
            "y": 0,
            "width": 133,
            "height": 40,
        },
    }


def maak_generalization_dict(general: dict, specific: dict, naam: str = None) -> dict:
    """
    Maakt een `dict` aan welke een generalization representeert.
    :param general: een `dict` waarin `id` het id is van de general class, `type` is
                    "Class"
    :param specific: een `dict` waarin `id` het id is van de specific class, `type` is
                     "Class"
    :param naam: de optionele naam van de relatie
    :param stereotype: het optionele stereotype van de relatie
    :returns: een `dict` waarmee een generalization tussen twee objecten wordt
              uitgedrukt.
    """
    return {
        "id": str(uuid4()).replace("-", ""),
        "name": naam,
        "description": None,
        "type": "Generalization",
        "propertyAssignments": None,
        "general": {"id": general["id"], "type": "Class"},
        "specific": {"id": specific["id"], "type": "Class"},
    }


def maak_generalization_en_generalizationview(
    context, general: Tuple[dict, dict], specific: Tuple[dict, dict], naam: str = None
) -> Tuple[dict, dict]:
    """
    Maakt de class en weergave van dezelfde class op een diagram aan en voegt deze toe
    aan de test `context`.
    :param context: De test `context`
    :param general: `Tuple` met hierin de `dict` die de `general` van de generalization
                    representeert en de `dict` die de weergave van de `general` op een
                    diagram representeert.
    :param specific: `Tuple` met hierin de `dict` die de `specific` van de
                     generalization representeert en de `dict` die de weergave van de
                     `specific` op een diagram representeert.
    :param naam: De naam van de generalization
    :returns: een `Tuple` met hierin de `dict` die de generalization representeert en
              de `dict` die de weergave van dezelfde generalization representeert.
    """
    general_class, general_classview = general
    specific_class, specific_classview = specific
    generalization = maak_generalization_dict(general_class, specific_class, naam)
    generalization_view = maak_generalizationview_dict(
        generalization, general_classview, specific_classview
    )
    voeg_object_toe_aan_model(context, generalization)
    voeg_objectview_toe_aan_diagram(context, generalization_view)

    return generalization, generalization_view


def maak_generalizationview_dict(
    generalization: dict, general_view: dict, specific_view: dict
) -> dict:
    """
    Maakt een `dict` aan welke een weergave van de `generalization` op een diagram
    representeert.
    :param generalization: een `dict` waarin `id` het id is van de generalization,
                           `type` is "Generalization".
    :param general: een `dict` waarin `id` het id is van de weergave van de generieke
                    class, `type` is "ClassView".
    :param specific: een `dict` waarin `id` het id is van de weergave van de specifieke
                     class, `type` is "ClassView".
    """
    return {
        "id": str(uuid4()).replace("-", ""),
        "type": "GeneralizationView",
        "modelElement": {"id": generalization["id"], "type": generalization["type"]},
        "source": {"id": general_view["id"], "type": general_view["type"]},
        "target": {"id": specific_view["id"], "type": specific_view["type"]},
        "shape": {
            "id": str(uuid4()).replace("-", ""),
            "type": "Path",
            "points": [{"x": 0, "y": 0}, {"x": 0, "y": 0}],
        },
    }


def maak_property_dict(
    related: dict, aggregation_kind: str = None, cardinality: str = None
) -> dict:
    """
    Maak een `dict` aan die een uml property, welke van toepassing is op `related`,
    representeert.
    :param related: een `dict` waarin `id` het id is van het source object, `type`
                   het type van het source object. Optioneel kan ook nog
                   `aggregation_kind` en / of `cardinality` gezet zijn.
    :param aggregation_kind: het soort aggregatie: `NONE`, `SHARED` of `COMPOSITE`
    :param cardinality: de kardinaliteit voor het object waarop de UML property van
                        toepassing is.
    :returns: een `dict` waarmee een UML property wordt uitgedrukt
    """
    return {
        "id": str(uuid4()).replace("-", ""),
        "name": "",
        "description": None,
        "type": "Property",
        "propertyAssignments": None,
        "stereotype": None,
        "isDerived": False,
        "isReadOnly": False,
        "isOrdered": False,
        "cardinality": cardinality,
        "propertyType": {"id": related["id"], "type": related["type"]},
        "subsettedProperties": None,
        "redefinedProperties": None,
        "aggregationKind": aggregation_kind,
    }


def maak_relation_dict(
    source: dict, target: dict, naam: str = None, stereotype: str = None
) -> dict:
    """
    Maakt een `dict` aan welke een relatie tussen twee uml objecten representeert.
    :param source: een `dict` waarin `id` het id is van het source object, `type`
                   het type van het source object. Optioneel kan ook nog
                   `aggregation_kind` en / of `cardinality` gezet zijn.
    :param target: een `dict` waarin `id` het id is van het target object, `type`
                   het type van het source object. Optioneel kan ook nog
                   `aggregation_kind` en / of `cardinality` gezet zijn.
    :param naam: de optionele naam van de relatie
    :param stereotype: het optionele stereotype van de relatie
    :returns: een `dict` waarmee een relatie tussen twee objecten wordt
              uitgedrukt.
    """
    return {
        "id": str(uuid4()).replace("-", ""),
        "name": naam,
        "description": None,
        "type": "Relation",
        "propertyAssignments": None,
        "stereotype": stereotype,
        "isAbstract": False,
        "isDerived": False,
        "properties": [
            maak_property_dict(
                source, source.get("aggregation_kind"), source.get("cardinality")
            ),
            maak_property_dict(
                target, target.get("aggregation_kind"), target.get("cardinality")
            ),
        ],
    }


def maak_relation_en_relationview(
    context,
    source: Tuple[dict, dict],
    target: Tuple[dict, dict],
    naam: str = None,
    stereotype: str = None,
) -> Tuple[dict, dict]:
    """
    Maakt de class en weergave van dezelfde class op een diagram aan en voegt deze toe
    aan de test `context`.
    :param context: De test context
    :param source: `Tuple` met hierin de `dict` die de `source` van de relatie
                   representeert en de `dict` die de weergave van de `source` op een
                   diagram representeert.
    :param target: `Tuple` met hierin de `dict` die de `target` van de relatie
                   representeert en de `dict` die de weergave van de `target` op een
                   diagram representeert.
    :param naam: De naam van de relatie
    :param stereotype: het optionele stereotype van de relatie
    :returns: een `Tuple` met hierin de `dict` die de relatie representeert en de `dict`
              die de weergave van dezelfde relatie representeert.
    """
    source_object, source_objectview = source
    target_object, target_objectview = target
    uml_relation = maak_relation_dict(source_object, target_object, naam, stereotype)
    uml_relation_view = maak_relationview_dict(
        uml_relation, source_objectview, target_objectview
    )
    voeg_object_toe_aan_model(context, uml_relation)
    voeg_objectview_toe_aan_diagram(context, uml_relation_view)

    return uml_relation, uml_relation_view


def maak_relationview_dict(
    relation_dict: dict, source_view: dict, target_view: dict
) -> dict:
    """
    Maakt een `dict` aan die de weergave van een relatie op een diagram representeert.
    :param relation_dict: een `dict` zoals aangemaakt door `_maak_relation_dict`
    :param source_view: een `dict` zoals aangemaakt door hetzij `_maak_classview_dict`
                        of `_maak_relationview_dict`. De startpunt van de relatie.
    :param source_view: een `dict` zoals aangemaakt door hetzij `_maak_classview_dict`
                        of `_maak_relationview_dict`. Het eindpunt van de relatie.
    :returns: een `dict` waarmee een relation zoals die op een view wordt weergegeven
              wordt uitgedrukt.
    """
    return {
        "id": str(uuid4()).replace("-", ""),
        "type": "RelationView",
        "modelElement": {"id": relation_dict["id"], "type": "Relation"},
        "shape": {
            "id": str(uuid4()).replace("-", ""),
            "type": "Path",
            "points": [{"x": 0, "y": 0}, {"x": 0, "y": 0}],
        },
        "source": {"id": source_view["id"], "type": source_view["type"]},
        "target": {"id": target_view["id"], "type": target_view["type"]},
    }


def voeg_object_toe_aan_model(context, uml_object):
    """
    Voegt de `dict` representatie van `uml_object` toe aan het model in de test
    `context`.
    :param context: de test context
    :param uml_object: een `dict` die moet worden toegevoegd aan de test context
    """
    target = context.project["model"]["contents"][0]["contents"]
    target.append(uml_object)


def voeg_objectview_toe_aan_diagram(context, objectview):
    """
    Voegt de `dict` representatie van de weergave van `objectview` op een diagram toe
    aan de test `context`.
    :param context: de test context
    :param objectview: een `dict` die moet worden toegevoegd aan de test context
    """
    target = context.project["diagrams"][0]["contents"]
    target.append(objectview)
