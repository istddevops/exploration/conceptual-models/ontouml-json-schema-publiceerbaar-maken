"""
This module implements the test automation for the behaviour of feature
`Association class tekenen`.
"""

import base64
import json
import zlib

# pylint:disable=[E0611]
from argparse import Namespace
from unittest.mock import mock_open, patch
from uuid import uuid4

from behave import given, then, when
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.edge.options import Options

from features.steps.onto_uml_json_helpers import (
    maak_class_en_classview,
    maak_generalization_en_generalizationview,
    maak_relation_en_relationview,
)
from src.onto_uml_json_importer import main

CLASSES = "classes_and_views"
RELATIONS = "relations_and_views"
GENERALIZATIONS = "generalizations_and_views"


@given("een diagram")
def een_diagram(context):
    """Instantieert een project en bewaart deze in `context.project`"""

    worker_package = {
        "id": str(uuid4()),
        "name": "Model",
        "description": None,
        "type": "Package",
        "propertyAssignments": None,
        "contents": [],
    }
    model = {
        "id": str(uuid4()),
        "name": context.scenario.name,
        "description": None,
        "type": "Package",
        "propertyAssignments": None,
        "contents": [worker_package],
    }
    diagram = {
        "id": str(uuid4()),
        "name": "Model class diagram",
        "description": "##publish##",
        "type": "Diagram",
        "owner": {"id": worker_package["id"], "type": "Package"},
        "contents": [],
    }
    project = {
        "id": str(uuid4()),
        "name": context.feature.name,
        "description": None,
        "type": "Project",
        "model": model,
        "diagrams": [diagram],
    }

    context.project = project


@given('op dit diagram staat class "{klasse_naam}"')
def op_dit_diagram_staat_class(context, klasse_naam):
    """Dummy"""
    worker = getattr(context, CLASSES, {})
    worker[klasse_naam] = maak_class_en_classview(context, f"##{klasse_naam}##")
    setattr(context, CLASSES, worker)


@given(
    'class "{source_class}" is via de relatie "{relatie_naam}" verbonden met class '
    + '"{target_class}"'
)
def class_is_via_de_relatie_verbonden_met_class(
    context, source_class, relatie_naam, target_class
):
    """Dummy"""
    worker = getattr(context, RELATIONS, {})
    worker[relatie_naam] = maak_relation_en_relationview(
        context,
        context.classes_and_views[source_class],
        context.classes_and_views[target_class],
        f"##{relatie_naam}##",
    )
    setattr(context, RELATIONS, worker)


@given(
    'aan de relatie "{relatie}" is een association class "{association_class}" '
    + 'verbonden met association "{association_class_relatie}"'
)
def aan_de_relatie_is_een_association_class_verbonden_met_relatie(
    context, relatie, association_class, association_class_relatie
):
    """Dummy"""
    class_worker = getattr(context, CLASSES, {})
    class_worker[association_class] = maak_class_en_classview(
        context, f"##{association_class}##"
    )
    setattr(context, CLASSES, class_worker)

    relation_worker = getattr(context, RELATIONS, {})
    relation_worker[association_class_relatie] = maak_relation_en_relationview(
        context,
        class_worker[association_class],
        relation_worker[relatie],
        f"##{association_class_relatie}##",
    )
    setattr(context, RELATIONS, relation_worker)


@given(
    'class "{specifieke_klasse_naam}" is via de generalization "{generalization_naam}" '
    + 'verbonden met class "{generieke_klasse_naam}"'
)
def class_is_via_de_generalization_verbonden_met_class(
    context, specifieke_klasse_naam, generalization_naam, generieke_klasse_naam
):
    """Dummy"""
    worker = getattr(context, GENERALIZATIONS, {})
    class_worker = getattr(context, CLASSES, {})
    worker[generalization_naam] = maak_generalization_en_generalizationview(
        context,
        class_worker[generieke_klasse_naam],
        class_worker[specifieke_klasse_naam],
        f"##{generalization_naam}##",
    )
    setattr(context, GENERALIZATIONS, worker)
    setattr(context, CLASSES, class_worker)


@when("het class diagram getekend wordt")
def het_class_diagram_getekend_wordt(context):
    """Dummy"""

    project: dict = context.project
    mock_data = json.dumps(project, indent=2)
    mocked_open = mock_open(read_data=mock_data)
    with patch("builtins.open", mocked_open):
        main(
            Namespace(
                input_file="fake",
                plant_uml=True,
                plant_uml_folder="./tmp/blah",
                save_metadata=False,
            )
        )
        data = []
        for call_args in mocked_open().writelines.call_args_list:
            data.extend(call_args.args[0])
        compressed_plant_uml = base64.urlsafe_b64encode(
            zlib.compress("".join(data[6:-1]).encode("utf-8"), 9)  # Compress plantuml
        ).decode("ascii")
        options = Options()
        options.add_argument("headless")
        context.browser = webdriver.Edge(options=options)
        context.browser.get(f"https://kroki.io/plantuml/svg/{compressed_plant_uml}")


@then('is class "{klasse_naam}" zichtbaar')
def is_class_zichtbaar(context, klasse_naam):
    """Dummy"""

    assert 1 == len(
        [
            g_element
            for g_element in context.browser.find_elements(By.TAG_NAME, "g")
            if g_element.text in (klasse_naam, f"##{klasse_naam}##")
            and 1 == len(g_element.find_elements(By.TAG_NAME, "rect"))
        ]
    ), f"Klasse '{klasse_naam}' is niet aangetroffen op het diagram"


@then(
    'is de relatie "{association}" zichtbaar tussen class "{source}" en class '
    + '"{target}"'
)
@then(
    'is de relatie "{association}" zichtbaar tussen class "{source}" en relatie'
    + ' "{target}"'
)
# pylint:disable=[W0613]
def is_de_relatie_zichtbaar_tussen_class_en_class(context, association, source, target):
    """Dummy"""
    assert 1 == len(
        [
            g_element
            for g_element in context.browser.find_elements(By.TAG_NAME, "g")
            if g_element.text in (association, f"##{association}##")
            and 1 == len(g_element.find_elements(By.TAG_NAME, "path"))
        ]
    ), f"Association '{association}' is niet aangetroffen op het diagram"


@then(
    'is de generalization "{generalization}" zichtbaar tussen class "{general_class}" '
    + 'en class "{specific_class}"'
)
# pylint:disable=[W0613]
def is_de_generalization_zichtbaar_tussen_class_en_class(
    context, generalization, general_class, specific_class
):
    """Dummy"""
    assert 1 == len(
        [
            g_element
            for g_element in context.browser.find_elements(By.TAG_NAME, "g")
            if g_element.text in (generalization, f"##{generalization}##")
            and 1 == len(g_element.find_elements(By.TAG_NAME, "path"))
        ]
    ), f"Generalization '{generalization}' is niet aangetroffen op het diagram"
    context.generalization_name = generalization


@then('wijst de open pijl van de generalization naar class "{general_class}"')
# pylint:disable=[W0613]
def wijst_de_open_pijl_van_de_generalization_naar_class(context, general_class):
    """Dummy"""
    g_elements = [
        g_element
        for g_element in context.browser.find_elements(By.TAG_NAME, "g")
        if g_element.text
        in (context.generalization_name, f"##{context.generalization_name}##")
        and 1 == len(g_element.find_elements(By.TAG_NAME, "path"))
        and 1 == len(g_element.find_elements(By.TAG_NAME, "polygon"))
    ]

    assert 1 == len(
        g_elements
    ), f"Generalization '{context.generalization_name}' heeft geen open pijl"

    polygon_elements = g_elements[0].find_elements(By.TAG_NAME, "polygon")
    polygon_location = polygon_elements[0].location

    g_elements = [
        g_element
        for g_element in context.browser.find_elements(By.TAG_NAME, "g")
        if g_element.text in (general_class, f"##{general_class}##")
    ]
    general_class_object = g_elements[0]
    rect = general_class_object.rect

    # Vaststellen dat de polygoon ergens aan de rectangle van de general class "vast
    # zit". We houden een marge van 5 pixels aan, want het is geen exacte wetenschap.
    assert (rect["x"] - 5) < polygon_location["x"] < (
        rect["x"] + rect["width"] + 5
    ) and (rect["y"] - 5) < polygon_location["y"] < (rect["y"] + rect["height"] + 5), (
        f"De open pijl van generalization '{context.generalization_name}' kleeft niet "
        + f"aan de randen van general class '{general_class}'"
    )
