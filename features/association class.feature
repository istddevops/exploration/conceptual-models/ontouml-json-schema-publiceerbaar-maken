#language: nl
Functionaliteit: Association class tekenen
    De UML association classes worden ondersteund; deze worden op de juiste wijze getekend.
    Een association class is een class waarmee je eigenschappen kunt toekennen aan een relatie tussen twee andere classes.
    Deze eigenschappen neem je dan op als members van de association class.

Scenario: Een association class wordt getekend als een class die via een relatie verbonden is aan een relatie tussen twee andere classes
    Een association class is, via een relatie, verbonden aan een relatie tussen twee andere classes.

    Gegeven een diagram
    Stel op dit diagram staat class "A"
    En op dit diagram staat class "B"
    En class "A" is via de relatie "connects" verbonden met class "B"
    Stel aan de relatie "connects" is een association class "X" verbonden met association "associates"
    Als het class diagram getekend wordt
    Dan is class "A" zichtbaar
    En is class "B" zichtbaar
    En is de relatie "connects" zichtbaar tussen class "A" en class "B"
    En is class "X" zichtbaar
    En is de relatie "associates" zichtbaar tussen class "X" en relatie "connects"

Scenario: Een generalization tussen twee classes wordt getekend als een relatie waarbij een open pijl aan de general class vast zit
    Een generalization is een relatie waarmee je aangeeft dat een class de eigenschappen van een andere class overneemt en wellicht verder specificeert. De class waarvan de eigenschappen overgenomen worden noemen we de general class, en de class die de eigenschappen overneemt en wellicht verder specificeert noemen we de specific class.

    Gegeven een diagram
    Stel op dit diagram staat class "General"
    En op dit diagram staat class "Specific"
    En class "Specific" is via de generalization "specifies" verbonden met class "General"
    Als het class diagram getekend wordt
    Dan is class "General" zichtbaar
    En is class "Specific" zichtbaar
    En is de generalization "specifies" zichtbaar tussen class "General" en class "Specific"
    En wijst de open pijl van de generalization naar class "General"